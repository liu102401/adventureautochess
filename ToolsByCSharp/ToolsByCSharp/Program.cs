﻿using System;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;

namespace ToolsByCSharp
{
    class ItemInfo
    {
        public string Name { get; set; }
        public string Quality { get; set; }
        public string Type { get; set; }
    }

    class Program
    {
        public static string ReplaceByQuality(ItemInfo itemInfo, string value)
        {
            if (itemInfo.Quality == "QUALITY_2")
            {
                value = "<font color='green'>" + value + "</font>";
            }
            else if (itemInfo.Quality == "QUALITY_3")
            {
                value = "<font color='#1297F3'>" + value + "</font>";
            }
            else if (itemInfo.Quality == "QUALITY_4")
            {
                value = "<font color='#A343C3'>" + value + "</font>";
            }
            else if (itemInfo.Quality == "QUALITY_5")
            {
                value = "<font color='#FFCC00'>" + value + "</font>";
            }
            else
            {
                value = "<font color='white'>" + value + "</font>";
            }
            return value;
        }

        static void Main(string[] args)
        {
            Console.WriteLine(System.Environment.CurrentDirectory);
            string code = File.ReadAllText("../../../../../game/adventure_auto_chess/scripts/vscripts/addon_game_mode.lua", Encoding.UTF8);

            code = code.Substring(code.IndexOf("CAddonTemplateGameMode:InitGameMode"));
            int length = code.IndexOf("--[[GameRules:GetGameModeEntity().shopItems");
            code = code.Substring(0, length);

            string pattern = "(..{name = \"item_.*, type=.*, quality=.*})";
            int count = 0;

            ArrayList itemList = new ArrayList();
            foreach (Match match in Regex.Matches(code, pattern))
            {
                count++;
                Console.WriteLine(count + ":" + match.Value);
                string extractPattern = "{name = \"(.*?)\",.*type=(.*?),.*quality=(QUALITY_.).*}";
                Match extractMatch = Regex.Match(match.Value, extractPattern);
                Console.WriteLine("name=" + extractMatch.Groups[1].Value + " -- type=" + extractMatch.Groups[2].Value + " -- quality=" + extractMatch.Groups[3].Value);
                if(match.Value.IndexOf("--") != 0)
                {
                    ItemInfo itemInfo = new ItemInfo
                    {
                        Name = extractMatch.Groups[1].Value,
                        Type = extractMatch.Groups[2].Value,
                        Quality = extractMatch.Groups[3].Value
                    };
                    itemList.Add(itemInfo);
                }
                
            }

            string txtFilePath = "../../../../../game/adventure_auto_chess/resource/addon_schinese.txt";
            string tmpFilePath = "../../../../../game/adventure_auto_chess/resource/addon_schinese.tmp";

            string addonChineseContent = File.ReadAllText(txtFilePath, Encoding.UTF8);
            //item name
            foreach(ItemInfo itemInfo in itemList)
            {
                string value = Regex.Match(addonChineseContent, "\"DOTA_Tooltip_ability_" + itemInfo.Name + "\"\t\"(.*)\"").Groups[1].Value;
                if (value.Contains("color"))
                {
                    value = Regex.Match(value, "<font color='.*'>(.*)</font>").Groups[1].Value;
                }

                Regex reg = new Regex("\"DOTA_Tooltip_ability_" + itemInfo.Name + "\"\t\"(.*)\"");

                value = ReplaceByQuality(itemInfo, value);

                addonChineseContent = reg.Replace(addonChineseContent, "\"DOTA_Tooltip_ability_" + itemInfo.Name + "\"\t\"" + value + "\"");
             }

            //ability name
            foreach (ItemInfo itemInfo in itemList)
            {
                string value = Regex.Match(addonChineseContent, "\"DOTA_Tooltip_ability_" + itemInfo.Name.Substring("item_".Length) + "\"\t\"(.*)\"").Groups[1].Value;
                if (value.Contains("color"))
                {
                    value = Regex.Match(value, "<font color='.*'>(.*)</font>").Groups[1].Value;
                }

                Regex reg = new Regex("\"DOTA_Tooltip_ability_" + itemInfo.Name.Substring("item_".Length) + "\"\t\"(.*)\"");

                value = ReplaceByQuality(itemInfo, value);

                addonChineseContent = reg.Replace(addonChineseContent, "\"DOTA_Tooltip_ability_" + itemInfo.Name.Substring("item_".Length) + "\"\t\"" + value + "\"");
            }

            //modifier name
            foreach (ItemInfo itemInfo in itemList)
            {
                string value = Regex.Match(addonChineseContent, "\"DOTA_Tooltip_modifier_" + itemInfo.Name.Substring("item_".Length) + "\"\t\"(.*)\"").Groups[1].Value;
                if (value.Contains("color"))
                {
                    value = Regex.Match(value, "<font color='.*'>(.*)</font>").Groups[1].Value;
                }

                Regex reg = new Regex("\"DOTA_Tooltip_modifier_" + itemInfo.Name.Substring("item_".Length) + "\"\t\"(.*)\"");

                value = ReplaceByQuality(itemInfo, value);

                addonChineseContent = reg.Replace(addonChineseContent, "\"DOTA_Tooltip_modifier_" + itemInfo.Name.Substring("item_".Length) + "\"\t\"" + value + "\"");
            }

            //shopitem descrtition
            foreach (ItemInfo itemInfo in itemList)
            {
                string value = Regex.Match(addonChineseContent, "\"DOTA_Tooltip_ability_" + itemInfo.Name + "_Shop_Item_Description\"\t\"(.*)\"").Groups[1].Value;
                if(value.IndexOf("\\n") != -1)
                {
                    value = value.Replace("\\n", "<br>");
                }

                Regex reg = new Regex("\"DOTA_Tooltip_ability_" + itemInfo.Name + "_Shop_Item_Description\"\t\"(.*)\"");

                addonChineseContent = reg.Replace(addonChineseContent, "\"DOTA_Tooltip_ability_" + itemInfo.Name + "_Shop_Item_Description\"\t\"" + value + "\"");
            }


            File.WriteAllText(tmpFilePath, addonChineseContent, Encoding.UTF8);

            FileInfo fileInfo = new FileInfo(txtFilePath);
            fileInfo.MoveTo(txtFilePath + "." + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".bak");
            fileInfo = new FileInfo(tmpFilePath);
            fileInfo.MoveTo(txtFilePath);
        }
    }
}
