function LocalizeWithTokens(message, tokens) {
	var message = $.Localize("#" + message);

	if(tokens != null) {
		var keys = Object.keys(tokens);
		for(i = 0; i < keys.length; i++) {
			var token = tokens[keys[i]];
			if(token.type == "value") {
				message = message.replace("${" + keys[i] + "}", token.value)
			}
			else if(token.type == "name") {
				var value = token.value;
				if(value.type == "player") {
					message = message.replace("${" + keys[i] + "}", value.name)
				}
				else if(value.type == "random") {
					var names = Object.keys(value.name);
					var name = "";
					for(var j = 1; j <= names.length; j++) {
						name = name + $.Localize("#" + value.sex + "_NAME_" + value.name[j]);
					}
					message = message.replace("${" + keys[i] + "}", name)
				}
				else if(value.type == "phantom") {
					message = message.replace("${" + keys[i] + "}", $.Localize("#PHANTOM"))
				}
				else if(value.type == "text") {
					message = message.replace("${" + keys[i] + "}", $.Localize("#" + value.name))
				}
				else{
					$.Msg("Invalid name type, type=" + value.type + "\n");
				}
			}
			else {
				if(token.value != null) {
					message = message.replace("${" + keys[i] + "}", $.Localize("#" + token.value))
				}
				else{
					message = message.replace("${" + keys[i] + "}", $.Localize("#" + keys[i]))
				}
				
			}
					
		}
	}
	return message;
}

function RemoveAllColorClass(label) {
	label.RemoveClass("AutoChessShopItemLabelColor_consumable")
	label.RemoveClass("AutoChessShopItemLabelColor_common")
	label.RemoveClass("AutoChessShopItemLabelColor_rare")
	label.RemoveClass("AutoChessShopItemLabelColor_epic")
	label.RemoveClass("AutoChessShopItemLabelColor_artifact")
}

function RemoveColorString(text) {
	text = text.replace(/<font color='[\w#]+'>/g,"");
	text = text.replace(/<\/font>/g, "");
	return text;
}

function NumberToStringWithSpace(integer, length) {
	var text = "" + integer;
	var needSpaceCount = length - text.length;
	for(var i = 0; i < needSpaceCount; i++) {
		text = " " + text;
	}
	return text;
}