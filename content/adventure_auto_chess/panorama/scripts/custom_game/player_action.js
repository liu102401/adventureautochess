
function LeftDown() {
	var cursor = GameUI.GetCursorPosition();
	var mousePos = Game.ScreenXYToWorld(cursor[0], cursor[1])
	GameEvents.SendCustomGameEventToServer("on_mouse_left_down", {
		x: mousePos[0],
		y: mousePos[1],
		z: mousePos[2],
	})
}

function GetName(nEntityIndex) {
	GameEvents.SendCustomGameEventToServer("on_get_name", {
		nEntityIndex: nEntityIndex
	})
}

(function() {
	GameUI.SetMouseCallback( function( eventName, arg ) {
		var nMouseButton = arg
		var CONSUME_EVENT = true;
		var CONTINUE_PROCESSING_EVENT = false;
		//$.Msg("eventName=" + eventName);
		if ( GameUI.GetClickBehaviors() !== CLICK_BEHAVIORS.DOTA_CLICK_BEHAVIOR_NONE )
			return CONTINUE_PROCESSING_EVENT;	
		if (eventName === "pressed") {
			if ( arg === 0 ) {
				if(picked) {
					LeftDown();
					picked = false;
					return CONSUME_EVENT;
				}
			}

			if ( arg === 1 ) {
				//RightDown();
				return CONTINUE_PROCESSING_EVENT;
			}
		}
		return CONTINUE_PROCESSING_EVENT;	
	})
})();