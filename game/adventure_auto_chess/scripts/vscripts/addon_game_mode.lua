-- Generated from template
require('Timers')

if CAddonTemplateGameMode == nil then
	CAddonTemplateGameMode = class({})
end

function Precache( context )
	--[[
		Precache things we know we'll use.  Possible file types include (but not limited to):
			PrecacheResource( "model", "*.vmdl", context )
			PrecacheResource( "soundfile", "*.vsndevts", context )
			PrecacheResource( "particle", "*.vpcf", context )
			PrecacheResource( "particle_folder", "particles/folder", context )
	]]

	PrecacheResource("model", "models/heroes/wisp/wisp.vmdl", context)
	PrecacheResource("model", "models/development/invisiblebox.vmdl", context)
	PrecacheResource("model", "models/heroes/kunkka/kunkka.vmdl", context)
	PrecacheResource("model", "models/heroes/omniknight/omniknight.vmdl", context)
	PrecacheResource("model", "models/heroes/crystal_maiden/crystal_maiden.vmdl", context)
	PrecacheResource("model", "models/heroes/lina/lina.vmdl", context)
	PrecacheResource("model", "models/heroes/phantom_assassin/phantom_assassin.vmdl", context)
	PrecacheResource("model", "models/heroes/windrunner/windrunner.vmdl", context)
	PrecacheResource("model", "models/heroes/lanaya/lanaya.vmdl", context)
	PrecacheResource("model", "models/heroes/luna/luna.vmdl", context)
	PrecacheResource("model", "models/heroes/axe/axe.vmdl", context)
	PrecacheResource("model", "models/heroes/juggernaut/juggernaut.vmdl", context)
	PrecacheResource("model", "models/heroes/huskar/huskar.vmdl", context)
	PrecacheResource("model", "models/heroes/abaddon/abaddon.vmdl", context)
	PrecacheResource("model", "models/heroes/lich/lich.vmdl", context)
	PrecacheResource("model", "models/heroes/drow/drow_base.vmdl", context)
	PrecacheResource("model", "models/heroes/legion_commander/legion_commander.vmdl", context)
	PrecacheResource("model", "models/heroes/skywrath_mage/skywrath_mage.vmdl", context)
	PrecacheResource("model", "models/heroes/doom/doom.vmdl", context)
	PrecacheResource("model", "models/heroes/shadow_fiend/shadow_fiend.vmdl", context)
	PrecacheResource("model", "models/heroes/dragon_knight/dragon_knight.vmdl", context)
	PrecacheResource("model", "models/heroes/puck/puck.vmdl", context)

	PrecacheResource("particle", "particles/units/heroes/hero_phantom_assassin/phantom_assassin_attack_blur.vpcf", context)
	PrecacheResource("particle", "particles/units/heroes/hero_phantom_assassin/phantom_assassin_attack_blur_b.vpcf", context)
	PrecacheResource("particle", "particles/units/heroes/hero_abaddon/abaddon_weapon_blur.vpcf", context)
	PrecacheResource("particle", "particles/units/heroes/hero_abaddon/abaddon_weapon_blur2.vpcf", context)
	PrecacheResource("particle", "particles/units/heroes/hero_legion_commander/legion_weapon_blur.vpcf", context)
	PrecacheResource("particle", "particles/units/heroes/hero_legion_commander/legion_weapon_blurb.vpcf", context)
	PrecacheResource("particle", "particles/units/heroes/hero_legion_commander/legion_weapon_blurc.vpcf", context)
	PrecacheResource("particle", "particles/units/heroes/hero_doom_bringer/doom_weapon_blurb.vpcf", context)
	PrecacheResource("particle", "particles/units/heroes/hero_doom_bringer/doom_weapon_blurb.vpcf.vpcf", context)
	PrecacheResource("particle", "particles/units/heroes/hero_doom_bringer/doom_weapon_blur_hyhy.vpcf", context)
	PrecacheResource("particle", "particles/units/heroes/hero_nevermore/nvm_atk_blur.vpcf", context)
	PrecacheResource("particle", "particles/units/heroes/hero_nevermore/nvm_atk_blur_b.vpcf", context)
	PrecacheResource("particle", "particles/units/heroes/hero_puck/puck_base_attack_warmup.vpcf", context)
	PrecacheResource("particle", "particles/items5_fx/neutral_treasurebox.vpcf", context)

	PrecacheResource("soundfile", "soundevents/custom_sounds.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_creeps.vsndevts", context)

	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_phantom_assassin.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_axe.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_life_stealer.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_mars.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_bounty_hunter.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_templar_assassin.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_enchantress.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_keeper_of_the_light.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_chen.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_dazzle.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_drowranger.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_medusa.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_naga_siren.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_beastmaster.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_treant.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_furion.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_lone_druid.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_abaddon.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_omniknight.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_skywrath_mage.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_warlock.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_lina.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_abyssal_underlord.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_earthshaker.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_tidehunter.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_ember_spirit.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_kunkka.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_crystalmaiden.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_windrunner.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_huskar.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_lich.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_legion_commander.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_doombringer.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_nevermore.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_dragon_knight.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_puck.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_juggernaut.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_magnataur.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_spectre.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_centaur.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_sniper.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_snapfire.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_gyrocopter.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_obsidian_destroyer.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_shadowshaman.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_terrorblade.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_morphling.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_winter_wyvern.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_crystalmaiden.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_jakiro.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_riki.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_razor.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_zuus.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_visage.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_nightstalker.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_silencer.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_necrolyte.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_death_prophet.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_faceless_void.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_antimage.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_enigma.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_phantom_lancer.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_chaos_knight.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_venomancer.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_viper.vsndevts", context)
	PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_disruptor.vsndevts", context)

	PrecacheResource("particle", "particles/units/heroes/hero_phantom_assassin/phantom_assassin_attack_blur_crit.vpcf", context)
	PrecacheResource("particle", "particles/units/heroes/hero_axe/axe_attack_blur_counterhelix.vpcf", context)
end

-- Create the game mode when we activate
function Activate()
	GameRules.AddonTemplate = CAddonTemplateGameMode()
	GameRules.AddonTemplate:InitGameMode()
end

QUALITY_1 = "consumable"
QUALITY_2 = "common"
QUALITY_3 = "rare"
QUALITY_4 = "epic"
QUALITY_5 = "artifact"
TYPE_ABILITY = "ability"
TYPE_HERO = "hero"
TYPE_CONSUMABLE = "consumable"
TYPE_EQUIPMENT = "EQUIPMENT"

MAN = "MAN"
WOMAN = "WOMAN"
NAME = "NAME"

ATTACK_TYPE_MELEE = "melee"
ATTACK_TYPE_RANGED = "ranged"

STAGE_PREPARE_STAGE = "PrepareStage";
STAGE_BATTLE_STAGE = "BattleStage";
DEFAULT_PREPARE_STAGE_MAX_TIME_IN_SECONDS = 60
DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_FIRST_STAGE_IN_SECONDS = 10
DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS = 150
DEFAULT_BATTLE_STAGE_MAX_TIME_IN_SECONDS = 60
DEFAULT_AUCTION_TIMEOUT = 5

ENEMY_TYPE_FIXED_ABILITIES = "EnemyTypeFixedAbilities"
ENEMY_TYPE_RANDOM_ABILITIES = "EnemyTypeRandomAbilities"

BUFF = "BUFF"
DEBUFF = "DEBUFF"

SecondaryProfession = "SecondaryProfession"
ProfessionTank = "ProfessionTank"
ProfessionPriest = "ProfessionPriest"
ProfessionPhysicalDamage = "ProfessionPhysicalDamage"
ProfessionMagicalDamage = "ProfessionMagicalDamage"
ProfessionAssist = "ProfessionAssist"
ProfessionControl = "ProfessionControl"

function InitShopItemExistCount( )
	for i = 1, #GameRules:GetGameModeEntity().shopItems do
		local shopItem = GameRules:GetGameModeEntity().shopItems[i]
		shopItem.cost = GetShopItemCost(shopItem.quality)
		shopItem.existCount = 0
	end
end

function TestReleaseSingleItem( )

	GameRules:GetGameModeEntity().heros = {
		hero_human = {
			base = {
				{name = "npc_dota_hero_kunkka", attackType = ATTACK_TYPE_MELEE},
				{name = "npc_dota_hero_omniknight", attackType = ATTACK_TYPE_MELEE},
				{name = "npc_dota_hero_crystal_maiden", attackType = ATTACK_TYPE_RANGED}
			},
			abilities = {
				"race_human"
			}
		},
	}

	GameRules:GetGameModeEntity().abilities = {
		race_human = {name = "race_human"},
		profession_warrior = {name = "profession_warrior", primaryAttribute = DOTA_ATTRIBUTE_STRENGTH, order = 1},
		profession_berserker = {name = "profession_berserker", primaryAttribute = DOTA_ATTRIBUTE_STRENGTH, needs={"profession_warrior"}, order = 2, attackType = ATTACK_TYPE_MELEE},
		axe_berserkers_call = {name = "axe_berserkers_call", needs={"profession_warrior"}},
		life_stealer_rage = {name = "life_stealer_rage", needs={"profession_berserker"}},
		gift_agility = {name = "gift_agility"},
	}


	GameRules:GetGameModeEntity().shopItems = {
		{name = "item_race_human", abilityName = "race_human", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_profession_warrior", abilityName = "profession_warrior", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_profession_berserker", abilityName = "profession_berserker", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_axe_berserkers_call", abilityName = "axe_berserkers_call", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_life_stealer_rage", abilityName = "life_stealer_rage", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},

		{name = "item_gift_agility", abilityName = "gift_agility", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
	
		{name = "item_health", type=TYPE_CONSUMABLE, quality=QUALITY_3, value=10, image="file://{images}/items/flask.png"},

		{name = "item_angel_sword", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/rapier.png", maxCount = 50},

		--hero quality works as >=quality
		{name = "item_hero_human", heroName ="hero_human", type=TYPE_HERO, quality=QUALITY_1, maxCount = 40},
		
	}

	InitShopItemExistCount()
	GameRules:GetGameModeEntity().probability = {
		{consumable = 50, common = 50, rare = 0, epic = 0, artifact = 0},
		{consumable = 50, common = 49, rare = 1, epic = 0, artifact = 0},
		{consumable = 50, common = 48, rare = 2, epic = 0, artifact = 0},
		{consumable = 50, common = 44, rare = 5, epic = 1, artifact = 0},
		{consumable = 50, common = 40, rare = 8, epic = 2, artifact = 0},
		{consumable = 50, common = 35, rare = 10, epic = 5, artifact = 0},
		{consumable = 40, common = 29, rare = 20, epic = 10, artifact = 1},
		{consumable = 40, common = 28, rare = 20, epic = 10, artifact = 2},
		{consumable = 40, common = 20, rare = 20, epic = 15, artifact = 5},
		{consumable = 30, common = 20, rare = 20, epic = 20, artifact = 10}
	}

	local testPlayer = {}
	function testPlayer:GetAssignedHero()
		local courier = {}
		function courier:GetLevel()
			return 10
		end

		return courier
	end
	local result = TableToStr(GameRules:GetGameModeEntity().shopItems)
	local finalProfessionAbilities = GetFinalProfessionAbilities()
	testPlayer.shopConfigs = {}
	testPlayer.shopConfigs.professions = {}
	for j = 1, #finalProfessionAbilities do
		testPlayer.shopConfigs.professions[finalProfessionAbilities[j].name] = {}
		testPlayer.shopConfigs.professions[finalProfessionAbilities[j].name].enable = true
	end
	for i = 1, 10 do
		RandomGenerateShopItems(testPlayer)
		local existCount = 0
		for i = 1, #testPlayer.shopItems do
			local shopItem = testPlayer.shopItems[i].shopItem
			if shopItem.name == "item_angel_sword" and shopItem.existCount == 0 then
				Msg("\nfatal error!!! generate angel sword without existCount\n")
			end
		end
		local beforeRelease = TableToStr(GameRules:GetGameModeEntity().shopItems)
		ReleaseShopItemsForPlayer(testPlayer)
		local currentResult = TableToStr(GameRules:GetGameModeEntity().shopItems)
		if result ~= currentResult then
			Msg("\nfatal error!!! inconsistent result, i=" .. i .. "\n")
			Msg("\nbeforeRelease=" .. beforeRelease .. "\n")
			Msg("\nexpectsResult=" .. result .. "\n")
			Msg("\ncurrentResult=" .. currentResult .. "\n")
		end
	end
	
	--test is isBought
	GameRules:GetGameModeEntity().shopItems = {
		{name = "item_race_human", abilityName = "race_human", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
	}
	InitShopItemExistCount()
	RandomGenerateShopItems(testPlayer)
	testPlayer.shopItems[1].isBought = true
	ReleaseShopItemsForPlayer(testPlayer)
	if GameRules:GetGameModeEntity().shopItems[1].existCount ~= 1 then
		Msg("\nfatal error!!! inconsistent result after bought, existCount=" .. GameRules:GetGameModeEntity().shopItems[1].existCount .. "\n")
	end

end

function UnitTest(  )
	


	TestReleaseSingleItem()
end

function DeepCopy(object)      
    local SearchTable = {}  

    local function Func(object)  
        if type(object) ~= "table" then  
            return object         
        end  
        local NewTable = {}  
        SearchTable[object] = NewTable  
        for k, v in pairs(object) do  
            NewTable[Func(k)] = Func(v)  
        end     

        return setmetatable(NewTable, getmetatable(object))      
    end    

    return Func(object)  
end

professionIndex = -1;
function GetProfessionIndex( )
	professionIndex = professionIndex + 1
	return professionIndex
end

function CAddonTemplateGameMode:InitGameMode()
	Msg( "Template addon is loaded.\n" )

	-- GameRules:GetGameModeEntity().playing_player_count = 0
	-- GameRules:GetGameModeEntity().obing_player_count = 0
	GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_GOODGUYS, 0)
	GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_BADGUYS, 4)
	GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_CUSTOM_1, 1)
	GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_CUSTOM_2, 1)
	GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_CUSTOM_3, 1)
	GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_CUSTOM_4, 1)
	--GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_CUSTOM_5, 1)
	--GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_CUSTOM_6, 1)

	SetTeamCustomHealthbarColor(DOTA_TEAM_GOODGUYS, 62, 179, 52)

	GameRules:GetGameModeEntity():SetCameraDistanceOverride(2000)
	GameRules:GetGameModeEntity():SetBuybackEnabled(false)

	GameRules:GetGameModeEntity().gameEnded = false
	GameRules:GetGameModeEntity().playersCount = 0
	GameRules:GetGameModeEntity().players = {}
	GameRules:GetGameModeEntity().currentStage = 1
	GameRules:GetGameModeEntity().gameMode = "CorpsMode"

	GameRules:SetHeroRespawnEnabled(false)

	GameRules:SetHeroSelectionTime(0)
	GameRules:SetStrategyTime(0)
	--GameRules:SetPreGameTime(3)
	--GameRules:SetPreGameTime(10)
	GameRules:SetPreGameTime(3)
	GameRules:SetShowcaseTime(0)
	GameRules:SetRuneSpawnTime(0)

	CustomGameEventManager:RegisterListener("test_to_server", OnToServer)
	CustomGameEventManager:RegisterListener("on_select_character", OnSelectCharacter)
	CustomGameEventManager:RegisterListener("on_mouse_left_down", OnMouseLeftDown)
	CustomGameEventManager:RegisterListener("on_sell_item", OnSellItem)
	CustomGameEventManager:RegisterListener("on_buy_shop_item", OnBuyShopItem)
	CustomGameEventManager:RegisterListener("on_share_item", OnShareItem)
	CustomGameEventManager:RegisterListener("on_cancel_share_item", OnCancelShareItem)
	CustomGameEventManager:RegisterListener("on_confirm_share_item", OnConfirmShareItem)
	CustomGameEventManager:RegisterListener("on_forget_ability", OnForgetAbility)
	CustomGameEventManager:RegisterListener("on_cancel_single_select_dialog_panel", OnCancelSingleSelectDialogPanel)
	CustomGameEventManager:RegisterListener("on_get_random_profession_ability_item", OnGetRandomProfessionAbilityItem)
	CustomGameEventManager:RegisterListener("on_get_wishing_item", OnGetWishingItem)
	CustomGameEventManager:RegisterListener("on_get_game_end_info", OnGetGameEndInfo)
	CustomGameEventManager:RegisterListener("on_get_name", OnGetName)
	CustomGameEventManager:RegisterListener("on_prepare_over", OnPrepareOver)
	CustomGameEventManager:RegisterListener("on_end_battle", OnEndBattle)
	CustomGameEventManager:RegisterListener("on_handle_quests", OnHandleQuests)
	CustomGameEventManager:RegisterListener("on_bid_auction_item", OnBidAuctionItem)
	CustomGameEventManager:RegisterListener("on_get_model_names", OnGetModelNames)
	CustomGameEventManager:RegisterListener("on_get_shop_configs", OnGetShopConfigs)
	CustomGameEventManager:RegisterListener("on_switch_shop_configs", OnSwitchShopConfigs)
	CustomGameEventManager:RegisterListener("on_change_shop_configs", OnChangeShopConfigs)
	CustomGameEventManager:RegisterListener("on_cancel_shop_configs", OnCancelShopConfigs)
	CustomGameEventManager:RegisterListener("on_get_profession_route", OnGetProfessionRoute)
	CustomGameEventManager:RegisterListener("on_select_game_mode", OnSelectGameMode)

	ListenToGameEvent("game_rules_state_change", Dynamic_Wrap(CAddonTemplateGameMode,"OnGameRulesStateChange"), self)
	ListenToGameEvent("dota_player_gained_level", Dynamic_Wrap(CAddonTemplateGameMode,"OnPlayerGainedLevel"), self)
	ListenToGameEvent("npc_spawned", Dynamic_Wrap(CAddonTemplateGameMode, "OnNpcSpawned"), self)
	ListenToGameEvent('entity_killed', Dynamic_Wrap(CAddonTemplateGameMode, 'OnEntityKilled'), self)

	GameRules:GetGameModeEntity():SetUseCustomHeroLevels(true)
	GameRules:GetGameModeEntity():SetCustomHeroMaxLevel(10)
	GameRules:GetGameModeEntity().gainExpForUnit = {
		[1] = 2,
		[2] = 4,
		[3] = 8,
		[4] = 12,
		[5] = 16,
		[6] = 20,
		[7] = 24,
		[8] = 28,
		[9] = 32,
		[10] = 36,
	}
    GameRules:GetGameModeEntity():SetCustomXPRequiredToReachNextLevel(
		{
			[1] = 0,
			[2] = 2,
			[3] = 6,
			[4] = 14,
			[5] = 26,
			[6] = 42,
			[7] = 62,
			[8] = 86,
			[9] = 114,
			[10] = 146,
		}
	)

 	GameRules:GetGameModeEntity():SetGoldSoundDisabled(true)
 	GameRules:GetGameModeEntity():SetLoseGoldOnDeath(true)
 	GameRules:GetGameModeEntity():SetFogOfWarDisabled(true)
	GameRules:SetUseBaseGoldBountyOnHeroes(true)
	GameRules:SetStartingGold(0)
	GameRules:SetCustomGameSetupAutoLaunchDelay(10)

	GameRules:SetUseCustomHeroXPValues(true)

	--UnitTest()

    --[[
    1-10关天灾小兵，boss超级兵
    11-20关狗头人，boss狗头人长官
    21-30关巨狼，boss头狼
    31-40关随机生成英雄,boss黑龙
    41-50关随机生成英雄，boss肉山
    特殊关卡：PK，拍卖行 日后再说
    
	GameRules:GetGameModeEntity().enemies = {
		{
			{name = "npc_dota_creep_badguys_melee", position = {x = 4, y = 10}},
		}, 
		{
			{name = "npc_dota_creep_badguys_ranged", position = {x = 4, y = 10}},
			{name = "npc_dota_creep_badguys_melee", position = {x = 5, y = 9}},
			{name = "npc_dota_creep_badguys_melee", position = {x = 4, y = 9}},
		},
		{
			{name = "npc_dota_creep_badguys_ranged", position = {x = 4, y = 10}},
			{name = "npc_dota_creep_badguys_ranged", position = {x = 5, y = 10}},
			{name = "npc_dota_creep_badguys_melee", position = {x = 4, y = 9}},
			{name = "npc_dota_creep_badguys_melee", position = {x = 5, y = 9}},
			{name = "npc_dota_creep_badguys_melee", position = {x = 6, y = 9}},
		}
	}]]--

	unitAbilities = {
		{name = "lina_dragon_slave"},
		{name = "guardian_of_earth"},
		{name = "tower_true_sight"},
		{name = "lone_druid_spirit_bear_demolish"},
		{name = "lone_druid_spirit_bear_entangle"},
		{name = "warlock_golem_flaming_fists"},
		{name = "kobold_taskmaster_speed_aura"},
		{name = "centaur_khan_endurance_aura"},
		{name = "centaur_khan_war_stomp"},
		{name = "phantom_assassin_coup_de_grace"},
		{name = "alpha_wolf_command_aura"},
	}
	GameRules:GetGameModeEntity().unitAbilities = unitAbilities

	artifacts = {
		{name = "angelic_alliance_aura"},
		{name = "armor_of_the_damned_aura"},
		{name = "assault_cuirass_aura"},
		{name = "cloak_of_the_undead_king_aura"},
		{name = "ring_of_the_magi_aura"},
		{name = "bow_of_the_sharpshooter_aura"},
		{name = "elixir_of_life_aura"},
		{name = "power_of_the_dragon_father_aura"},
		{name = "well_of_wizard_aura"},
	}
	GameRules:GetGameModeEntity().artifacts = artifacts

	GameRules:GetGameModeEntity().heros = {
		hero_human = {
			base = {
				{name = "npc_dota_hero_omniknight", attackType = ATTACK_TYPE_MELEE, sex = MAN},
				{name = "npc_dota_hero_kunkka", attackType = ATTACK_TYPE_MELEE, sex = MAN},
				{name = "npc_dota_hero_crystal_maiden", attackType = ATTACK_TYPE_RANGED, sex = WOMAN},
				{name = "npc_dota_hero_lina", attackType = ATTACK_TYPE_RANGED, sex = WOMAN}
			},
			abilities = {
				"race_human"
			}
		},
		hero_elf = {
			base = {
				{name = "npc_dota_hero_phantom_assassin", attackType = ATTACK_TYPE_MELEE, sex = WOMAN},
				{name = "npc_dota_hero_windrunner", attackType = ATTACK_TYPE_RANGED, sex = WOMAN},
				{name = "npc_dota_hero_templar_assassin", attackType = ATTACK_TYPE_RANGED, sex = WOMAN},
				{name = "npc_dota_hero_luna", attackType = ATTACK_TYPE_RANGED, sex = WOMAN},
			},
			abilities = {
				"race_elf"
			}
		},
		hero_orc = {
			base = {
				{name = "npc_dota_hero_axe", attackType = ATTACK_TYPE_MELEE, sex = MAN},
				{name = "npc_dota_hero_juggernaut", attackType = ATTACK_TYPE_MELEE, sex = MAN},
				{name = "npc_dota_hero_huskar", attackType = ATTACK_TYPE_RANGED, sex = MAN},
			},
			abilities = {
				"race_orc"
			}
		},
		hero_undead = {
			base = {
				{name = "npc_dota_hero_abaddon", attackType = ATTACK_TYPE_MELEE, sex = MAN},
				{name = "npc_dota_hero_lich", attackType = ATTACK_TYPE_RANGED, sex = MAN},
				{name = "npc_dota_hero_drow_ranger", attackType = ATTACK_TYPE_RANGED, sex = WOMAN},
			},
			abilities = {
				"race_undead"
			}
		},
		hero_angel = {
			base = {
				{name = "npc_dota_hero_legion_commander", attackType = ATTACK_TYPE_MELEE, sex = WOMAN},
				{name = "npc_dota_hero_skywrath_mage", attackType = ATTACK_TYPE_RANGED, sex = MAN},
			},
			abilities = {
				"race_angel"
			}
		},
		hero_demon = {
			base = {
				{name = "npc_dota_hero_doom_bringer", attackType = ATTACK_TYPE_MELEE, sex = MAN},
				{name = "npc_dota_hero_nevermore", attackType = ATTACK_TYPE_RANGED, sex = MAN},
			},
			abilities = {
				"race_demon"
			}
		},
		hero_dragon = {
			base = {
				{name = "npc_dota_hero_dragon_knight", attackType = ATTACK_TYPE_MELEE, sex = MAN},
				{name = "npc_dota_hero_puck", attackType = ATTACK_TYPE_RANGED, sex = WOMAN},
			},
			abilities = {
				"race_dragon"
			}
		}
	}


	GameRules:GetGameModeEntity().abilities = {
		race_human = {name = "race_human", index = GetProfessionIndex()},
		race_elf = {name = "race_elf", index = GetProfessionIndex()},
		race_orc = {name = "race_orc", index = GetProfessionIndex()},
		race_undead = {name = "race_undead", index = GetProfessionIndex()},
		race_angel = {name = "race_angel", index = GetProfessionIndex()},
		race_demon = {name = "race_demon", index = GetProfessionIndex()},
		race_dragon = {name = "race_dragon", index = GetProfessionIndex()},
		profession_warrior = {name = "profession_warrior", primaryAttribute = DOTA_ATTRIBUTE_STRENGTH, order = 1, index = GetProfessionIndex()},
		profession_berserker = {name = "profession_berserker", primaryAttribute = DOTA_ATTRIBUTE_STRENGTH, needs={"profession_warrior"}, order = 2, attackType = ATTACK_TYPE_MELEE, index = GetProfessionIndex(), professionType = ProfessionPhysicalDamage},
		profession_shield_warrior = {name = "profession_shield_warrior", primaryAttribute = DOTA_ATTRIBUTE_STRENGTH, needs={"profession_warrior"}, order = 2, attackType = ATTACK_TYPE_MELEE, index = GetProfessionIndex(), professionType = ProfessionTank},
		profession_assassin = {name = "profession_assassin", primaryAttribute = DOTA_ATTRIBUTE_AGILITY, order = 1, index = GetProfessionIndex()},
		profession_phantom_assassin = {name = "profession_phantom_assassin", primaryAttribute = DOTA_ATTRIBUTE_AGILITY, needs={"profession_assassin"}, order = 2, attackType = ATTACK_TYPE_MELEE, index = GetProfessionIndex(), professionType = ProfessionPhysicalDamage},
		profession_templar_assassin = {name = "profession_templar_assassin", primaryAttribute = DOTA_ATTRIBUTE_AGILITY, needs={"profession_assassin"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionPhysicalDamage},
		profession_priest = {name = "profession_priest", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, order = 1, index = GetProfessionIndex()},
		profession_holy_light_priest = {name = "profession_holy_light_priest", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_priest"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionPriest},
		profession_shadow_priest = {name = "profession_shadow_priest", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_priest"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionPriest},
		profession_archer = {name = "profession_archer", primaryAttribute = DOTA_ATTRIBUTE_AGILITY, order = 1, index = GetProfessionIndex()},
		profession_phantom_archer = {name = "profession_phantom_archer", primaryAttribute = DOTA_ATTRIBUTE_AGILITY, needs={"profession_archer"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionPhysicalDamage},
		profession_arcane_archer = {name = "profession_arcane_archer", primaryAttribute = DOTA_ATTRIBUTE_AGILITY, needs={"profession_archer"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionMagicalDamage},
		profession_animal_trainer = {name = "profession_animal_trainer", primaryAttribute = DOTA_ATTRIBUTE_STRENGTH, order = 1, index = GetProfessionIndex()},
		profession_beastmaster = {name = "profession_beastmaster", primaryAttribute = DOTA_ATTRIBUTE_STRENGTH, needs={"profession_animal_trainer"}, order = 2, attackType = ATTACK_TYPE_MELEE, index = GetProfessionIndex(), professionType = ProfessionTank},
		profession_druid = {name = "profession_druid", primaryAttribute = DOTA_ATTRIBUTE_STRENGTH, order = 1, index = GetProfessionIndex()},
		profession_beast_druid = {name = "profession_beast_druid", primaryAttribute = DOTA_ATTRIBUTE_STRENGTH, needs={"profession_druid"}, order = 2, attackType = ATTACK_TYPE_MELEE, index = GetProfessionIndex(), professionType = ProfessionTank},
		profession_talon_druid = {name = "profession_talon_druid", primaryAttribute = DOTA_ATTRIBUTE_STRENGTH, needs={"profession_druid"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionAssist},
		profession_knight = {name = "profession_knight", primaryAttribute = DOTA_ATTRIBUTE_STRENGTH, order = 1, index = GetProfessionIndex()},
		profession_paladin = {name = "profession_paladin", primaryAttribute = DOTA_ATTRIBUTE_STRENGTH, needs={"profession_knight"}, order = 2, attackType = ATTACK_TYPE_MELEE, index = GetProfessionIndex(), professionType = ProfessionTank},
		profession_bramble_knight = {name = "profession_bramble_knight", primaryAttribute = DOTA_ATTRIBUTE_STRENGTH, needs={"profession_knight"}, order = 2, attackType = ATTACK_TYPE_MELEE, index = GetProfessionIndex(), professionType = ProfessionTank},
		profession_swordsman = {name = "profession_swordsman", primaryAttribute = DOTA_ATTRIBUTE_AGILITY, order = 1, index = GetProfessionIndex()},
		profession_spellsword = {name = "profession_spellsword", primaryAttribute = DOTA_ATTRIBUTE_AGILITY, needs={"profession_swordsman"}, order = 2, attackType = ATTACK_TYPE_MELEE, index = GetProfessionIndex(), professionType = ProfessionMagicalDamage},
		profession_blade_master = {name = "profession_blade_master", primaryAttribute = DOTA_ATTRIBUTE_AGILITY, needs={"profession_swordsman"}, order = 2, attackType = ATTACK_TYPE_MELEE, index = GetProfessionIndex(), professionType = ProfessionPhysicalDamage},
		profession_sharpshooter = {name = "profession_sharpshooter", primaryAttribute = DOTA_ATTRIBUTE_AGILITY, order = 1, index = GetProfessionIndex()},
		profession_gun_fighter = {name = "profession_gun_fighter", primaryAttribute = DOTA_ATTRIBUTE_AGILITY, needs={"profession_sharpshooter"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionPhysicalDamage},
		profession_guns_division = {name = "profession_guns_division", primaryAttribute = DOTA_ATTRIBUTE_AGILITY, needs={"profession_sharpshooter"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionPhysicalDamage},
		profession_mage = {name = "profession_mage", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, order = 1, index = GetProfessionIndex()},
		profession_light_mage = {name = "profession_light_mage", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_mage"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionPriest},
		profession_dark_mage = {name = "profession_dark_mage", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_mage"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionPhysicalDamage},
		profession_water_mage = {name = "profession_water_mage", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_mage"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionControl},
		profession_fire_mage = {name = "profession_fire_mage", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_mage"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionMagicalDamage},
		profession_air_mage = {name = "profession_air_mage", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_mage"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionAssist},
		profession_thunder_mage = {name = "profession_thunder_mage", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_mage"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionMagicalDamage},
		profession_earth_mage = {name = "profession_earth_mage", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_mage"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionAssist},
		profession_life_mage = {name = "profession_life_mage", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_mage"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionPriest},
		profession_necromancer = {name = "profession_necromancer", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_mage"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionPhysicalDamage},
		profession_mind_mage = {name = "profession_mind_mage", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_mage"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionControl},
		profession_soul_mage = {name = "profession_soul_mage", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_mage"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionMagicalDamage},
		profession_time_mage = {name = "profession_time_mage", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_mage"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionControl},
		profession_space_mage = {name = "profession_space_mage", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_mage"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionAssist},
		profession_illusionist = {name = "profession_illusionist", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_mage"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionPhysicalDamage},
		profession_poison_mage = {name = "profession_poison_mage", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, needs={"profession_mage"}, order = 2, attackType = ATTACK_TYPE_RANGED, index = GetProfessionIndex(), professionType = ProfessionMagicalDamage},
		profession_antimage = {name = "profession_antimage", primaryAttribute = DOTA_ATTRIBUTE_AGILITY, order = 1, index = GetProfessionIndex()},
		profession_spell_breaker = {name = "profession_spell_breaker", primaryAttribute = DOTA_ATTRIBUTE_AGILITY, needs={"profession_antimage"}, order = 2, attackType = ATTACK_TYPE_MELEE, index = GetProfessionIndex(), professionType = ProfessionMagicalDamage},
		axe_berserkers_call = {name = "axe_berserkers_call", needs={"profession_warrior"}, index = GetProfessionIndex()},
		axe_culling_blade = {name = "axe_culling_blade", needs={"profession_warrior"}, index = GetProfessionIndex()},
		life_stealer_rage = {name = "life_stealer_rage", needs={"profession_berserker"}, index = GetProfessionIndex()},
		huskar_berserkers_blood = {name = "huskar_berserkers_blood", needs={"profession_berserker"}, index = GetProfessionIndex()},
		mars_gods_rebuke = {name = "mars_gods_rebuke", needs={"profession_shield_warrior"}, index = GetProfessionIndex()},
		mars_bulwark = {name = "mars_bulwark", needs={"profession_shield_warrior"}, index = GetProfessionIndex()},
		nyx_assassin_vendetta = {name = "nyx_assassin_vendetta", needs={"profession_assassin"}, index = GetProfessionIndex()},
		bounty_hunter_shuriken_toss = {name = "bounty_hunter_shuriken_toss", needs={"profession_assassin"}, index = GetProfessionIndex()},
		phantom_assassin_phantom_strike = {name = "phantom_assassin_phantom_strike", needs={"profession_phantom_assassin"}, index = GetProfessionIndex()},
		phantom_assassin_coup_de_grace = {name = "phantom_assassin_coup_de_grace", needs={"profession_phantom_assassin"}, index = GetProfessionIndex()},
		templar_assassin_refraction = {name = "templar_assassin_refraction", needs={"profession_templar_assassin"}, index = GetProfessionIndex()},
		templar_assassin_psi_blades = {name = "templar_assassin_psi_blades", needs={"profession_templar_assassin"}, index = GetProfessionIndex()},
		enchantress_natures_attendants = {name = "enchantress_natures_attendants", needs={"profession_priest", "profession_life_mage", "profession_light_mage", "profession_water_mage"}, index = GetProfessionIndex()},
		keeper_of_the_light_chakra_magic = {name = "keeper_of_the_light_chakra_magic", needs={"profession_holy_light_priest"}, index = GetProfessionIndex()},
		chen_divine_favor = {name = "chen_divine_favor", needs={"profession_priest"}, index = GetProfessionIndex()},
		chen_hand_of_god = {name = "chen_hand_of_god", needs={"profession_holy_light_priest", "profession_life_mage"}, index = GetProfessionIndex()},
		dazzle_bad_juju = {name = "dazzle_bad_juju", needs={"profession_shadow_priest"}, index = GetProfessionIndex()},
		dazzle_shallow_grave = {name = "dazzle_shallow_grave", needs={"profession_shadow_priest"}, index = GetProfessionIndex()},
		dazzle_shadow_wave = {name = "dazzle_shadow_wave", needs={"profession_shadow_priest"}, index = GetProfessionIndex()},
		drow_ranger_marksmanship = {name = "drow_ranger_marksmanship", needs={"profession_archer"}, index = GetProfessionIndex()},
		drow_ranger_wave_of_silence = {name = "drow_ranger_wave_of_silence", needs={"profession_archer"}, index = GetProfessionIndex()},
		medusa_split_shot = {name = "medusa_split_shot", needs={"profession_phantom_archer"}, index = GetProfessionIndex()},
		phantom = {name = "phantom", needs={"profession_phantom_archer"}, index = GetProfessionIndex()},
		windrunner_powershot = {name = "windrunner_powershot", needs={"profession_arcane_archer"}},
		obsidian_destroyer_arcane_orb = {name = "obsidian_destroyer_arcane_orb", needs={"profession_arcane_archer"}, index = GetProfessionIndex()},
		call_of_the_wild_hawk = {name = "call_of_the_wild_hawk", needs={"profession_animal_trainer"}, index = GetProfessionIndex()},
		beastmaster_wild_axes = {name = "beastmaster_wild_axes", needs={"profession_animal_trainer"}, index = GetProfessionIndex()},
		beastmaster_inner_beast = {name = "beastmaster_inner_beast", needs={"profession_beastmaster"}, index = GetProfessionIndex()},
		call_of_the_wild_boar = {name = "call_of_the_wild_boar", needs={"profession_beastmaster"}, index = GetProfessionIndex()},
		beastmaster_primal_roar = {name = "beastmaster_primal_roar", needs={"profession_beastmaster"}, index = GetProfessionIndex()},
		treant_living_armor = {name = "treant_living_armor", needs={"profession_druid"}, index = GetProfessionIndex()},
		tree_call = {name = "tree_call", needs={"profession_druid"}, index = GetProfessionIndex()},
		call_of_spirit_bear = {name = "call_of_spirit_bear", needs={"profession_beast_druid"}, index = GetProfessionIndex()},
		lone_druid_true_form = {name = "lone_druid_true_form", needs={"profession_beast_druid"}, index = GetProfessionIndex()},
		nevermore_dark_lord = {name = "nevermore_dark_lord", needs={"profession_talon_druid"}, index = GetProfessionIndex()},
		treant_overgrowth = {name = "treant_overgrowth", needs={"profession_talon_druid", "profession_life_mage"}, index = GetProfessionIndex()},
		abaddon_aphotic_shield = {name = "abaddon_aphotic_shield", needs={"profession_knight"}, index = GetProfessionIndex()},
		omniknight_repel = {name = "omniknight_repel", needs={"profession_knight"}, index = GetProfessionIndex()},
		omniknight_purification = {name = "omniknight_purification", needs={"profession_paladin"}, index = GetProfessionIndex()},
		omniknight_guardian_angel = {name = "omniknight_guardian_angel", needs={"profession_paladin"}, index = GetProfessionIndex()},
		centaur_double_edge = {name = "centaur_double_edge", needs={"profession_bramble_knight"}, index = GetProfessionIndex()},
		spectre_dispersion = {name = "spectre_dispersion", needs={"profession_bramble_knight"}, index = GetProfessionIndex()},
		blade_dance = {name = "blade_dance", needs={"profession_swordsman"}, index = GetProfessionIndex()},
		juggernaut_blade_fury = {name = "juggernaut_blade_fury", needs={"profession_swordsman"}, index = GetProfessionIndex()},
		doom_bringer_infernal_blade = {name = "doom_bringer_infernal_blade", needs={"profession_spellsword"}, index = GetProfessionIndex()},
		magnataur_shockwave = {name = "magnataur_shockwave", needs={"profession_spellsword"}, index = GetProfessionIndex()},
		sven_great_cleave = {name = "sven_great_cleave", needs={"profession_blade_master"}, index = GetProfessionIndex()},
		juggernaut_omni_slash = {name = "juggernaut_omni_slash", needs={"profession_blade_master"}, index = GetProfessionIndex()},
		sniper_take_aim = {name = "sniper_take_aim", needs={"profession_sharpshooter"}, index = GetProfessionIndex()},
		sniper_assassinate = {name = "sniper_assassinate", needs={"profession_sharpshooter"}, index = GetProfessionIndex()},
		sniper_headshot = {name = "sniper_headshot", needs={"profession_gun_fighter"}, index = GetProfessionIndex()},
		snapfire_scatterblast = {name = "snapfire_scatterblast", needs={"profession_gun_fighter"}, index = GetProfessionIndex()},
		gyrocopter_flak_cannon = {name = "gyrocopter_flak_cannon", needs={"profession_guns_division"}, index = GetProfessionIndex()},
		gyrocopter_call_down = {name = "gyrocopter_call_down", needs={"profession_guns_division"}, index = GetProfessionIndex()},
		meditate = {name = "meditate", needs={"profession_mage"}, index = GetProfessionIndex()},
		skywrath_mage_arcane_bolt = {name = "skywrath_mage_arcane_bolt", needs={"profession_mage"}, index = GetProfessionIndex()},
		spell_amplify = {name = "spell_amplify", needs={"profession_mage"}, index = GetProfessionIndex()},
		keeper_of_the_light_blinding_light = {name = "keeper_of_the_light_blinding_light", needs={"profession_light_mage"}, index = GetProfessionIndex()},
		keeper_of_the_light_illuminate = {name = "keeper_of_the_light_illuminate", needs={"profession_light_mage"}, index = GetProfessionIndex()},
		prayer = {name = "prayer", needs={"profession_light_mage"}, index = GetProfessionIndex()},
		shadow_shaman_voodoo = {name = "shadow_shaman_voodoo", needs={"profession_dark_mage"}, index = GetProfessionIndex()},
		doom_bringer_doom = {name = "doom_bringer_doom", needs={"profession_dark_mage"}, index = GetProfessionIndex()},
		metamorphosis = {name = "metamorphosis", needs={"profession_dark_mage"}, index = GetProfessionIndex()},
		--morphling_waveform = {name = "morphling_waveform", needs={"profession_water_mage"}, index = GetProfessionIndex()},
		winter_wyvern_cold_embrace = {name = "winter_wyvern_cold_embrace", needs={"profession_water_mage"}, index = GetProfessionIndex()},
		crystal_maiden_crystal_nova = {name = "crystal_maiden_crystal_nova", needs={"profession_water_mage"}, index = GetProfessionIndex()},
		crystal_maiden_frostbite = {name = "crystal_maiden_frostbite", needs={"profession_water_mage"}, index = GetProfessionIndex()},
		jakiro_ice_path = {name = "jakiro_ice_path", needs={"profession_water_mage"}, index = GetProfessionIndex()},
		fire_golem_summon = {name = "fire_golem_summon", needs={"profession_fire_mage", "profession_dark_mage"}, index = GetProfessionIndex()},
		lina_dragon_slave = {name = "lina_dragon_slave", needs={"profession_fire_mage"}, index = GetProfessionIndex()},
		lina_light_strike_array = {name = "lina_light_strike_array", needs={"profession_fire_mage"}, index = GetProfessionIndex()},
		abyssal_underlord_firestorm = {name = "abyssal_underlord_firestorm", needs={"profession_fire_mage"}, index = GetProfessionIndex()},
		ember_spirit_flame_guard = {name = "ember_spirit_flame_guard", needs={"profession_fire_mage"}, index = GetProfessionIndex()},
		riki_smoke_screen = {name = "riki_smoke_screen", needs={"profession_air_mage"}, index = GetProfessionIndex()},
		windrunner_windrun = {name = "windrunner_windrun", needs={"profession_air_mage"}, index = GetProfessionIndex()},
		accelerating_aura = {name = "accelerating_aura", needs={"profession_air_mage"}, index = GetProfessionIndex()},
		razor_eye_of_the_storm = {name = "razor_eye_of_the_storm", needs={"profession_air_mage"}, index = GetProfessionIndex()},
		haste = {name = "haste", needs={"profession_air_mage"}, index = GetProfessionIndex()},
		zuus_arc_lightning = {name = "zuus_arc_lightning", needs={"profession_thunder_mage"}, index = GetProfessionIndex()},
		zuus_lightning_bolt = {name = "zuus_lightning_bolt", needs={"profession_thunder_mage"}, index = GetProfessionIndex()},
		zuus_static_field = {name = "zuus_static_field", needs={"profession_thunder_mage"}, index = GetProfessionIndex()},
		zuus_thundergods_wrath = {name = "zuus_thundergods_wrath", needs={"profession_thunder_mage"}, index = GetProfessionIndex()},
		guardian_of_earth = {name = "guardian_of_earth", needs={"profession_earth_mage"}, index = GetProfessionIndex()},
		absolute_defense = {name = "absolute_defense", needs={"profession_earth_mage"}, index = GetProfessionIndex()},
		earthshaker_aftershock = {name = "earthshaker_aftershock", needs={"profession_earth_mage"}, index = GetProfessionIndex()},
		earthshaker_echo_slam = {name = "earthshaker_echo_slam", needs={"profession_earth_mage"}, index = GetProfessionIndex()},
		tidehunter_ravage = {name = "tidehunter_ravage", needs={"profession_earth_mage"}, index = GetProfessionIndex()},
		furion_wrath_of_nature = {name = "furion_wrath_of_nature", needs={"profession_life_mage"}, index = GetProfessionIndex(), index = GetProfessionIndex()},
		vampiric_aura = {name = "vampiric_aura", needs={"profession_necromancer"}, index = GetProfessionIndex()},
		necronomicon_warrior_summon = {name = "necronomicon_warrior_summon", needs={"profession_necromancer"}, index = GetProfessionIndex()},
		necronomicon_archer_summon = {name = "necronomicon_archer_summon", needs={"profession_necromancer"}, index = GetProfessionIndex()},
		bone_dragon_summon = {name = "bone_dragon_summon", needs={"profession_necromancer"}, index = GetProfessionIndex()},
		enigma_midnight_pulse = {name = "enigma_midnight_pulse", needs={"profession_necromancer", "profession_dark_mage"}, index = GetProfessionIndex()},
		night_stalker_crippling_fear = {name = "night_stalker_crippling_fear", needs={"profession_mind_mage"}, index = GetProfessionIndex()},
		silencer_global_silence = {name = "silencer_global_silence", needs={"profession_mind_mage"}, index = GetProfessionIndex()},
		obsidian_destroyer_sanity_eclipse = {name = "obsidian_destroyer_sanity_eclipse", needs={"profession_mind_mage"}, index = GetProfessionIndex()},
		necrolyte_death_pulse = {name = "necrolyte_death_pulse", needs={"profession_soul_mage"}, index = GetProfessionIndex()},
		death_prophet_exorcism = {name = "death_prophet_exorcism", needs={"profession_soul_mage"}, index = GetProfessionIndex()},
		necrolyte_reapers_scythe = {name = "necrolyte_reapers_scythe", needs={"profession_soul_mage"}, index = GetProfessionIndex()},
		faceless_void_time_lock = {name = "faceless_void_time_lock", needs={"profession_time_mage"}, index = GetProfessionIndex()},
		faceless_void_chronosphere = {name = "faceless_void_chronosphere", needs={"profession_time_mage"}, index = GetProfessionIndex()},
		skeleton_king_reincarnation = {name = "skeleton_king_reincarnation", needs={"profession_time_mage"}, index = GetProfessionIndex()},
		antimage_blink = {name = "antimage_blink", needs={"profession_space_mage"}, index = GetProfessionIndex()},
		space_shield = {name = "space_shield", needs={"profession_space_mage"}, index = GetProfessionIndex()},
		enigma_black_hole = {name = "enigma_black_hole", needs={"profession_space_mage"}, index = GetProfessionIndex()},
		phantom_lancer_spirit_lance = {name = "phantom_lancer_spirit_lance", needs={"profession_illusionist"}, index = GetProfessionIndex()},
		phantom_lancer_juxtapose = {name = "phantom_lancer_juxtapose", needs={"profession_illusionist"}, index = GetProfessionIndex()},
		chaos_knight_phantasm = {name = "chaos_knight_phantasm", needs={"profession_illusionist"}, index = GetProfessionIndex()},
		venomancer_poison_sting = {name = "venomancer_poison_sting", needs={"profession_poison_mage"}, index = GetProfessionIndex()},
		viper_nethertoxin = {name = "viper_nethertoxin", needs={"profession_poison_mage"}, index = GetProfessionIndex()},
		viper_viper_strike = {name = "viper_viper_strike", needs={"profession_poison_mage"}, index = GetProfessionIndex()},
		antimage_mana_break = {name = "antimage_mana_break", needs={"profession_antimage"}, index = GetProfessionIndex()},
		antimage_spell_shield = {name = "antimage_spell_shield", needs={"profession_antimage"}, index = GetProfessionIndex()},
		disruptor_static_storm = {name = "disruptor_static_storm", needs={"profession_spell_breaker"}, index = GetProfessionIndex()},
		antimage_mana_void = {name = "antimage_mana_void", needs={"profession_spell_breaker"}, index = GetProfessionIndex()},

		slardar_bash = {name ="slardar_bash", index = GetProfessionIndex()},

		gift_agility = {name = "gift_agility", index = GetProfessionIndex()},
		gift_intellect = {name = "gift_intellect", index = GetProfessionIndex()},
		gift_strength = {name = "gift_strength", index = GetProfessionIndex()},

		profession_businessman = {name = "profession_businessman", index = GetProfessionIndex(), type = SecondaryProfession},
		invest = {name = "invest", needs={"profession_businessman"}, index = GetProfessionIndex(), type = SecondaryProfession},
		business = {name = "business", needs={"profession_businessman"}, index = GetProfessionIndex(), type = SecondaryProfession},
		profession_lord = {name = "profession_lord", index = GetProfessionIndex(), type = SecondaryProfession},
		offense = {name = "offense", needs={"profession_lord"}, index = GetProfessionIndex(), type = SecondaryProfession},
		armorer = {name = "armorer", needs={"profession_lord"}, index = GetProfessionIndex(), type = SecondaryProfession},
		resistance = {name = "resistance", needs={"profession_lord"}, index = GetProfessionIndex(), type = SecondaryProfession},
		profession_pharmaceutist = {name = "profession_pharmaceutist", index = GetProfessionIndex(), type = SecondaryProfession},
		health_potion = {name = "health_potion", needs={"profession_pharmaceutist"}, index = GetProfessionIndex(), type = SecondaryProfession},
		mana_potion = {name = "mana_potion", needs={"profession_pharmaceutist"}, index = GetProfessionIndex(), type = SecondaryProfession},
		profession_blacksmith = {name = "profession_blacksmith", index = GetProfessionIndex(), type = SecondaryProfession},
		forge = {name = "forge", needs={"profession_blacksmith"}, index = GetProfessionIndex(), type = SecondaryProfession},
		blacksmith_resistance = {name = "blacksmith_resistance", index = GetProfessionIndex(), needs={"profession_blacksmith"}, type = SecondaryProfession},
		profession_navigator = {name = "profession_navigator", index = GetProfessionIndex(), type = SecondaryProfession},
		explore = {name = "explore", needs={"profession_navigator"}, index = GetProfessionIndex(), type = SecondaryProfession},
		storm_sailing = {name = "storm_sailing", needs={"profession_navigator"}, index = GetProfessionIndex(), type = SecondaryProfession},
		profession_prophet = {name = "profession_prophet", index = GetProfessionIndex(), type = SecondaryProfession},
		prophecy = {name = "prophecy", needs={"profession_prophet"}, index = GetProfessionIndex(), type = SecondaryProfession},
		big_prophecy = {name = "big_prophecy", needs={"profession_prophet"}, index = GetProfessionIndex(), type = SecondaryProfession},
		profession_diviner = {name = "profession_diviner", index = GetProfessionIndex(), type = SecondaryProfession},
		luck = {name = "luck", needs={"profession_diviner"}, index = GetProfessionIndex(), type = SecondaryProfession},
		unluck = {name = "unluck", needs={"profession_diviner"}, index = GetProfessionIndex(), type = SecondaryProfession},
		
		--[[beastmaster_boar_poison = {name = "beastmaster_boar_poison"},
		tower_true_sight = {name = "tower_true_sight"},
		lone_druid_spirit_bear_demolish = {name = "lone_druid_spirit_bear_demolish"},
		lone_druid_spirit_bear_entangle = {name = "lone_druid_spirit_bear_entangle"},
		warlock_golem_flaming_fists = {name = "warlock_golem_flaming_fists"},
		warlock_golem_permanent_immolation = {name = "warlock_golem_permanent_immolation"},]]--
	}

	GameRules:GetGameModeEntity().shopItems = {
		{name = "item_gold1", type=TYPE_CONSUMABLE, quality=QUALITY_3, value=10, image="file://{images}/items/medallion_of_courage.png"},

		{name = "item_race_human", abilityName = "race_human", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_race_elf", abilityName = "race_elf", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_race_orc", abilityName = "race_orc", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_race_undead", abilityName = "race_undead", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_race_angel", abilityName = "race_angel", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_race_demon", abilityName = "race_demon", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_race_dragon", abilityName = "race_dragon", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_profession_warrior", abilityName = "profession_warrior", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_profession_berserker", abilityName = "profession_berserker", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_profession_shield_warrior", abilityName = "profession_shield_warrior", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_profession_assassin", abilityName = "profession_assassin", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_profession_phantom_assassin", abilityName = "profession_phantom_assassin", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_profession_templar_assassin", abilityName = "profession_templar_assassin", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_profession_priest", abilityName = "profession_priest", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_profession_holy_light_priest", abilityName = "profession_holy_light_priest", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_profession_shadow_priest", abilityName = "profession_shadow_priest", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_profession_archer", abilityName = "profession_archer", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_profession_phantom_archer", abilityName = "profession_phantom_archer", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_profession_arcane_archer", abilityName = "profession_arcane_archer", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_profession_animal_trainer", abilityName = "profession_animal_trainer", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_profession_beastmaster", abilityName = "profession_beastmaster", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		
		{name = "item_profession_druid", abilityName = "profession_druid", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_profession_beast_druid", abilityName = "profession_beast_druid", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_profession_talon_druid", abilityName = "profession_talon_druid", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_profession_knight", abilityName = "profession_knight", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_profession_paladin", abilityName = "profession_paladin", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_profession_bramble_knight", abilityName = "profession_bramble_knight", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_profession_swordsman", abilityName = "profession_swordsman", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_profession_spellsword", abilityName = "profession_spellsword", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_profession_blade_master", abilityName = "profession_blade_master", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		
		{name = "item_profession_sharpshooter", abilityName = "profession_sharpshooter", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_profession_gun_fighter", abilityName = "profession_gun_fighter", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_profession_guns_division", abilityName = "profession_guns_division", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		
		{name = "item_profession_mage", abilityName = "profession_mage", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png", maxCount = 30},
		{name = "item_profession_light_mage", abilityName = "profession_light_mage", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_profession_dark_mage", abilityName = "profession_dark_mage", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_profession_water_mage", abilityName = "profession_water_mage", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_profession_fire_mage", abilityName = "profession_fire_mage", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		
		{name = "item_profession_air_mage", abilityName = "profession_air_mage", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_profession_thunder_mage", abilityName = "profession_thunder_mage", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_profession_earth_mage", abilityName = "profession_earth_mage", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_profession_life_mage", abilityName = "profession_life_mage", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_profession_necromancer", abilityName = "profession_necromancer", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_profession_mind_mage", abilityName = "profession_mind_mage", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_profession_soul_mage", abilityName = "profession_soul_mage", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_profession_time_mage", abilityName = "profession_time_mage", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_profession_space_mage", abilityName = "profession_space_mage", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_profession_illusionist", abilityName = "profession_illusionist", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_profession_poison_mage", abilityName = "profession_poison_mage", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
		{name = "item_profession_antimage", abilityName = "profession_antimage", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_profession_spell_breaker", abilityName = "profession_spell_breaker", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},

		
		{name = "item_axe_berserkers_call", abilityName = "axe_berserkers_call", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_axe_culling_blade", abilityName = "axe_culling_blade", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_life_stealer_rage", abilityName = "life_stealer_rage", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_huskar_berserkers_blood", abilityName = "huskar_berserkers_blood", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_mars_gods_rebuke", abilityName = "mars_gods_rebuke", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_mars_bulwark", abilityName = "mars_bulwark", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_nyx_assassin_vendetta", abilityName = "nyx_assassin_vendetta", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_bounty_hunter_shuriken_toss", abilityName = "bounty_hunter_shuriken_toss", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_phantom_assassin_phantom_strike", abilityName = "phantom_assassin_phantom_strike", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_phantom_assassin_coup_de_grace", abilityName = "phantom_assassin_coup_de_grace", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_templar_assassin_refraction", abilityName = "templar_assassin_refraction", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_templar_assassin_psi_blades", abilityName = "templar_assassin_psi_blades", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_enchantress_natures_attendants", abilityName = "enchantress_natures_attendants", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_keeper_of_the_light_chakra_magic", abilityName = "keeper_of_the_light_chakra_magic", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_chen_divine_favor", abilityName = "chen_divine_favor", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		
		{name = "item_chen_hand_of_god", abilityName = "chen_hand_of_god", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		
		{name = "item_dazzle_bad_juju", abilityName = "dazzle_bad_juju", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_dazzle_shallow_grave", abilityName = "dazzle_shallow_grave", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_dazzle_shadow_wave", abilityName = "dazzle_shadow_wave", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		
		{name = "item_drow_ranger_marksmanship", abilityName = "drow_ranger_marksmanship", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_drow_ranger_wave_of_silence", abilityName = "drow_ranger_wave_of_silence", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_medusa_split_shot", abilityName = "medusa_split_shot", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_phantom", abilityName = "phantom", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_windrunner_powershot", abilityName = "windrunner_powershot", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_obsidian_destroyer_arcane_orb", abilityName = "obsidian_destroyer_arcane_orb", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		
		{name = "item_call_of_the_wild_hawk", abilityName = "call_of_the_wild_hawk", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_beastmaster_wild_axes", abilityName = "beastmaster_wild_axes", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_beastmaster_inner_beast", abilityName = "beastmaster_inner_beast", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_call_of_the_wild_boar", abilityName = "call_of_the_wild_boar", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_beastmaster_primal_roar", abilityName = "beastmaster_primal_roar", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		
		{name = "item_treant_living_armor", abilityName = "treant_living_armor", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_tree_call", abilityName = "tree_call", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_call_of_spirit_bear", abilityName = "call_of_spirit_bear", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_lone_druid_true_form", abilityName = "lone_druid_true_form", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_nevermore_dark_lord", abilityName = "nevermore_dark_lord", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_treant_overgrowth", abilityName = "treant_overgrowth", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		
		{name = "item_abaddon_aphotic_shield", abilityName = "abaddon_aphotic_shield", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_omniknight_repel", abilityName = "omniknight_repel", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_omniknight_purification", abilityName = "omniknight_purification", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_omniknight_guardian_angel", abilityName = "omniknight_guardian_angel", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_centaur_double_edge", abilityName = "centaur_double_edge", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_spectre_dispersion", abilityName = "spectre_dispersion", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		
		{name = "item_blade_dance", abilityName = "blade_dance", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_juggernaut_blade_fury", abilityName = "juggernaut_blade_fury", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_doom_bringer_infernal_blade", abilityName = "doom_bringer_infernal_blade", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_magnataur_shockwave", abilityName = "magnataur_shockwave", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_sven_great_cleave", abilityName = "sven_great_cleave", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_juggernaut_omni_slash", abilityName = "juggernaut_omni_slash", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		
		{name = "item_sniper_take_aim", abilityName = "sniper_take_aim", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tpscroll.png"},
		{name = "item_sniper_assassinate", abilityName = "sniper_assassinate", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_sniper_headshot", abilityName = "sniper_headshot", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_snapfire_scatterblast", abilityName = "snapfire_scatterblast", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_gyrocopter_flak_cannon", abilityName = "gyrocopter_flak_cannon", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_gyrocopter_call_down", abilityName = "gyrocopter_call_down", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		
		{name = "item_meditate", abilityName = "meditate", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png", maxCount = 25},
		{name = "item_skywrath_mage_arcane_bolt", abilityName = "skywrath_mage_arcane_bolt", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_spell_amplify", abilityName = "spell_amplify", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png", maxCount = 10},
		{name = "item_keeper_of_the_light_blinding_light", abilityName = "keeper_of_the_light_blinding_light", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_keeper_of_the_light_illuminate", abilityName = "keeper_of_the_light_illuminate", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_prayer", abilityName = "prayer", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_shadow_shaman_voodoo", abilityName = "shadow_shaman_voodoo", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_doom_bringer_doom", abilityName = "doom_bringer_doom", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_metamorphosis", abilityName = "metamorphosis", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		--{name = "item_morphling_waveform", abilityName = "morphling_waveform", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_winter_wyvern_cold_embrace", abilityName = "winter_wyvern_cold_embrace", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_crystal_maiden_crystal_nova", abilityName = "crystal_maiden_crystal_nova", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_crystal_maiden_frostbite", abilityName = "crystal_maiden_frostbite", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_jakiro_ice_path", abilityName = "jakiro_ice_path", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_fire_golem_summon", abilityName = "fire_golem_summon", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_lina_dragon_slave", abilityName = "lina_dragon_slave", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_lina_light_strike_array", abilityName = "lina_light_strike_array", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_abyssal_underlord_firestorm", abilityName = "abyssal_underlord_firestorm", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_ember_spirit_flame_guard", abilityName = "ember_spirit_flame_guard", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		
		{name = "item_riki_smoke_screen", abilityName = "riki_smoke_screen", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_windrunner_windrun", abilityName = "windrunner_windrun", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_accelerating_aura", abilityName = "accelerating_aura", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_razor_eye_of_the_storm", abilityName = "razor_eye_of_the_storm", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_haste", abilityName = "haste", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},

		{name = "item_zuus_arc_lightning", abilityName = "zuus_arc_lightning", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_zuus_lightning_bolt", abilityName = "zuus_lightning_bolt", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_zuus_static_field", abilityName = "zuus_static_field", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_zuus_thundergods_wrath", abilityName = "zuus_thundergods_wrath", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
	
		
		{name = "item_guardian_of_earth", abilityName = "guardian_of_earth", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_absolute_defense", abilityName = "absolute_defense", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_earthshaker_aftershock", abilityName = "earthshaker_aftershock", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_earthshaker_echo_slam", abilityName = "earthshaker_echo_slam", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_tidehunter_ravage", abilityName = "tidehunter_ravage", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_furion_wrath_of_nature", abilityName = "furion_wrath_of_nature", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		
		{name = "item_vampiric_aura", abilityName = "vampiric_aura", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_necronomicon_warrior_summon", abilityName = "necronomicon_warrior_summon", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_necronomicon_archer_summon", abilityName = "necronomicon_archer_summon", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_bone_dragon_summon", abilityName = "bone_dragon_summon", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_enigma_midnight_pulse", abilityName = "enigma_midnight_pulse", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},

		{name = "item_night_stalker_crippling_fear", abilityName = "night_stalker_crippling_fear", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_silencer_global_silence", abilityName = "silencer_global_silence", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_obsidian_destroyer_sanity_eclipse", abilityName = "obsidian_destroyer_sanity_eclipse", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},

		{name = "item_necrolyte_death_pulse", abilityName = "necrolyte_death_pulse", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_death_prophet_exorcism", abilityName = "death_prophet_exorcism", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_necrolyte_reapers_scythe", abilityName = "necrolyte_reapers_scythe", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},

		{name = "item_faceless_void_time_lock", abilityName = "faceless_void_time_lock", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_faceless_void_chronosphere", abilityName = "faceless_void_chronosphere", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_skeleton_king_reincarnation", abilityName = "skeleton_king_reincarnation", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},

		{name = "item_antimage_blink", abilityName = "antimage_blink", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_space_shield", abilityName = "space_shield", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},
		{name = "item_enigma_black_hole", abilityName = "enigma_black_hole", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},

		{name = "item_phantom_lancer_spirit_lance", abilityName = "phantom_lancer_spirit_lance", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_phantom_lancer_juxtapose", abilityName = "phantom_lancer_juxtapose", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_chaos_knight_phantasm", abilityName = "chaos_knight_phantasm", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},

		{name = "item_venomancer_poison_sting", abilityName = "venomancer_poison_sting", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_viper_nethertoxin", abilityName = "viper_nethertoxin", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_viper_viper_strike", abilityName = "viper_viper_strike", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},

		{name = "item_antimage_mana_break", abilityName = "antimage_mana_break", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_antimage_spell_shield", abilityName = "antimage_spell_shield", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_disruptor_static_storm", abilityName = "disruptor_static_storm", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_antimage_mana_void", abilityName = "antimage_mana_void", type=TYPE_ABILITY, quality=QUALITY_5, image="file://{images}/items/tpscroll.png"},

		{name = "item_slardar_bash", abilityName = "slardar_bash", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},


		{name = "item_gift_agility", abilityName = "gift_agility", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_gift_intellect", abilityName = "gift_intellect", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_gift_strength", abilityName = "gift_strength", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		
		{name = "item_profession_businessman", abilityName = "profession_businessman", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_invest", abilityName = "invest", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_business", abilityName = "business", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_profession_lord", abilityName = "profession_lord", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_offense", abilityName = "offense", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_armorer", abilityName = "armorer", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_resistance", abilityName = "resistance", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_profession_pharmaceutist", abilityName = "profession_pharmaceutist", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_health_potion", abilityName = "health_potion", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_mana_potion", abilityName = "mana_potion", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_profession_blacksmith", abilityName = "profession_blacksmith", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_forge", abilityName = "forge", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_blacksmith_resistance", abilityName = "blacksmith_resistance", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_profession_navigator", abilityName = "profession_navigator", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tango.png"},
		{name = "item_explore", abilityName = "explore", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_storm_sailing", abilityName = "storm_sailing", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_profession_prophet", abilityName = "profession_prophet", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_prophecy", abilityName = "prophecy", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_big_prophecy", abilityName = "big_prophecy", type=TYPE_ABILITY, quality=QUALITY_4, image="file://{images}/items/tpscroll.png"},
		{name = "item_profession_diviner", abilityName = "profession_diviner", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		{name = "item_luck", abilityName = "luck", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},
		{name = "item_unluck", abilityName = "unluck", type=TYPE_ABILITY, quality=QUALITY_2, image="file://{images}/items/tpscroll.png"},

		--item
		{name = "item_set_primary_attribute_strength", primaryAttribute = DOTA_ATTRIBUTE_STRENGTH, type=TYPE_CONSUMABLE, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_set_primary_attribute_agility", primaryAttribute = DOTA_ATTRIBUTE_AGILITY, type=TYPE_CONSUMABLE, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		{name = "item_set_primary_attribute_intellect", primaryAttribute = DOTA_ATTRIBUTE_INTELLECT, type=TYPE_CONSUMABLE, quality=QUALITY_3, image="file://{images}/items/tpscroll.png"},
		--{name = "item_gold1", type=TYPE_CONSUMABLE, quality=QUALITY_3, value=10, image="file://{images}/items/medallion_of_courage.png"},
		{name = "item_gold2", type=TYPE_CONSUMABLE, quality=QUALITY_4, value=15, image="file://{images}/items/medallion_of_courage.png"},
		{name = "item_gold3", type=TYPE_CONSUMABLE, quality=QUALITY_5, value=20, image="file://{images}/items/medallion_of_courage.png"},
		{name = "item_health", type=TYPE_CONSUMABLE, quality=QUALITY_4, value=20, image="file://{images}/items/flask.png", maxCount = 5},
		{name = "item_ability_upgrade", type=TYPE_CONSUMABLE, quality=QUALITY_5, image="file://{images}/items/demonicon.png"},
		{name = "item_eternal_contract", type=TYPE_CONSUMABLE, quality=QUALITY_5, image="file://{images}/items/infused_raindrop.png"},
		{name = "item_attribute_strength", type=TYPE_CONSUMABLE, quality=QUALITY_5, attribute="strength", value=10, image="file://{images}/items/river_painter7.png"},
		{name = "item_attribute_agility", type=TYPE_CONSUMABLE, quality=QUALITY_5, attribute="agility", value=10, image="file://{images}/items/river_painter3.png"},
		{name = "item_attribute_intellect", type=TYPE_CONSUMABLE, quality=QUALITY_5, attribute="intellect", value=10, image="file://{images}/items/river_painter5.png"},
		{name = "item_attribute_all", type=TYPE_CONSUMABLE, quality=QUALITY_5, attribute="attributeAll", value=10, image="file://{images}/items/river_painter.png", maxCount = 6},
		--{name = "item_attribute_health", type=TYPE_CONSUMABLE, quality=QUALITY_5, attribute="health", value=200, image="file://{images}/items/river_painter4.png"}, --有bug，两个函数都没法改，只能放弃了
		{name = "item_water_of_forgetting", type=TYPE_CONSUMABLE, quality=QUALITY_3, image="file://{images}/items/smoke_of_deceit.png", maxCount = 15},
		{name = "item_random_profession_ability", type=TYPE_CONSUMABLE, quality=QUALITY_4, image="file://{images}/items/tome_of_knowledge.png"},
		{name = "item_wishing_water", type=TYPE_CONSUMABLE, quality=QUALITY_5, image="file://{images}/items/spell_prism.png", canUseDirectly=true, maxCount = 10},
		{name = "item_wishing_water_for_ability", type=TYPE_CONSUMABLE, quality=QUALITY_5, image="file://{images}/items/spell_prism.png", qualities={QUALITY_1, QUALITY_2, QUALITY_3, QUALITY_4, QUALITY_5}, canUseDirectly=true, maxCount = 10},
		{name = "item_wishing_water_for_ability_and_quality1", type=TYPE_CONSUMABLE, quality=QUALITY_1, image="file://{images}/items/spell_prism.png", qualities={QUALITY_1}, canUseDirectly=false, maxCount = 20},
		{name = "item_wishing_water_for_ability_and_quality2", type=TYPE_CONSUMABLE, quality=QUALITY_2, image="file://{images}/items/spell_prism.png", qualities={QUALITY_2}, canUseDirectly=false, maxCount = 18},
		{name = "item_wishing_water_for_ability_and_quality3", type=TYPE_CONSUMABLE, quality=QUALITY_3, image="file://{images}/items/spell_prism.png", qualities={QUALITY_3}, canUseDirectly=false, maxCount = 16},
		{name = "item_wishing_water_for_ability_and_quality4", type=TYPE_CONSUMABLE, quality=QUALITY_4, image="file://{images}/items/spell_prism.png", qualities={QUALITY_4}, canUseDirectly=false, maxCount = 14},
		{name = "item_wishing_water_for_ability_and_quality5", type=TYPE_CONSUMABLE, quality=QUALITY_5, image="file://{images}/items/spell_prism.png", qualities={QUALITY_5}, canUseDirectly=false, maxCount = 12},

		{name = "item_angel_sword", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/rapier.png", maxCount = 1},
		{name = "item_angel_shield", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/mirror_shield.png", maxCount = 1},
		{name = "item_angel_helmet", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/helm_of_the_undying.png", maxCount = 1},
		{name = "item_angel_armor", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/craggy_coat.png", maxCount = 1},
		{name = "item_angel_shoes", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/arcane_boots.png", maxCount = 1},
		{name = "item_angel_necklace", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/nullifier.png", maxCount = 1},
		
		{name = "item_curse_sword", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/abyssal_blade.png", maxCount = 1},
		{name = "item_curse_shield", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/vanguard.png", maxCount = 1},
		{name = "item_curse_helmet", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/mask_of_madness.png", maxCount = 1},
		{name = "item_curse_armor", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/crimson_guard.png", maxCount = 1},
		{name = "item_curse_shoes", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/travel_boots_2.png", maxCount = 1},
		{name = "item_curse_ring", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/soul_ring.png", maxCount = 1},

		{name = "item_assault_sword", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/demon_edge.png", maxCount = 1},
		{name = "item_assault_shield", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/poor_mans_shield.png", maxCount = 1},
		{name = "item_assault_helmet", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/headdress.png", maxCount = 1},
		{name = "item_assault_armor", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/assault.png", maxCount = 1},

		{name = "item_undead_ring", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/satanic.png", maxCount = 1},
		{name = "item_undead_cloak", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/vladmir.png", maxCount = 1},
		{name = "item_undead_fangs", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/vampire_fangs.png", maxCount = 1},

		{name = "item_magic_ring", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/arcane_ring.png", maxCount = 1},
		{name = "item_magic_cloak", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/glimmer_cape.png", maxCount = 1},
		{name = "item_magic_book", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/tome_of_aghanim.png", maxCount = 1},

		{name = "item_sharpshooter_bow", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/grove_bow.png", maxCount = 1},
		{name = "item_sharpshooter_bowstring", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/clumsy_net.png", maxCount = 1},
		{name = "item_sharpshooter_arrows", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/the_leveller.png", maxCount = 1},

		{name = "item_ring_of_health_override", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/ring_of_health.png", maxCount = 1},
		{name = "item_ring_of_regen_override", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/ring_of_regen.png", maxCount = 1},
		{name = "item_heart_override", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/heart.png", maxCount = 1},

		{name = "item_dragon_lance_override", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/dragon_lance.png", maxCount = 1},
		{name = "item_dragon_shield", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/solar_crest.png", maxCount = 1},
		{name = "item_dragon_helmet", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/helm_of_the_dominator.png", maxCount = 1},
		{name = "item_dragon_armor", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/hood_of_defiance.png", maxCount = 1},
		{name = "item_dragon_shoes", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/power_treads.png", maxCount = 1},
		{name = "item_dragon_scale_override", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/dragon_scale.png", maxCount = 1},

		{name = "item_mana_orb", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/ex_machina.png", maxCount = 1},
		{name = "item_mana_stone", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/void_stone.png", maxCount = 1},
		{name = "item_mana_heart", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/ocean_heart.png", maxCount = 1},

		{name = "item_cornucopia_1", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/hand_of_midas.png", maxCount = 1},
		{name = "item_cornucopia_2", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/hand_of_midas.png", maxCount = 1},
		{name = "item_cornucopia_3", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/hand_of_midas.png", maxCount = 1},
		{name = "item_cornucopia_4", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/hand_of_midas.png", maxCount = 1},

		--hero quality works as >=quality
		{name = "item_hero_human", heroName ="hero_human", type=TYPE_HERO, quality=QUALITY_1, maxCount = 9},
		{name = "item_hero_elf", heroName ="hero_elf", type=TYPE_HERO, quality=QUALITY_1, maxCount = 7},
		{name = "item_hero_orc", heroName ="hero_orc", type=TYPE_HERO, quality=QUALITY_1, maxCount = 9},
		{name = "item_hero_undead", heroName ="hero_undead", type=TYPE_HERO, quality=QUALITY_1, maxCount = 9},
		{name = "item_hero_angel", heroName ="hero_angel", type=TYPE_HERO, quality=QUALITY_1, maxCount = 5},
		{name = "item_hero_demon", heroName ="hero_demon", type=TYPE_HERO, quality=QUALITY_1, maxCount = 5},
		{name = "item_hero_dragon", heroName ="hero_dragon", type=TYPE_HERO, quality=QUALITY_1, maxCount = 5}
		
	}

	--[[GameRules:GetGameModeEntity().shopItems = {
		{name = "item_race_human", abilityName = "race_human", type=TYPE_ABILITY, quality=QUALITY_1, image="file://{images}/items/tango.png"},
		

		--{name = "item_gift_agility", abilityName = "gift_agility", type=TYPE_ABILITY, quality=QUALITY_3, image="file://{images}/items/tango.png"},
	
		--{name = "item_health", type=TYPE_CONSUMABLE, quality=QUALITY_3, value=10, image="file://{images}/items/flask.png"},

		{name = "item_angel_sword", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/rapier.png", maxCount = 1},
		{name = "item_angel_shield", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/mirror_shield.png", maxCount = 1},
		{name = "item_angel_helmet", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/helm_of_the_undying.png", maxCount = 1},
		{name = "item_angel_armor", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/craggy_coat.png", maxCount = 1},
		{name = "item_angel_shoes", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/arcane_boots.png", maxCount = 1},
		{name = "item_angel_necklace", type=TYPE_EQUIPMENT, quality=QUALITY_5, image="file://{images}/items/nullifier.png", maxCount = 1},

		{name = "item_hero_human", heroName ="hero_human", type=TYPE_HERO, quality=QUALITY_1, maxCount = 5},
		
	}]]--
	InitShopItemExistCount()

	GameRules:GetGameModeEntity().equipmentAbilities = {
		{name = "angelic_alliance_aura", type = BUFF, needs={"item_angel_sword", "item_angel_shield", "item_angel_helmet", "item_angel_armor", "item_angel_shoes", "item_angel_necklace"}},
		{name = "armor_of_the_damned_aura", type = DEBUFF, needs={"item_curse_sword", "item_curse_shield", "item_curse_helmet", "item_curse_armor", "item_curse_shoes", "item_curse_ring"}},
		{name = "assault_cuirass_aura", type = DEBUFF, needs={"item_assault_sword", "item_assault_shield", "item_assault_helmet", "item_assault_armor"}},
		{name = "cloak_of_the_undead_king_aura", type = BUFF, needs={"item_undead_ring", "item_undead_cloak", "item_undead_fangs"}},
		{name = "ring_of_the_magi_aura", type = DEBUFF, needs={"item_magic_ring", "item_magic_cloak", "item_magic_book"}},
		{name = "bow_of_the_sharpshooter_aura", type = BUFF, needs={"item_sharpshooter_bow", "item_sharpshooter_bowstring", "item_sharpshooter_arrows"}},
		{name = "elixir_of_life_aura", type = BUFF, needs={"item_ring_of_health_override", "item_ring_of_regen_override", "item_heart_override"}},
		{name = "power_of_the_dragon_father_aura", type = BUFF, needs={"item_dragon_lance_override", "item_dragon_shield", "item_dragon_helmet", "item_dragon_armor", "item_dragon_shoes", "item_dragon_scale_override"}},
		{name = "well_of_wizard_aura", type = BUFF, needs={"item_mana_orb", "item_mana_stone", "item_mana_heart"}},
	}

	GameRules:GetGameModeEntity().probability = {
		--{consumable = 0, common = 0, rare = 50, epic = 50, artifact = 0}, --1th
		{consumable = 50, common = 50, rare = 0, epic = 0, artifact = 0},
		{consumable = 50, common = 49, rare = 1, epic = 0, artifact = 0},
		{consumable = 50, common = 48, rare = 2, epic = 0, artifact = 0},
		{consumable = 50, common = 44, rare = 5, epic = 1, artifact = 0},
		{consumable = 50, common = 40, rare = 8, epic = 2, artifact = 0},
		{consumable = 50, common = 35, rare = 10, epic = 5, artifact = 0},
		{consumable = 40, common = 29, rare = 20, epic = 10, artifact = 1},
		{consumable = 40, common = 28, rare = 20, epic = 10, artifact = 2},
		{consumable = 40, common = 20, rare = 20, epic = 15, artifact = 5},
		{consumable = 30, common = 20, rare = 20, epic = 20, artifact = 10}
		--{consumable = 0, common = 0, rare = 30, epic = 30, artifact = 40} --10th
	}

	GameRules:GetGameModeEntity().abilityUpgradeProbability = {
		1, 2, 4, 8, 12, 16, 20, 24, 28, 32
	}

	GameRules:GetGameModeEntity().waitCount = 0 --防止兽王斧子收不回

	InitAbilityExecutorConfigs()
	InitQuestsConfigs()
	InitSecondaryProfessionHandlers()

	GameRules:GetGameModeEntity().dummyPlayers = {}
	for i = 1, 4 do
		local courier = GameRules:AddBotPlayerWithEntityScript("npc_dota_hero_wisp", "Abyss", DOTA_TEAM_BADGUYS, nil , false)
		courier:GetPlayerOwner():SetAssignedHeroEntity(courier)
		courier:ForceKill(false)
		table.insert(GameRules:GetGameModeEntity().dummyPlayers, courier:GetPlayerOwner())
	end
	

	--only enable when online
	--if GameRules:IsCheatMode() then
	--	GameRules:SetGameWinner(DOTA_TEAM_BADGUYS)
	--	GameRules:GetGameModeEntity().gameEnded = true
	--end
end

function InitStageEnemyConfigsForCorpsMode( )
	unitAbilities = GameRules:GetGameModeEntity().unitAbilities
	GameRules:GetGameModeEntity().enemyConfigs = {
		{
			normal = {
				--prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_IN_SECONDS + 10,
				--battleStageMaxTimeInSeconds = 10,
				stageName = "STAGE_NOVICE_VILLAGE",
				minCount = 1,
				--maxCount = 1,
				--battleType = "combined",
				{name = "npc_dota_creep_badguys_melee", attackType=ATTACK_TYPE_MELEE, health = 100, attackDamageMin=10, attackDamageMax=10, drop = {types={TYPE_ABILITY}, quality = QUALITY_1, probability = 1}},
				--{name = "npc_dota_creep_badguys_melee", attackType=ATTACK_TYPE_MELEE, health = 500, attackDamageMin=100, attackDamageMax=100, drop = {type=TYPE_ABILITY, quality = QUALITY_5, probability = 100}},
				{name = "npc_dota_creep_badguys_ranged", attackType=ATTACK_TYPE_RANGED, health = 50, attackDamageMin=8, attackDamageMax=8, drop = {types={TYPE_ABILITY}, quality = QUALITY_1, probability = 1}},
				--{name = "npc_dota_roshan", attackType=ATTACK_TYPE_MELEE, health=10000, attackDamageMin=10, attackDamageMax=15, abilities = {{name = "phantom_assassin_coup_de_grace"}}},
				--{name = "npc_dota_roshan", attackType=ATTACK_TYPE_MELEE, health=10000, attackDamageMin=300, attackDamageMax=500, abilities = {{name = "phantom_assassin_coup_de_grace"}}},
				--{name = "npc_dota_warlock_golem1", attackType=ATTACK_TYPE_MELEE, health=10000, attackDamageMin=400, attackDamageMax=600, abilities = {{name = "phantom_assassin_coup_de_grace"}, {name = "alpha_wolf_command_aura"}}, randomAbilities = {minCount = 1, maxCount = 2, abilities = unitAbilities}},
				--{type = TYPE_HERO, quality = QUALITY_5, attackDamageMin=1, attackDamageMax=5, level = 10},
				--{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, attackDamageMin=1, attackDamageMax=5, level = 10},
				--{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_2, attackDamageMin=1, attackDamageMax=5, level = 1, profession="profession_holy_light_priest"}
			},
			--[[[2] = {
				{name = "npc_dota_creep_badguys_melee", attackType=ATTACK_TYPE_MELEE, count=1, health=100, attackDamageMin=15, attackDamageMax=15, drop = {type=TYPE_ABILITY, quality = QUALITY_3, probability = 100}},
				{name = "npc_dota_creep_badguys_ranged", attackType=ATTACK_TYPE_RANGED, count=1, health=50, attackDamageMin=10, attackDamageMax=10, drop = {type=TYPE_ABILITY, quality = QUALITY_3, probability = 100}}
			},]]--
			[5] = {
				stageName = "LITTLE_CORPS_BATTLE",
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_2, level = 2, count=1, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY}, quality = QUALITY_3, probability = 50}},
				--{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY}, quality = QUALITY_3, probability = 50}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_2, level = 2, count=1, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY}, quality = QUALITY_3, probability = 50}},
			},
			[10] = {
				stageName = "STAGE_NOVICE_VILLAGE_FOR_BOSS",
				{name = "npc_dota_creep_badguys_melee_upgraded_mega", attackType=ATTACK_TYPE_MELEE, count=2, health=300, attackDamageMin=40, attackDamageMax=60, drop = {types={TYPE_ABILITY}, quality = QUALITY_3, probability = 100}},
				{name = "npc_dota_creep_badguys_ranged_upgraded_mega", attackType=ATTACK_TYPE_RANGED, count=1, health=150, attackDamageMin=30, attackDamageMax=50, drop = {types={TYPE_ABILITY}, quality = QUALITY_3, probability =  100}}
			}
		},
		{
			normal = {
				stageName = "KOBOLD_FOREST",
				minCount = 3,
				{name = "npc_dota_neutral_kobold", attackType=ATTACK_TYPE_MELEE, health = 300, attackDamageMin=30, attackDamageMax=50, drop = {types={TYPE_ABILITY}, quality = QUALITY_2, probability = 1}},
				{name = "npc_dota_neutral_kobold_tunneler", attackType=ATTACK_TYPE_MELEE, health = 400, attackDamageMin=40, attackDamageMax=60, drop = {types={TYPE_ABILITY}, quality = QUALITY_2, probability = 1}}
			},
			[5] = {
				stageName = "LITTLE_CORPS_BATTLE",
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_3, level = 4, count=2, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY}, quality = QUALITY_4, probability = 30}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_3, level = 4, count=2, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
			},
			[10] = {
				stageName = "KOBOLD_FOREST_FOR_BOSS",
				{name = "npc_dota_neutral_kobold_tunneler", attackType=ATTACK_TYPE_MELEE, count=6, health=500, attackDamageMin=50, attackDamageMax=100, drop = {types={TYPE_ABILITY}, quality = QUALITY_4, probability = 50}},
				{name = "npc_dota_neutral_kobold_taskmaster", attackType=ATTACK_TYPE_MELEE, count=1, health=500, attackDamageMin=100, attackDamageMax=150, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 100}, abilities = {{name = "kobold_taskmaster_speed_aura"}}}
			}
		},
		{
			normal = {
				stageName = "BEAST_MOUNTAIN",
				minCount = 5,
				{name = "npc_dota_neutral_giant_wolf", attackType=ATTACK_TYPE_MELEE, health = 500, attackDamageMin=50, attackDamageMax=100, drop = {types={TYPE_ABILITY}, quality = QUALITY_3, probability = 1}, abilities = {{name = "phantom_assassin_coup_de_grace"}}},
			},
			[5] = {
				stageName = "LITTLE_CORPS_BATTLE",
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_3, level = 6, count=2, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY}, quality = QUALITY_4, probability = 30}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_3, level = 6, count=2, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
			},
			[10] = {
				stageName = "BEAST_MOUNTAIN_FOR_BOSS",
				{name = "npc_dota_neutral_giant_wolf", attackType=ATTACK_TYPE_MELEE, count=6, health=1000, attackDamageMin=80, attackDamageMax=100, drop = {types={TYPE_ABILITY}, quality = QUALITY_4, probability = 50}, abilities = {{name = "phantom_assassin_coup_de_grace"}}},
				{name = "npc_dota_neutral_alpha_wolf", attackType=ATTACK_TYPE_MELEE, count=2, health=2000, attackDamageMin=100, attackDamageMax=120, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 100}, abilities = {{name = "phantom_assassin_coup_de_grace"}, {name = "alpha_wolf_command_aura"}}}
			}
		},
		{
			normal = {
				stageName = "ARENA",
				minCount = 3,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_3, level = 6, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY, TYPE_CONSUMABLE}, quality = QUALITY_3, probability = 1}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_3, level = 6, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY, TYPE_CONSUMABLE}, quality = QUALITY_3, probability = 1}}
			},
			[5] = {
				stageName = "ARENA",
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_4, level = 6, count=3, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_CONSUMABLE}, quality = QUALITY_4, probability = 30}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_4, level = 6, count=2, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
			},
			[10] = {
				stageName = "ARENA_FOR_BOSS",
				battleType = "combined",
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_3, level = 8, count=3, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_CONSUMABLE}, quality = QUALITY_4, probability = 50}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_3, level = 8, count=3, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 50}}
			}
		},
		{
			normal = {
				stageName = "HELL",
				minCount = 2,
				maxCount = 6,
				{name = "npc_dota_warlock_golem5", attackType=ATTACK_TYPE_MELEE, health = 2000, attackDamageMin=80, attackDamageMax=120, drop = {types={TYPE_ABILITY, TYPE_CONSUMABLE}, quality = QUALITY_4, probability = 1}, abilities = {{name = "warlock_golem_flaming_fists"}}},
			},
			[5] = {
				stageName = "CORPS_BATTLE",
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_3, level = 8, count=3, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY}, quality = QUALITY_5, probability = 30}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_3, level = 8, count=3, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY}, quality = QUALITY_5, probability = 30}},
			},
			[10] = {
				stageName = "HELL_FOR_BOSS",
				battleType = "combined",
				battleStageMaxTimeInSeconds = 300,
				{name = "npc_dota_warlock_golem5", attackType=ATTACK_TYPE_MELEE, count=2, health=3000, attackDamageMin=100, attackDamageMax=150, drop = {types={TYPE_ABILITY}, quality = QUALITY_5, probability = 50}, abilities = {{name = "warlock_golem_flaming_fists"}}},
				{name = "npc_dota_warlock_golem5", attackType=ATTACK_TYPE_MELEE, count=2, health=3000, attackDamageMin=100, attackDamageMax=150, drop = {types={TYPE_ABILITY}, quality = QUALITY_5, probability = 50}, abilities = {{name = "warlock_golem_flaming_fists"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_4, level = 8, count=1, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY}, quality = QUALITY_5, probability = 50}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_4, level = 8, count=1, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY}, quality = QUALITY_5, probability = 50}},
			}
		},
		{
			normal = {
				stageName = "DRAGON_MOUNTAIN",
				minCount = 3,
				battleStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				{name = "npc_dota_neutral_black_dragon", attackType=ATTACK_TYPE_RANGED, health = 3000, attackDamageMin=100, attackDamageMax=150, drop = {types={TYPE_ABILITY, TYPE_CONSUMABLE}, quality = QUALITY_4, probability = 1}, abilities = {{name = "lina_dragon_slave"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_4, level = 8, count=2, attackDamageMin=1, attackDamageMax=5, race = "hero_dragon", drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 1}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_4, level = 8, count=2, attackDamageMin=1, attackDamageMax=5, race = "hero_dragon", drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 1}},
			},
			[5] = {
				stageName = "CORPS_BATTLE",
				battleStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_4, level = 10, count=3, attackDamageMin=1, attackDamageMax=5, race = "hero_dragon", drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_4, level = 10, count=2, attackDamageMin=1, attackDamageMax=5, race = "hero_dragon", drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
			},
			[10] = {
				stageName = "DRAGON_MOUNTAIN_FOR_BOSS",
				battleType = "combined",
				prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				battleStageMaxTimeInSeconds = 300,
				{name = "npc_dota_neutral_black_dragon", attackType=ATTACK_TYPE_RANGED, count=2, health=4000, attackDamageMin=120, attackDamageMax=150, drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}, abilities = {{name = "lina_dragon_slave"}}},
				{name = "npc_dota_neutral_black_dragon", attackType=ATTACK_TYPE_RANGED, count=2, health=5000, attackDamageMin=150, attackDamageMax=180, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}, abilities = {{name = "lina_dragon_slave"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_4, level = 10, count=2, attackDamageMin=1, attackDamageMax=5, race = "hero_dragon", drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_4, level = 10, count=2, attackDamageMin=1, attackDamageMax=5, race = "hero_dragon", drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
			}
		},
		{
			normal = {
				stageName = "CORPS_BATTLE",
				minCount = 3,
				battleStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_4, level = 10, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 1}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_4, level = 10, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 1}}
			},
			[5] = {
				stageName = "CORPS_BATTLE",
				battleStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 8, count=3, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT, TYPE_CONSUMABLE}, quality = QUALITY_5, probability = 30}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 8, count=3, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
			},
			[10] = {
				stageName = "CORPS_BATTLE_FOR_BOSS",
				battleType = "combined",
				prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				battleStageMaxTimeInSeconds = 300,
				{name = "npc_dota_neutral_black_dragon", attackType=ATTACK_TYPE_RANGED, count=3, health=5000, attackDamageMin=200, attackDamageMax=250, drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT, TYPE_CONSUMABLE}, quality = QUALITY_5, probability = 30}, abilities = {{name = "lina_dragon_slave"}}, randomAbilities = {minCount = 1, maxCount = 2, abilities = unitAbilities}},
				{name = "npc_dota_roshan", attackType=ATTACK_TYPE_MELEE, count=2, health=8000, attackDamageMin=200, attackDamageMax=300, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}, abilities = {{name = "centaur_khan_war_stomp"}}, randomAbilities = {minCount = 1, maxCount = 2, abilities = unitAbilities}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 8, count=2, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT, TYPE_CONSUMABLE}, quality = QUALITY_5, probability = 30}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 8, count=2, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
			}
		},
		{
			normal = {
				stageName = "PLANES_BATTLE",
				minCount = 3,
				battleStageMaxTimeInSeconds = 300,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 8, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 1}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 8, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 1}},
				{name = "npc_dota_neutral_black_dragon", attackType=ATTACK_TYPE_RANGED, health=8000, attackDamageMin=200, attackDamageMax=250, drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT, TYPE_CONSUMABLE}, quality = QUALITY_5, probability = 1}, abilities = {{name = "lina_dragon_slave"}}, randomAbilities = {minCount = 1, maxCount = 2, abilities = unitAbilities}},
			},
			[5] = {
				stageName = "PLANES_BATTLE",
				battleStageMaxTimeInSeconds = 300,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 8, count=2, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT, TYPE_CONSUMABLE}, quality = QUALITY_5, probability = 30}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 8, count=2, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
				{name = "npc_dota_neutral_black_dragon", attackType=ATTACK_TYPE_RANGED, count=2, health=8000, attackDamageMin=200, attackDamageMax=300, drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT, TYPE_CONSUMABLE}, quality = QUALITY_5, probability = 30}, abilities = {{name = "lina_dragon_slave"}}, randomAbilities = {minCount = 1, maxCount = 2, abilities = unitAbilities}},
				{name = "npc_dota_roshan", attackType=ATTACK_TYPE_MELEE, count=1, health=10000, attackDamageMin=200, attackDamageMax=300, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}, abilities = {{name = "centaur_khan_war_stomp"}}, randomAbilities = {minCount = 3, maxCount = 4, abilities = unitAbilities}}
			},
			[10] = {
				stageName = "PLANES_BATTLE",
				battleType = "combined",
				prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				battleStageMaxTimeInSeconds = 300,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 8, count=3, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT, TYPE_CONSUMABLE}, quality = QUALITY_5, probability = 30}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 8, count=3, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
				{name = "npc_dota_neutral_black_dragon", attackType=ATTACK_TYPE_RANGED, count=3, health=10000, attackDamageMin=250, attackDamageMax=300, drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT, TYPE_CONSUMABLE}, quality = QUALITY_5, probability = 30}, abilities = {{name = "lina_dragon_slave"}}, randomAbilities = {minCount = 2, maxCount = 3, abilities = unitAbilities}},
				{name = "npc_dota_roshan", attackType=ATTACK_TYPE_MELEE, count=2, health=15000, attackDamageMin=250, attackDamageMax=300, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}, abilities = {{name = "centaur_khan_war_stomp"}}, randomAbilities = {minCount = 2, maxCount = 3, abilities = unitAbilities}}
			}
		},
		{
			normal = {
				stageName = "ABYSS",
				battleType = "combined",
				minCount = 10,
				maxCount = 10,
				battleStageMaxTimeInSeconds = 300,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, attackDamageMin=1, attackDamageMax=5, race="hero_demon"},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, attackDamageMin=1, attackDamageMax=5, race="hero_demon"},
				{name = "npc_dota_warlock_golem6", attackType=ATTACK_TYPE_MELEE, health=15000, attackDamageMin=200, attackDamageMax=300, abilities = {{name = "warlock_golem_flaming_fists"}}, randomAbilities = {minCount = 3, maxCount = 4, abilities = unitAbilities}}
			},
			[10] = {
				stageName = "ABYSS",
				battleType = "combined",
				prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				battleStageMaxTimeInSeconds = 300,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, race="hero_demon"},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, race="hero_demon"},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, race="hero_demon"},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, race="hero_demon"},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, race="hero_demon"},	
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, race="hero_demon"},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, race="hero_demon"},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, race="hero_demon"},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, race="hero_demon"},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, race="hero_demon"},
				{name = "npc_dota_warlock_golem6", attackType=ATTACK_TYPE_MELEE, count=2, health=20000, attackDamageMin=250, attackDamageMax=300, abilities = {{name = "warlock_golem_flaming_fists"}}, randomAbilities = {minCount = 3, maxCount = 4, abilities = unitAbilities}}
			}
		},
		{
			normal = {
				stageName = "PLANES_OF_GOD",
				battleType = "combined",
				minCount = 12,
				maxCount = 12,
				prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				battleStageMaxTimeInSeconds = 300,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, attackDamageMin=1, attackDamageMax=5},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, attackDamageMin=1, attackDamageMax=5}
			},
			[9] = {
				stageName = "PLANES_OF_GOD_FOR_BOSS",
				battleType = "combined",
				prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				battleStageMaxTimeInSeconds = 300,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_berserker", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_shield_warrior", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_phantom_assassin", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_phantom_archer", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_bramble_knight", artifacts={{name="well_of_wizard_aura"}}},	
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_paladin", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_talon_druid", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_dark_mage", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_air_mage", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_holy_light_priest", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_time_mage", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_shadow_priest", artifacts={{name="well_of_wizard_aura"}}},
			},
			[10] = {
				stageName = "PLANES_OF_GOD_FOR_BOSS",
				battleType = "combined",
				prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				battleStageMaxTimeInSeconds = 300,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_spellsword", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_shield_warrior", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_phantom_assassin", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_fire_mage", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_bramble_knight", artifacts={{name="well_of_wizard_aura"}}},	
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_paladin", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_time_mage", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_thunder_mage", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_soul_mage", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_life_mage", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_mind_mage", artifacts={{name="well_of_wizard_aura"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_earth_mage", artifacts={{name="well_of_wizard_aura"}}},
			}
		}
	}

	
end

function InitStageEnemyConfigsForSoloMode( )
	unit = GameRules:GetGameModeEntity().unitAbilities
	GameRules:GetGameModeEntity().enemyConfigs = {
		{
			normal = {
				--prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_IN_SECONDS + 10,
				--battleStageMaxTimeInSeconds = 10,
				stageName = "STAGE_NOVICE_VILLAGE",
				minCount = 1,
				--maxCount = 1,
				--battleType = "combined",
				{name = "npc_dota_creep_badguys_melee", attackType=ATTACK_TYPE_MELEE, health = 100, attackDamageMin=10, attackDamageMax=10, drop = {types={TYPE_ABILITY}, quality = QUALITY_1, probability = 1}},
				--{name = "npc_dota_creep_badguys_melee", attackType=ATTACK_TYPE_MELEE, health = 500, attackDamageMin=100, attackDamageMax=100, drop = {type=TYPE_ABILITY, quality = QUALITY_5, probability = 100}},
				{name = "npc_dota_creep_badguys_ranged", attackType=ATTACK_TYPE_RANGED, health = 50, attackDamageMin=8, attackDamageMax=8, drop = {types={TYPE_ABILITY}, quality = QUALITY_1, probability = 1}},
				--{name = "npc_dota_roshan", attackType=ATTACK_TYPE_MELEE, health=10000, attackDamageMin=10, attackDamageMax=15, abilities = {{name = "phantom_assassin_coup_de_grace"}}},
				--{name = "npc_dota_roshan", attackType=ATTACK_TYPE_MELEE, health=10000, attackDamageMin=300, attackDamageMax=500, abilities = {{name = "phantom_assassin_coup_de_grace"}}},
				--{name = "npc_dota_warlock_golem1", attackType=ATTACK_TYPE_MELEE, health=10000, attackDamageMin=400, attackDamageMax=600, abilities = {{name = "phantom_assassin_coup_de_grace"}, {name = "alpha_wolf_command_aura"}}, randomAbilities = {minCount = 1, maxCount = 2, abilities = unitAbilities}},
				--{type = TYPE_HERO, quality = QUALITY_5, attackDamageMin=1, attackDamageMax=5, level = 10, abilities = {{name = "phantom_assassin_coup_de_grace"}}},
				--{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, attackDamageMin=1, attackDamageMax=5, level = 10},
				--{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_2, attackDamageMin=1, attackDamageMax=5, level = 1, profession="profession_holy_light_priest"}
			},
			[5] = {
				stageName = "LITTLE_CORPS_BATTLE",
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_2, level = 2, count=1, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY}, quality = QUALITY_3, probability = 50}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_2, level = 2, count=1, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY}, quality = QUALITY_3, probability = 50}},
			},
			[10] = {
				stageName = "STAGE_NOVICE_VILLAGE_FOR_BOSS",
				{name = "npc_dota_creep_badguys_melee_upgraded_mega", attackType=ATTACK_TYPE_MELEE, count=2, health=300, attackDamageMin=40, attackDamageMax=60, drop = {types={TYPE_ABILITY}, quality = QUALITY_3, probability = 100}},
				{name = "npc_dota_creep_badguys_ranged_upgraded_mega", attackType=ATTACK_TYPE_RANGED, count=1, health=150, attackDamageMin=30, attackDamageMax=50, drop = {types={TYPE_ABILITY}, quality = QUALITY_3, probability =  100}}
			}
		},
		{
			normal = {
				stageName = "KOBOLD_FOREST",
				minCount = 3,
				{name = "npc_dota_neutral_kobold", attackType=ATTACK_TYPE_MELEE, health = 300, attackDamageMin=30, attackDamageMax=50, drop = {types={TYPE_ABILITY}, quality = QUALITY_2, probability = 1}},
				{name = "npc_dota_neutral_kobold_tunneler", attackType=ATTACK_TYPE_MELEE, health = 400, attackDamageMin=40, attackDamageMax=60, drop = {types={TYPE_ABILITY}, quality = QUALITY_2, probability = 1}}
			},
			[5] = {
				stageName = "LITTLE_CORPS_BATTLE",
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_3, level = 5, count=1, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY}, quality = QUALITY_4, probability = 30}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_3, level = 5, count=1, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
			},
			[10] = {
				stageName = "KOBOLD_FOREST_FOR_BOSS",
				{name = "npc_dota_neutral_kobold_tunneler", attackType=ATTACK_TYPE_MELEE, count=5, health=500, attackDamageMin=50, attackDamageMax=100, drop = {types={TYPE_ABILITY}, quality = QUALITY_5, probability = 50}},
				{name = "npc_dota_neutral_kobold_taskmaster", attackType=ATTACK_TYPE_MELEE, count=1, health=500, attackDamageMin=100, attackDamageMax=150, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 100}, abilities = {{name = "kobold_taskmaster_speed_aura"}}}
			}
		},
		{
			normal = {
				stageName = "BEAST_MOUNTAIN",
				minCount = 4,
				{name = "npc_dota_neutral_giant_wolf", attackType=ATTACK_TYPE_MELEE, health = 500, attackDamageMin=50, attackDamageMax=100, drop = {types={TYPE_ABILITY}, quality = QUALITY_3, probability = 1}, abilities = {{name = "phantom_assassin_coup_de_grace"}}},
			},
			[5] = {
				stageName = "LITTLE_CORPS_BATTLE",
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_3, level = 6, count=1, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY}, quality = QUALITY_4, probability = 30}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_3, level = 6, count=1, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
			},
			[10] = {
				stageName = "BEAST_MOUNTAIN_FOR_BOSS",
				{name = "npc_dota_neutral_giant_wolf", attackType=ATTACK_TYPE_MELEE, count=4, health=1000, attackDamageMin=80, attackDamageMax=100, drop = {types={TYPE_ABILITY}, quality = QUALITY_5, probability = 50}, abilities = {{name = "phantom_assassin_coup_de_grace"}}},
				{name = "npc_dota_neutral_alpha_wolf", attackType=ATTACK_TYPE_MELEE, count=2, health=2000, attackDamageMin=100, attackDamageMax=120, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 100}, abilities = {{name = "phantom_assassin_coup_de_grace"}, {name = "alpha_wolf_command_aura"}}}
			}
		},
		{
			normal = {
				stageName = "CORPS_BATTLE",
				minCount = 2,
				maxCount = 2,
				battleStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_4, level = 8, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY, TYPE_CONSUMABLE}, quality = QUALITY_3, probability = 1}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_4, level = 8, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY, TYPE_CONSUMABLE}, quality = QUALITY_3, probability = 1}}
			},
			[5] = {
				stageName = "CORPS_BATTLE",
				battleStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 8, count=1, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_CONSUMABLE}, quality = QUALITY_4, probability = 30}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 8, count=1, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
			},
			[10] = {
				stageName = "CORPS_BATTLE_FOR_BOSS",
				battleStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 8, count=1, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_CONSUMABLE}, quality = QUALITY_4, probability = 50}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 8, count=1, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 50}}
			}
		},
		{
			normal = {
				stageName = "DRAGON_MOUNTAIN",
				minCount = 3,
				maxCount = 3,
				battleStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				{name = "npc_dota_neutral_black_dragon", attackType=ATTACK_TYPE_RANGED, health = 3000, attackDamageMin=100, attackDamageMax=150, drop = {types={TYPE_ABILITY, TYPE_CONSUMABLE}, quality = QUALITY_5, probability = 1}, abilities = {{name = "lina_dragon_slave"}}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, race = "hero_dragon", drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 1}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, race = "hero_dragon", drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 1}},
			},
			[5] = {
				stageName = "CORPS_BATTLE",
				battleStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=2, attackDamageMin=1, attackDamageMax=5, race = "hero_dragon", drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, race = "hero_dragon", drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
			},
			[10] = {
				stageName = "DRAGON_MOUNTAIN_FOR_BOSS",
				prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				battleStageMaxTimeInSeconds = 300,
				{name = "npc_dota_neutral_black_dragon", attackType=ATTACK_TYPE_RANGED, count=1, health=4000, attackDamageMin=120, attackDamageMax=150, drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}, abilities = {{name = "lina_dragon_slave"}}},
				{name = "npc_dota_roshan", attackType=ATTACK_TYPE_MELEE, count=1, health=5000, attackDamageMin=150, attackDamageMax=180, drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}, abilities = {{name = "warlock_golem_flaming_fists"}}, randomAbilities = {minCount = 3, maxCount = 4, abilities = unitAbilities}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, race = "hero_dragon", drop = {types={TYPE_ABILITY, TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, race = "hero_dragon", drop = {types={TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 30}},
			}
		},
		{
			normal = {
				stageName = "PLANES_OF_GOD",
				minCount = 1,
				maxCount = 1,
				prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				battleStageMaxTimeInSeconds = 300,
				{type = TYPE_HERO, quality = QUALITY_5, level = 10, attackDamageMin=1, attackDamageMax=5, artifacts={{name="well_of_wizard_aura"}}, randomArtifacts={minCount = 1, maxCount = 1, artifacts = GameRules:GetGameModeEntity().artifacts}},
			},
			[6] = {
				stageName = "PLANES_OF_GOD_FOR_BOSS",
				prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				battleStageMaxTimeInSeconds = 300,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_blade_master", artifacts={{name="elixir_of_life_aura"}, {name="well_of_wizard_aura"}, {name="cloak_of_the_undead_king_aura"}}},
			},
			[7] = {
				stageName = "PLANES_OF_GOD_FOR_BOSS",
				prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				battleStageMaxTimeInSeconds = 300,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_necromancer", artifacts={{name="elixir_of_life_aura"}, {name="well_of_wizard_aura"}, {name="cloak_of_the_undead_king_aura"}}},
			},
			[8] = {
				stageName = "PLANES_OF_GOD_FOR_BOSS",
				prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				battleStageMaxTimeInSeconds = 300,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_guns_division", artifacts={{name="elixir_of_life_aura"}, {name="well_of_wizard_aura"}, {name="ring_of_the_magi_aura"}}, abilities = {{name = "phantom_assassin_coup_de_grace"}}},
			},
			[9] = {
				stageName = "PLANES_OF_GOD_FOR_BOSS",
				prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				battleStageMaxTimeInSeconds = 300,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_thunder_mage", artifacts={{name="elixir_of_life_aura"}, {name="well_of_wizard_aura"}, {name="ring_of_the_magi_aura"}}},
			},
			[10] = {
				stageName = "PLANES_OF_GOD_FOR_BOSS",
				prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_BOSS_IN_SECONDS,
				battleStageMaxTimeInSeconds = 300,
				{type = TYPE_HERO, attackType=ATTACK_TYPE_RANGED, quality = QUALITY_5, level = 10, count=1, attackDamageMin=1, attackDamageMax=5, profession="profession_time_mage", artifacts={{name="elixir_of_life_aura"}, {name="well_of_wizard_aura"}, {name="ring_of_the_magi_aura"}}},
			},
		}
	}
end

function IsCorpsMode( )
	local gameMode = GameRules:GetGameModeEntity().gameMode
	if gameMode == "CorpsMode" or gameMode == "CorpsModeForEntertainment" then
		return true
	else
		return false
	end
end

function IsSoloMode( )
	local gameMode = GameRules:GetGameModeEntity().gameMode
	if gameMode == "SoloMode" or gameMode == "SoloModeForEntertainment" then
		return true
	else
		return false
	end
end

function InitStageEnemyConfigs( )
	if IsCorpsMode() then
		InitStageEnemyConfigsForCorpsMode()
	else
		InitStageEnemyConfigsForSoloMode()
	end

	GameRules:GetGameModeEntity().enemiesConfig = {}
	for stage = 1, (#GameRules:GetGameModeEntity().enemyConfigs * 10) do
		local index = math.floor((stage - 1) / 10) + 1
		local enemyConfigs = GameRules:GetGameModeEntity().enemyConfigs[index]
		local enemyConfig = enemyConfigs[(stage - 1) % 10 + 1]
		if enemyConfig == nil then
			enemyConfig = DeepCopy(enemyConfigs.normal)
		end
		
		local enemiesConfig = DeepCopy(enemyConfig)
		for i = 1, #enemiesConfig do
			enemiesConfig[i] = nil
		end
		if enemyConfig.minCount ~= nil then
			local enemyCount = enemyConfig.minCount
			if IsCorpsMode() then
				enemyCount = enemyCount + math.floor((stage % 10) / 2)
			else
				enemyCount = enemyCount + math.floor((stage % 10) / 4)
			end
			if enemyConfig.maxCount ~= nil then
				enemyCount = math.min(enemyCount, enemyConfig.maxCount)
			end
			for i = 1, enemyCount do
				local currentEnemyConfig = enemyConfig[(i - 1) % #enemyConfig + 1]
				table.insert(enemiesConfig, currentEnemyConfig)
			end
		else
			for i = 1, #enemyConfig do
				for j = 1, enemyConfig[i].count do
					local currentEnemyConfig = enemyConfig[i]
					table.insert(enemiesConfig, currentEnemyConfig)
				end
			end
		end
		GameRules:GetGameModeEntity().enemiesConfig[stage] = enemiesConfig
		--Msg("enemiesConfig=" .. TableToStr(enemiesConfig) .. "\n")
	end
end


function CAddonTemplateGameMode:OnGameRulesStateChange(keys)
        print("OnGameRulesStateChange")
        DeepPrintTable(keys)    --详细打印传递进来的表

        Msg("time=" .. GameRules:GetGameTime() .. "\n")
        --获取游戏进度
        local newState = GameRules:State_Get()

        if newState == DOTA_GAMERULES_STATE_HERO_SELECTION then
            Msg("Player begin select hero\n")  --玩家处于选择英雄界面

        elseif newState == DOTA_GAMERULES_STATE_PRE_GAME then
            Msg("Player ready game begin\n")  --玩家处于游戏准备状态
            CustomUI:DynamicHud_Create(-1, "AdventureJournalsPanel", "file://{resources}/layout/custom_game/adventure_journals.xml", nil)
            CustomUI:DynamicHud_Create(-1, "StatusPanel", "file://{resources}/layout/custom_game/status.xml",nil)
            CustomUI:DynamicHud_Create(-1, "ChessSetInfo", "file://{resources}/layout/custom_game/chess_set_info.xml", nil)
            CustomUI:DynamicHud_Create(-1, "StagePanel", "file://{resources}/layout/custom_game/stage.xml", nil)
            CustomUI:DynamicHud_Create(-1, "SellerPanel", "file://{resources}/layout/custom_game/sell.xml", nil)
            CustomUI:DynamicHud_Create(-1, "AutoChessShopPanel", "file://{resources}/layout/custom_game/shop.xml", nil)
            CustomUI:DynamicHud_Create(-1, "HintTextAreaPanel", "file://{resources}/layout/custom_game/hint_text_area.xml", nil)
            CustomUI:DynamicHud_Create(-1, "QuestsPanel", "file://{resources}/layout/custom_game/quests.xml", nil)
            CustomUI:DynamicHud_Create(-1, "CharacterSelectPanel", "file://{resources}/layout/custom_game/character_select.xml",nil)
            InitStageEnemyConfigs()
            InitAuctions()

        elseif newState == DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then
            Msg("Player game begin\n")  --玩家开始游戏

            GameRules:GetGameModeEntity().time = 0
           	GameRules:GetGameModeEntity().stage = STAGE_PREPARE_STAGE
           	GameRules:GetGameModeEntity().toPrepared = true
            GameRules:GetGameModeEntity():SetContextThink(DoUniqueString("StageThink"), StageThink, 0)

            InitCouriers()
        end
end

function CAddonTemplateGameMode:OnPlayerGainedLevel( keys )
	-- Msg("OnPlayerGainedLevel, player=" .. TableToStr(keys) .. "\n")
	local player = PlayerResource:GetPlayer(keys.PlayerID)

	local hero = EntIndexToHScript(keys.hero_entindex)
	hero:SetAbilityPoints(0)
	local courier = player:GetAssignedHero()
	if hero == courier then
		local ability = courier:FindAbilityByName("gain_exp_for_courier")
		ability:SetLevel(courier:GetLevel())
		ability = courier:FindAbilityByName("regenerate_shop_items")
		ability:SetLevel(courier:GetLevel())
		CustomGameEventManager:Send_ServerToPlayer(player, "on_hero_count_in_chessboard_update", 
			{heroCountInChessboard = player.heroCountInChessboard, maxHeroCountInChessboard = GetMaxHeroCountInChessboard(player:GetAssignedHero())})
		
		if GameRules:GetGameModeEntity().ignoreGainedLevel == false then
			SendAdventureJournals(player, "ADVENTURE_JOURNALS_LEVEL_UP_FOR_COURIER",
			{
				name = {type = "value", value = PlayerResource:GetPlayerName(keys.PlayerID)},
				level = {type = "value", value = courier:GetLevel()}
			})
		end
	else
		if GameRules:GetGameModeEntity().ignoreGainedLevel == false then
			SendAdventureJournals(player, "ADVENTURE_JOURNALS_LEVEL_UP_FOR_UNIT",
			{
				heroName = {type = "name", value = GetUnitName(hero)},
				level = {type = "value", value = hero:GetLevel()}
			})
		end
	end
end

function Exist( array, value )
	for i = 1, #array do
		if array[i] == value then
			return true
		end
	end
	return false
end

function InsertUnit(player, unit)
	local team = unit:GetTeam()
	if team == DOTA_TEAM_BADGUYS then
		table.insert(player.enemies, unit)
		player.enemiesCount = player.enemiesCount + 1
	else
		player.otherUnits = player.otherUnits or {}
		table.insert(player.otherUnits, unit)
	end
end

function CAddonTemplateGameMode:OnNpcSpawned( keys )
	--Msg("OnNpcSpawned, keys=" .. TableToStr(keys) .. "\n")
	if GameRules:GetGameModeEntity().createUnit then
		return
	end

	local unit = EntIndexToHScript(keys.entindex)
	--Msg("OnNpcSpawned, name=" .. unit:GetName() .. "\n")
	local filterUnits = {
		"npc_dota_base_additive",
		"npc_dota_thinker",
	}
	if IsIn(filterUnits, unit:GetName()) then
		return
	end

	local player = GetPlayerOwner(unit)

	if player == nil then
		return
	end

	--Msg("player.name=" .. PlayerResource:GetPlayerName(player:GetPlayerID()) .. ", unit=" .. unit:GetName() .. "\n")

	if unit:GetName() == "npc_dota_hero_wisp" then
		return
	end

	local heros = GetAllHeroChessInChessBoard(player)
	local existUnits = heros

	if player.enemies ~= nil then
		for i = 1, #player.enemies  do
			table.insert(existUnits, player.enemies[i])
		end
	end


	if Exist(existUnits, unit) then
		return
	end

	--Msg(unit:GetName() .. " is new, create unit=" ..  (GameRules:GetGameModeEntity().createUnit and "true" or "false") .. " ,playerId=" .. player:GetPlayerID() 
	--	.. ", team=" .. unit:GetTeam() .. " \n")
	unit:SetControllableByPlayer(-1, false)
	unit:SetDeathXP(0)
	unit:SetMaximumGoldBounty(0)
	unit:SetMinimumGoldBounty(0)
	unit:SetHullRadius(24)

	InsertUnit(player, unit)

	--Msg("player.name2=" .. PlayerResource:GetPlayerName(player:GetPlayerID()) .. ", unit=" .. unit:GetName() .. "\n")
end

function CAddonTemplateGameMode:OnEntityKilled( keys )
	local killedUnit = EntIndexToHScript( keys.entindex_killed )
    if killedUnit.enemyConfig ~= nil and killedUnit.enemyConfig.drop ~= nil then
    	local probability = killedUnit.enemyConfig.drop.probability
    	local random = RandomInt(1, 100)
    	if random > probability then
    		return
    	end

    	local shopItems = GameRules:GetGameModeEntity().shopItems
		local tmpShopItems = {}
		local tmpShopItemCount = 0
		for i = 1, #shopItems do
			if (IsIn(killedUnit.enemyConfig.drop.types, shopItems[i].type)) then
				tmpShopItems[tmpShopItemCount + 1] = shopItems[i]
				tmpShopItemCount = tmpShopItemCount + 1
				--Msg("shopItems[i]=" .. shopItems[i].name .. "\n")
			end
		end
		shopItems = tmpShopItems

		local quality = killedUnit.enemyConfig.drop.quality
		local leftCountTable = GetShopItemsLeftCountTable(shopItems)
		--Msg("leftCountTable=" .. TableToStr(leftCountTable) .. ", quality=" .. quality .. "\n")

		if leftCountTable[quality] > 0 then
			local random = RandomInt(1, leftCountTable[quality])
			local shopItem = GetShopItem(shopItems, random, quality)
			shopItem.existCount = shopItem.existCount + 1
			-- Msg("Random quality=" .. quality .. ", random=" .. random .. ", ability=" .. shopItem.abilityName .. "\n")
			if IsCombinedBattle() then
				local players = GetAllAlivePlayers()
				local random = RandomInt(1, #players)
				local player = players[random]
				local newItem = CreateShopItem(player, shopItem, player:GetAssignedHero():GetAbsOrigin())

				SendHintMessageToPlayer(player, 
					"KILL_ENEMY_GET_ITEM_FOR_COMBINED_BATTLE", 
					{
						name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
						enemyName = {type = "name", value = GetUnitName(killedUnit)},
						itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. shopItem.name},
					},
					true,
					"item_drop_gem_shop"
				)
				SendAdventureJournals(player, "ADVENTURE_JOURNALS_KILL_ENEMY_GET_ITEM_FOR_COMBINED_BATTLE",
					{
						name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
						enemyName = {type = "name", value = GetUnitName(killedUnit)},
						itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. shopItem.name},
					})
			else
				local player = killedUnit.player
				local newItem = CreateShopItem(player, shopItem, killedUnit:GetAbsOrigin())
				SendHintMessageToPlayer(player, 
					"KILL_ENEMY_GET_ITEM", 
					{
						name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
						enemyName = {type = "name", value = GetUnitName(killedUnit)},
						itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. shopItem.name},
					},
					true,
					"item_drop_gem_shop"
				)
				SendAdventureJournals(player, "ADVENTURE_JOURNALS_KILL_ENEMY_GET_ITEM",
					{
						name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
						enemyName = {type = "name", value = GetUnitName(killedUnit)},
						itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. shopItem.name},
					})
			end

		else
			-- 所有的都抽完了
			return
		end
    end
end

function RemoveWearables(hero)
	local model_name = hero:GetModelName()
	local wearables = {}
	--获取英雄的第一个子模型
	local model = hero:FirstMoveChild()
	--循环所有模型
	if model then print(model:GetModelName()) end
	while model ~= nil do
		--确保获取到的是dota_item_wearable的模型
		if model ~= nil and model:GetClassname() ~= "" and model:GetClassname() == "dota_item_wearable" then
			table.insert(wearables, model)
		end
		--获取下一个模型
		model = model:NextMovePeer()
	end
	--循环所有的模型，并移除
	for i = 1, #wearables do
		print("removed 1 wearable")
		wearables[i]:RemoveSelf()
	end
	if #wearables>=1 then
		print("MODEL REMOVED, RESPAWNING HERO")
		--hero:SetRespawnPosition(hero:GetOrigin())
		hero:RespawnHero(false,false)
	end
end

function InitCouriers( )
	local players = GetAllPlayers()
    for i = 1, #players do
    	--Msg("player = " .. i .. "\n")
    	players[i].lose = false
    	players[i].heroCountInChessboard = 0
    	local courier = players[i]:GetAssignedHero()
    	for i = 1,6 do
    		RemoveAbilityAndModifier(courier, "dummy" .. i)
    	end
    	
    	AddAbility(courier, "put_chess")
    	AddAbility(courier, "take_back_chess")
    	AddAbility(courier, "sell_chess")
    	AddAbility(courier, "regenerate_shop_items")
    	AddAbility(courier, "gain_exp_for_courier")
    	AddAbility(courier, "gain_exp_for_unit")
    	
    	if IsCorpsMode() then
    		AddAbility(courier, "disable_random_team")
			AddAbility(courier, "recruit_teammate")
    		AddAbility(courier, "hire_hero")
		end
    	
    	AddAbility(courier, "identify")
    	AddAbility(courier, "tower_true_sight_hidden")
    	AddAbility(courier, "no_collision")
    	AddAbility(courier, "invulnerable")
    	RemoveWearables(courier)
    	
		if IsCorpsMode() then
			courier:SetMana(10)	
		else
			courier:SetMana(10)
		end
    	
    	--courier:SetHealth(50)
    	--courier:SetMana(50)
    	--courier:SetMana(0)
    	courier.winningStreak = 0
    	courier.losingStreak = 0
    	courier:SetGold(0, true)
    	courier:SetAbilityPoints(0)
    	courier:SetHullRadius(0)

    	--[[for i = 1, 9 do
    		GameRules:GetGameModeEntity().ignoreGainedLevel = true
    		courier:HeroLevelUp(false)
    		GameRules:GetGameModeEntity().ignoreGainedLevel = false
    	end]]--

    	SendUpdateStatusMessage(players[i])

    	InitShopConfigs(players[i])
    	players[i].enemyPlayer = GameRules:GetGameModeEntity().dummyPlayers[i]
    	players[i].endBattle = false
    end
end

function AddAbility( unit, abilityName, level, count )
	if level == nil then
		level = 1
	end

	if count == nil then
		count = false
	end

	local ability = unit:FindAbilityByName(abilityName)
	if ability == nil then
		unit:AddAbility(abilityName):SetLevel(level)
		if count == true then
			unit.abilitiesCount = unit.abilitiesCount + 1
			table.insert(unit.abilities, abilityName)
		end
	else
		--RemoveAbilityAndModifier(unit, abilityName)
		ability:SetLevel(level)
	end
end

function IsIn( array, value )
	for i = 1, #array do
		if array[i] == value then
			return true
		end
	end
	return false
end

function GetAllPlayers() 
	local allCouriers = HeroList:GetAllHeroes()
	-- Msg("allCouriers=" .. TableToStr(allCouriers) .. "\n")
	local playerIds = {}
	local index = 1
	for i = 1, #allCouriers do
		if IsIn(playerIds, allCouriers[i]:GetPlayerID()) == false then
			playerIds[index] = allCouriers[i]:GetPlayerID()
			index = index + 1
		end
	end

	local players = {}
	for i = 1, #playerIds do
		local player = PlayerResource:GetPlayer(playerIds[i])
		if IsIn(GameRules:GetGameModeEntity().dummyPlayers, player) == false  then
			table.insert(players, player)
		end
	end

	return players
end

function GetAllAlivePlayers()
	local players  = GetAllPlayers()
	local alivePlayers = {}
	for i = 1, #players do
		local courier = players[i]:GetAssignedHero()
		if courier:IsAlive() then
			table.insert(alivePlayers, players[i])
		end
	end
	return alivePlayers
end

function GetAllAliveCouriers( )
	local players = GetAllAlivePlayers()
	local couriers = {}
	for i = 1, #players do
		table.insert(couriers, players[i])
	end
	return couriers
end

function GetPlayerByTeam(teamId)
	local players = GetAllAlivePlayers()
	for i = 1, #players do
		local player = players[i]
		if PlayerResource:GetTeam(player:GetPlayerID()) == teamId then
			return player
		end
	end

	return nil
end

function OnToServer( index, keys)
	Msg("To Server, num=" .. keys.num .. " str=" .. keys.str .. "\n");
	CustomGameEventManager:Send_ServerToPlayer(PlayerResource:GetPlayer(keys.PlayerID),"test_to_client", {str="Lua"})
	CustomUI:DynamicHud_Destroy(keys.PlayerID, "CharacterSelect")
end

function FindBaseGridMarker( playerId )
	local baseGridMarker = nil
	if playerId == -1 then
		baseGridMarker = Entities:FindByName(nil, "combined_grid_marker")
	else
		--Msg("gridMarker=" .. "team" .. (PlayerResource:GetTeam(playerId) - DOTA_TEAM_CUSTOM_1 + 1) .. "_grid_marker" .. "\n")
		baseGridMarker = Entities:FindByName(nil, "team" .. (PlayerResource:GetTeam(playerId) - DOTA_TEAM_CUSTOM_1 + 1) .. "_grid_marker")
	end
	return baseGridMarker
end

function GetAllRaces()
	local herosConfigs = GameRules:GetGameModeEntity().heros
	local races = {}
	for raceId, value in pairs(herosConfigs) do
		table.insert(races, raceId)
	end

	return races
end

RANDOM = "random"
function GetModelInfo(player, race, attackType, modelName )
	if race == RANDOM then
		local races = GetAllRaces()
		race = races[RandomInt(1, #races)]
	end

	if attackType == RANDOM then
		local attackTypes = {ATTACK_TYPE_MELEE, ATTACK_TYPE_RANGED}
		attackType = attackTypes[RandomInt(1, #attackTypes)]
	end

	local herosConfigs = GameRules:GetGameModeEntity().heros
	local heroConfig = herosConfigs[race]
	local baseInfos = {}
	for i = 1, #heroConfig.base do
		local baseInfo = heroConfig.base[i]
		if baseInfo.attackType == attackType then
			table.insert(baseInfos, baseInfo)
		end
	end

	if #baseInfos <= 0 then
		SendErrorMessageToPlayer(player, "THERE_IS_THAT_MODEL_NAME_RANDOM_SELECT")
		Msg("Can't Find model, use default model, race=" .. race .. ", attackType=" .. attackType .. "\n")
		return {name = "npc_dota_hero_omniknight", race = race, 
			baseInfo = GameRules:GetGameModeEntity().heros.hero_human.base[1]}
	else
		local baseInfo
		if modelName ~= nil then
			for i = 1, #baseInfos do
				if baseInfos[i].name == modelName then
					baseInfo = baseInfos[i]
				end
			end

			if baseInfo == nil then
				SendErrorMessageToPlayer(player, "THERE_IS_THAT_MODEL_NAME_RANDOM_SELECT")
			end
		end

		if baseInfo == nil then
			baseInfo = baseInfos[RandomInt(1, #baseInfos)]
		end
		return {name = baseInfo.name, race = race, baseInfo = baseInfo}
	end
end

function GetAttackTypeByInt( attackTypeInt )
	local attackType = ATTACK_TYPE_MELEE
	if attackTypeInt == DOTA_UNIT_CAP_MELEE_ATTACK then
		attackType = ATTACK_TYPE_MELEE
	else
		attackType = ATTACK_TYPE_RANGED
	end

	return attackType
end

function GetModelNames( race, attackType )
	local herosConfigs = GameRules:GetGameModeEntity().heros
	local heroConfig = herosConfigs[race]
	local modelNames = {}
	for i = 1, #heroConfig.base do
		local baseInfo = heroConfig.base[i]
		if baseInfo.attackType == attackType then
			table.insert(modelNames, baseInfo.name)
		end
	end
	return modelNames
end

function OnGetModelNames( index, keys )
	local player = GetAuctionPlayer(keys.PlayerID)
	if keys.race == RANDOM or keys.attackType == RANDOM then
		SendErrorMessageToPlayer(player, "CANNOT_GET_MODEL_NAMES_WHEN_RANDOM")
		return
	end
	
	local modelNames = GetModelNames(keys.race, keys.attackType)
	CustomGameEventManager:Send_ServerToPlayer(player, "on_receive_model_names", 
				{modelNames = modelNames})
end

function OnSelectCharacter( index, keys )
	--Msg("OnSelectCharacter, playerId=" .. keys.PlayerID .. ", teamId=" .. PlayerResource:GetTeam(keys.PlayerID) .. "\n")
	local team1_grid_marker = FindBaseGridMarker(keys.PlayerID)

	local player = PlayerResource:GetPlayer(keys.PlayerID)

	if player:GetAssignedHero() == nil or player:GetAssignedHero():IsAlive() == false then
		CustomUI:DynamicHud_Destroy(keys.PlayerID, "CharacterSelectPanel")
		return
	end

	local playerIndex = GameRules:GetGameModeEntity().playersCount + 1
	GameRules:GetGameModeEntity().players[playerIndex] = player
	GameRules:GetGameModeEntity().playersCount = playerIndex

	--Msg(TableToStr(keys) .. "\n")

	if player.units ~= nil then
		Msg("Error, units already exists");
		CustomUI:DynamicHud_Destroy(keys.PlayerID, "CharacterSelectPanel")
		return
	end

	player.units = {}
	for i=1, 8 do
		player.units[i] = {}

		for j=1, 10 do
			player.units[i][j] = nil
		end
	end

	player.randomTeam = true

	local modelInfo = GetModelInfo(player, keys.race, keys.attackType, keys.modelName)

	GameRules:GetGameModeEntity().createUnit = true
	local hero = CreateHeroForPlayer(modelInfo.name, player)
	GameRules:GetGameModeEntity().createUnit = false
	InitName(player, true, hero, modelInfo.baseInfo)
	hero:SetAbsOrigin(team1_grid_marker:GetOrigin())
	hero:SetControllableByPlayer(keys.PlayerID, false)
	hero:SetMoveCapability(DOTA_UNIT_CAP_MOVE_NONE)
	hero.attackType = hero:GetAttackCapability() -- 这里必须记录attackType，因为战斗时准备区和战斗区的棋子阵营不一样，所以要把棋子设置成无法攻击
	hero:SetAttackCapability(DOTA_UNIT_CAP_NO_ATTACK)
	--hero:SetBaseDamageMax(500)
	hero.abilities = {}
	hero.abilitiesCount = 0
	hero.canSell = false
	AddAbility(hero, "main_character")
	
	-- Msg("hull size=" .. hero: GetHullRadius() .. "\n")
	AddAbility(hero, "invulnerable")
	AddAbility(hero, "silenced")
	--AddAbility(hero, "race_human", 1, true)

	local herosConfigs = GameRules:GetGameModeEntity().heros
	heroConfigs = herosConfigs[modelInfo.race]
	for i = 1, #heroConfigs.abilities do
		local ability = heroConfigs.abilities[i]
		local shopItem = FindAbilityShopItem(GameRules:GetGameModeEntity().shopItems, ability)
		AddAbility(hero, ability, GetQualityLevel(shopItem.quality), true)
	end

	InitUnit(hero)
	--AddAbility(hero, "armor_of_the_damned_aura")
	--AddAbility(hero, "bow_of_the_sharpshooter_aura")
	--AddAbility(hero, "angelic_alliance_aura")
	CustomUI:DynamicHud_Destroy(keys.PlayerID, "CharacterSelectPanel")

	-- for test
	--hero:SetBaseDamageMin(100)
	--hero:SetBaseDamageMax(100)
	--AddAbility(hero, "race_human", 1, true)
	--AddAbility(hero, "race_elf", 1, true)
	--AddAbility(hero, "race_orc", 1, true)
	--AddAbility(hero, "race_undead", 1, true)
	--AddAbility(hero, "race_angel", 1, true)
	--AddAbility(hero, "race_demon", 1, true)
	--AddAbility(hero, "race_dragon", 5, true)

	--AddAbility(hero, "profession_warrior", 1, true)
	--AddAbility(hero, "profession_berserker", 1, true)
	--AddAbility(hero, "profession_shield_warrior", 1, true)
	--AddAbility(hero, "profession_assassin", 5, true)
	--AddAbility(hero, "profession_phantom_assassin", 1, true)

	for i = 1, #GameRules:GetGameModeEntity().shopItems do
		local shopItem = GameRules:GetGameModeEntity().shopItems[i]
		if IsCorpsMode() and shopItem.name == "item_water_of_forgetting" then
			for j = 1, 3 do
				CreateShopItem(player, shopItem, player:GetAssignedHero():GetAbsOrigin())
			end
		end

		if GameRules:GetGameModeEntity().gameMode == "CorpsModeForEntertainment" then
			if shopItem.name == "item_wishing_water_for_ability"
				or shopItem.name == "item_wishing_water" then
				for j = 1, 3 do
					CreateShopItem(player, shopItem, player:GetAssignedHero():GetAbsOrigin())
				end
			end
		end

		if GameRules:GetGameModeEntity().gameMode == "SoloModeForEntertainment" then
			if shopItem.name == "item_wishing_water_for_ability"
				or shopItem.name == "item_wishing_water" then
				for j = 1, 3 do
					CreateShopItem(player, shopItem, player:GetAssignedHero():GetAbsOrigin())
				end
			end
		end
			
		if --shopItem.name == "item_sharpshooter_arrows"
			--or shopItem.name == "item_sharpshooter_bow"
			--or shopItem.name == "item_sharpshooter_bowstring"
			--or shopItem.name == "item_wishing_water"
			--shopItem.name == "item_random_profession_ability"
			--or shopItem.name == "item_wishing_water"
			--or shopItem.name == "item_water_of_forgetting"
			--or shopItem.name == "item_wishing_water_for_ability"
			--or shopItem.name == "item_random_profession_ability"
			--or shopItem.name == "item_sharpshooter_bowstring" 
			--or shopItem.name == "item_gold2" 
			--or shopItem.name == "item_gold1" 
			--or shopItem.name == "item_gold3" 
			--or shopItem.name == "item_health" 
			--or shopItem.name == "item_cornucopia_1"
			--or shopItem.name == "item_cornucopia_2"
			--or shopItem.name == "item_cornucopia_3"
			--or shopItem.name == "item_cornucopia_4"
			shopItem.name == "item_dragon_lance_override"
			or shopItem.name == "item_dragon_shoes"
			or shopItem.name == "item_dragon_armor"
			or shopItem.name == "item_dragon_helmet"
			or shopItem.name == "item_dragon_shield"
			or shopItem.name == "item_dragon_scale_override"
			or shopItem.name == "item_ring_of_regen_override"
			or shopItem.name == "item_ring_of_health_override"
			or shopItem.name == "item_heart_override"
			then
			for j = 1, 1 do
				--CreateShopItemForHero(player, hero, shopItem, player:GetAssignedHero():GetAbsOrigin())
				--shopItem.existCount = shopItem.existCount + 1
			end
		end
	end

	--[[for i = 1, 9 do
    	GameRules:GetGameModeEntity().ignoreGainedLevel = true
    	hero:HeroLevelUp(false)
    	GameRules:GetGameModeEntity().ignoreGainedLevel = false
    
	--test final
	hero:SetBaseAgility(150)
	hero:SetBaseIntellect(150)
	hero:SetBaseStrength(150)end]]--
	
	--hero:SetBaseDamageMax(1000)

	--AddAbility(hero, "axe_berserkers_call", 5, false)

	PutChess(player, {x = 1, y = 1}, hero, nil)

	Msg("playerId=" .. keys.PlayerID .. " current=" .. hero:GetPlayerID() .. "\n")

	EmitSoundOnClient("HeroPicker.Selected", player)
	SendShopUpdateMessage(player)
	SendQuestsUpdateMessage(player)

	local attackType = GetAttackTypeByInt(hero.attackType)
	SendHintMessageToPlayer(player, 
		"VERSION_INFO", 
		{
			addonname = {type="text"}, 
			version={type = "value", value = 9}
		},
		false
	)

	SendAdventureJournalsForHeroInfo(player, 
		"ADVENTURE_JOURNALS_SELECT_CHARACTER",
		{
			playerName = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
			modelName = {type = "text", value = hero:GetName()},
			race = {type = "text", value = modelInfo.race},
			attackType = {type = "text", value = attackType}
		},
		hero)

end

function GetPlayerLogs( player )
	if player.logs == nil then
		player.logs = {}
	end
	return player.logs
end


function ToStringEx(value)
    if type(value)=='table' then
       return TableToStr(value)
    elseif type(value)=='string' then
        return "\'"..value.."\'"
    else
       return tostring(value)
    end
end

function TableToStr(t)
    if t == nil then return "" end
    local retstr= "{"

    local i = 1
    for key,value in pairs(t) do
        local signal = ","
        if i==1 then
          signal = ""
        end

        if key == i then
            retstr = retstr..signal..ToStringEx(value)
        else
            if type(key)=='number' or type(key) == 'string' then
                retstr = retstr..signal..'['..ToStringEx(key).."]="..ToStringEx(value)
            else
                if type(key)=='userdata' then
                    retstr = retstr..signal.."*s"..TableToStr(getmetatable(key)).."*e".."="..ToStringEx(value)
                else
                    retstr = retstr..signal..key.."="..ToStringEx(value)
                end
            end
        end

        i = i+1
    end

     retstr = retstr.."}"
     return retstr
end

function StrToTable(str)
    if str == nil or type(str) ~= "string" then
        return
    end
    
    return loadstring("return " .. str)()
end

function FindIndexPickedChess(player, pickedChess)
	for i=1, 8 do
		for j=1, 10 do
			if player.units[i][j] == pickedChess then
				return {x = i, y = j}
			end
		end
	end

	return nil
end

function FindSpareIndexForWaitArea(player)
	for i = 1, 8 do
		if player.units[i][1] == nil then
			return {x = i, y = 1}
		end
	end

	return nil
end

function FindSpareIndexForChessboard( player )
	for j = 6, 3, -1 do
		for i = 1, 8 do
			if player.units[i][j] == nil then
				return {x = i, y = j}
			end
		end

	end

	return nil
end

function PickChess(keys)
	-- Msg("Pick chess" .. "\n")
	-- Msg("info=" .. keys.target:GetPlayerID() .. "\n")
	local playerId = keys.target:GetPlayerID()
	local player = PlayerResource:GetPlayer(playerId)
	local index = FindIndexPickedChess(player, keys.target)
	if index == nil then
		Msg("Error, can't find chess\n");
		return
	end
	-- Msg("stage=" .. GameRules:GetGameModeEntity().stage .. ", index.y=" .. index.y .. "\n")
	if GameRules:GetGameModeEntity().stage == STAGE_BATTLE_STAGE and index.y ~= 1 then
		SendErrorMessageToPlayer(player, "CANNOT_PICK_THIS_CHESS_IN_CURRENT_STAGE")
		return
	end
	player.picked = true
	player.target = keys.target
	player.index = index
	CustomGameEventManager:Send_ServerToPlayer(player, "on_chess_picked", index)
    
end

function TakeBackChess( keys )
	local playerId = keys.target:GetPlayerID()
	local player = PlayerResource:GetPlayer(playerId)
	if player.picked then
		SendErrorMessageToPlayer(player, "CANNOT_TACK_BACK_CHESS_WHEN_PICKED")
		return
	end

	local index = FindIndexPickedChess(player, keys.target)
	if index == nil then
		Msg("Error, can't find chess\n");
		return
	end
	-- Msg("stage=" .. GameRules:GetGameModeEntity().stage .. ", index.y=" .. index.y .. "\n")
	if GameRules:GetGameModeEntity().stage == STAGE_BATTLE_STAGE and index.y ~= 1 then
		SendErrorMessageToPlayer(player, "CANNOT_PICK_THIS_CHESS_IN_CURRENT_STAGE")
		return
	end

	local emptyChessIndex = FindSpareIndexForWaitArea(player)
	if emptyChessIndex == nil then
		SendErrorMessageToPlayer(player, "NO_SPARE_PLACE")
		return
	end

	PutChess(player, emptyChessIndex, keys.target, index)
	EmitSoundOnClient("ui.click_back", player)
	CustomGameEventManager:Send_ServerToPlayer(player, "on_hero_count_in_chessboard_update", 
		{heroCountInChessboard = player.heroCountInChessboard, maxHeroCountInChessboard = GetMaxHeroCountInChessboard(player:GetAssignedHero())})
end

function GetBaseIndex(playerId)
	local team1_grid_marker = FindBaseGridMarker(playerId)
	local baseGridPosX = GridNav:WorldToGridPosX(team1_grid_marker:GetOrigin().x)
	local baseGridPosY = GridNav:WorldToGridPosY(team1_grid_marker:GetOrigin().y)
	return {x = baseGridPosX, y = baseGridPosY}
end

function CanSet( player, index )
	if index.y == 2 then
		return false
	end

	if index.x <= 0 or index.x >= 9 then
		return false
	end

	if index.y <= 0 or index.y >= 7 then
		return false
	end

	if player.units[index.x][index.y] == nil then
		return true
	else
		return false
	end	
end

function IndexToGridIndex( playerId, index )
	local baseIndex = GetBaseIndex(playerId)
	local gridIndex = {x = (index.x - 1) * 4 + 1, y = (index.y - 1) * 4 + 1}
	gridIndex.x = gridIndex.x + baseIndex.x
	gridIndex.y = gridIndex.y + baseIndex.y
	-- Msg("gridIndex.x=" .. gridIndex.x .. ", gridIndex.y=" .. gridIndex.y .. "\n")
	return gridIndex
end

function IndexToWorldPos( playerId, index )
	local gridIndex = IndexToGridIndex(playerId, index)
	return {x = GridNav:GridPosToWorldCenterX(gridIndex.x), y = GridNav:GridPosToWorldCenterY(gridIndex.y)}
end

function WorldPosToIndex( playerId, position )
	local gridPosX = GridNav:WorldToGridPosX(position.x)
	local gridPosY = GridNav:WorldToGridPosY(position.y)
	-- Msg("gridPosX=" .. gridPosX .. ", gridPosY=" .. gridPosY .. "\n")

	local baseGridPosX = GetBaseIndex(playerId).x
	local baseGridPosY = GetBaseIndex(playerId).y
	-- Msg("baseGridPosX=" .. baseGridPosX .. ", baseGridPosY=" .. baseGridPosY .. "\n")

	local index = { x = math.floor((gridPosX - baseGridPosX) / 4 + 1), y = math.floor((gridPosY - baseGridPosY) / 4 + 1)}
	return index
end

function PutChess( player, index, hero, originalIndex )
	local position = hero:GetOrigin()
	local indexPos =  IndexToWorldPos(player:GetPlayerID(), index)
	position = Vector(indexPos.x, indexPos.y, position.z)
	hero:SetAbsOrigin(position)
	player.units[index.x][index.y] = hero
	if originalIndex ~= nil then
		player.units[originalIndex.x][originalIndex.y] = nil
	end
	if index.y >= 3 and index.y <=6 then
		if originalIndex and originalIndex.y == 1 then
			player.heroCountInChessboard = player.heroCountInChessboard + 1
			CustomGameEventManager:Send_ServerToPlayer(player, "on_hero_count_in_chessboard_update", 
				{heroCountInChessboard = player.heroCountInChessboard, maxHeroCountInChessboard = GetMaxHeroCountInChessboard(player:GetAssignedHero())})
		end
	elseif index.y == 1 then
		if originalIndex and originalIndex.y >= 3 and originalIndex.y <= 6 then
			player.heroCountInChessboard = player.heroCountInChessboard - 1
			CustomGameEventManager:Send_ServerToPlayer(player, "on_hero_count_in_chessboard_update", 
				{heroCountInChessboard = player.heroCountInChessboard, maxHeroCountInChessboard = GetMaxHeroCountInChessboard(player:GetAssignedHero())})
		end
	end
end

function GetAllHeroChess( player )
	local heros = {}
	local index = 1
	for i=1, 8 do
		for j=1, 10 do
			if player.units ~= nil and player.units[i][j] ~= nil then
				heros[index] = player.units[i][j]
				index = index + 1
			end
		end
	end

	return heros
end

function GetAllHeroChessInChessBoard( player )
	local heros = {}
	local index = 1
	for i=1, 8 do
		for j=3, 6 do
			if player.units ~= nil and player.units[i][j] ~= nil then
				heros[index] = player.units[i][j]
				index = index + 1
			end
		end
	end

	return heros
end

function GetAllHeroChessInChessBoardForCombinedBattle( )
	local players = GetAllAlivePlayers()
	local heros = {}
	for i = 1, #players do
		local herosForPlayer = GetAllHeroChessInChessBoard(players[i])
		for j = 1, #herosForPlayer do
			table.insert(heros, herosForPlayer[j])
		end
	end

	return heros
end

function GetAllHeroChessWhichIsNotTemp( player )
	local heros = GetAllHeroChess(player)
	local result = {}
	for i = 1, #heros do
		local hero = heros[i]
		if hero.temp == nil then
			table.insert(result, hero)
		end
	end

	return result
end

function GetAllHeroChessInChessBoardWhichIsNotTemp( player )
	local heros = GetAllHeroChessInChessBoard(player)
	local result = {}
	for i = 1, #heros do
		local hero = heros[i]
		if hero.temp == nil then
			table.insert(result, hero)
		end
	end

	return result
end

function SendErrorMessageToPlayer( player, message, message2 )
	CustomGameEventManager:Send_ServerToPlayer(player, "show_error_message", {message=message, message2=message2})
	EmitSoundForDeny(player)
end

function SendErrorMessageToPlayerWithoutSound( player, message, message2 )
	CustomGameEventManager:Send_ServerToPlayer(player, "show_error_message", {message=message, message2=message2})
end

function SendHintMessageToPlayerEx( player, data, playSound, sound)
	--Msg("SendHintMessageToPlayerEx, " .. TableToStr(data) .. "\n")
	CustomGameEventManager:Send_ServerToPlayer(player, "on_receive_hint_message", data)
	if playSound then
		if sound == nil then
			EmitSoundOnClient("Loot_Drop_Sfx_Minor", player)
		else
			EmitSoundOnClient(sound, player)
		end
	end
end

function SendHintMessageToPlayer( player, message, tokens, playSound, sound)
	SendHintMessageToPlayerEx(player, {message = message, tokens = tokens}, playSound, sound)
end

function SendHintMessageToAllPlayers( message, tokens, playSound, sound)
	local players =  GetAllAlivePlayers()
	for i = 1, #players do
		Msg("message=" .. message .. "\n")
		SendHintMessageToPlayer(players[i], 
			message,
			tokens,
			playSound,
			sound
		)
	end
end

function SendAdventureJournalsEx( player, data )
	data.team = PlayerResource:GetTeam(player:GetPlayerID()) - DOTA_TEAM_CUSTOM_1 + 1
	CustomGameEventManager:Send_ServerToPlayer(player, "on_receive_adventure_journals", data)
end

function SendAdventureJournals( player, message, tokens )
	local data = {}
	data.adventureJournals = BuildAdventureJournals(message, tokens)
	SendAdventureJournalsEx(player, data)
end

function SendAdventureJournalsForAdventureJournals( player, adventureJournals )
	-- body
	local data = {}
	data.adventureJournals = adventureJournals
	SendAdventureJournalsEx(player, data)
end

function SendAdventureJournalsToAllPlayers( message, tokens) 
	local players =  GetAllPlayers()
	for i = 1, #players do
		SendAdventureJournals(players[i], message, tokens)
	end
end

function SendAdventureJournalsForHeroInfo( player, message, tokens, hero )
	local data = {}
	data.adventureJournals = BuildAdventureJournals(message, tokens)
	local heroAdventureJournals = BuildAdventureJournalsForHeroInfo(hero)
	MergeTable(data.adventureJournals, heroAdventureJournals)
	SendAdventureJournalsEx(player, data)
end

function BuildAdventureJournal(message, tokens)
	local adventureJournal = {}
	adventureJournal = {}
	adventureJournal.message = message
	adventureJournal.tokens = tokens
	return adventureJournal;
end

function BuildAdventureJournals(message, tokens)
	local adventureJournals = {}
	adventureJournals[1] = {}
	adventureJournals[1].message = message
	adventureJournals[1].tokens = tokens
	return adventureJournals;
end

function MergeTable( table1, table2 )
	for i = 1, #table2 do
		table.insert(table1, table2[i])
	end
end


function IsInForRandomConfigs( array, value )
	for i = 1, #array do
		if array[i].name == value then
			return true
		end
	end
	return false
end

function FilterTableForRandomConfigs(inputTable, filterTable )
	local result = {}
	filterTable = filterTable or {}
	for i = 1, #inputTable do
		if IsInForRandomConfigs(filterTable, inputTable[i].name) == false then
			table.insert(result, inputTable[i])
		end
	end
	return result
end

function IgnoreAbility( abilityName )
	local filterAbilityNames = {
		"healthy_gaming_advisory1",
		"healthy_gaming_advisory2",
		"healthy_gaming_advisory3",
		"healthy_gaming_advisory4",
		"healthy_gaming_advisory5",
		"healthy_gaming_advisory6",
		"healthy_gaming_advisory7",
		"healthy_gaming_advisory8",
		"healthy_gaming_advisory1",
		"main_character",
		"naga_siren_mirror_image",
		"invulnerable",
		"silenced",
	}
	for i = 1, #filterAbilityNames do
		if filterAbilityNames[i] == abilityName then
			return true
		end
	end
	return false
end

function IsCombinedArtifactAbility( abilityName )
	local equipmentAbilities = GameRules:GetGameModeEntity().equipmentAbilities
	for i = 1, #equipmentAbilities do
		local equipmentAbility = equipmentAbilities[i]
		if equipmentAbility.name == abilityName then
			return true
		end
	end
	return false
end

function HideAbilityLevel( abilityName )
	if IsCombinedArtifactAbility(abilityName) then
		return true
	end

	local hideAbilityNames = {
		"is_random_teammate",
		"is_hire_hero",
	}
	
	for i = 1, #hideAbilityNames do
		local hideAbilityName = hideAbilityNames[i]
		if hideAbilityName == abilityName then
			return true
		end
	end

	return false
end

function GenerateName( sex )
	local nameCount = 41
	local nameLength = RandomInt(1, 4)
	local name = {}
	for i = 1, nameLength do
		table.insert(name, RandomInt(1, nameCount))
	end
	return name
end

function InitName( player, isPlayer, hero, baseInfo )
	if isPlayer then
		hero.nameInfo = {
			type = "player",
			sex = baseInfo.sex,
			name = PlayerResource:GetPlayerName(player:GetPlayerID()),
			baseInfo = baseInfo
		}
	else
		hero.nameInfo = {
			type = "random",
			sex = baseInfo.sex,
			name = GenerateName(sex),
			baseInfo = baseInfo
		}
	end
end

function InitPhantomName( hero, originalHero )
	hero.nameInfo = {
		type = "phantom",
		nameInfo = originalHero.nameInfo
	}
end

function GetUnitName( hero )
	if hero:IsHero() then
		return hero.nameInfo
	else
		return {
			type = "text",
			name = hero:GetUnitName()
		}
	end
end

function AddAdventureJournalsForAbilities( hero, adventureJournals )
	table.insert(adventureJournals, {
		message = "ADVENTURE_JOURNALS_ABILITY_HINT"
	})

	for i = 1, hero:GetAbilityCount() do
		if i < 32 then
			local ability = hero:GetAbilityByIndex(i)
			if ability ~= nil and IgnoreAbility(ability:GetName()) == false then
				local adventureJournal = {}
				local abilityName = ability:GetName()
				if HideAbilityLevel(abilityName) then
					adventureJournal.message = "ADVENTURE_JOURNALS_COMBINED_ARTIFACT_ABILITY"
					adventureJournal.tokens = {
						name = {type = "text", value = "DOTA_Tooltip_ability_" .. abilityName}
					}
					adventureJournal.newLine = false
				else
					adventureJournal.message = "ADVENTURE_JOURNALS_ABILITY"
					adventureJournal.tokens = {
						name = {type = "text", value = "DOTA_Tooltip_ability_" .. abilityName},
						level = {type = "value", value = ability:GetLevel()}
					}
					adventureJournal.newLine = false
				end
				
				table.insert(adventureJournals, adventureJournal)
				adventureJournal = BuildAdventureJournal("ADVENTURE_JOURNALS_SPACE", nil)
				adventureJournal.newLine = false
				table.insert(adventureJournals, adventureJournal)
			end
		end
	end

	local adventureJournal = BuildAdventureJournal("ADVENTURE_JOURNALS_NEW_LINE", nil)
	adventureJournal.newLine = false
	table.insert(adventureJournals, adventureJournal)
end

function AddAdventureJournalsForItems( hero, adventureJournals )
	table.insert(adventureJournals, {
		message = "ADVENTURE_JOURNALS_ITEM_HINT"
	})
	local itemCount = 0
	for i = 0, 8 do
		local item = hero:GetItemInSlot(i)
		if item ~= nil then
			local adventureJournal = {}
			adventureJournal.message = "ADVENTURE_JOURNALS_ITEM"
			adventureJournal.tokens = {
				name = {type = "text", value = "DOTA_Tooltip_ability_" .. item:GetName()}
			}
			adventureJournal.newLine = false
			table.insert(adventureJournals, adventureJournal)
			adventureJournal = BuildAdventureJournal("ADVENTURE_JOURNALS_SPACE", nil)
			adventureJournal.newLine = false
			table.insert(adventureJournals, adventureJournal)
			itemCount = itemCount + 1
		end
	end

	if itemCount == 0 then
		adventureJournals[#adventureJournals].newLine = false
		table.insert(adventureJournals, {
			message = "NONE"
		})
	end
end

function BuildAdventureJournalsForHeroInfo( hero )
	local adventureJournals = {}
	local adventureJournal = {}
	--[[adventureJournal.message = "ADVENTURE_JOURNALS_BASIC_HERO_INFO"
	adventureJournal.tokens = {
		name = {type = "name", value = GetUnitName(hero)},
		sex = {type = "text", value = hero.nameInfo.sex},
		modelName = {type = "text", value = hero:GetUnitName()},
		attackType = {type = "text", value = GetAttackTypeByInt(hero.attackType)},
		attackDamageMin = {type = "value", value = hero:GetBaseDamageMin()},
		attackDamageMax = {type = "value", value = hero:GetBaseDamageMax()},
		attackSpeed = {type = "value", value = math.floor(hero:GetAttackSpeed() * 100)},
		maxHealth = {type = "value", value = hero:GetMaxHealth()},
		maxMana = {type = "value", value = hero:GetMaxMana()},
		strength = {type = "value", value = hero:GetStrength()},
		agility = {type = "value", value = hero:GetAgility()},
		intellect = {type = "value", value = hero:GetIntellect()},
		attackRange = {type = "value", value = hero:Script_GetAttackRange()},
		moveSpeed = {type = "value", value = hero:GetBaseMoveSpeed()},
		physicalArmor = {type = "value", value = math.floor(hero:GetPhysicalArmorValue(false))},
		magicalResistance = {type = "value", value = hero:GetBaseMagicalResistanceValue()},
		evasion = {type = "value", value = math.floor(hero:GetEvasion() * 100)},
		manaRegen = {type = "value", value = "" .. (math.floor((hero:GetManaRegen() * 10)) / 10)},
		healthRegen = {type = "value", value = "" .. (math.floor((hero:GetBaseHealthRegen() * 10)) / 10)},
	}]]--
	adventureJournal.message = "ADVENTURE_JOURNALS_BASIC_HERO_BRIEF_INFO"
	adventureJournal.tokens = {
		name = {type = "name", value = GetUnitName(hero)},
		sex = {type = "text", value = hero.nameInfo.sex},
		modelName = {type = "text", value = hero:GetUnitName()},
		attackType = {type = "text", value = GetAttackTypeByInt(hero.attackType)},
	}
	table.insert(adventureJournals, adventureJournal)
	AddAdventureJournalsForAbilities(hero, adventureJournals)
	AddAdventureJournalsForItems(hero, adventureJournals)

	return adventureJournals
end

function BuildAdventureJournalsForUnitInfo( hero )
	local adventureJournals = {}
	local adventureJournal = {}
	--[[adventureJournal.message = "ADVENTURE_JOURNALS_BASIC_UNIT_INFO"
	adventureJournal.tokens = {
		name = {type = "name", value = GetUnitName(hero)},
		modelName = {type = "text", value = hero:GetUnitName()},
		attackType = {type = "text", value = GetAttackTypeByInt(hero.attackType)},
		attackDamageMin = {type = "value", value = hero:GetBaseDamageMin()},
		attackDamageMax = {type = "value", value = hero:GetBaseDamageMax()},
		attackSpeed = {type = "value", value = math.floor(hero:GetAttackSpeed() * 100)},
		maxHealth = {type = "value", value = hero:GetMaxHealth()},
		maxMana = {type = "value", value = hero:GetMaxMana()},
		attackRange = {type = "value", value = hero:Script_GetAttackRange()},
		moveSpeed = {type = "value", value = hero:GetBaseMoveSpeed()},
		physicalArmor = {type = "value", value = math.floor(hero:GetPhysicalArmorValue(false))},
		magicalResistance = {type = "value", value = hero:GetBaseMagicalResistanceValue()},
		evasion = {type = "value", value = math.floor(hero:GetEvasion() * 100)},
		manaRegen = {type = "value", value = "" .. (math.floor((hero:GetManaRegen() * 10)) / 10)},
		healthRegen = {type = "value", value = "" .. (math.floor((hero:GetBaseHealthRegen() * 10)) / 10)},
	}]]--
	adventureJournal.message = "ADVENTURE_JOURNALS_BASIC_UNIT_BRIEF_INFO"
	adventureJournal.tokens = {
		name = {type = "name", value = GetUnitName(hero)},
		modelName = {type = "text", value = hero:GetUnitName()},
		attackType = {type = "text", value = GetAttackTypeByInt(hero.attackType)},
	}
	table.insert(adventureJournals, adventureJournal)
	AddAdventureJournalsForAbilities(hero, adventureJournals)
	AddAdventureJournalsForItems(hero, adventureJournals)

	return adventureJournals
end

function SendAdventureJournalsForBattleResult( player, status, isCombinedBattle )
	--GameRules:GetGameModeEntity().time = 5
	local adventureJournals = BuildAdventureJournals("",
			{
				playerName = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
				time = {type = "vale", value = GameRules:GetGameModeEntity().time }
			})
	local adventureJournal = adventureJournals[1]
	if status == "win" then
		if GameRules:GetGameModeEntity().time < 10 then
			adventureJournal.message = "ADVENTURE_JOURNALS_BATTLE_WIN_FOR_LESS_TIME"
		elseif GameRules:GetGameModeEntity().time > DEFAULT_BATTLE_STAGE_MAX_TIME_IN_SECONDS - 10 then
			adventureJournal.message = "ADVENTURE_JOURNALS_BATTLE_WIN_FOR_MORE_TIME"
		else
			adventureJournal.message = "ADVENTURE_JOURNALS_BATTLE_WIN_FOR_MIDDLE_TIME"
		end
	elseif status == "fail" then
		if GameRules:GetGameModeEntity().time < 10 then
			adventureJournal.message = "ADVENTURE_JOURNALS_BATTLE_LOSE_FOR_LESS_TIME"
		elseif GameRules:GetGameModeEntity().time > DEFAULT_BATTLE_STAGE_MAX_TIME_IN_SECONDS - 10 then
			adventureJournal.message = "ADVENTURE_JOURNALS_BATTLE_LOSE_FOR_MORE_TIME"
		else
			adventureJournal.message = "ADVENTURE_JOURNALS_BATTLE_LOSE_FOR_MIDDLE_TIME"
		end
	elseif status == "draw" then
		adventureJournal.message = "ADVENTURE_JOURNALS_BATTLE_DRAW"
	end

	local friends = nil
	if isCombinedBattle then
		friends = GetUnitsInChessBoardAliveForCombinedBattle()
	else
		friends = GetUnitsInChessBoardAlive(player)
	end

	if #friends > 0 then
		local adventureJournal = BuildAdventureJournal("ADVENTURE_JOURNALS_BATTLE_FRINDS_INFO", nil)
		adventureJournal.newLine = false
		table.insert(adventureJournals, adventureJournal)
		for i = 1, #friends do
			local unit = friends[i]
			local adventureJournal = BuildAdventureJournal("ADVENTURE_JOURNALS_BRIEF_NAME_INFO", nil)
			adventureJournal.tokens = {
				name = {type = "name", value = GetUnitName(unit)}
			}
			adventureJournal.newLine = false
			table.insert(adventureJournals, adventureJournal)
		end
	end

	local enemies = GetAliveEnemies(player)
	if #enemies > 0 then
		if #friends > 0 then
			local adventureJournal = BuildAdventureJournal("ADVENTURE_JOURNALS_NEW_LINE", nil)
			table.insert(adventureJournals, adventureJournal)
			adventureJournal.newLine = false
		end
		local adventureJournal = BuildAdventureJournal("ADVENTURE_JOURNALS_BATTLE_ENEMIES_INFO", nil)
		adventureJournal.newLine = false
		table.insert(adventureJournals, adventureJournal)
		for i = 1, #enemies do
			local unit = enemies[i]
			local adventureJournal = BuildAdventureJournal("ADVENTURE_JOURNALS_BRIEF_NAME_INFO", nil)
			adventureJournal.tokens = {
				name = {type = "name", value = GetUnitName(unit)}
			}
			adventureJournal.newLine = false
			table.insert(adventureJournals, adventureJournal)
		end
	end

	local data = {}
	data.adventureJournals = adventureJournals
	SendAdventureJournalsEx(player, data)
end

function SendUpdateStatusMessage( player )
	local courier = player:GetAssignedHero()
	CustomGameEventManager:Send_ServerToPlayer(player, "on_status_update", 
		{
			health = math.floor(courier:GetHealth()), 
			mana = math.floor(courier:GetMana()),
			maintenance = GetMaintenance(player),
			winningStreak = player:GetAssignedHero().winningStreak,
		})
end

function SendShopUpdateMessage( player )
	if player.units ~= nil and player.shopItems ~= nil then
		CustomGameEventManager:Send_ServerToPlayer(player, "on_shop_items_update", player.shopItems)
	end
end

function SendQuestsUpdateMessage( player )
	if player.units ~= nil and player.quests ~= nil then
		CustomGameEventManager:Send_ServerToPlayer(player, "on_quests_update", player.quests)
	end
end

function GetMaxHeroCountInChessboard( courier )
	if IsCorpsMode() then
		if courier:GetLevel() <= 6 then
			return courier:GetLevel()
		else
			return 6
		end
	else
		return 1
	end
	
end

function OnMouseLeftDown(index, keys )
	-- Msg("OnMouseLeftDown" .. TableToStr(keys) .. "\n")
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	player.picked = false
	CustomGameEventManager:Send_ServerToPlayer(player, "hide_chess_set_info", {})

	local index = WorldPosToIndex(player:GetPlayerID(), keys)
	-- Msg("index.x=" .. index.x .. ", index.y=" .. index.y .. "\n")

	if player.target:IsNull() then
		return
	end

	if CanSet(player, index) then
		if player.index.y == 1 and index.y == 1 then
			PutChess(player, index, player.target, player.index)
			player.index = nil
			EmitSoundOnClient("ui.click_alt", player)
		elseif (player.heroCountInChessboard < GetMaxHeroCountInChessboard(player:GetAssignedHero())) or (player.index.y >= 3 and player.index.y <= 6) then
			if GameRules:GetGameModeEntity().stage == STAGE_BATTLE_STAGE then
				SendErrorMessageToPlayer(player, "CANNOT_PUT_CHESS_IN_CURRENT_STAGE")
				return
			end
			PutChess(player, index, player.target, player.index)
			player.index = nil
			EmitSoundOnClient("ui.click_alt", player)
		else
			SendErrorMessageToPlayer(player, "HERO_COUNT_IN_CHESSBOARD_REACH_MAX_COUNT")
		end
	else
		SendErrorMessageToPlayer(player, "CANNOT_SET_CHESS_HERE")
	end

	player.target = nil
end

function IsSecondaryProfession( abilityName )
	local abilitiesConfig = GameRules:GetGameModeEntity().abilities
	local abilityConfig = abilitiesConfig[abilityName]
	if abilityConfig.type == SecondaryProfession then
		return true
	else
		return false
	end
end

function RandomSelectAbilityForEnemy(abilities, level)
	local shopItems = GameRules:GetGameModeEntity().shopItems
	local tmpShopItems = {}
	local tmpShopItemCount = 0
	for i = 1, #shopItems do
		if (shopItems[i].type == TYPE_ABILITY) 
			and (ContainAbility(abilities, shopItems[i].abilityName) == false)
			and IsSecondaryProfession(shopItems[i].abilityName) == false
			and IsProfessionAbilityName(shopItems[i].abilityName) == false then
			-- Msg("abilityName=" .. shopItems[i].abilityName .. "\n")
			tmpShopItems[tmpShopItemCount + 1] = shopItems[i]
			tmpShopItemCount = tmpShopItemCount + 1
		end
	end
	shopItems = tmpShopItems
	local probability = GameRules:GetGameModeEntity().probability[level]

	local probablitiesRange = 
	{
		consumable = probability[QUALITY_1], 
		common = (probability[QUALITY_1] + probability[QUALITY_2]),
		rare = (probability[QUALITY_1] + probability[QUALITY_2] + probability[QUALITY_3]),
		epic = (probability[QUALITY_1] + probability[QUALITY_2] + probability[QUALITY_3] + probability[QUALITY_4]),
		artifact = (probability[QUALITY_1] + probability[QUALITY_2] + probability[QUALITY_3] + probability[QUALITY_4] + probability[QUALITY_5]),
	}
	for i = 1, 5 do
		probablitiesRange[i] = probability[QUALITY_1]
	end

	local leftCountTable = GetShopItemsCountTable(shopItems)
	local random = RandomInt(1, 100)
	-- Msg("Random random=" .. random .. "\n")
	local quality
	if random > probablitiesRange[QUALITY_4] then
		quality = QUALITY_5
	elseif random > probablitiesRange[QUALITY_3] then
		quality = QUALITY_4
	elseif random > probablitiesRange[QUALITY_2] then
		quality = QUALITY_3
	elseif random > probablitiesRange[QUALITY_1] then
		quality = QUALITY_2
	else
		quality = QUALITY_1
	end


	if leftCountTable[quality] > 0 then
		random = RandomInt(1, leftCountTable[quality])
		local shopItem = GetShopItemForEnemy(shopItems, random, quality)
		-- Msg("Random quality=" .. quality .. ", random=" .. random .. ", ability=" .. shopItem.abilityName .. "\n")
		return shopItem
	else
		-- 所有的都抽完了
		return nil
	end
end

function ApplyPrimaryAttribute(hero, abilityName )
	local abilitiesConfig = GameRules:GetGameModeEntity().abilities
	local abilityConfig = abilitiesConfig[abilityName]
	--Msg("setPrimaryAttribute=" .. (hero.setPrimaryAttribute and "true" or "false") .. ", abilityConfig=" .. TableToStr(abilityConfig)
	--	.. ", primaryAttribute=" .. (abilityConfig.primaryAttribute and "true" or "false") .. "\n")
	hero.setPrimaryAttribute = hero.setPrimaryAttribute or false
	if hero.setPrimaryAttribute == false and abilityConfig ~= nil and abilityConfig.primaryAttribute ~= nil then
		--Msg("ApplyPrimaryAttribute done" .. "\n")
		hero:SetPrimaryAttribute(abilityConfig.primaryAttribute)
		hero.setPrimaryAttribute = true
		return true
	end

	return false
end

function GetFinalProfessionAbilities( )
	local abilitiesConfig = GameRules:GetGameModeEntity().abilities
	local abilitiesConfigForFinalProfession = {}
	for k, abilityConfig in pairs(abilitiesConfig) do
		if abilityConfig.order ~= nil and abilityConfig.order == 2 then
			table.insert(abilitiesConfigForFinalProfession, abilityConfig)
		end
	end
	return abilitiesConfigForFinalProfession
end

function GetFinalProfessionAbilitiesForMelee( )
	local abilitiesConfig = GameRules:GetGameModeEntity().abilities
	local abilitiesConfigForFinalProfession = {}
	for k, abilityConfig in pairs(abilitiesConfig) do
		if abilityConfig.order ~= nil and abilityConfig.order == 2 and abilityConfig.attackType == ATTACK_TYPE_MELEE then
			table.insert(abilitiesConfigForFinalProfession, abilityConfig)
		end
	end
	return abilitiesConfigForFinalProfession
end

function GetFinalProfessionAbilitiesForMeleeWithShopItemConfig( )
	local abilitiesConfig = GameRules:GetGameModeEntity().abilities
	local shopItems = GameRules:GetGameModeEntity().shopItems
	local abilitiesConfigForFinalProfession = {}
	for k, abilityConfig in pairs(abilitiesConfig) do
		if abilityConfig.order ~= nil and abilityConfig.order == 2 and abilityConfig.attackType == ATTACK_TYPE_MELEE then
			local shopItem = FindAbilityShopItem(shopItems, abilityConfig.name)
			if shopItem ~= nil and (GetShopItemMaxCount(shopItem) - shopItem.existCount) > 0 then
				table.insert(abilitiesConfigForFinalProfession, abilityConfig)
			end
		end
	end
	return abilitiesConfigForFinalProfession
end

function GetFinalProfessionAbilitiesForRanged( )
	local abilitiesConfig = GameRules:GetGameModeEntity().abilities
	local abilitiesConfigForFinalProfession = {}
	for k, abilityConfig in pairs(abilitiesConfig) do
		if abilityConfig.order ~= nil and abilityConfig.order == 2 and abilityConfig.attackType == ATTACK_TYPE_RANGED then
			table.insert(abilitiesConfigForFinalProfession, abilityConfig)
		end
	end
	return abilitiesConfigForFinalProfession
end

function GetFinalProfessionAbilitiesForRangedWithShopItemConfig( )
	local abilitiesConfig = GameRules:GetGameModeEntity().abilities
	local shopItems = GameRules:GetGameModeEntity().shopItems
	local abilitiesConfigForFinalProfession = {}
	for k, abilityConfig in pairs(abilitiesConfig) do
		if abilityConfig.order ~= nil and abilityConfig.order == 2 and abilityConfig.attackType == ATTACK_TYPE_RANGED then
			local shopItem = FindAbilityShopItem(shopItems, abilityConfig.name)
			if shopItem ~= nil and (GetShopItemMaxCount(shopItem) - shopItem.existCount) > 0 then
				table.insert(abilitiesConfigForFinalProfession, abilityConfig)
			end
		end
	end
	return abilitiesConfigForFinalProfession
end

function GetAllAbilitiesForNeeds( abilityName )
	--Msg("GetAllAbilitiesForNeeds, abilityName=" .. abilityName .. "\n")
	local abilitiesConfig = GameRules:GetGameModeEntity().abilities
	local abilitiesConfigForNeeds = {}
	local currentAbilityConfig = abilitiesConfig[abilityName]

	if currentAbilityConfig.needs ~= nil then
		for i = 1, #currentAbilityConfig.needs do
			local subAbilitiesConfigForNeeds = GetAllAbilitiesForNeeds(currentAbilityConfig.needs[i])
			for j = 1, #subAbilitiesConfigForNeeds do
				table.insert(abilitiesConfigForNeeds, subAbilitiesConfigForNeeds[j])
			end
		end
	end

	table.insert(abilitiesConfigForNeeds, abilityName)

	return abilitiesConfigForNeeds
end

--[[
	GetAllAbilitiesForProfessions({"profession_warrior", "profession_berserker"})
	ignore profession, get:
	axe_berserkers_call
	axe_culling_blade
	huskar_berserkers_blood
	life_stealer_rage
]]--
function GetAllAbilitiesForProfessions( abilityNames )
	local abilitiesConfig = GameRules:GetGameModeEntity().abilities
	local abilitiesConfigForSingleRelyOn = {}
	for i = 1, #abilityNames do
		for k, abilityConfig in pairs(abilitiesConfig) do
			if abilityConfig.needs ~= nil then
				if IsIn(abilityConfig.needs, abilityNames[i]) then
					if string.sub(abilityConfig.name, 1, string.len("profession_")) ~= "profession_" then
						table.insert(abilitiesConfigForSingleRelyOn, abilityConfig.name)
					end
					
				end
			end
		end
	end

	return abilitiesConfigForSingleRelyOn
end

function GetAllAbilitiesForProfessionsWithShopItemConfig( abilityNames )
	local abilitiesConfig = GameRules:GetGameModeEntity().abilities
	local shopItems = GameRules:GetGameModeEntity().shopItems
	local abilitiesConfigForSingleRelyOn = {}
	for i = 1, #abilityNames do
		for k, abilityConfig in pairs(abilitiesConfig) do
			if abilityConfig.needs ~= nil then
				if abilityConfig.needs[1] == abilityNames[i] then
					if string.sub(abilityConfig.name, 1, string.len("profession_")) ~= "profession_" then
						local shopItem = FindAbilityShopItem(shopItems, abilityConfig.name)
						if (GetShopItemMaxCount(shopItem) - shopItem.existCount) > 0 then
							table.insert(abilitiesConfigForSingleRelyOn, abilityConfig.name)
						end
						
					end
				end
			end
		end
	end

	return abilitiesConfigForSingleRelyOn
end

function RandomCreateHeroForEnemy(player, currentStageEnemyConfig, location, isEnemy)
	local herosConfig = GameRules:GetGameModeEntity().heros
	local meleeHerosConfig = {}
	local rangedHerosConfig = {}

	local heroConfig = nil
	if isEnemy == nil then
		isEnemy = true
	end

	--Msg("heros=" .. TableToStr(herosConfig) .. "\n")
	for k, heroConfig in pairs(herosConfig) do
		-- Msg("k=" .. k ..", heroConfig=" .. TableToStr(heroConfig) .. "\n")
		for j = 1, #heroConfig.base do
			local base = heroConfig.base[j]
			if base.attackType == ATTACK_TYPE_MELEE then
				if currentStageEnemyConfig.enemyConfig.race == nil or (currentStageEnemyConfig.enemyConfig.race == k) then
					table.insert(meleeHerosConfig, {name = base.name, abilities = heroConfig.abilities, baseInfo = base} )
				end
			else
				if currentStageEnemyConfig.enemyConfig.race == nil or (currentStageEnemyConfig.enemyConfig.race == k) then
					table.insert(rangedHerosConfig, {name = base.name, abilities = heroConfig.abilities, baseInfo = base} )
				end
			end
		end
	end

	-- Msg("meleeHerosConfig=" .. TableToStr(meleeHerosConfig) .. "\n")
	-- Msg("rangedHerosConfig=" .. TableToStr(rangedHerosConfig) .. "\n")

	if currentStageEnemyConfig.enemyConfig.attackType == nil then
		local random = RandomInt(1, 10)
		if random <= 5 then
			currentStageEnemyConfig.enemyConfig.attackType = ATTACK_TYPE_MELEE
		else
			currentStageEnemyConfig.enemyConfig.attackType = ATTACK_TYPE_RANGED
		end
	end
		
	if currentStageEnemyConfig.enemyConfig.attackType == ATTACK_TYPE_MELEE then
		local index = RandomInt(1, #meleeHerosConfig)
		heroConfig = meleeHerosConfig[index]
	else
		local index = RandomInt(1, #rangedHerosConfig)
		heroConfig = rangedHerosConfig[index]
	end

	--Msg("heroConfig=" .. TableToStr(heroConfig) .. "\n")
	local abilities = {}
	local abilitiesCount = 0

	local level = currentStageEnemyConfig.enemyConfig.level

	local abilityLevel = math.floor(level / 2)
	local shopItems = GameRules:GetGameModeEntity().shopItems

	for i = 1, #heroConfig.abilities do
		abilitiesCount = abilitiesCount + 1
		abilities[abilitiesCount] = {
			abilityName = heroConfig.abilities[i], 
			level = RandomInt(QualityToNumber(currentStageEnemyConfig.enemyConfig.quality), math.max(abilityLevel, QualityToNumber(currentStageEnemyConfig.enemyConfig.quality)))
		}
	end

	local addAbilityCount = level - #heroConfig.abilities

	if currentStageEnemyConfig.enemyConfig.abilities ~= nil then
		addAbilityCount = addAbilityCount - #currentStageEnemyConfig.enemyConfig.abilities
		for i = 1, #currentStageEnemyConfig.enemyConfig.abilities do
			local abilityName = currentStageEnemyConfig.enemyConfig.abilities[i].name
			abilitiesCount = abilitiesCount + 1
			abilities[abilitiesCount] = {
				abilityName = abilityName, 
				level = RandomInt(QualityToNumber(currentStageEnemyConfig.enemyConfig.quality), math.max(abilityLevel, QualityToNumber(currentStageEnemyConfig.enemyConfig.quality)))
			}
		end
	end
	if addAbilityCount > 3 then
		-- enter complete profession mode
		local abilitiesConfigForFinalProfession = nil
		if currentStageEnemyConfig.enemyConfig.profession == nil then
			local attackType = ATTACK_TYPE_MELEE
			local randomForAttackType = RandomInt(1, 100)
			if currentStageEnemyConfig.enemyConfig.attackType == ATTACK_TYPE_MELEE then
				if randomForAttackType <= 20 then
					attackType = ATTACK_TYPE_RANGED
				else
					attackType = ATTACK_TYPE_MELEE
				end
			else
				if randomForAttackType <= 20 then
					attackType = ATTACK_TYPE_MELEE
				else
					attackType = ATTACK_TYPE_RANGED
				end
			end

			if attackType == ATTACK_TYPE_MELEE then
				abilitiesConfigForFinalProfession = GetFinalProfessionAbilitiesForMelee()
			else
				abilitiesConfigForFinalProfession = GetFinalProfessionAbilitiesForRanged()
			end
		else
			abilitiesConfigForFinalProfession = {GameRules:GetGameModeEntity().abilities[currentStageEnemyConfig.enemyConfig.profession]}
			--Msg("abilitiesConfigForFinalProfession=" .. TableToStr(abilitiesConfigForFinalProfession) .. ", count=" .. #abilitiesConfigForFinalProfession ..  "\n")
		end

		if #abilitiesConfigForFinalProfession > 0 then
			local random = RandomInt(1, #abilitiesConfigForFinalProfession)
			--Msg("abilitiesConfigForFinalProfession=" .. TableToStr(abilitiesConfigForFinalProfession) .. "\n")
			local finalProfessionAbility = abilitiesConfigForFinalProfession[random]
			-- add all basic profession ability
			local needsAbilities = GetAllAbilitiesForNeeds(finalProfessionAbility.name)
			--Msg("finalProfessionAbility=" .. TableToStr(finalProfessionAbility) .. "\nneedsAbilities=" .. TableToStr(needsAbilities) .. "\n")
			for i = 1, #needsAbilities do
				abilitiesCount = abilitiesCount + 1
				abilities[abilitiesCount] = {
					abilityName = needsAbilities[i], 
					level = RandomInt(QualityToNumber(currentStageEnemyConfig.enemyConfig.quality), math.max(abilityLevel, QualityToNumber(currentStageEnemyConfig.enemyConfig.quality)))
				}
			end

			-- add all profession ability
			addAbilityCount = addAbilityCount - #needsAbilities
			local abilitiesForProfessions = GetAllAbilitiesForProfessions(needsAbilities)
			--[[for i = 1, #abilitiesForProfessions do
				Msg("selected abilitiesForProfessions=" .. abilitiesForProfessions[i] .. "\n")
			end]]--
			local selectedAbilitiesCount = 0
			selectedAbilitiesCount = math.min(addAbilityCount, #abilitiesForProfessions)
			if selectedAbilitiesCount > 4 then
				selectedAbilitiesCount = RandomInt(4, selectedAbilitiesCount)
			end
			--Msg("addAbilityCount=" .. addAbilityCount .. ", abilitiesForProfessions=" .. (#abilitiesForProfessions) .. ", selectedAbilitiesCount=" .. selectedAbilitiesCount .. "\n")

			addAbilityCount = addAbilityCount - selectedAbilitiesCount
			for i = 1, selectedAbilitiesCount do
				local random = RandomInt(1, #abilitiesForProfessions)
				abilitiesCount = abilitiesCount + 1
				abilities[abilitiesCount] = {
					abilityName = abilitiesForProfessions[random], 
					level = RandomInt(QualityToNumber(currentStageEnemyConfig.enemyConfig.quality), math.max(abilityLevel, QualityToNumber(currentStageEnemyConfig.enemyConfig.quality)))
				}

				--Msg("selected=" .. i .. ", name=" .. abilitiesForProfessions[random] ..  "\n")
				table.remove(abilitiesForProfessions, random)
			end

			--Msg("addAbilityCount=" .. addAbilityCount .. ", #heroConfig.abilities=" .. #heroConfig.abilities .. ", #needsAbilities=" .. #needsAbilities
			--	.. ", selectedAbilitiesCount=" .. selectedAbilitiesCount .. "\n")
		end
	end

	for i = 1, addAbilityCount do
		local abilityShopItem = RandomSelectAbilityForEnemy(abilities, level)
		if abilityShopItem ~= nil then
			abilitiesCount = abilitiesCount + 1
			abilities[abilitiesCount] = {
				abilityName = abilityShopItem.abilityName, 
				level = RandomInt(QualityToNumber(currentStageEnemyConfig.enemyConfig.quality), math.max(abilityLevel, QualityToNumber(currentStageEnemyConfig.enemyConfig.quality)))
			}
		end
	end

	local team1_grid_marker = FindBaseGridMarker(player:GetPlayerID())
	GameRules:GetGameModeEntity().createUnit = true
	local hero = nil
	if isEnemy then
		hero = CreateHeroForPlayer(heroConfig.name, player.enemyPlayer)
		hero.player = player
	else
		hero = CreateHeroForPlayer(heroConfig.name, player)
	end
	--hero:SetControllableByPlayer(GameRules:GetGameModeEntity().dummyPlayer:GetPlayerID(), false)
	hero.abilities = {}
	hero.abilitiesCount = 0
	GameRules:GetGameModeEntity().createUnit = false
	InitName(player, false, hero, heroConfig.baseInfo)
	hero:SetTeam(DOTA_TEAM_BADGUYS)
	--hero:SetTeam(DOTA_TEAM_CUSTOM_1)
	hero:SetAbsOrigin(location)
	-- Msg("abilities=" .. TableToStr(abilities) .. "\n")
	for i = 1, #abilities do
		local ability = abilities[i]
		AddAbility(hero, ability.abilityName, ability.level, true)
	end

	for i = 2, level do
		GameRules:GetGameModeEntity().ignoreGainedLevel = true
		hero:HeroLevelUp(false)
		GameRules:GetGameModeEntity().ignoreGainedLevel = false
	end

	--Msg("hero.abilitiesCount=" .. hero.abilitiesCount .. "\n")

	return hero
	
end

function CustomCreateUnitByName( szUnitName, vLocation, bFindClearSpace, hNPCOwner, hUnitOwner, iTeamNumber, player )
	local unit = CreateUnitByName(szUnitName, vLocation, bFindClearSpace, hNPCOwner, hUnitOwner, iTeamNumber)
	unit.player = player
	--[[if player == nil then
		InsertUnit(unit:GetPlayerOwner(), unit)
	else
		InsertUnit(player, unit)
	end]]--
	return unit
end

function FindByName( table, name )
	for i = 1, #table do
		if table[i].name == name then
			return table[i]
		end
	end

	return nil
end

function CreateUnit(player, currentStageEnemyConfig)
	local team1_grid_marker = FindBaseGridMarker(player:GetPlayerID())
	local worldPosition = IndexToWorldPos(player:GetPlayerID(), currentStageEnemyConfig.position)
	local location = team1_grid_marker:GetOrigin()
	location.x = worldPosition.x
	location.y = worldPosition.y
	local unit = nil
	if currentStageEnemyConfig.enemyConfig.type == TYPE_HERO then
		unit = RandomCreateHeroForEnemy(player, currentStageEnemyConfig, location)
		unit:SetBaseDamageMin(currentStageEnemyConfig.enemyConfig.attackDamageMin)
		unit:SetBaseDamageMax(currentStageEnemyConfig.enemyConfig.attackDamageMax)

		if currentStageEnemyConfig.enemyConfig.artifacts ~= nil then
			for i = 1, #currentStageEnemyConfig.enemyConfig.artifacts do
				local artifact = currentStageEnemyConfig.enemyConfig.artifacts[i].name
				local equipmentConfig = FindByName(GameRules:GetGameModeEntity().equipmentAbilities, artifact)
				--Msg("equipmentConfig=" .. TableToStr(needEquipment) .. ", artifact=" .. artifact .. "\n")
				for j = 1, #equipmentConfig.needs do
					local itemName = equipmentConfig.needs[j]
					--Msg("itemName=" .. itemName .. "\n")
					CreateShopItemForHero(player, unit, FindShopItem(GameRules:GetGameModeEntity().shopItems, itemName), nil, false)
				end
			end
		end

		if currentStageEnemyConfig.enemyConfig.randomArtifacts ~= nil then
			local randomArtifacts = FilterTableForRandomConfigs(currentStageEnemyConfig.enemyConfig.randomArtifacts.artifacts, currentStageEnemyConfig.enemyConfig.artifacts)
			--Msg("artifacts=" .. TableToStr(currentStageEnemyConfig.enemyConfig.artifacts) .. ", randomArtifacts=" .. TableToStr(randomArtifacts) .. "\n")
			if #randomArtifacts > 0 then
				local maxCount = math.min(#randomArtifacts, currentStageEnemyConfig.enemyConfig.randomArtifacts.maxCount)
				local minCount = math.min(#randomArtifacts, currentStageEnemyConfig.enemyConfig.randomArtifacts.minCount)
				local randomArtifactsCount = RandomInt(minCount, maxCount)
				local artifacts = RandomSort(randomArtifacts)
				for i = 1, randomArtifactsCount do
					local artifact = artifacts[i].name
					local equipmentConfig = FindByName(GameRules:GetGameModeEntity().equipmentAbilities, artifact)
					--Msg("equipmentConfig=" .. TableToStr(needEquipment) .. ", artifact=" .. artifact .. "\n")
					for j = 1, #equipmentConfig.needs do
						local itemName = equipmentConfig.needs[j]
						--Msg("itemName=" .. itemName .. "\n")
						CreateShopItemForHero(player, unit, FindShopItem(GameRules:GetGameModeEntity().shopItems, itemName), nil, false)
					end
				end
			end
		end
	else
		GameRules:GetGameModeEntity().createUnit = true
		unit = CustomCreateUnitByName(currentStageEnemyConfig.enemyConfig.name, location, true, nil, nil, DOTA_TEAM_BADGUYS, player)
		--unit = CreateUnitByName(currentStageEnemyConfig.enemyConfig.name, location, true, nil, nil, PlayerResource:GetTeam(player:GetPlayerID()))
		GameRules:GetGameModeEntity().createUnit = false
		unit:SetBaseDamageMin(currentStageEnemyConfig.enemyConfig.attackDamageMin)
		unit:SetBaseDamageMax(currentStageEnemyConfig.enemyConfig.attackDamageMax)
		unit:SetBaseMaxHealth(currentStageEnemyConfig.enemyConfig.health)
		unit.abilities = {}
		unit.abilitiesCount = 0
		GameRules:GetGameModeEntity().createUnit = true
		unit:RespawnUnit()
		GameRules:GetGameModeEntity().createUnit = false

		if currentStageEnemyConfig.enemyConfig.abilities ~= nil then
			for i = 1, #currentStageEnemyConfig.enemyConfig.abilities do
				local ability = currentStageEnemyConfig.enemyConfig.abilities[i]
				local minLevel = math.min(math.floor(GameRules:GetGameModeEntity().currentStage / 10), 5)
				local maxLevel = 5
				AddAbility(unit, ability.name, RandomInt(minLevel, maxLevel), true) 
			end
		end

		if currentStageEnemyConfig.enemyConfig.randomAbilities ~= nil then
			local randomAbilities = FilterTableForRandomConfigs(currentStageEnemyConfig.enemyConfig.randomAbilities.abilities, currentStageEnemyConfig.enemyConfig.abilities)
			--Msg("abilities=" .. TableToStr(currentStageEnemyConfig.enemyConfig.abilities) .. ", randomAbilities=" .. TableToStr(randomAbilities) .. "\n")
			if #randomAbilities > 0 then
				local maxCount = math.min(#randomAbilities, currentStageEnemyConfig.enemyConfig.randomAbilities.maxCount)
				local minCount = math.min(#randomAbilities, currentStageEnemyConfig.enemyConfig.randomAbilities.minCount)
				local randomAbilitiesCount = RandomInt(minCount, maxCount)
				local abilities = RandomSort(randomAbilities)
				for i = 1, randomAbilitiesCount do
					local ability = abilities[i]
					local minLevel = math.min(math.floor(GameRules:GetGameModeEntity().currentStage / 10), 5)
					local maxLevel = 5
					AddAbility(unit, ability.name, RandomInt(minLevel, maxLevel), true) 
				end
			end
		end
	end

	AddAbility(unit, "invulnerable")
	AddAbility(unit, "silenced")

	InitUnit(unit)
	unit.enemyConfig = currentStageEnemyConfig.enemyConfig
	unit.player = player

	return unit
end

function ContainsPosition( positions, position )
	for i = 1, #positions do
		if positions[i].x == position.x and positions[i].y == position.y then
			return true
		end
	end

	return false
end

function GetRandomEnemySparePosition( enemies)
	local usedPositions = {}
	for i = 1, #enemies do
		if enemies[i].position ~= nil then
			table.insert(usedPositions, enemies[i].position)
		end
	end

	local canUsePositions = {}
	for i = 1, 8 do
		for j = 7, 10 do
			if ContainsPosition(usedPositions, {x = i, y = j}) == false then
				table.insert(canUsePositions, {x = i, y = j})
			end
		end
	end

	if #canUsePositions <= 0 then
		return {x = 4, y = 9}
	end
	local random = RandomInt(1, #canUsePositions)
	return canUsePositions[random]
end


function GetRandomEnemySparePositionForAttackType( enemies, enemy)
	local usedPositions = {}
	for i = 1, #enemies do
		if enemies[i].position ~= nil then
			table.insert(usedPositions, enemies[i].position)
		end
	end

	local canUsePositions = {}
	if enemy.enemyConfig.attackType == ATTACK_TYPE_MELEE then
		for i = 1, 8 do
			for j = 7, 8 do
				if ContainsPosition(usedPositions, {x = i, y = j}) == false then
					table.insert(canUsePositions, {x = i, y = j})
				end
			end
		end
	else
		for i = 1, 8 do
			for j = 9, 10 do
				if ContainsPosition(usedPositions, {x = i, y = j}) == false then
					table.insert(canUsePositions, {x = i, y = j})
				end
			end
		end
	end
	
	if #canUsePositions <= 0 then
		for i = 1, 8 do
			for j = 7, 10 do
				if ContainsPosition(usedPositions, {x = i, y = j}) == false then
					table.insert(canUsePositions, {x = i, y = j})
				end
			end
		end
	end

	if #canUsePositions <= 0 then
		return {x = 4, y = 9}
	end
	local random = RandomInt(1, #canUsePositions)
	return canUsePositions[random]
end

function SetEnemyPosition( enemies )
	local meleeCount = 0
	local rangedCount = 0
	for i = 1, #enemies do
		if enemies[i].attackType == ATTACK_TYPE_MELEE then
			meleeCount = meleeCount + 1
		else
			rangedCount = rangedCount + 1
		end
	end

	local formation = RandomInt(1, 2)
	--Msg("\nformation=" .. formation .. "\n")
	if formation == 1 then
		--随机阵型
		for i = 1, #enemies do
			enemies[i].position = GetRandomEnemySparePosition(enemies)
		end
	else
		--前近后远
		for i = 1, #enemies do
			enemies[i].position = GetRandomEnemySparePositionForAttackType(enemies, enemies[i])
		end
	end
end

function GetStageEnemyConfigs( stage )
	-- body
	local enemiesConfig = GameRules:GetGameModeEntity().enemiesConfig
	local currentEnemyIndex = stage
	if currentEnemyIndex > #enemiesConfig then
		currentEnemyIndex = currentEnemyIndex % #enemiesConfig
		if currentEnemyIndex == 0 then
			currentEnemyIndex = #enemiesConfig
		end
	end
	local currentStageEnemyConfigs = enemiesConfig[currentEnemyIndex];
	return currentStageEnemyConfigs
end

function GetCurrentStageEnemyConfigs( )
	return GetStageEnemyConfigs(GameRules:GetGameModeEntity().currentStage)
end

function CreateEnemiesForPlayer( player )
	player.enemies = {}
	local enemyConfig = GetCurrentStageEnemyConfigs()
	local enemiesConfig = {}
	for i = 1, #enemyConfig do
		table.insert(enemiesConfig, {enemyConfig = enemyConfig[i]})
	end

	SetEnemyPosition(enemiesConfig)

	-- create enemy

	for i = 1, #enemiesConfig do
		local enemyConfig = enemiesConfig[i]
		-- Msg("enemyConfig=" .. TableToStr(enemyConfig) .. "\n")
		local unit = CreateUnit(player, enemyConfig)
		player.enemies[i] = unit
	end

	player.enemiesCount = #enemiesConfig
end

function CreateEnemiesForCombinedBattle( )
	local players = GetAllAlivePlayers()
	CreateEnemiesForPlayer(players[1])

	for i = 1, #players[1].enemies do
		local enemy = players[1].enemies[i]
		local position = enemy:GetAbsOrigin()
		local index = WorldPosToIndex(players[1]:GetPlayerID(), enemy:GetAbsOrigin())
		--Msg("CreateEnemiesForCombinedBattle, index.x=" .. index.x .. ", index.y=" .. index.y .. "\n")
		local worldPosition = IndexToWorldPos(-1, index)
		position.x = worldPosition.x
		position.y = worldPosition.y
		enemy:SetAbsOrigin(position)
		--Msg("CreateEnemiesForCombinedBattle, position.x=" .. position.x .. ", position.y=" .. position.y .. "\n")
	end

	for i = 2, #players do
		local player = players[i]

		for j = 1, #players[1].enemies do
			local unit = players[1].enemies[j]
			if player.enemies == nil then
				player.enemies = {}
			end
			player.enemies[j] = unit
		end

		player.enemiesCount = #players[1].enemies
	end
end

function ResetUnitsForPlayer( player )
	local heros = GetAllHeroChess(player)
	for i = 1, #heros do
		local hero = heros[i]
		if hero:HasModifier("modifier_lone_druid_true_form") then
			hero:RemoveModifierByName("modifier_lone_druid_true_form")
		end

		RemoveAbilityForCombinedArtifact(hero)
		ClearLegendAbilities(hero)
		ClearOtherAbilities(hero)

		if hero.temp then
			hero.lifeTime = hero.lifeTime - 1
			if hero.lifeTime == 0 and hero.canBuy ~= nil then
				if hero.canBuy == true then
					DestroyChess(player, hero, true)
				else
					DestroyChess(player, hero, false)
				end
			end
		end
	end

	local heros = GetAllHeroChessInChessBoard(player)
	for i, hero in pairs(heros) do
		if IsUnitAlive(hero) then
			hero:ForceKill(false)
		end

		GameRules:GetGameModeEntity().createUnit = true
		hero:RespawnHero(false, false)
		GameRules:GetGameModeEntity().createUnit = false
		AddAbility(hero, "invulnerable")
		AddAbility(hero, "silenced")
		hero:ClearActivityModifiers()
		if hero:HasModifier("modifier_enchantress_natures_attendants") then
			hero:RemoveModifierByName("modifier_enchantress_natures_attendants")
		end
		hero:SetControllableByPlayer(player:GetPlayerID(), false)
		hero:SetMoveCapability(DOTA_UNIT_CAP_MOVE_NONE)
		hero.attackType = hero:GetAttackCapability()
		hero:SetAttackCapability(DOTA_UNIT_CAP_NO_ATTACK)
		hero:SetTeam(PlayerResource:GetTeam(player:GetPlayerID()))

		local index = FindIndexPickedChess(player, hero)
		local position = hero:GetOrigin()
		local indexPos =  IndexToWorldPos(player:GetPlayerID(), index)
		position = Vector(indexPos.x, indexPos.y, position.z)
		hero:SetAbsOrigin(position)
	end

	if player.otherUnits ~= nil then
		for i = 1, #player.otherUnits do
			if player.otherUnits[i]:IsNull() == false then
				player.otherUnits[i]:Destroy()
			end
		end

		player.otherUnits = nil
	end

	if IsCombinedBattleForStage(GameRules:GetGameModeEntity().currentStage - 1) then
		PlayerResource:SetCameraTarget(player:GetPlayerID(), heros[1])
		player:SetContextThink(DoUniqueString("camera_back"), 
			function() PlayerResource:SetCameraTarget(player:GetPlayerID(), nil)  end, 1)
	end
end

function RandomGenerateTeammate( player )
	if player.randomTeam == false or GameRules:GetGameModeEntity().currentStage == 1 or player.units == nil then
		return
	end

	if IsSoloMode() then
		return
	end

	local maxHeroCount = GetMaxHeroCountInChessboard(player:GetAssignedHero())
	local heros = GetAllHeroChessInChessBoard(player)
	local maxRandomHeroCount = maxHeroCount - #heros
	if maxRandomHeroCount <= 0 then
		return
	end

	local randomHeroCount = RandomInt(0, maxRandomHeroCount)
	if GameRules:GetGameModeEntity().currentStage <= 10 then
		randomHeroCount = maxRandomHeroCount
	end

	if randomHeroCount <= 0 then
		SendHintMessageToPlayer(player, 
			"CANNOT_FIND_TEAMMATE", 
			nil,
			true,
			"underdraft_drafted"
		)
		return
	end

	local shopItems = GameRules:GetGameModeEntity().shopItems
	for i = 1, randomHeroCount do
		local heroShopItems = FindHeroShopItemsWithMaxCountLimit()
		if #heroShopItems > 0 then
			local random = RandomInt(1, #heroShopItems)
			local shopItem = heroShopItems[random]
			shopItem.existCount = shopItem.existCount + 1

			local courier = player:GetAssignedHero()
			local quality = math.floor(courier:GetLevel() / 2)
			quality = NumberToQuality(quality)

			local heroShopItem = CreateHeroShopItem(player:GetAssignedHero(), shopItem, shopItems, quality)
			local emptyChessIndex = FindSpareIndexForChessboard(player)
			if emptyChessIndex == nil then
				Msg("RandomGenerateTeammate error, no spare space\n")
				return
			end
			local hero = CreateHero(player, heroShopItem)
			hero.canBuy = true
			hero.temp = true
			hero.lifeTime = 3
			hero.shopItem = heroShopItem
			InitUnit(hero)
			PutChess(player, emptyChessIndex, hero, nil)
			player.heroCountInChessboard = player.heroCountInChessboard + 1
			CustomGameEventManager:Send_ServerToPlayer(player, "on_hero_count_in_chessboard_update", 
				{heroCountInChessboard = player.heroCountInChessboard, maxHeroCountInChessboard = GetMaxHeroCountInChessboard(player:GetAssignedHero())})
			AddAbility(hero, "is_random_teammate", 1, false)

			SendAdventureJournalsForHeroInfo(player, "ADVENTURE_JOURNALS_RANDOM_TEAMMATE",
			{
				name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
				heroName = {type = "name", value = GetUnitName(hero)}
			}, hero)
		end
		
	end

	SendHintMessageToPlayer(player, 
		"FIND_TEAMMATE_SUCCESS", 
		nil,
		true,
		"ui_treasure_map"
	)
end

function InitSecondaryProfessionHandlers()
	local commonHandlers = {
		doNothing = function ( player, ability, context, handle )
			handle(players, ability, context)
		end,
		everyCountRounds = function ( player, ability, context, handle )
			ability.stage = ability.stage or 0
			ability.stage = ability.stage + 1
			local turn_count = ability:GetSpecialValueFor("turn_count")
			if ability.stage >= turn_count then
				handle(player, ability, context)
				ability.stage = 0
			end
		end,
		streakWinEnd = function ( player, ability, context, handle )
			local courier = player:GetAssignedHero()
			ability.winningStreak = ability.winningStreak or 0
			local turn_count = ability:GetSpecialValueFor("turn_count")

			if context.status ~= "win" then
				ability.winningStreak = 0
			else
				ability.winningStreak = ability.winningStreak + 1			
			end

			--Msg("ability.winningStreak=" .. ability.winningStreak .. ", status=" .. context.status .. "\n")

			if ability.winningStreak >= turn_count then
				handle(player, ability, context)
				ability.winningStreak = 0
			end
		end,
		probability = function ( player, ability, context, handle )
			local probability = ability:GetSpecialValueFor("probability")
			local random = RandomInt(1, 100)
			if random <= probability then
				handle(player, ability, context)
			end
		end
	}

	local rewordHandlers = {
		gold = function ( player, ability, context )
			local quest = {}
			quest.context = {}
			quest.filterItems = {}
			quest.cost = -4 --default is 5-quest.cost
			GameRules:GetGameModeEntity().rewordConfigs.REWARD_GOLD.prepareExecute(quest)
			GameRules:GetGameModeEntity().rewordConfigs.REWARD_GOLD.onComplete(quest, player)

			SendAdventureJournals(player, "ADVENTURE_JOURNALS_SECONDARY_PROFESSION_" ..  string.upper(ability:GetName()),
			{
				name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
				heroName = {type = "name", value = GetUnitName(context.hero)},
				abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()},
			})

			--Msg("stage=" .. (GameRules:GetGameModeEntity().currentStage - 1) ..  ", Get gold for secondary profession\n")
		end,
		drug = function ( player, ability, context )
			local itemNames = {
				--"item_water_of_forgetting",
				--"item_set_primary_attribute_strength",
				--"item_set_primary_attribute_agility",
				--"item_set_primary_attribute_intellect",
				--"item_health",
				"item_attribute_strength",
				"item_attribute_agility",
				"item_attribute_intellect",
				"item_attribute_all",
			}
			local shopItems = GetShopItemsForSecondaryProfession(itemNames)
			local shopItem
			if #shopItems > 0 then
				shopItem = shopItems[RandomInt(1, #shopItems)]
			else
				shopItem = GameRules:GetGameModeEntity().shopItems[1]
			end
			shopItem.existCount = shopItem.existCount + 1
			CreateShopItem(player, shopItem, player:GetAssignedHero():GetAbsOrigin(), true)

			SendHintMessageToPlayer(player, 
				"SECONDARY_PROFESSION_HINT_REWARD_CONSUMABLE", 
				{
					abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()},
					itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. shopItem.name}
				},
				true,
				"tutorial_fanfare"
			)
			SendAdventureJournals(player, "ADVENTURE_JOURNALS_SECONDARY_PROFESSION_" ..  string.upper(ability:GetName()),
				{
					name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
					heroName = {type = "name", value = GetUnitName(context.hero)},
					abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()},
					itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. shopItem.name}
			})
			--Msg("stage=" .. (GameRules:GetGameModeEntity().currentStage - 1) ..  ", Get " .. shopItem.name .. " for secondary profession\n")
		end,
		equipment = function ( player, ability, context )
			local rewordConfig = GameRules:GetGameModeEntity().rewordConfigs[REWARD_EQUIPMENT]
			local quest = {}
			quest.context = {}
			quest.filterItems = {}
			rewordConfig.prepareExecute(quest)
			rewordConfig.onComplete(quest, player)
			SendHintMessageToPlayer(player, 
				"SECONDARY_PROFESSION_HINT_REWARD_EQUIPMENT", 
				{
					abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()},
					itemName = {type = "text", value = quest.context.itemName.value}
				},
				true,
				"tutorial_fanfare"
			)
			
			SendAdventureJournals(player, "ADVENTURE_JOURNALS_SECONDARY_PROFESSION_" ..  string.upper(ability:GetName()),
				{
					name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
					heroName = {type = "name", value = GetUnitName(context.hero)},
					abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()},
					itemName = quest.context.itemName
			})
			--Msg("Get One equipment\n")
		end,
		all = function ( player, ability, context )
			local allRewords = {
				{type = REWARD_EQUIPMENT, probability = 20},
				{type = REWARD_ABILITY, probability = 40},
				{type = REWARD_CONSUMABLE, probability = 40}
			}
			local reword = GetByProbabilityFromTable(allRewords)
			local rewordConfig = GameRules:GetGameModeEntity().rewordConfigs[reword.type]
			local quest = {}
			quest.context = {}
			quest.filterItems = {}
			rewordConfig.prepareExecute(quest)
			rewordConfig.onComplete(quest, player)
			SendHintMessageToPlayer(player, 
				"SECONDARY_PROFESSION_HINT_REWARD_ABILITY", 
				{
					abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()},
					itemName = quest.context.itemName
				},
				true,
				"tutorial_fanfare"
			)

			SendAdventureJournals(player, "ADVENTURE_JOURNALS_SECONDARY_PROFESSION_" ..  string.upper(ability:GetName()),
				{
					name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
					heroName = {type = "name", value = GetUnitName(context.hero)},
					abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()},
					itemName = quest.context.itemName
			})
			--Msg("Get all\n")
		end,
		levelUp = function ( player, ability, context )
			local allRewords = {
				{type = REWARD_ABILITY_LEVEL_UP, probability = 50},
				{type = REWARD_HERO_LEVEL_UP, probability = 50}
			}

			SendHintMessageToPlayer(player, 
				"SECONDARY_PROFESSION_HINT_REWARD_LEVEL_UP", 
				{
					abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()}
				},
				true,
				"tutorial_fanfare"
			)

			local reword = GetByProbabilityFromTable(allRewords)
			local rewordConfig = GameRules:GetGameModeEntity().rewordConfigs[reword.type]
			local quest = {}
			quest.context = {}
			quest.filterItems = {}
			rewordConfig.prepareExecute(quest)
			rewordConfig.onComplete(quest, player)

			if quest.context.name ~= nil then
				local adventureJournals = BuildAdventureJournals("ADVENTURE_JOURNALS_SECONDARY_PROFESSION_" ..  string.upper(ability:GetName()),
					{
						name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
						heroName = {type = "name", value = GetUnitName(context.hero)},
						abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()},
				})
				adventureJournals[1].newLine = false

				local adventureJournal = BuildAdventureJournal(reword.type, nil)
				adventureJournal.tokens = quest.context
				adventureJournal.newLine = false
				table.insert(adventureJournals, adventureJournal)

				SendAdventureJournalsForAdventureJournals(player, adventureJournals)
			else
				SendAdventureJournals(player, "ADVENTURE_JOURNALS_SECONDARY_PROFESSION_GOLD",
				{
					name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
					heroName = {type = "name", value = GetUnitName(context.hero)},
					abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()},
					gold = {type = "value", value = 5}
				})
			end
			--Msg("Level up\n")
		end,
		itemIgnoreExistCount = function ( player, ability, context )
			local shopItem = FindShopItem(GameRules:GetGameModeEntity().shopItems, context.config.itemName)
			CreateShopItem(player, shopItem, player:GetAssignedHero():GetAbsOrigin(), true)
			SendHintMessageToPlayer(player, 
				"SECONDARY_PROFESSION_HINT_REWARD_CONSUMABLE", 
				{
					abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()},
					itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. shopItem.name}
				},
				true,
				"tutorial_fanfare"
			)
			SendAdventureJournals(player, "ADVENTURE_JOURNALS_SECONDARY_PROFESSION_" ..  string.upper(ability:GetName()),
				{
					name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
					heroName = {type = "name", value = GetUnitName(context.hero)},
					abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()},
					itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. shopItem.name}
			})
			--Msg("wishingWater, stage=" .. (GameRules:GetGameModeEntity().currentStage - 1) ..  ", Get " .. shopItem.name .. " for secondary profession\n")
		end,
		buff = function ( player, ability, context )
			local type = context.config.type

			local equipmentAbilities = GameRules:GetGameModeEntity().equipmentAbilities
			local allAbilities = {}
			for i = 1, #equipmentAbilities do
				if equipmentAbilities[i].type == type then
					table.insert(allAbilities, equipmentAbilities[i])
				end
			end

			if type == BUFF then
				table.insert(allAbilities, {name = "self_luck", type = BUFF})
			else
				table.insert(allAbilities, {name = "self_unluck", type = DEBUFF})
			end

			--Msg("allAbilities=" .. TableToStr(allAbilities) .. "\n")
			local heros = GetAllHeroChessInChessBoard(player)
			local canAddAbilities = {}
			for j = 1, #allAbilities do
				local has = false
				for i = 1, #heros do
					local hero = heros[i]
					if hero:HasAbility(allAbilities[j].name) then
						has = true
					end
				end
				if has == false then
					table.insert(canAddAbilities, allAbilities[j])
				end
			end
			

			--Msg("canAddAbilities=" .. TableToStr(canAddAbilities) .. "\n")

			local random = RandomInt(1, #canAddAbilities)
			--Msg("random = " .. random .. "\n")
			local abilitiesConfig = canAddAbilities[random]
			if abilitiesConfig.name == "self_luck" or abilitiesConfig.name == "self_unluck" then
				AddAbility(context.hero, abilitiesConfig.name, ability:GetLevel(), false)
			else
				AddAbility(context.hero, abilitiesConfig.name, RandomInt(3, 4), false)
			end

			SendHintMessageToPlayer(player, 
				"SECONDARY_PROFESSION_HINT_REWARD_BUFF", 
				{
					heroName = {type = "name", value = GetUnitName(context.hero)},
					abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()},
					gainedAbilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. abilitiesConfig.name}
				},
				true,
				"tutorial_fanfare"
			)

			SendAdventureJournals(player, "ADVENTURE_JOURNALS_SECONDARY_PROFESSION_" ..  string.upper(ability:GetName()),
				{
					name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
					heroName = {type = "name", value = GetUnitName(context.hero)},
					abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()},
					gainedAbilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. abilitiesConfig.name}
			})

			--Msg("buff, stage=" .. (GameRules:GetGameModeEntity().currentStage - 1) ..  ", Get ability " .. abilitiesConfig.name .. " for secondary profession\n")
		end,
	}

	local secondaryProfessionHandlers = {
		profession_businessman = {
			onBattleOver = commonHandlers.everyCountRounds,
			handle = rewordHandlers.gold
		},
		invest = {
			onGetInterest = function( player, ability, context )
				local interest = ability:GetSpecialValueFor("interest")
				context.interest = context.interest + interest
			end
		},
		business = {
			onBattleOver = commonHandlers.streakWinEnd,
			handle = rewordHandlers.gold
		},
		profession_lord = {
			onBattleOver = commonHandlers.everyCountRounds,
			handle = rewordHandlers.gold
		},
		profession_pharmaceutist = {
			onBattleOver = commonHandlers.everyCountRounds,
			handle = rewordHandlers.drug
		},
		forge = {
			onBattleOver = commonHandlers.streakWinEnd,
			handle = rewordHandlers.equipment
		},
		profession_navigator = {
			onBattleOver = commonHandlers.everyCountRounds,
			handle = rewordHandlers.all
		},
		explore = {
			onBattleOver = commonHandlers.streakWinEnd,
			handle = rewordHandlers.all
		},
		storm_sailing = {
			onBattleOver = commonHandlers.streakWinEnd,
			handle = rewordHandlers.levelUp
		},
		prophecy = {
			onBattleOver = commonHandlers.streakWinEnd,
			handle = rewordHandlers.itemIgnoreExistCount,
			itemName = "item_wishing_water_for_ability"
		},
		big_prophecy = {
			onBattleOver = commonHandlers.streakWinEnd,
			handle = rewordHandlers.itemIgnoreExistCount,
			itemName = "item_wishing_water"
		},
		luck = {
			onBattleStart = commonHandlers.probability,
			handle = rewordHandlers.buff,
			type = BUFF,
		},
		unluck = {
			onBattleStart = commonHandlers.probability,
			handle = rewordHandlers.buff,
			type = DEBUFF,
		}
	}

	GameRules:GetGameModeEntity().secondaryProfessionHandlers = secondaryProfessionHandlers
end

function ApplySecondaryProfessionAbilityForHero(player, hero, handlerName, context )
	local secondaryProfessionHandlers = GameRules:GetGameModeEntity().secondaryProfessionHandlers
	context = context or {}
	context.hero = hero
	for i = 1, hero.abilitiesCount do
		local abilityName = hero.abilities[i]
		if IsSecondaryProfession(abilityName) then
			local ability = hero:FindAbilityByName(abilityName)
			local handlerConfig = secondaryProfessionHandlers[abilityName]
			if handlerConfig ~= nil and handlerConfig[handlerName] ~= nil then
				context.config = handlerConfig
				handlerConfig[handlerName](player, ability, context, handlerConfig.handle)
			end
		end
	end
end

function ApplySecondaryProfessionAbility( player, handlerName, context )
	local heros = GetAllHeroChessInChessBoard(player)
	for i = 1, #heros do
		ApplySecondaryProfessionAbilityForHero(player, heros[i], handlerName, context)
	end
end

function GetMaxInterest(player)
	local context = {}
	context.interest = 5
	ApplySecondaryProfessionAbility(player, "onGetInterest", context)
	return context.interest
end

function InitAuctions(  )
	local auctions
	if IsCorpsMode() then
		auctions = {
			[31] = {--31
				count = 3,
				qualities = {QUALITY_4, QUALITY_5},
				types = {TYPE_ABILITY, TYPE_CONSUMABLE} 
			},
			[51] = {
				count = 4,
				qualities = {QUALITY_4, QUALITY_5},
				types = {TYPE_ABILITY, TYPE_CONSUMABLE, TYPE_EQUIPMENT} 
			},
			[71] = {
				count = 5,
				qualities = {QUALITY_5},
				types = {TYPE_ABILITY, TYPE_CONSUMABLE, TYPE_EQUIPMENT} 
			}
		}
	else
		auctions = {
			[21] = {--31
				count = 3,
				qualities = {QUALITY_4, QUALITY_5},
				types = {TYPE_ABILITY, TYPE_EQUIPMENT} 
			},
			[41] = {
				count = 4,
				qualities = {QUALITY_5},
				types = {TYPE_CONSUMABLE, TYPE_EQUIPMENT} 
			}
		}
	end
	GameRules:GetGameModeEntity().auctions = auctions
	local businessman = {}
	businessman.nameInfo = {
		type = "random",
		sex = MAN,
		name = GenerateName(sex)
	}

	businessman.name = {
		name = {type = "name", value = businessman.nameInfo}
	}

	local businesswoman = {}
	businesswoman.nameInfo = {
		type = "random",
		sex = WOMAN,
		name = GenerateName(sex)
	}

	businesswoman.name = {
		name = {type = "name", value = businesswoman.nameInfo}
	}

	local businessmen = {}
	table.insert(businessmen, businessman)
	table.insert(businessmen, businesswoman)
	GameRules:GetGameModeEntity().businessmen = businessmen
end

function InitAuction( auction )
	local auctionStatus = {}
	auctionStatus.auctionItems = {}
	for i = 1, auction.count do
		local shopItems = GetShopItemsForQuests(auction.qualities, auction.types)
		local random = RandomInt(1, #shopItems)
		local shopItem = shopItems[random]
		table.insert(auctionStatus.auctionItems, {price = shopItem.cost, shopItem = shopItem})
		shopItem.existCount = shopItem.existCount + 1
	end
	auctionStatus.currentAuctionIndex = 1
	auctionStatus.timeout = DEFAULT_AUCTION_TIMEOUT
	GameRules:GetGameModeEntity().auctionStatus = auctionStatus

	SendHintMessageToAllPlayers(
		"AUCTION_START",
		{
			itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. auctionStatus.auctionItems[auctionStatus.currentAuctionIndex].shopItem.name}
		},
		true,
		"auction_start"
	)

	SendAdventureJournalsToAllPlayers("ADVENTURE_JOURNALS_AUCTION_INIT", {})

	SendAdventureJournalsToAllPlayers("ADVENTURE_JOURNALS_AUCTION_START",
		{
			itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. auctionStatus.auctionItems[auctionStatus.currentAuctionIndex].shopItem.name}
		}
	)

	local businessmen = GameRules:GetGameModeEntity().businessmen
	for i = 1, #businessmen do
		local businessman = businessmen[i]
		businessman.gold = #auctionStatus.auctionItems * 10
		businessman.endFlag = false
	end

	auctionStatus.businessmen = businessmen

	CustomUI:DynamicHud_Create(-1, "AuctionPanel", "file://{resources}/layout/custom_game/auction.xml", nil)
	CustomGameEventManager:Send_ServerToAllClients("on_auction_status_update", auctionStatus)
end

function GetAuctionPlayer( playerId )
	if playerId ~= -1 then
		return PlayerResource:GetPlayer(playerId)
	else
		return nil
	end
end

function GetAuctionCourier( player, keys )
	if player ~= nil then
		return player:GetAssignedHero() 
	else
		return keys.businessman
	end
end

function IsCourier( courier )
	if courier.GetMana ~= nil then
		return true
	else
		return false
	end
end

function GetAuctionCourierGold( courier )
	if IsCourier(courier) then
		return courier:GetMana()
	else
		return courier.gold
	end
end

function AddAuctionCourierGold( courier, gold )
	if IsCourier(courier) then
		courier:SetMana(courier:GetMana() + gold)
	else
		courier.gold = courier.gold + gold
	end
end

function GetAuctionNameInfo( player, courier )
	if player ~= nil then
		return {
			name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
		}
	else
		return courier.name
	end
end

function BidAuctionItem( index, keys )
	local player = GetAuctionPlayer(keys.PlayerID)
	local auctionStatus = GameRules:GetGameModeEntity().auctionStatus
	local auctionItem = auctionStatus.auctionItems[auctionStatus.currentAuctionIndex]
	local courier = GetAuctionCourier(player, keys)
	if GetAuctionCourierGold(courier) <= auctionItem.price then
		SendErrorMessageToPlayer(player, "NOT_ENOUGH_MONEY_TO_BID")
		return
	else
		if auctionStatus.courier ~= nil then
			AddAuctionCourierGold(auctionStatus.courier, auctionItem.price)
			if IsCourier(auctionStatus.courier) then
				local oldPlayerId = auctionStatus.courier:GetPlayerID()
				local oldPlayer = PlayerResource:GetPlayer(oldPlayerId)
				SendUpdateStatusMessage(oldPlayer)
			end
		end
	end
	auctionItem.price = auctionItem.price + 1
	AddAuctionCourierGold(courier, -auctionItem.price)
	auctionStatus.currentOwner = GetAuctionNameInfo(player, courier)
	-- use auctionStatus.player = player在OnBidAuctionItem里面程序会崩溃
	auctionStatus.courier = courier
	auctionStatus.timeout = DEFAULT_AUCTION_TIMEOUT
	if player ~= nil then
		SendUpdateStatusMessage(player)
	end
	SendHintMessageToAllPlayers(
		"BID_MESSAGE",
		{
			name = auctionStatus.currentOwner.name,
			gold = {type = "value", value = auctionItem.price}
		},
		true,
		"auction_bid"
	)
	SendAdventureJournalsToAllPlayers("ADVENTURE_JOURNALS_BID_MESSAGE",
		{
			name = auctionStatus.currentOwner.name,
			gold = {type = "value", value = auctionItem.price}
		}
	)
	CustomGameEventManager:Send_ServerToAllClients("on_auction_status_update", auctionStatus)
end

function OnBidAuctionItem( index, keys )
	local player = GetAuctionPlayer(keys.PlayerID)
	local auctionStatus = GameRules:GetGameModeEntity().auctionStatus
	local courier = GetAuctionCourier(player, keys)
	if auctionStatus.courier ~= courier then
		local businessmen = auctionStatus.businessmen
		for i = 1, #businessmen do
			local businessman = businessmen[i]
			businessman.endFlag = false
		end
	end

	BidAuctionItem(index, keys)
end

function RandomBidForBusinessmen(  )
	local auctionStatus = GameRules:GetGameModeEntity().auctionStatus
	local businessmen = auctionStatus.businessmen
	local auctionItem = auctionStatus.auctionItems[auctionStatus.currentAuctionIndex]
	--Msg("\nitemName=" .. auctionItem.shopItem.name .. "\n")

	if auctionStatus.timeout >= DEFAULT_AUCTION_TIMEOUT then
		return
	end

	local index = auctionStatus.timeout % #businessmen + 1
	local businessman = businessmen[index]
	if businessman.endFlag == false then
		local random = RandomInt(1, 30)
		--Msg("\nbusinessman i=" .. index .. " bid item, random=" .. random .. ", gold=" .. businessman.gold .. "\n")
		-- random 1-50, 加成剩余金额/5，加成12/剩余数量, quality * 2
		--30 / 5 = 6
		--12 / 1 = 12 12 / 2 = 6
		random = random + math.floor((businessman.gold - auctionItem.price) / 5)
		local leftItemCount = ((GetShopItemMaxCount(auctionItem.shopItem) - auctionItem.shopItem.existCount) + 1)
		if leftItemCount <= 0 then
			leftItemCount = 1
		end
		random = random + math.floor(12 / leftItemCount)
		random = random + QualityToNumber(auctionItem.shopItem.quality) * 2
		--Msg("\nbusinessman i=" .. index .. " bid item, random=" .. random .. ", gold=" .. businessman.gold
		--	.. ", leftItemCount=" .. leftItemCount .. "\n")
		local threshold = 30
		
		if (random > threshold and businessman.gold > auctionItem.price and auctionStatus.courier ~= businessman) then
			BidAuctionItem({}, {PlayerID = -1, businessman = businessman})
		elseif random <= threshold then
			businessman.endFlag = true
		end
	end
end

function IsAuctionEnd( )
	local auctionStatus = GameRules:GetGameModeEntity().auctionStatus
	if auctionStatus == nil or auctionStatus.currentAuctionIndex == nil or auctionStatus.auctionItems == nil then
		return true
	end
	return auctionStatus.currentAuctionIndex > #auctionStatus.auctionItems
end

function FinishAuction(  )
	local auctionStatus = GameRules:GetGameModeEntity().auctionStatus
	local auctionItem = auctionStatus.auctionItems[auctionStatus.currentAuctionIndex]
	if auctionStatus.courier ~= nil and IsCourier(auctionStatus.courier) then
		-- player成功竞拍
		local courier = auctionStatus.courier
		local playerId = courier:GetPlayerID()
		local player = PlayerResource:GetPlayer(playerId)
		CreateShopItem(player, auctionItem.shopItem, player:GetAssignedHero():GetAbsOrigin(), true)
	else
		-- npc竞拍或者流拍
		auctionItem.shopItem.existCount = auctionItem.shopItem.existCount - 1
	end

	if auctionStatus.courier ~= nil then
		SendHintMessageToAllPlayers(
			"AUCTION_DONE_MESSAGE",
			{
				name = auctionStatus.currentOwner.name,
				gold = {type = "value", value = auctionItem.price},
				itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. auctionItem.shopItem.name}
			},
			true,
			"auction_done"
		)
		SendAdventureJournalsToAllPlayers("ADVENTURE_JOURNALS_AUCTION_DONE_MESSAGE",
			{
				name = auctionStatus.currentOwner.name,
				gold = {type = "value", value = auctionItem.price},
				itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. auctionItem.shopItem.name}
			}
		)
	else
		SendHintMessageToAllPlayers(
			"AUCTION_FAILED_MESSAGE",
			{
				itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. auctionItem.shopItem.name}
			},
			true,
			"auction_fail"
		)
		SendAdventureJournalsToAllPlayers("ADVENTURE_JOURNALS_AUCTION_FAILED_MESSAGE",
			{
				itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. auctionItem.shopItem.name}
			}
		)
	end

	auctionStatus.currentAuctionIndex = auctionStatus.currentAuctionIndex + 1
	auctionStatus.currentOwner = nil
	auctionStatus.courier = nil
	auctionStatus.timeout = DEFAULT_AUCTION_TIMEOUT
	local businessmen = auctionStatus.businessmen
	for i = 1, #businessmen do
		local businessman = businessmen[i]
		businessman.endFlag = false
	end

	if IsAuctionEnd() then
		CustomUI:DynamicHud_Destroy(-1, "AuctionPanel")
		auctionStatus = nil
		SendAdventureJournalsToAllPlayers("ADVENTURE_JOURNALS_AUCTION_END", {})
	else
		CustomGameEventManager:Send_ServerToAllClients("on_auction_status_update", auctionStatus)
		SendHintMessageToAllPlayers(
			"AUCTION_START",
			{
				itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. auctionStatus.auctionItems[auctionStatus.currentAuctionIndex].shopItem.name}
			},
			true,
			"auction_start"
		)

		SendAdventureJournalsToAllPlayers("ADVENTURE_JOURNALS_AUCTION_START",
			{
				itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. auctionStatus.auctionItems[auctionStatus.currentAuctionIndex].shopItem.name}
			}
		)
	end

end

function ClearAllEnemiesForPlayer( player )
	for i = 1, player.enemiesCount do
		if player.enemies[i]:IsNull() == false then
			player.enemies[i]:Destroy()
		end
		player.enemies[i] = nil
	end
	player.enemiesCount = 0
end

function GetTotalLevelOfAliveEnemies( player )
	local totalLevel = 0
	for i = 1, player.enemiesCount do
		local enemy = player.enemies[i]
		if IsUnitAlive(enemy) then
			totalLevel = totalLevel + enemy:GetLevel()
		end
	end

	return totalLevel
end

function GetAliveEnemies( player )
	local enemies = {}
	for i = 1, player.enemiesCount do
		local enemy = player.enemies[i]
		if IsUnitAlive(enemy) then
			table.insert(enemies, enemy)
		end
	end

	return enemies
end

function IsHerosInChessBoardAlive( player )
	local heros = GetAllHeroChessInChessBoard(player)
	for i = 1, #heros do
		if heros[i]:IsAlive() then
			return true
		end
	end

	if player.otherUnits ~= nil then
		for i = 1, #player.otherUnits do
			if player.otherUnits[i]:IsNull() == false and player.otherUnits[i]:IsAlive() then
				return true
			end
		end
	end
	return false
end

function IsHerosInChessBoardAliveForCombinedBattle( )
	local players = GetAllAlivePlayers()
	for i = 1, #players do
		if IsHerosInChessBoardAlive(players[i]) then
			return true
		end
	end
	
	return false
end

function GetUnitsInChessBoardAlive( player )
	local heros = GetAllHeroChessInChessBoard(player)
	local units = {}
	for i = 1, #heros do
		if heros[i]:IsAlive() then
			table.insert(units, heros[i])
		end
	end

	if player.otherUnits ~= nil then
		for i = 1, #player.otherUnits do
			if player.otherUnits[i]:IsNull() == false and player.otherUnits[i]:IsAlive() then
				table.insert(units, player.otherUnits[i])
			end
		end
	end
	return units
end

function GetUnitsInChessBoardAliveForCombinedBattle( )
	local players = GetAllAlivePlayers()
	local units = {}
	for i = 1, #players do
		local unitsOfPlayer = GetUnitsInChessBoardAlive(players[i])
		MergeTable(units, unitsOfPlayer)
	end
	
	return units
end

function GetMaintenance( player )
	local heros = GetAllHeroChessWhichIsNotTemp(player)
	local maintenance = 0
	for i = 1, #heros do
		local hero = heros[i]
		if hero.canSell ~= false then
			maintenance = maintenance + 1
		end
	end

	if maintenance < 0 then
		return 0
	else
		return maintenance
	end
end

function CountGold( player, courier, status )
	local currentGold = courier:GetMana()
	if status == "win" then
		currentGold = currentGold + 1 + 1
	else
		currentGold = currentGold + 1
	end

	local courierLevel = courier:GetLevel()
	if courierLevel >= 8 then
		currentGold = currentGold + 1
	end

	if courierLevel >= 10 then
		currentGold = currentGold + 1
	end
	--Msg("after 1, currentGold=" .. currentGold .. "\n")

	-- 利息
	local interest = math.floor(currentGold / 10)
	if interest > GetMaxInterest(player) then
		interest = GetMaxInterest(player)
	end
	--Msg("Current interest=" .. interest .. ", currentGold=" .. currentGold .. "\n")
	currentGold = currentGold + interest
	--Msg("after interest, currentGold=" .. currentGold .. "\n")

	if status == "win" then
		--连胜
		if courier.winningStreak > 5 then
			currentGold = currentGold + 4
		else
			currentGold = currentGold + courier.winningStreak - 1
		end
	elseif status == "fail" then
		--连败
		if courier.losingStreak > 5 then
			currentGold = currentGold + 4
		else
			currentGold = currentGold + courier.losingStreak - 1
		end
	end

	--Msg("after streak, currentGold=" .. currentGold .. "\n")

	currentGold = currentGold - GetMaintenance(player)
	--Msg("after not temp, currentGold=" .. currentGold .. "\n")

	local heros = GetAllHeroChessInChessBoard(player)
	table.insert(heros, courier)
	local combinedArtifactConfig = {name = "cornucopia", needs={"item_cornucopia_1", "item_cornucopia_2", "item_cornucopia_3", "item_cornucopia_4"}}
	local hasCornucopia = false
	for i = 1, #heros do
		if HasCombinedArtifact(heros[i], combinedArtifactConfig) then
			hasCornucopia = true
			break
		end
	end
	if hasCornucopia then
		currentGold = currentGold + 5
	end
	--丰收之角
	courier:SetMana(currentGold)
end

function GetShopItemMaxCount(shopItem)
	local quality = shopItem.quality
	if shopItem.maxCount ~= nil then
		return shopItem.maxCount
	end

	if quality == QUALITY_5 then
		return 5
	elseif quality == QUALITY_4 then
		return 8
	elseif quality == QUALITY_3 then
		return 11
	elseif quality == QUALITY_2 then
		return 14
	else
		return 17
	end
end

function GetCanUpgradeAbilities( heros )
	local shopItems = GameRules:GetGameModeEntity().shopItems
	local canUpgradeAbilites = {}
	for i = 1, #heros do
		local hero = heros[i]
		if hero.abilitiesCount ~= nil then
			for j = 1, hero.abilitiesCount do
				local abilityName = hero.abilities[j]
				local ability = hero:FindAbilityByName(abilityName)
				if ability ~= nil then -- to avoid 真熊形态出bug
					local shopItem = FindAbilityShopItem(shopItems)
					if shopItem ~= nil and (GetShopItemMaxCount(shopItem) - shopItem.existCount) > 0 
						and ability:GetLevel() < 5 then
						table.insert(canUpgradeAbilites, {ability = ability, shopItem = shopItem, hero = hero})
					end
				end
			end
		end
	end

	return canUpgradeAbilites
end

function RandomUpgradeHerosAbility( player )
	local heros = GetAllHeroChessInChessBoard(player)
	if #heros == 0 then
		return
	end
	local canUpgradeAbilites = GetCanUpgradeAbilities(heros)
	if #canUpgradeAbilites == 0 then
		return
	end
	local random = RandomInt(1, #canUpgradeAbilites)
	local ability = canUpgradeAbilites[random].ability
	local abilityShopItem = canUpgradeAbilites[random].shopItem
	abilityShopItem.existCount = abilityShopItem.existCount + 1
	ability:UpgradeAbility(false)
	EmitSoundForAbilityUpgrade(player)
	--GameRules:SendCustomMessage("#CANNOT_SET_CHESS_HERE", PlayerResource:GetTeam(player:GetPlayerID()), player:GetPlayerID())
	--Msg("hero=" .. canUpgradeAbilites[random].hero:GetName() .. ", abilityName=" .. ability:GetName() .. "\n")
	local hero = canUpgradeAbilites[random].hero
	SendHintMessageToPlayer(player, 
		"RANDOM_UPGRADE_ABILITY", 
		{
			heroName = {type = "name", value = GetUnitName(hero)}, 
			abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()}
		},
		false
	)
	SendAdventureJournals(player, "ADVENTURE_JOURNALS_RANDOM_UPGRADE",
		{
			name = {type = "name", value = GetUnitName(hero)},
			abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()},
			level = {type = "value", value = ability:GetLevel()}
		})
end

function ClearAllUnitsWhenCourierDead( player )
	ClearAllEnemiesForPlayer(player)

	local heros = GetAllHeroChess(player)
	for i = 1, #heros do
		local hero = heros[i]
		if hero:HasModifier("modifier_lone_druid_true_form") then
			hero:RemoveModifierByName("modifier_lone_druid_true_form")
		end

		RemoveAbilityForCombinedArtifact(hero)
		ClearLegendAbilities(hero)
		ClearOtherAbilities(hero)

		if hero.temp then
			if hero.canBuy == true then
				DestroyChess(player, hero, true)
			else
				DestroyChess(player, hero, false)
			end
		else
			hero:ForceKill(false)
		end
	end

	if player.otherUnits ~= nil then
		for i = 1, #player.otherUnits do
			if player.otherUnits[i]:IsNull() == false then
				player.otherUnits[i]:Destroy()
			end
		end

		player.otherUnits = nil
	end
end

function MakeTeamLose( player )
	ClearAllUnitsWhenCourierDead(player)
end

function OnBattleOver(player, isCombinedBattle) 
	local totalLevel = GetTotalLevelOfAliveEnemies(player)
	local courier = player:GetAssignedHero()
	local hasHeroAlive = false
	if isCombinedBattle then
		hasHeroAlive = IsHerosInChessBoardAliveForCombinedBattle()
	else
		hasHeroAlive = IsHerosInChessBoardAlive(player)
	end
	
	local status = nil

	if player.endBattle then
		hasHeroAlive = false
	end

	if totalLevel > 0 and hasHeroAlive == false then
		-- fail
		status = "fail"
		totalLevel = math.min(20, totalLevel) --限制死得太快
		SendAdventureJournalsForBattleResult(player, status, isCombinedBattle)
		--totalLevel = 50
		if courier:GetHealth() - totalLevel > 0 then
			courier:SetHealth(courier:GetHealth() - totalLevel)
		else
			courier:ForceKill(false)
			player.lose = true
			MakeTeamLose(player)
			SendAdventureJournals(player, "ADVENTURE_JOURNALS_GAME_OVER",
			{
				playerName = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
				result = {type = "text", value = "ADVENTURE_JOURNALS_GAME_OVER_ENDING"}
			})

			CustomGameEventManager:Send_ServerToPlayer(player, "on_game_over", {})
		end
		courier.winningStreak = 0
		courier.losingStreak = courier.losingStreak + 1
		if courier:IsAlive() then
			CountGold(player, courier, status)
		end
		GetPlayerLogs(player).numberOfLose = (GetPlayerLogs(player).numberOfLose or 0) + 1
		if (GameRules:GetGameModeEntity().currentStage - 1) % 10 == 0 then
			GetPlayerLogs(player).bossStageOfLose = (GetPlayerLogs(player).bossStageOfLose or "") .. (GameRules:GetGameModeEntity().currentStage - 1) .. " "
		end
		GetPlayerLogs(player).maxLosingStreak = math.max(GetPlayerLogs(player).maxLosingStreak or 0, courier.losingStreak)
		SendHintMessageToPlayer(player, 
			"BATTLE_LOSE", 
			nil,
			true,
			"UI.Bounty.Failed"
		)
		
	elseif totalLevel == 0 and hasHeroAlive then
		status = "win"
		courier.winningStreak = courier.winningStreak + 1
		courier.losingStreak = 0
		CountGold(player, courier, status)
		GetPlayerLogs(player).numberOfWins = (GetPlayerLogs(player).numberOfWins or 0) + 1
		GetPlayerLogs(player).maxWinningStreak = math.max(GetPlayerLogs(player).maxWinningStreak or 0, courier.winningStreak)
		SendHintMessageToPlayer(player, 
			"BATTLE_WIN", 
			nil,
			true,
			"ui.weekend_tourney_ready_up"
		)
		SendAdventureJournalsForBattleResult(player, status, isCombinedBattle)

		local probability = GameRules:GetGameModeEntity().abilityUpgradeProbability[courier:GetLevel()]
		local random = RandomInt(1, 100)
		if random <= probability then
			RandomUpgradeHerosAbility(player)
		end

	else
		--打平
		status = "draw"
		courier.winningStreak = 0
		courier.losingStreak = 0
		CountGold(player, courier, status)
		GetPlayerLogs(player).numberOfDraw = (GetPlayerLogs(player).numberOfDraw or 0) + 1
		SendHintMessageToPlayer(player, 
			"BATTLE_DRAW", 
			nil,
			true,
			"UI.Bounty.Claimed"
		)
		SendAdventureJournalsForBattleResult(player, status, isCombinedBattle)
	end

	GetPlayerLogs(player).currentStage = GameRules:GetGameModeEntity().currentStage - 1
	if courier:IsAlive() then
		EmitSoundForSell(player)
	end
	SendUpdateStatusMessage(player)

	if player.quests ~= nil then
		for i = 1, #player.quests do
			local quest = player.quests[i]
			if quest.quest.onBattleOver ~= nil then
				quest.quest.onBattleOver(quest, player, GameRules:GetGameModeEntity().currentStage - 1, status)
			end

			if quest.endFlag ~= true and quest.context.stage ~= nil and quest.context.stage.value < GameRules:GetGameModeEntity().currentStage - 1 then
				if quest.rewordConfig.onCancel ~= nil then
					quest.rewordConfig.onCancel(quest)
				end
				quest.status = "Timeout"
				SendAdventureJournals(player, "ADVENTURE_JOURNALS_QUEST_TIMEOUT",
					{
						name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
						questName = {type = "text", value = quest.quest.type .. "_TITLE"}
				})
				SendQuestsUpdateMessage(player)
				quest.endFlag = true
			end
		end
	end

	ApplySecondaryProfessionAbility(player, "onBattleOver", {status = status})
end

function RemoveAbilityAndModifier( unit, name )
	unit:RemoveAbility(name)
	unit:RemoveModifierByName("modifier_" .. name)
	--Msg("RemoveAbilityAndModifier name=" .. name .. "\n")
	unit:ClearActivityModifiers()
end

function RemoveAbility(unit, name)
	RemoveAbilityAndModifier(unit, name)
	for i = 1, unit.abilitiesCount do
		if unit.abilities[i] == name then
			table.remove(unit.abilities, i)
			unit.abilitiesCount = unit.abilitiesCount - 1
			return
		end
	end
end

function ClearCooldown( unit )
	for i = 1, unit:GetAbilityCount() do
		if i < 32 then
			local ability = unit:GetAbilityByIndex(i)
			if ability ~= nil then
				ability:EndCooldown()
			end
		end

	end
end

function ApplyHeroPrimaryAttributes( unit )
	if unit:IsNull() == false and unit:IsHero() then
		for i = 1, unit:GetAbilityCount() do
			if i < 32 then
				local ability = unit:GetAbilityByIndex(i)
				if ability ~= nil then
					--Msg("ability.name=" .. ability:GetName() .. "\n")
					local result = ApplyPrimaryAttribute(unit, ability:GetName())
					if result then
						return result
					end
				end
			end

		end
	end

	return false
end

function HasItemInSlot( unit, itemName )
	for i = 0, 8 do
		local item = unit:GetItemInSlot(i)
		if item ~= nil and item:GetName() == itemName then
			return true
		end
	end
	return false
end

function GetItemInSlot(unit, itemName) 
	for i = 0, 8 do
		local item = unit:GetItemInSlot(i)
		if item ~= nil and item:GetName() == itemName then
			return item
		end
	end
	return nil
end

function HasCombinedArtifact(unit, combinedArtifactConfig )
	local hasAll = true
	for j = 1, #combinedArtifactConfig.needs do
		local needEquipment = combinedArtifactConfig.needs[j]
		if HasItemInSlot(unit, needEquipment) == false then
			hasAll = false
			break
		end
	end

	if hasAll then
		return true
	else
		return false
	end
end

function ApplyAbilityForCombinedArtifact( unit )
	local equipmentAbilities = GameRules:GetGameModeEntity().equipmentAbilities
	for i = 1, #equipmentAbilities do
		local equipmentAbility = equipmentAbilities[i]
		local hasAll = true
		for j = 1, #equipmentAbility.needs do
			local needEquipment = equipmentAbility.needs[j]
			if HasItemInSlot(unit, needEquipment) == false then
				hasAll = false
				break
			end
		end

		if hasAll then
			--Msg("Add CombinedArtifact ability, ability=" .. equipmentAbility.name .. "\n")
			AddAbility(unit, equipmentAbility.name, 5, false)
		end
	end
end

function RemoveAbilityForCombinedArtifact(unit)
	local equipmentAbilities = GameRules:GetGameModeEntity().equipmentAbilities
	for i = 1, #equipmentAbilities do
		local equipmentAbility = equipmentAbilities[i]
		if unit:FindAbilityByName(equipmentAbility.name) then
			RemoveAbilityAndModifier(unit, equipmentAbility.name)
		end
	end

end

function CreatePhantom( player, unit, isEnemy)

	if unit:IsHero() == false then
		return	
	end

	local ability = unit:FindAbilityByName("phantom")
	if ability == nil then
		return
	end

	local count = ability:GetSpecialValueFor("phantom_count")
	local inherit_percent = ability:GetSpecialValueFor("inherit_percent")

	local selectedPositions = FindEmptyPositions( player, unit:GetAbsOrigin(), count)
	if #selectedPositions == 0 then
		SendErrorMessageToPlayer(PlayerResource:GetPlayer(player:GetPlayerID()), "MAGIC_FAILED_FOR_PHANTOM")
		return
	end
	
	for i = 1, count do
		GameRules:GetGameModeEntity().createUnit = true
		local phantom
		if isEnemy then
			phantom = CreateHeroForPlayer(unit:GetName(), player.enemyPlayer)
			phantom.player = player
		else
			phantom = CreateHeroForPlayer(unit:GetName(), player)
		end
		 
		phantom.abilities = {}
		phantom.abilitiesCount = 0
		GameRules:GetGameModeEntity().createUnit = false
		InitPhantomName(phantom, unit)
		player.otherUnits = player.otherUnits or {}
		if isEnemy then
			player.enemiesCount = player.enemiesCount + 1
			player.enemies[player.enemiesCount] = phantom
		else
			table.insert(player.otherUnits, phantom)
		end
		
		phantom:SetTeam(unit:GetTeam())
		InitUnit(phantom, true)
		phantom:SetAbsOrigin(selectedPositions[i])
		phantom:SetBaseAgility(math.floor(unit:GetAgility() * inherit_percent / 100))
		phantom:SetBaseIntellect(math.floor(unit:GetIntellect() * inherit_percent / 100))
		phantom:SetBaseStrength(math.floor(unit:GetStrength() * inherit_percent / 100))
		local primaryAttribute = unit:GetPrimaryAttribute()
		player:SetContextThink(DoUniqueString("init_phantom"), 
			function()
				if IsUnitAlive(phantom) then 
					phantom:SetPrimaryAttribute(primaryAttribute)
				end
				--GameRules:GetGameModeEntity().createUnit = true
				--phantom:RespawnHero(false, false)
				--GameRules:GetGameModeEntity().createUnit = false
					
				--phantom:SetAbsOrigin(selectedPositions[i])
			end, 0.3
		)
	end

end

function StartBattle( player )
	-- Msg("StartBattle\n")
	local data = {}

	--local showDetailInfo = (GameRules:GetGameModeEntity().currentStage % 5 == 0)
	local showDetailInfo = false
	if showDetailInfo then
		data.adventureJournals = BuildAdventureJournals("ADVENTURE_JOURNALS_START_BATTLE")
		table.insert(data.adventureJournals, BuildAdventureJournal("ADVENTURE_JOURNALS_ENEMIES_INFO"))
	else
		data.adventureJournals = BuildAdventureJournals("ADVENTURE_JOURNALS_ENEMIES_BRIEF_INFO")
		data.adventureJournals[1].newLine = false
	end

	local enemiesCount = player.enemiesCount
	for i = 1, enemiesCount  do 
		if player.enemies ~= nil then
			local enemy = player.enemies[i]
			RemoveAbilityAndModifier(enemy, "invulnerable")
			RemoveAbilityAndModifier(enemy, "silenced")
			enemy:ClearActivityModifiers()
			ClearCooldown(enemy)
			local result = ApplyHeroPrimaryAttributes(enemy)
			--Msg("result=" .. (result and "true" or "false") .. "\n")
			--[[if ApplyHeroPrimaryAttributes(enemy) then
				local location = enemy:GetAbsOrigin()
				GameRules:GetGameModeEntity().createUnit = true
				enemy:RespawnHero(false, false)
				GameRules:GetGameModeEntity().createUnit = false
				enemy:SetAbsOrigin(location)
			end]]--
			ApplyAbilityForCombinedArtifact(enemy)
			ApplyLegendAbilities(enemy)
			CreatePhantom(player, enemy, true)

			if showDetailInfo then
				if enemy:IsHero() then
					MergeTable(data.adventureJournals, BuildAdventureJournalsForHeroInfo(enemy))
				else
					MergeTable(data.adventureJournals, BuildAdventureJournalsForUnitInfo(enemy))
				end
			else
				local adventureJournal = BuildAdventureJournal("ADVENTURE_JOURNALS_BRIEF_NAME_INFO")
				adventureJournal.tokens = {name = {type = "name", value = GetUnitName(enemy)}}
				adventureJournal.newLine = false
				table.insert(data.adventureJournals, adventureJournal)
			end
			
		end
	end

	if showDetailInfo then
		table.insert(data.adventureJournals, BuildAdventureJournal("ADVENTURE_JOURNALS_FRIENDS_INFO"))
	else
		table.insert(data.adventureJournals, BuildAdventureJournal("ADVENTURE_JOURNALS_NEW_LINE"))
		local adventureJournal = BuildAdventureJournal("ADVENTURE_JOURNALS_FRIENDS_INFO")
		table.insert(data.adventureJournals, adventureJournal)
		adventureJournal.newLine = false
	end
	
	local heros = GetAllHeroChessInChessBoard(player)
	for i = 1, #heros do
		local hero = heros[i]
		RemoveAbilityAndModifier(hero, "invulnerable")
		RemoveAbilityAndModifier(hero, "silenced")
		hero:SetControllableByPlayer(-1, false)
		hero:SetMoveCapability(DOTA_UNIT_CAP_MOVE_GROUND)
		hero:SetAttackCapability(hero.attackType)
		hero:SetTeam(DOTA_TEAM_GOODGUYS)
		hero:ClearActivityModifiers()
		ClearCooldown(hero)

		ApplyAbilityForCombinedArtifact(hero)
		ApplyLegendAbilities(hero)
		CreatePhantom(player, hero, false)


		if showDetailInfo then
			MergeTable(data.adventureJournals, BuildAdventureJournalsForHeroInfo(hero))
		else
			local adventureJournal = BuildAdventureJournal("ADVENTURE_JOURNALS_BRIEF_NAME_INFO")
			adventureJournal.tokens = {name = {type = "name", value = GetUnitName(hero)}}
			adventureJournal.newLine = false
			table.insert(data.adventureJournals, adventureJournal)
		end
	end

	table.insert(data.adventureJournals, BuildAdventureJournal("ADVENTURE_JOURNALS_NEW_LINE"))

	heros = GetAllHeroChess(player)
	for i = 1, #heros do
		local hero = heros[i]
		ApplyHeroPrimaryAttributes(hero)
		--[[
		if ApplyHeroPrimaryAttributes(hero) then
			local location = hero:GetAbsOrigin()
			GameRules:GetGameModeEntity().createUnit = true
			hero:RespawnHero(false, false)
			GameRules:GetGameModeEntity().createUnit = false
			hero:SetAbsOrigin(location)
		end]]--
	end

	SendAdventureJournalsEx(player, data)

	ApplySecondaryProfessionAbility(player, "onBattleStart", {})
end

function FindPlayerHero( heros )
	for i = 1, #heros do
		local hero = heros[i]
		if hero.canSell == false then
			return {index = i, hero = hero}
		end
	end

	return nil
end

function FindPlayerHeroByPlayerId( heros, playerId )
	for i = 1, #heros do
		local hero = heros[i]
		if hero.canSell == false and hero:GetPlayerID() then
			return hero
		end
	end

	return heros[1]
end

function RandomSelectHero( heros )
	local heroInfo = FindPlayerHero(heros)
	if heroInfo ~= nil then
		return heroInfo
	end

	if #heros <= 0 then
		return nil 
	end
	local random = RandomInt(1, #heros)
	return {index = random, hero = heros[random]}
end

function FindSpareIndex( spareIndexes, expectedIndex )
	for i = 1, #spareIndexes do
		local index = spareIndexes[i]
		if index.x == expectedIndex.x and index.y == expectedIndex.y then
			table.remove(spareIndexes, i)
			return expectedIndex
		end
	end

	local random = RandomInt(1, #spareIndexes)
	local index = spareIndexes[random]
	table.remove(spareIndexes, random)
	return index
end

function StartCombinedBattle( players )
	-- select heros to battle
	local heros = {}
	for i = 1, #players do
		local player = players[i]
		local herosOfPlayer = GetAllHeroChessInChessBoard(player)
		for j = 1, #herosOfPlayer do
			local hero = herosOfPlayer[j]
			table.insert(heros, hero)
		end
	end

	local selectedHeros = {}
	for i = 1, 6 do
		local heroInfo = RandomSelectHero(heros)
		if heroInfo == nil then
			break
		end	

		table.insert(selectedHeros, heroInfo.hero)
		table.remove(heros, heroInfo.index)
	end

	for i = 1, #heros do
		local hero = heros[i]
		hero:ForceKill(false)
	end

	--move chess to middle battle
	local indexes = {}
	local spareIndexes = {}
	for i = 1, 8 do
		for j = 3, 6 do
			table.insert(spareIndexes, {x = i, y = j})
		end
	end
	for i = 1, #selectedHeros do
		local hero = selectedHeros[i]
		indexes[i] = FindSpareIndex(spareIndexes, WorldPosToIndex(hero:GetPlayerID(), hero:GetAbsOrigin()))
	end

	for i = 1, #selectedHeros do
		local hero = selectedHeros[i]
		local position = hero:GetAbsOrigin()
		local worldPosition = IndexToWorldPos(-1, indexes[i])
		position.x = worldPosition.x
		position.y = worldPosition.y
		hero:SetAbsOrigin(position)
	end

	for i = 1, #players do
		if #selectedHeros > 0 then
			local player = players[i]
			PlayerResource:SetCameraTarget(players[i]:GetPlayerID(), selectedHeros[1])
			player:SetContextThink(DoUniqueString("camera_later"), 
				function() PlayerResource:SetCameraTarget(player:GetPlayerID(), nil)  end, 1)
		end
	end

end

function CanUnitFind( unit )
	if IsUnitAlive(unit) and unit:HasModifier('modifier_bounty_hunter_wind_walk') == false 
		and unit:HasModifier('invulnerable') == false 
		and unit:HasModifier('modifier_nyx_assassin_vendetta') == false
		and unit:HasModifier('modifier_juggernaut_omnislash_invulnerability') == false then
		return true
	else
		return false
	end
end

function FindClosestUnit( unit, units, maxDistance )
	local distance = maxDistance or 99999
	local closestUnit = nil
	for i = 1, #units do
		if CanUnitFind(units[i]) then
			local tmpDistance = (units[i]:GetAbsOrigin() - unit:GetAbsOrigin()):Length2D()
			-- Msg("tmpDistance=" .. tmpDistance .. ", distance=" .. distance .. "\n")
			if tmpDistance < distance then
				distance = tmpDistance
				closestUnit = units[i]
			end
		end
	end

	return closestUnit
end

function FindClosestUnitForIndex(player, index, units, maxDistance )
	local distance = maxDistance or 99999
	local closestUnit = nil
	local currentPosition = nil
	for i = 1, #units do
		if CanUnitFind(units[i]) then
			local worldPos
			if IsCombinedBattle() then
				worldPos = IndexToWorldPos(-1, index)
			else
				worldPos = IndexToWorldPos(player:GetPlayerID(), index)
			end
			 
			local position = Vector(worldPos.x, worldPos.y, units[i]:GetAbsOrigin().z)
			local tmpDistance = (units[i]:GetAbsOrigin() - position):Length2D()
			--Msg("tmpDistance=" .. tmpDistance .. ", distance=" .. distance .. "\n")
			if tmpDistance < distance then
				distance = tmpDistance
				closestUnit = units[i]
				currentPosition = position
			end
		end
	end

	return {closestUnit = closestUnit, distance = distance, positon = currentPosition}
end

function FindClosestAndMinHealthUnit( unit, units, maxDistance )
	local distance = maxDistance or 99999
	local closestUnit = nil
	local minHealth = 99999
	for i = 1, #units do
		if CanUnitFind(units[i]) then
			local tmpDistance = (units[i]:GetAbsOrigin() - unit:GetAbsOrigin()):Length2D()
			-- Msg("tmpDistance=" .. tmpDistance .. ", distance=" .. distance .. "\n")
			if tmpDistance < distance and units[i]:GetHealth() < minHealth then
				closestUnit = units[i]
				minHealth = units[i]:GetHealth()
			end
		end
	end

	return closestUnit
end

function FindClosestAndMinHealthUnitForDazzleShallowGrave( unit, units, maxDistance )
	local distance = maxDistance or 99999
	local closestUnit = nil
	local minHealth = 99999
	for i = 1, #units do
		if CanUnitFind(units[i]) and unit:HasModifier("modifier_dazzle_shallow_grave") == false then
			local tmpDistance = (units[i]:GetAbsOrigin() - unit:GetAbsOrigin()):Length2D()
			-- Msg("tmpDistance=" .. tmpDistance .. ", distance=" .. distance .. "\n")
			if tmpDistance < distance and units[i]:GetHealth() < minHealth then
				closestUnit = units[i]
				minHealth = units[i]:GetHealth()
			end
		end
	end

	return closestUnit
end

function FindClosestAndMinHealthUnitForCure( unit, units, maxDistance )
	local distance = maxDistance or 99999
	local closestUnit = nil
	local minHealth = 99999
	for i = 1, #units do
		if CanUnitFind(units[i]) then
			local tmpDistance = (units[i]:GetAbsOrigin() - unit:GetAbsOrigin()):Length2D()
			-- Msg("tmpDistance=" .. tmpDistance .. ", distance=" .. distance .. "\n")
			-- Msg("unit=" .. units[i]:GetName() .. ", percent=" .. units[i]:GetHealthPercent() .. "\n")
			if tmpDistance < distance and units[i]:GetHealth() < minHealth and units[i]:GetHealthPercent() < 90 then
				closestUnit = units[i]
				minHealth = units[i]:GetHealth()
			end
		end
	end

	return closestUnit
end

function IsUnitAlive( unit )
	if unit == nil or unit:IsNull() then
		return false
	else
		return unit:IsAlive() or (unit:IsHero() and unit:IsReincarnating())
	end
end

function CanReincarnation( unit )
	local ability = unit:FindAbilityByName("skeleton_king_reincarnation")
	if ability ~= nil and ability:IsCooldownReady() and unit:GetMana() >= ability:GetManaCost(-1) then
		return true
	else
		return false
	end
end

function IsBattleOver( player, isCombinedBattle )
	if player.endBattle then
		return true
	end
	local aliveEnemyCount = 0
	for i = 1, player.enemiesCount do 
		local enemy = player.enemies[i]
		if IsUnitAlive(enemy) then
			aliveEnemyCount = aliveEnemyCount + 1
		end
	end

	local aliveHeroCount = 0
	local heros = nil
	if isCombinedBattle then
		heros = GetAllHeroChessInChessBoardForCombinedBattle()
	else
		heros = GetAllHeroChessInChessBoard(player)
	end
	for i = 1, #heros do
		local hero = heros[i]
		if IsUnitAlive(hero) then
			aliveHeroCount = aliveHeroCount + 1
		end
	end

	if isCombinedBattle then
		local players = GetAllAlivePlayers()
		for j = 1, #players do
			local combinedPlayer = players[j]
			if player.otherUnits ~= nil then
				for i = 1, #combinedPlayer.otherUnits do
					if combinedPlayer.otherUnits[i]:IsNull() == false and combinedPlayer.otherUnits[i]:IsAlive() then
						aliveHeroCount = aliveHeroCount + 1
					end
				end
			end
		end
	else
		if player.otherUnits ~= nil then
			for i = 1, #player.otherUnits do
				if player.otherUnits[i]:IsNull() == false and player.otherUnits[i]:IsAlive() then
					aliveHeroCount = aliveHeroCount + 1
				end
			end
		end
	end
	

	if aliveEnemyCount == 0 or aliveHeroCount == 0 then
		return true
	else
		return false
	end
end

function IsAllPlayerBattleOver(isCombinedBattle)
	local players =  GetAllAlivePlayers()
	for i = 1, #players do
		if IsBattleOver(players[i], isCombinedBattle) == false then
			return false
		end
	end

	return true
end


DOTA_UNIT_ORDER_CAST_POSITION_FOR_WAVEFORM = 100
DOTA_UNIT_ORDER_CAST_POSITION_FOR_BLINK = 101
function InitAbilityExecutorConfigs( )
	local abilityExecutorConfigs = {
		{
			name = "axe_berserkers_call",
			prechecks = {{name="FindClosestUnit", maxDistance = 500}},
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "axe_culling_blade",
			prechecks = {{name="FindClosestAndMinHealthUnit", maxDistance = 200}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestAndMinHealthUnit"
		},
		{
			name = "life_stealer_rage",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "mars_gods_rebuke",
			prechecks = {{name="FindClosestUnit", maxDistance = 400}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit"
		},
		{
			name = "nyx_assassin_vendetta",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET},
			postExecute = function ( unit, units, enemyUnits, ability, abilityExecutorConfig)
				local target = FindClosestAndMinHealthUnit(unit, enemyUnits, 3000)
				if target == nil then
					target = FindClosestUnit(unit, enemyUnits)
				end
				if IsUnitAlive(target)
					and unit:HasModifier('modifier_treant_overgrowth') == false
					and unit:HasModifier('modifier_crystal_maiden_frostbite') == false then
					local newOrder = {
						UnitIndex = unit:entindex(), 
						OrderType = DOTA_UNIT_ORDER_ATTACK_TARGET,
						TargetIndex = target:entindex(), --Optional.  Only used when targeting units
						AbilityIndex = 0, --Optional.  Only used when casting abilities
						Position = nil, --Optional.  Only used when targeting the ground
						Queue = 1 --Optional.  Used for queueing up abilities
						}

					ExecuteOrderFromTable(newOrder)
					unit.target = target
				end
			end
		},
		{
			name = "bounty_hunter_shuriken_toss",
			prechecks = {{name="FindClosestAndMinHealthUnit", maxDistance = 700}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestAndMinHealthUnit"
		},
		{
			name = "phantom_assassin_phantom_strike",
			prechecks = {{name="FindClosestAndMinHealthUnit", maxDistance = 1000}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestAndMinHealthUnit"
		},
		{
			name = "templar_assassin_refraction",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "enchantress_natures_attendants",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "keeper_of_the_light_chakra_magic",
			prechecks = {{name="GetManaPercent", minPercent = 90}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "GetSelf"
		},
		{
			name = "chen_hand_of_god",
			prechecks = {{name="FindClosestAndMinHealthUnitForCure"}},
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "dazzle_shallow_grave",
			prechecks = {{name="FindClosestAndMinHealthUnitForDazzleShallowGrave", maxDistance = 1000}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestAndMinHealthUnitForDazzleShallowGrave"
		},
		{
			name = "dazzle_shadow_wave",
			prechecks = {{name="FindClosestAndMinHealthUnitForCure", maxDistance = 750}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestAndMinHealthUnitForCure"
		},
		{
			name = "drow_ranger_wave_of_silence",
			prechecks = {{name="FindClosestUnit", maxDistance = 400}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit"
		},
		{
			name = "windrunner_powershot",
			prechecks = {{name="FindClosestUnit", maxDistance = 2000}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit"
		},
		{
			name = "call_of_the_wild_hawk",
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "GetSelf"
		},
		{
			name = "beastmaster_wild_axes",
			prechecks = {{name="FindClosestUnit", maxDistance = 1400}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit",
			postExecute = function ( unit, units, enemyUnits, ability, abilityExecutorConfig)
				GameRules:GetGameModeEntity().waitCount = GameRules:GetGameModeEntity().waitCount + 1
				Timers:CreateTimer(2, function()
					GameRules:GetGameModeEntity().waitCount = GameRules:GetGameModeEntity().waitCount - 1
				end)
			end
		},
		{
			name = "call_of_the_wild_boar",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "beastmaster_primal_roar",
			prechecks = {{name="FindClosestUnit", maxDistance = 500}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestUnit"
		},
		{
			name = "treant_living_armor",
			prechecks = {{name="FindClosestAndMinHealthUnitForCure", maxDistance = 600}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestAndMinHealthUnitForCure"
		},
		{
			name = "tree_call",
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "GetSelf"
		},
		{
			name = "call_of_spirit_bear",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "lone_druid_true_form",
			prechecks = {{name="NoDeform"}},
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "treant_overgrowth",
			prechecks = {{name="FindClosestUnit", maxDistance = 1000}},
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},		
		{
			name = "abaddon_aphotic_shield",
			prechecks = {{name="FindClosestAndMinHealthUnitOfFriend", maxDistance = 300}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestAndMinHealthUnitOfFriend"
		},
		{
			name = "omniknight_repel",
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "GetSelf"
		},
		{
			name = "omniknight_purification",
			prechecks = {{name="FindClosestAndMinHealthUnitForCure", maxDistance = 600}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestAndMinHealthUnitForCure"
		},
		{
			name = "omniknight_guardian_angel",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "centaur_double_edge",
			prechecks = {{name="FindClosestUnit", maxDistance = 200}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestUnit"
		},
		{
			name = "juggernaut_blade_fury",
			prechecks = {{name="FindClosestUnit", maxDistance = 200}},
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "magnataur_shockwave",
			prechecks = {{name="FindClosestUnit", maxDistance = 1000}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit"
		},
		{
			name = "juggernaut_omni_slash",
			prechecks = {{name="FindClosestUnit", maxDistance = 200}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestUnit"
		},
		{
			name = "sniper_assassinate",
			prechecks = {{name="FindClosestAndMinHealthUnit", maxDistance = 2500}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestAndMinHealthUnit"
		},
		{
			name = "snapfire_scatterblast",
			prechecks = {{name="FindClosestUnit", maxDistance = 800}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit"
		},
		{
			name = "gyrocopter_flak_cannon",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "gyrocopter_call_down",
			prechecks = {{name="FindClosestAndMinHealthUnit", maxDistance = 2000}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestAndMinHealthUnit"
		},
		{
			name = "skywrath_mage_arcane_bolt",
			prechecks = {{name="FindClosestUnit", maxDistance = 850}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestUnit"
		},
		{
			name = "keeper_of_the_light_blinding_light",
			prechecks = {{name="FindClosestUnit", maxDistance = 1000}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit"
		},
		{
			name = "keeper_of_the_light_illuminate",
			prechecks = {{name="FindClosestUnit", maxDistance = 1600}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit"
		},
		{
			name = "prayer",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "shadow_shaman_voodoo",
			prechecks = {{name="FindClosestUnit", maxDistance = 800}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestUnit"
		},
		{
			name = "doom_bringer_doom",
			prechecks = {{name="FindClosestUnit", maxDistance = 800}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestUnit"
		},
		{
			name = "metamorphosis",
			prechecks = {{name="NoDeform"}},
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "morphling_waveform",
			prechecks = {{name="FindClosestUnit", maxDistance = 300}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION_FOR_WAVEFORM},
			target = "FindClosestUnit"
		},
		{
			name = "winter_wyvern_cold_embrace",
			prechecks = {{name="FindClosestAndMinHealthUnitForCure", maxDistance = 1000}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestAndMinHealthUnitForCure"
		},
		{
			name = "crystal_maiden_crystal_nova",
			prechecks = {{name="FindClosestUnit", maxDistance = 700}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit"
		},
		{
			name = "crystal_maiden_frostbite",
			prechecks = {{name="FindClosestUnit", maxDistance = 800}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestUnit"
		},
		{
			name = "jakiro_ice_path",
			prechecks = {{name="FindClosestUnit", maxDistance = 1200}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit"
		},
		{
			name = "fire_golem_summon",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "lina_dragon_slave",
			prechecks = {{name="FindClosestUnit", maxDistance = 1000}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit"
		},
		{
			name = "lina_light_strike_array",
			prechecks = {{name="FindClosestUnit", maxDistance = 750}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit"
		},
		{
			name = "abyssal_underlord_firestorm",
			prechecks = {{name="FindClosestUnit", maxDistance = 1000}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit"
		},
		{
			name = "ember_spirit_flame_guard",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "riki_smoke_screen",
			prechecks = {{name="FindClosestUnit", maxDistance = 900}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit"
		},
		{
			name = "windrunner_windrun",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "razor_eye_of_the_storm",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "haste",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "zuus_arc_lightning",
			prechecks = {{name="FindClosestUnit", maxDistance = 850}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestUnit"
		},
		{
			name = "zuus_lightning_bolt",
			prechecks = {{name="FindClosestUnit", maxDistance = 850}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestUnit"
		},
		{
			name = "zuus_thundergods_wrath",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "guardian_of_earth",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "absolute_defense",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "earthshaker_echo_slam",
			prechecks = {{name="FindClosestAndMinHealthUnit", maxDistance = 500}},
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "tidehunter_ravage",
			prechecks = {{name="FindClosestUnit", maxDistance = 1000}},
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "furion_wrath_of_nature",
			prechecks = {{name="FindClosestAndMinHealthUnit", maxDistance = 3000}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestAndMinHealthUnit"
		},
		{
			name = "necronomicon_warrior_summon",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "necronomicon_archer_summon",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "bone_dragon_summon",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "enigma_midnight_pulse",
			prechecks = {{name="FindClosestUnit", maxDistance = 700}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit",
			postExecute = function ( unit, units, enemyUnits, ability, abilityExecutorConfig)
				GameRules:GetGameModeEntity().waitCount = GameRules:GetGameModeEntity().waitCount + 1
				Timers:CreateTimer(ability:GetSpecialValueFor("duration") + 1, function()
					GameRules:GetGameModeEntity().waitCount = GameRules:GetGameModeEntity().waitCount - 1
				end)
			end
		},
		{
			name = "night_stalker_crippling_fear",
			prechecks =  {{name="FindClosestUnit", maxDistance = 300}},
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "silencer_global_silence",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "obsidian_destroyer_sanity_eclipse",
			prechecks = {{name="FindClosestUnit", maxDistance = 700}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit"
		},
		{
			name = "necrolyte_death_pulse",
			prechecks = {{name="FindClosestAndMinHealthUnitForCure", maxDistance = 800}},
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "death_prophet_exorcism",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "necrolyte_reapers_scythe",
			prechecks = {{name="FindClosestUnit", maxDistance = 800}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestUnit"
		},
		{
			name = "faceless_void_chronosphere",
			prechecks = {{name="FindClosestUnit", maxDistance = 800}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit"
		},
		{
			name = "antimage_blink",
			prechecks = {{name="FindClosestUnit", maxDistance = 400}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION_FOR_BLINK},
		},
		{
			name = "space_shield",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "enigma_black_hole",
			prechecks = {{name="FindClosestUnit", maxDistance = 800}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit",
			postExecute = function ( unit, units, enemyUnits, ability, abilityExecutorConfig)
				GameRules:GetGameModeEntity().waitCount = GameRules:GetGameModeEntity().waitCount + 1
				Timers:CreateTimer(ability:GetSpecialValueFor("duration") + 1, function()
					GameRules:GetGameModeEntity().waitCount = GameRules:GetGameModeEntity().waitCount - 1
				end)
			end
		},
		{
			name = "phantom_lancer_spirit_lance",
			prechecks = {{name="FindClosestUnit", maxDistance = 750}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestUnit"
		},
		{
			name = "chaos_knight_phantasm",
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		},
		{
			name = "viper_nethertoxin",
			prechecks = {{name="FindClosestUnit", maxDistance = 900}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit",
			postExecute = function ( unit, units, enemyUnits, ability, abilityExecutorConfig)
				GameRules:GetGameModeEntity().waitCount = GameRules:GetGameModeEntity().waitCount + 1
				Timers:CreateTimer(ability:GetSpecialValueFor("duration") + 1, function()
					GameRules:GetGameModeEntity().waitCount = GameRules:GetGameModeEntity().waitCount - 1
				end)
			end
		},
		{
			name = "viper_viper_strike",
			prechecks = {{name="FindClosestUnit", maxDistance = 700}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestUnit"
		},
		{
			name = "disruptor_static_storm",
			prechecks = {{name="FindClosestUnit", maxDistance = 800}},
			execute = {type=DOTA_UNIT_ORDER_CAST_POSITION},
			target = "FindClosestUnit"
		},
		{
			name = "antimage_mana_void",
			prechecks = {{name="FindClosestUnit", maxDistance = 600}},
			execute = {type=DOTA_UNIT_ORDER_CAST_TARGET},
			target = "FindClosestUnit"
		},
		{
			name = "centaur_khan_war_stomp",
			prechecks =  {{name="FindClosestUnit", maxDistance = 500}},
			execute = {type=DOTA_UNIT_ORDER_CAST_NO_TARGET}
		}
	}
	GameRules:GetGameModeEntity().abilityExecutorConfigs = abilityExecutorConfigs

	local abilityPrecheckExecutors = {
		FindClosestUnit = function (unit, units, enemyUnits, precheck)
			if FindClosestUnit(unit, enemyUnits, precheck.maxDistance) == nil then
				return false
			end
			return true
		end,
		FindClosestAndMinHealthUnit = function (unit, units, enemyUnits, precheck)
			if FindClosestAndMinHealthUnit(unit, enemyUnits, precheck.maxDistance) == nil then
				return false
			end
			return true
		end,
		FindClosestAndMinHealthUnitOfFriend = function (unit, units, enemyUnits, precheck)
			if FindClosestAndMinHealthUnit(unit, units, precheck.maxDistance) == nil then
				return false
			end
			return true
		end,
		GetManaPercent = function ( unit, units, enemyUnits, precheck )
			if unit:GetManaPercent() < precheck.minPercent then
				return true
			end
			return false
		end,
		FindClosestAndMinHealthUnitForCure = function ( unit, units, enemyUnits, precheck )
			if precheck.maxDistance == nil then
				if FindClosestAndMinHealthUnitForCure(unit, units) == nil then
					return false
				end
			else
				
				if FindClosestAndMinHealthUnitForCure(unit, units, precheck.maxDistance) == nil then
					return false
				end
			end

			return true
		end,
		FindClosestAndMinHealthUnitForDazzleShallowGrave = function ( unit, units, enemyUnits, precheck )
			if FindClosestAndMinHealthUnitForDazzleShallowGrave(unit, units, precheck.maxDistance) == nil then
				return false
			end

			return true
		end,
		NoDeform = function ( unit, units, enemyUnits, precheck )
			local deformModifiers = {
				"modifier_lone_druid_true_form",
				"modifier_metamorphosis"
			}
			for i = 1, #deformModifiers do
				if unit:HasModifier(deformModifiers[i]) then
					return false
				end
			end
			return true
		end
	}
	GameRules:GetGameModeEntity().abilityPrecheckExecutors = abilityPrecheckExecutors

	local abilityGetTargetExecutors = {
		FindClosestUnit = function (unit, units, enemyUnits, precheck)
			return FindClosestUnit(unit, enemyUnits, precheck.maxDistance)
		end,
		FindClosestAndMinHealthUnit = function (unit, units, enemyUnits, precheck)
			return FindClosestAndMinHealthUnit(unit, enemyUnits, precheck.maxDistance)
		end,
		FindClosestAndMinHealthUnitOfFriend = function (unit, units, enemyUnits, precheck)
			return FindClosestAndMinHealthUnit(unit, units, precheck.maxDistance)
		end,
		GetSelf = function ( unit, units, enemyUnits, precheck )
			return unit
		end,
		FindClosestAndMinHealthUnitForCure = function ( unit, units, enemyUnits, precheck )
			if precheck.maxDistance == nil then
				return FindClosestAndMinHealthUnitForCure(unit, units)
			else
				return FindClosestAndMinHealthUnitForCure(unit, units, precheck.maxDistance)
			end
		end,
		FindClosestAndMinHealthUnitForDazzleShallowGrave = function ( unit, units, enemyUnits, precheck )
			return FindClosestAndMinHealthUnitForDazzleShallowGrave(unit, units, precheck.maxDistance)
		end,
	}
	GameRules:GetGameModeEntity().abilityGetTargetExecutors = abilityGetTargetExecutors

	local abilityExecutors = {
		[DOTA_UNIT_ORDER_CAST_NO_TARGET] = function( unit, units, enemyUnits, ability, abilityExecutorConfig)
			local newOrder = {
				UnitIndex = unit:entindex(),
				OrderType = DOTA_UNIT_ORDER_CAST_NO_TARGET,
				TargetIndex = nil, --Optional.  Only used when targeting units
				AbilityIndex = ability:entindex(), --Optional.  Only used when casting abilities
				Position = nil, --Optional.  Only used when targeting the ground
				Queue = 0 --Optional.  Used for queueing up abilities
			}

			ExecuteOrderFromTable(newOrder)
			return true
		end,
		[DOTA_UNIT_ORDER_CAST_TARGET] = function( unit, units, enemyUnits, ability, abilityExecutorConfig)
			--Msg("target, abilityExecutorConfig=" .. TableToStr(abilityExecutorConfig) .. "\n")
			local abilityGetTargetExecutor = GameRules:GetGameModeEntity().abilityGetTargetExecutors[abilityExecutorConfig.target]
			local target = abilityGetTargetExecutor(unit, units, enemyUnits, abilityExecutorConfig)
			local newOrder = {
				UnitIndex = unit:entindex(), 
				OrderType = DOTA_UNIT_ORDER_CAST_TARGET,
				TargetIndex = target:entindex(), --Optional.  Only used when targeting units
				AbilityIndex = ability:entindex(), --Optional.  Only used when casting abilities
				Position = nil, --Optional.  Only used when targeting the ground
				Queue = 0 --Optional.  Used for queueing up abilities
			}

			ExecuteOrderFromTable(newOrder)
			return true
		end,
		[DOTA_UNIT_ORDER_CAST_POSITION] = function( unit, units, enemyUnits, ability, abilityExecutorConfig)
			local abilityGetTargetExecutor = GameRules:GetGameModeEntity().abilityGetTargetExecutors[abilityExecutorConfig.target]
			local target = abilityGetTargetExecutor(unit, units, enemyUnits, abilityExecutorConfig)
			--点敌人
			local newOrder = {
				UnitIndex = unit:entindex(),
				OrderType = DOTA_UNIT_ORDER_CAST_POSITION,
				TargetIndex = nil, --Optional.  Only used when targeting units
				AbilityIndex = ability:entindex(), --Optional.  Only used when casting abilities
				Position = target:GetAbsOrigin(), --Optional.  Only used when targeting the ground
				Queue = 0 --Optional.  Used for queueing up abilities
			}

			ExecuteOrderFromTable(newOrder)
			return true
		end,
		[DOTA_UNIT_ORDER_CAST_POSITION_FOR_WAVEFORM] = function( unit, units, enemyUnits, ability, abilityExecutorConfig)
			local abilityGetTargetExecutor = GameRules:GetGameModeEntity().abilityGetTargetExecutors[abilityExecutorConfig.target]
			local target = abilityGetTargetExecutor(unit, units, enemyUnits, abilityExecutorConfig)
			local targetPosition = target:GetAbsOrigin()
			local unitPosition = unit:GetAbsOrigin()
			local currentLength = (targetPosition - unitPosition):Length2D()
			local finalPosition = targetPosition
			local abilityLength = 800
			finalPosition.x = (targetPosition.x - unitPosition.x) * abilityLength / currentLength + targetPosition.x
			finalPosition.y = (targetPosition.y - unitPosition.y) * abilityLength / currentLength + targetPosition.y
			--点敌人
			local newOrder = {
				UnitIndex = unit:entindex(),
				OrderType = DOTA_UNIT_ORDER_CAST_POSITION,
				TargetIndex = nil, --Optional.  Only used when targeting units
				AbilityIndex = ability:entindex(), --Optional.  Only used when casting abilities
				Position = finalPosition, --Optional.  Only used when targeting the ground
				Queue = 0 --Optional.  Used for queueing up abilities
			}

			ExecuteOrderFromTable(newOrder)
			return true
		end,
		[DOTA_UNIT_ORDER_CAST_POSITION_FOR_BLINK] = function( unit, units, enemyUnits, ability, abilityExecutorConfig)
			local finalPosition = nil
			if unit:GetAttackCapability() == DOTA_UNIT_CAP_RANGED_ATTACK then
				local distance = 0
				local farestUnit
				for i = 1, 8 do
					for j = 2, 10 do
						index = {x = i, y = j}
						local closestUnit = FindClosestUnitForIndex(GetPlayerOwner(unit), index, enemyUnits)
						--Msg("closestUnit=" .. closestUnit.distance .. ", i=" .. i .. ", j=" .. j .. "\n");
						if closestUnit ~= nil and closestUnit.distance > distance then
							distance = closestUnit.distance
							farestUnit = closestUnit
						end
					end
				end

				if farestUnit ~= nil then
					finalPosition = farestUnit.positon
				end
				
			end

			if finalPosition == nil then
				local abilityGetTargetExecutor = GameRules:GetGameModeEntity().abilityGetTargetExecutors["FindClosestAndMinHealthUnit"]
				local target = abilityGetTargetExecutor(unit, units, enemyUnits, abilityExecutorConfig)
				finalPosition = target:GetAbsOrigin()
			end
			

			local newOrder = {
				UnitIndex = unit:entindex(),
				OrderType = DOTA_UNIT_ORDER_CAST_POSITION,
				TargetIndex = nil, --Optional.  Only used when targeting units
				AbilityIndex = ability:entindex(), --Optional.  Only used when casting abilities
				Position = finalPosition, --Optional.  Only used when targeting the ground
				Queue = 0 --Optional.  Used for queueing up abilities
			}

			ExecuteOrderFromTable(newOrder)
			return true
		end,
	}

	GameRules:GetGameModeEntity().abilityExecutors = abilityExecutors
end

function PrecheckAbilityExecutor(unit, units, enemyUnits, abilityPrechecks )
	if abilityPrechecks == nil then
		return true
	end

	local abilityPrecheckExecutors = GameRules:GetGameModeEntity().abilityPrecheckExecutors
	for i = 1, #abilityPrechecks do
		local precheck = abilityPrechecks[i]
		local abilityPrecheckExcutor = abilityPrecheckExecutors[precheck.name]

		if abilityPrecheckExcutor(unit, units, enemyUnits, precheck) == false then
			return false
		end
	end

	return true
end

function GetAllAbilityExecutorConfigs(unit)
	local abilityExecutorConfigs = GameRules:GetGameModeEntity().abilityExecutorConfigs
	local abilityExecutorConfigsOfUnit = {}
	for i = 1, #abilityExecutorConfigs do
		local abilityExecutorConfig = abilityExecutorConfigs[i]
		local ability = unit:FindAbilityByName(abilityExecutorConfig.name)
		if ability ~= nil then
			table.insert(abilityExecutorConfigsOfUnit, {abilityExecutorConfig = abilityExecutorConfig, ability = ability})
		end
	end

	return abilityExecutorConfigsOfUnit
end

function FindAbilityExecutorConfigFromAbilityExecutorConfigs(abilityExecutorConfigs, abilityName )
	for i = 1, #abilityExecutorConfigs do
		local abilityExecutorConfig = abilityExecutorConfigs[i]
		if abilityExecutorConfig.name == abilityName or abilityExecutorConfig.abilityExecutorConfig.name == abilityName then
			return abilityExecutorConfig
		end
	end

	return nil
end

function RandomSort( tableToSort )
	local result ={}
	local tmpTable = {}
	for i = 1, #tableToSort do
		table.insert(tmpTable, tableToSort[i])
	end

	for i = 1, #tableToSort do
		local index = RandomInt(1, #tmpTable)
		table.insert(result, tmpTable[index])
		table.remove(tmpTable, index)
	end
	return result
end

function GetAllAbilityExecutorConfigsWithPriorityAndRandomSort( unit )
	local abilityExecutorConfigs = GetAllAbilityExecutorConfigs(unit)
	local highPriorityAbilityNames = {
		"silencer_global_silence",		
		"jakiro_ice_path",
		"tidehunter_ravage",
		"treant_overgrowth",
		"lone_druid_true_form",
		"metamorphosis",
		"juggernaut_omni_slash",
	}

	local highPriorityAbilityExecutorConfigs = {}
	local lowPriorityAbilityExecutorConfigs = {}
	for i = 1, #abilityExecutorConfigs do
		local abilityExecutorConfig = abilityExecutorConfigs[i]
		if IsIn(highPriorityAbilityNames, abilityExecutorConfig.abilityExecutorConfig.name) then
			table.insert(highPriorityAbilityExecutorConfigs, abilityExecutorConfig)
		else
			table.insert(lowPriorityAbilityExecutorConfigs, abilityExecutorConfig)
		end
	end

	local finalAbilityExecutorConfigs = {}
	for i = 1, #highPriorityAbilityNames do
		local abilityExecutorConfig = 
			FindAbilityExecutorConfigFromAbilityExecutorConfigs(highPriorityAbilityExecutorConfigs, highPriorityAbilityNames[i])
		if abilityExecutorConfig ~= nil then
			table.insert(finalAbilityExecutorConfigs, abilityExecutorConfig)
		end
	end
	--Msg("abilityExecutorConfigs=" .. TableToStr(abilityExecutorConfigs) .. "\n")
	MergeTable(finalAbilityExecutorConfigs, RandomSort(lowPriorityAbilityExecutorConfigs))
	return finalAbilityExecutorConfigs
end

function ExecuteAbility( unit, units, enemyUnits)
	--[[local allModifiers = unit:FindAllModifiers()
	Msg("unit=" .. unit:GetName() .. "\n")
	for i = 1, #allModifiers do
		Msg("Modifier=" .. allModifiers[i]:GetName() .. "\n")
	end
	if unit.abilities ~= nil then
		for i = 1, #unit.abilities do
			Msg("Ability=" .. unit.abilities[i] .. "\n")
		end
	end]]--
	--Msg("unit=" .. unit:GetName() .. "\n")

	if  unit:IsStunned() or unit:IsFrozen() or unit:HasModifier('modifier_axe_berserkers_call')
		or unit:HasModifier('modifier_faceless_void_chronosphere_freeze') then
		--Msg("unit.name=" .. unit:GetName() .. "\n")
		return true
	end

	if unit:HasModifier('modifier_drowranger_wave_of_silence')
		or unit:HasModifier('modifier_shadow_shaman_voodoo')
		or unit:HasModifier('modifier_doom_bringer_doom')
		or unit:IsSilenced() then
		return false
	end

	local channelingAbilites = {
		"windrunner_powershot",
		"keeper_of_the_light_illuminate",
		"enigma_black_hole",
	}
	for i = 1, #channelingAbilites do
		local ability = unit:FindAbilityByName(channelingAbilites[i])
		if ability ~= nil and ability:IsChanneling() then
			return true
		end
	end
	

	local autoToggleAbilities = {
		"medusa_split_shot",
	}
	for i = 1, #autoToggleAbilities do
		local ability = unit:FindAbilityByName(autoToggleAbilities[i])
		if ability ~= nil and ability:GetToggleState() == false then
			ability:ToggleAbility()
		end
	end
	
	local autoCastAbilities = {
		"doom_bringer_infernal_blade",
		"obsidian_destroyer_arcane_orb"
	}

	for i = 1, #autoCastAbilities do
		local ability = unit:FindAbilityByName(autoCastAbilities[i])
		if ability ~= nil and ability:GetAutoCastState() == false then
			ability:ToggleAutoCast()
		end
	end

	--真熊形态结束时会删除一个技能，没法
	--[[local trueFormAbility = unit:FindAbilityByName("lone_druid_true_form")
	if trueFormAbility ~= nil then
		local demolishAbility = unit:FindAbilityByName("lone_druid_spirit_bear_demolish")
		local entangleAbility = unit:FindAbilityByName("lone_druid_spirit_bear_entangle")
		if unit:HasModifier('modifier_lone_druid_true_form') then
			if demolishAbility == nil then
				AddAbility(unit, "lone_druid_spirit_bear_demolish", trueFormAbility:GetLevel())
			end
			if entangleAbility == nil then
				AddAbility(unit, "lone_druid_spirit_bear_entangle", trueFormAbility:GetLevel())
			end
		else
			if demolishAbility ~= nil then
				RemoveAbilityAndModifier(unit, "lone_druid_spirit_bear_demolish")
			end
			if entangleAbility ~= nil then
				RemoveAbilityAndModifier(unit, "lone_druid_spirit_bear_entangle")
			end
		end
	end]]--

	if unit:HasModifier('modifier_bounty_hunter_wind_walk') == true
		or (unit:HasModifier('modifier_nyx_assassin_vendetta') == true and IsUnitAlive(unit.target)) then --可以添加两个命令么，这个要等待1秒
		local target = FindClosestAndMinHealthUnit(unit, enemyUnits, 3000)
		if target == nil then
			target = FindClosestUnit(unit, enemyUnits)
		end
		if target ~= nil
			and unit:HasModifier('modifier_treant_overgrowth') == false
			and unit:HasModifier('modifier_crystal_maiden_frostbite') == false then
			local newOrder = {
				UnitIndex = unit:entindex(), 
				OrderType = DOTA_UNIT_ORDER_ATTACK_TARGET,
				TargetIndex = target:entindex(), --Optional.  Only used when targeting units
				AbilityIndex = 0, --Optional.  Only used when casting abilities
				Position = nil, --Optional.  Only used when targeting the ground
				Queue = 1 --Optional.  Used for queueing up abilities
				}

			ExecuteOrderFromTable(newOrder)
			unit.target = target
		end

		return true
	end

	--new code
	local abilityExecutorConfigs = GetAllAbilityExecutorConfigsWithPriorityAndRandomSort(unit)
	--Msg("random abilityExecutorConfigs=" .. TableToStr(abilityExecutorConfigs) .. "\n")
	local prechecks = GameRules:GetGameModeEntity().prechecks
	for i = 1, #abilityExecutorConfigs do
		local abilityExecutorConfig = abilityExecutorConfigs[i]
		--Msg("abilityExecutorConfigsCount=" .. #abilityExecutorConfigs .. ", abilityExecutorConfig=" .. TableToStr(abilityExecutorConfig) .. "\n")
		local ability = abilityExecutorConfig.ability
		--Msg("abilityExecutorConfig=" .. TableToStr(abilityExecutorConfig) .. "\n")
		--[[Msg("mana=" .. unit:GetMana() .. ", cost=" .. ability:GetManaCost(ability:GetLevel()) 
			.. ", IsCooldownReady=" .. (ability:IsCooldownReady() and "true" or "false") 
			.. ", level=" .. ability:GetLevel() .."\n")
		Msg("manacost1=" .. ability:GetManaCost(1) .. ", 2=" .. ability:GetManaCost(2) ..
			", 3=" .. ability:GetManaCost(3) .. ", 4=" .. ability:GetManaCost(4) .. ", 5=" .. ability:GetManaCost(5)
			 .. ability:GetManaCost(-1) .. "\n")]]--
		if unit:GetMana() >= ability:GetManaCost(-1) and ability:IsCooldownReady() then
			local abilityPrechecks = abilityExecutorConfig.abilityExecutorConfig.prechecks
			--Msg("abilityPrechecks=" .. TableToStr(abilityPrechecks) .. "\n")
			if PrecheckAbilityExecutor(unit, units, enemyUnits, abilityPrechecks) then
				local executeConfig = abilityExecutorConfig.abilityExecutorConfig.execute
				local execute = GameRules:GetGameModeEntity().abilityExecutors[executeConfig.type]
				--Msg("executeConfig=" .. TableToStr(executeConfig) .. "\n")
				--Msg("GameRules:GetGameModeEntity().abilityExecutors=" .. TableToStr( GameRules:GetGameModeEntity().abilityExecutors) .. ", type=" .. executeConfig.type .. "\n")
				if execute(unit, units, enemyUnits, ability, abilityExecutorConfig.abilityExecutorConfig) then
					local postExecute = abilityExecutorConfig.abilityExecutorConfig.postExecute
					if postExecute ~= nil then
						postExecute(unit, units, enemyUnits, ability, abilityExecutorConfig.abilityExecutorConfig)
					end
					--Msg("Done " .. ability:GetName() .. ", time=" .. GameRules:GetGameModeEntity().time .. "\n")
					return true
				end
			end
		end
	end

	return false
end

function AutoChessFight( units, enemyUnits )
	-- Msg("start time=" .. GetSystemTime() .. "\n")
	if units == nil or enemyUnits == nil then
		return
	end

	for i = 1, #units do
		local unit = units[i]
		if IsUnitAlive(unit) and unit:HasModifier('modifier_winter_wyvern_cold_embrace') == false
			and ExecuteAbility(unit, units, enemyUnits) == false 
			and unit:GetAttackCapability() ~= DOTA_UNIT_CAP_NO_ATTACK then
			if unit:HasModifier('modifier_pangolier_luckyshot_disarm') == false
				and unit:HasModifier('modifier_juggernaut_omnislash_invulnerability') == false
				and unit:HasModifier('modifier_treant_overgrowth') == false
				and unit:HasModifier('modifier_shadow_shaman_voodoo') == false
				and unit:HasModifier('modifier_crystal_maiden_frostbite') == false then
				local targetUnit = nil

				local ability = unit:FindAbilityByName("profession_assassin")
				if ability ~= nil then
					targetUnit = FindClosestAndMinHealthUnit(unit, enemyUnits, 500)
				end

				if targetUnit == nil then
					targetUnit = FindClosestUnit(unit, enemyUnits)
				end
				
				if IsUnitAlive(targetUnit) then
					local newOrder = {
						 UnitIndex = unit:entindex(), 
						 OrderType = DOTA_UNIT_ORDER_ATTACK_TARGET,
						 TargetIndex = targetUnit:entindex(), --Optional.  Only used when targeting units
						 AbilityIndex = 0, --Optional.  Only used when casting abilities
						 Position = nil, --Optional.  Only used when targeting the ground
						Queue = 0 --Optional.  Used for queueing up abilities
				 	}

					ExecuteOrderFromTable(newOrder)
					--Msg("unit=" .. unit:GetName() .. " attack, target=" .. targetUnit:GetName() .. "\n")
					--Msg("GetAttackCapability=" .. unit:GetAttackCapability() .. "\n")
				end
			end
		end

		-- to fix a bug
		if unit:IsNull() == false and unit:FindAbilityByName("invulnerable") then
			RemoveAbilityAndModifier(unit, "invulnerable")
		end

		if unit:IsNull() == false and unit:IsInvulnerable() then
			local allModifiers = unit:FindAllModifiers()
			Msg("unit=" .. unit:GetName() .. "\n")
			for i = 1, #allModifiers do
				Msg("Modifier=" .. allModifiers[i]:GetName() .. "\n")
			end
			unit:RemoveModifierByName("modifier_invulnerable")
		end
	end

	-- Msg("end time=" .. GetSystemTime() .. "\n")
end

function AutoChess( player, isCombinedBattle )
	if IsBattleOver(player, isCombinedBattle) then
		return
	end

	local heros = GetAllHeroChessInChessBoard(player)
	local playerUnits = heros
	if player.otherUnits ~= nil then
		for i = 1, #player.otherUnits do
			table.insert(playerUnits, player.otherUnits[i])
		end
	end
	AutoChessFight(playerUnits, player.enemies)
	AutoChessFight(player.enemies, playerUnits)
end

function IsCombinedBattle( )
	return IsCombinedBattleForStage(GameRules:GetGameModeEntity().currentStage)
end

function IsCombinedBattleForStage( stage )
	if stage <= 0 then
		return false
	end
	local enemiesConfig = GameRules:GetGameModeEntity().enemiesConfig
	local currentEnenyIndex = stage
	if currentEnenyIndex > #enemiesConfig then
		currentEnenyIndex = currentEnenyIndex % #enemiesConfig
		if currentEnenyIndex == 0 then
			currentEnenyIndex = #enemiesConfig
		end
	end
	local currentStageEnemyConfigs = enemiesConfig[currentEnenyIndex]
	local isCombinedBattle = false
	if currentStageEnemyConfigs.battleType ~= nil and currentStageEnemyConfigs.battleType == "combined" then
		isCombinedBattle = true
	end

	return isCombinedBattle
end

function GetAllAbilitiesForProfessionsOfHero( hero, abilityName )
	-- body
	local abilities = {}
	local abilityConfig = GameRules:GetGameModeEntity().abilities[abilityName]
	for i = 1, hero.abilitiesCount do
		repeat
			local abilityNameOfHero = hero.abilities[i]
			local abilityConfigOfHero = GameRules:GetGameModeEntity().abilities[abilityNameOfHero]
			local ability = hero:FindAbilityByName(abilityNameOfHero)
			--职业名称
			if abilityNameOfHero == abilityName or IsIn(abilityConfig.needs, abilityNameOfHero) then
				table.insert(abilities, ability)
				break
			end

			if abilityConfigOfHero.needs == nil then
				break
			end
			--高级职业技能
			if IsIn(abilityConfigOfHero.needs, abilityName) then
				table.insert(abilities, ability)
				break
			end

			--低级职业技能
			for i = 1, #abilityConfig.needs do
				if IsIn(abilityConfigOfHero.needs, abilityConfig.needs[i]) then
					table.insert(abilities, ability)
					break
				end
			end
		until true
	end
	return abilities
end

function HasAllAbiliitiesForLegendProfession( abilities )
	--for i = 1, #abilities do
	--	Msg("HasAllAbiliitiesForLegendProfession, abilityName=" .. abilities[i]:GetName() .. "\n")
	--end

	if #abilities >= 6 then
		for i = 1, #abilities do
			local ability = abilities[i]
			local abilityConfig = GameRules:GetGameModeEntity().abilities[ability:GetName()]
			if abilityConfig.order == 1 then
				--Msg("HasAllAbiliitiesForLegendProfession\n")
				return true
			end
		end
	end
	return false
end

function AddLegendProfessionAbility( hero, abilities, professionName )
	local avgLevel = 0
	for i = 1, 6 do
		avgLevel = avgLevel + abilities[i]:GetLevel()
	end
	avgLevel = math.floor(avgLevel / 6)
	AddAbility(hero, "legend_profession", avgLevel)
	AddAbility(hero, "legend_" .. professionName, avgLevel)
end

function AddLegendRaceAbility( hero )
	local avgLevel = 0
	for i = 1, 10 do
		local abilityName = hero.abilities[i]
		local ability = hero:FindAbilityByName(abilityName)
		avgLevel = avgLevel + ability:GetLevel()
	end
	avgLevel = math.floor(avgLevel / 10)
	AddAbility(hero, "legend_profession", avgLevel)
	AddAbility(hero, "legend_race", avgLevel)
end

function ClearLegendAbilities( hero )
	local legendProfessions = {
		"legend_profession",
		"legend_race",
		"legend_profession_berserker",
		"legend_profession_shield_warrior",
		"legend_profession_phantom_assassin",
		"legend_profession_templar_assassin",
		"legend_profession_holy_light_priest",
		"legend_profession_shadow_priest",
		"legend_profession_phantom_archer",
		"legend_profession_beastmaster",
		"legend_profession_beast_druid",
		"legend_profession_paladin",
		"legend_profession_fire_mage",
		"legend_profession_earth_mage"
	}

	for i = 1, #legendProfessions do
		if hero:HasAbility(legendProfessions[i]) then
			RemoveAbilityAndModifier(hero, legendProfessions[i])
		end
	end
end

function ClearOtherAbilities( hero )
	local legendProfessions = {
		"self_luck",
		"self_unluck",
	}

	for i = 1, #legendProfessions do
		if hero:HasAbility(legendProfessions[i]) then
			RemoveAbilityAndModifier(hero, legendProfessions[i])
		end
	end
end

function ApplyLegendAbilities( hero )
	--Msg("hero.name=" .. hero:GetName() .. ", IsHero()=" .. (hero:IsHero() and "true" or "false") .. "\n")
	if hero:IsHero() then
		ClearLegendAbilities(hero)
		--Msg("hero.abilitiesCount=" .. hero.abilitiesCount .. "\n")
		for i = 1, hero.abilitiesCount do
			local abilityName = hero.abilities[i]
			--Msg("abilityName=".. abilityName .. "\n")
			local abilityConfig = GameRules:GetGameModeEntity().abilities[abilityName]
			if abilityConfig ~= nil and abilityConfig.order == 2 then
				local abilities = GetAllAbilitiesForProfessionsOfHero(hero, abilityName)
				if HasAllAbiliitiesForLegendProfession(abilities) then
					AddLegendProfessionAbility(hero, abilities, abilityName)
					return
				end
			end
		end

		if #GetAllAbilityNamesOfHeroWithoutSecondaryProfession(hero) >= 10 then
			AddLegendRaceAbility(hero)
		end
	end

	
end

BattleEnd = false

function StageThinkExecutor( )
	local currentStageEnemyConfigs = GetCurrentStageEnemyConfigs()
	prepareStageMaxTimeInSeconds = DEFAULT_PREPARE_STAGE_MAX_TIME_IN_SECONDS
	if currentStageEnemyConfigs.prepareStageMaxTimeInSeconds ~= nil then
		prepareStageMaxTimeInSeconds = currentStageEnemyConfigs.prepareStageMaxTimeInSeconds
	end

	if GameRules:GetGameModeEntity().currentStage == 1 then
		prepareStageMaxTimeInSeconds = prepareStageMaxTimeInSeconds + DEFAULT_PREPARE_STAGE_MAX_TIME_FOR_FIRST_STAGE_IN_SECONDS
	end

	battleStageMaxTimeInSeconds = DEFAULT_BATTLE_STAGE_MAX_TIME_IN_SECONDS
	if currentStageEnemyConfigs.battleStageMaxTimeInSeconds ~= nil then
		battleStageMaxTimeInSeconds = currentStageEnemyConfigs.battleStageMaxTimeInSeconds
	end

	maxStage = #GameRules:GetGameModeEntity().enemiesConfig
	--maxStage = 3

	GameRules:GetGameModeEntity().time = GameRules:GetGameModeEntity().time + 1

	local leftTime

	local isCombinedBattle = IsCombinedBattle()

	local auction =  GameRules:GetGameModeEntity().auctions[GameRules:GetGameModeEntity().currentStage]
	local auctionStatus = GameRules:GetGameModeEntity().auctionStatus
	if auction ~= nil and IsAuctionEnd() == false then
		GameRules:GetGameModeEntity().time = 0
	end

	if GameRules:GetGameModeEntity().stage == STAGE_PREPARE_STAGE then
		local allPrepare = true
		local players =  GetAllAlivePlayers()
		for i = 1, #players do
			local player = players[i]
			allPrepare = allPrepare and players[i].prepareOver
		end

		if allPrepare then
			GameRules:GetGameModeEntity().time = math.max(GameRules:GetGameModeEntity().time, prepareStageMaxTimeInSeconds - 3)
		end

		if GameRules:GetGameModeEntity().time >= prepareStageMaxTimeInSeconds then
			GameRules:GetGameModeEntity().time = 0
			GameRules:GetGameModeEntity().stage = STAGE_BATTLE_STAGE
			BattleEnd = false
			-- switch to battle
			local players =  GetAllAlivePlayers()
			for i = 1, #players do
				StartBattle(players[i])
			end

			if isCombinedBattle then
				StartCombinedBattle(players)
			end
		end

		if GameRules:GetGameModeEntity().toPrepared then
			-- Msg("playersCount=" .. GameRules:GetGameModeEntity().playersCount .. "\n")
			if GameRules:GetGameModeEntity().currentStage ~= 1 then
				for i = 1, GameRules:GetGameModeEntity().playersCount do
					local player = GameRules:GetGameModeEntity().players[i]
					local heros = GetAllHeroChessInChessBoard(player)
					-- Msg("heros.length=" .. #heros .. "\n")
					for j, hero in pairs(heros) do
						hero:AddExperience(1, DOTA_ModifyGold_GameTick, false, false)
					end

					local courier = player:GetAssignedHero()
					courier:AddExperience(1, DOTA_ModifyGold_GameTick, false, false)
				end
			end

			local players =  GetAllAlivePlayers()
			for i = 1, #players do
				ResetUnitsForPlayer(players[i])
				RandomGenerateTeammate(players[i])
			end

			if isCombinedBattle then
				CreateEnemiesForCombinedBattle()
			else
				for i = 1, #players do
					CreateEnemiesForPlayer(players[i])
				end
			end

			GenerateShopItems()

			if GameRules:GetGameModeEntity().currentStage % 10 == 1 then
				if (IsCorpsMode() and GameRules:GetGameModeEntity().currentStage <= 60)
					or (IsSoloMode() and GameRules:GetGameModeEntity().currentStage <= 40) then
					GenerateQuests()
				end
				
			end

			EncounterAdventure()

			SendAdventureJournalsToAllPlayers("ADVENTURE_JOURNALS_CURRENT_STAGE",
				{
					days = { type = "value", value = (GameRules:GetGameModeEntity().currentStage - 1) * 5 + 1},
					stageName = {type = "text", value = currentStageEnemyConfigs.stageName},
				}
			)

			for i = 1, #players do
				SendHintMessageToPlayer(players[i], 
					currentStageEnemyConfigs.stageName, 
					nil,
					false
				)
			end

			if auction ~= nil then
				InitAuction(auction)
			end

			GameRules:GetGameModeEntity().toPrepared = false
		end
	elseif GameRules:GetGameModeEntity().stage == STAGE_BATTLE_STAGE then
		if (GameRules:GetGameModeEntity().time >= battleStageMaxTimeInSeconds or IsAllPlayerBattleOver(isCombinedBattle)) and GameRules:GetGameModeEntity().waitCount == 0 then
			if BattleEnd then
				GameRules:GetGameModeEntity().stage = STAGE_PREPARE_STAGE
				GameRules:GetGameModeEntity().currentStage = GameRules:GetGameModeEntity().currentStage + 1
				GameRules:GetGameModeEntity().toPrepared = true

				local players = GetAllAlivePlayers()
				for i = 1, #players do
					OnBattleOver(players[i])
					players[i].prepareOver = false
					players[i].endBattle = false
				end

				for i = 1, #players do
					local player = players[i]
					ClearAllEnemiesForPlayer(player)
				end

				players = GetAllPlayers()
				local allLose = true
				for i = 1, #players do
					if players[i].lose == false then 
						allLose = false
						break
					end
				end

				if allLose then
					GameRules:SetGameWinner(DOTA_TEAM_BADGUYS)
					GameRules:GetGameModeEntity().gameEnded = true
					return
				end

				GameRules:GetGameModeEntity().time = 0
			else
				Timers:CreateTimer(3, function()
					BattleEnd = true
				end)
			end
			
		else
			local players = GetAllAlivePlayers()
			for i = 1, #players do
				if players[i].endBattle == false then
					AutoChess(players[i])
				end
			end
		end
	end

	if GameRules:GetGameModeEntity().currentStage == maxStage + 1 then
		GameRules:SetGameWinner(DOTA_TEAM_GOODGUYS)
		GameRules:GetGameModeEntity().gameEnded = true
		return
	end 

	local emitSoundTime = 5
	if GameRules:GetGameModeEntity().stage == STAGE_PREPARE_STAGE then
		leftTime = prepareStageMaxTimeInSeconds - GameRules:GetGameModeEntity().time
	elseif GameRules:GetGameModeEntity().stage == STAGE_BATTLE_STAGE then
		leftTime = battleStageMaxTimeInSeconds - GameRules:GetGameModeEntity().time
		emitSoundTime = 10
	end

	if leftTime <= emitSoundTime then
		EmitGlobalSound("General.CastFail_AbilityInCooldown")
	end

	if auction ~= nil and IsAuctionEnd() == false then
		local auctionStatus = GameRules:GetGameModeEntity().auctionStatus
		if auctionStatus.timeout >= 0 then
			RandomBidForBusinessmen()
			CustomGameEventManager:Send_ServerToAllClients("on_auction_status_update", auctionStatus)
			auctionStatus.timeout = auctionStatus.timeout - 1
			EmitGlobalSound("General.CastFail_AbilityInCooldown")
		else
			FinishAuction()
		end
		
	end

	CustomGameEventManager:Send_ServerToAllClients("on_stage_think", 
		{stage = GameRules:GetGameModeEntity().stage, leftTime = leftTime, currentStage = GameRules:GetGameModeEntity().currentStage, maxStage = maxStage})
	
end

function ErrorHandler(err)
	Msg("Error=" .. err .. "\n")
	Msg(debug.traceback() .. "\n")
end

function StageThink( )
	if GameRules:IsGamePaused() then
		return 1
	end

	if GameRules:GetGameModeEntity().gameEnded then
		return
	end

	
	local status = xpcall(StageThinkExecutor, ErrorHandler)   --当发生错误时，调用第二个参数（一个错误处理函数）
	--Msg(status) 

	return 1
end

function GetQualityLevel(quality)
	if quality == QUALITY_1 then
		return 1
	elseif quality == QUALITY_2 then
		return 2
	elseif quality == QUALITY_3 then
		return 3
	elseif quality == QUALITY_4 then
		return 3
	elseif quality == QUALITY_5 then
		return 3
	else
		return 1
	end
end

function GetAllAbilityNamesOfHeroWithoutSecondaryProfession( hero )
	local abilityNames = {}
	local abilitiesConfig = GameRules:GetGameModeEntity().abilities
	for i = 1, hero.abilitiesCount do
		local abilityName = hero.abilities[i]
		local abilityConfig = abilitiesConfig[abilityName]
		if abilityConfig.type ~= SecondaryProfession then
			table.insert(abilityNames, abilityName)
		end
	end
	return abilityNames
end

function GetAllAbilityNamesOfHeroWithoutFirstProfession( hero )
	local abilityNames = {}
	local abilitiesConfig = GameRules:GetGameModeEntity().abilities
	for i = 1, hero.abilitiesCount do
		local abilityName = hero.abilities[i]
		local abilityConfig = abilitiesConfig[abilityName]
		if abilityConfig.type == SecondaryProfession then
			table.insert(abilityNames, abilityName)
		end
	end
	return abilityNames
end

-- items
function UseAbilityItem( keys )
	-- Msg("UseAbilityItem" .. TableToStr(keys) .. "\n")
	-- GameRules:SendCustomMessage("UseAbilityItem", PlayerResource:GetTeam(keys.target:GetPlayerID()), keys.target:GetPlayerID())
	local unit = keys.target
	local item = keys.ability
	local abilityName = item.shopItem.abilityName
	-- Msg("unit=" .. TableToStr(unit) .. "\n")
	-- Msg("item=" .. TableToStr(item) .. "\n")
	local ability = unit:FindAbilityByName(abilityName)

	local player = PlayerResource:GetPlayer(keys.target:GetPlayerID())

	if unit:IsHero() == false then
		SendErrorMessageToPlayer(player, "ONLY_HERO_CAN_USE_THIS_ITEM")
		return
	end

	local courier = player:GetAssignedHero()

	if keys.target == courier then
		SendErrorMessageToPlayer(player, "COURIER_CANNOT_USE_THIS_ITEM")
		return
	end

	local abilitiesConfig = GameRules:GetGameModeEntity().abilities
	local abilityConfig = abilitiesConfig[abilityName]
	if abilityConfig.type ~= nil and abilityConfig.type == SecondaryProfession then
		if unit.abilitiesCount ~= nil and #GetAllAbilityNamesOfHeroWithoutFirstProfession(unit) >= 3 and ability == nil then
			-- Msg("Ability is full\n")
			SendErrorMessageToPlayer(player, "ABILITIES_IS_FULL")
			return
		end
	else
		if unit.abilitiesCount ~= nil and #GetAllAbilityNamesOfHeroWithoutSecondaryProfession(unit) >= courier:GetLevel() and ability == nil then
			-- Msg("Ability is full\n")
			SendErrorMessageToPlayer(player, "ABILITIES_IS_FULL")
			return
		end
	end
	

	if unit.temp == true and unit.canBuy == false then
		SendErrorMessageToPlayer(player, "THIS_UNIT_CANNOT_USE_THIS_ITEM")
		return
	end

	if ability == nil then
		if item.canUseDirectly == false or item:GetCurrentCharges() > 1 then
			local needs = abilityConfig.needs or {}
			local has = false
			if #needs == 0 then
				has = true
			end
			for i = 1, #needs do
				if unit:FindAbilityByName(needs[i]) ~= nil then
					has = true
				end
			end
			if has == false then
				SendErrorMessageToPlayer(player, "WITHOUT_NEEDS_ABILITY")
				return
			end
		end
		if unit:IsHero() then
			ApplyPrimaryAttribute(unit, abilityName)
		end
		AddAbility(unit, abilityName, GetQualityLevel(item.shopItem.quality), true)
		if item.canUseDirectly == false or item:GetCurrentCharges() > 1 then
			SendAdventureJournals(player, "ADVENTURE_JOURNALS_LEARN_SKILL",
				{
					name = {type = "name", value = GetUnitName(unit)},
					itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. item:GetName()},
					abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. abilityName},
					level = {type = "value", value = GetQualityLevel(item.shopItem.quality)}
				})
		else
			SendAdventureJournals(player, "ADVENTURE_JOURNALS_LEARN_GENERAL_SKILL",
				{
					name = {type = "name", value = GetUnitName(unit)},
					itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. item:GetName()},
					abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. abilityName},
					level = {type = "value", value = GetQualityLevel(item.shopItem.quality)}
				})
		end
	elseif ability:GetLevel() >= ability:GetMaxLevel() then
		-- Msg("Ability reach max level\n")
		SendErrorMessageToPlayer(player, "ABILITY_REACH_MAX_LEVEL")
		return
	else
		local level = ability:GetLevel() + 1
		AddAbility(unit, abilityName, level, true)
		SendAdventureJournals(player, "ADVENTURE_JOURNALS_LEARN_SKILL_TO_UPGRADE",
			{
				name = {type = "name", value = GetUnitName(unit)},
				itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. item:GetName()},
				abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. abilityName},
				level = {type = "value", value = level}
			})
	end

	if item:GetCurrentCharges() == 1 then
		item:Destroy()
	else
		item:SetCurrentCharges(item:GetCurrentCharges() - 1)
	end

	EmitSoundForAbilityUpgrade(player)
end

function GetShopItemCost(quality)
	return QualityToNumber(quality)
end

function EmitSoundForSell( player )
	EmitSoundOnClient("General.Coins", player)
end

function EmitSoundForBuy( player )
	EmitSoundOnClient("General.CoinsBig", player)
end

function EmitSoundForDrink( player )
	EmitSoundOnClient("DOTA_Item.HealingSalve.Activate", player)
end

function EmitSoundForAbilityUpgrade( player )
	EmitSoundOnClient("ui.trophy_levelup", player)
end

function EmitSoundForDeny( player )
	EmitSoundOnClient("General.CastFail_NoMana", player)
end

function OnSellItem( index, keys )
	--DeepPrintTable(keys)
	local itemIndex = keys.itemIndex
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	local courier = player:GetAssignedHero()
	local item = courier:GetItemInSlot(itemIndex - 1)
	if item == nil then
		SendErrorMessageToPlayer(player, "NO_ITEM_IN_SLOT")
		return
	end
	-- Msg("item.abilityName=" .. item:GetName() .. "\n")
	local shopItem = item.shopItem
	local gold = 0
	local count = 0
	if shopItem.type == TYPE_EQUIPMENT then
		shopItem.existCount = shopItem.existCount - 1
		gold = item.shopItem.cost
		count = 1
	else
		shopItem.existCount = shopItem.existCount - item:GetCurrentCharges()
		gold = item.shopItem.cost * item:GetCurrentCharges()
		count = item:GetCurrentCharges()
	end
	
	if shopItem.name == "item_water_of_forgetting" then
		gold = 0
	end

	if shopItem.name == "item_eternal_contract" then
		shopItem.existCount = shopItem.existCount + item:GetCurrentCharges()
	end

	SendAdventureJournals(player, "ADVENTURE_JOURNALS_SELL_SHOP_ITEM",
		{
			name = {type = "value", value = PlayerResource:GetPlayerName(keys.PlayerID)},
			itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. item:GetName()},
			count = {type = "value", value = count}
		})

	item:Destroy()
	courier:SetMana(courier:GetMana() + gold)
	EmitSoundForSell(player)
	SendUpdateStatusMessage(player)
end

function CountHeroGold( hero )
	local cost = QualityToNumber(hero.shopItem.quality) + hero:GetLevel()
	for i = 1, hero.abilitiesCount do
		local abilityName = hero.abilities[i]
		local ability = hero:FindAbilityByName(abilityName)
		cost = cost + GetAbilityCost(abilityName, ability:GetLevel())
	end

	for i = 0, 8 do
		local item = hero:GetItemInSlot(i)
		if item ~= nil then
			local shopItem = item.shopItem
			cost = cost + GetShopItemCost(shopItem.quality)
		end
	end
	return cost
end

function DestroyChess( player, chess, releaseHero)
	local index = FindIndexPickedChess(player, chess)
	if index == nil then
		Msg("Error, can't find chess\n");
	end
	if index.y ~= 1 then
		player.heroCountInChessboard = player.heroCountInChessboard - 1
	end
	player.units[index.x][index.y] = nil

	if releaseHero then
		ReleaseHero(chess) --RealseHero包括ReleaseItems
	else
		ReleaseItems(chess) --禁了学技能，没禁给装备
	end
	chess:Destroy()

	CustomGameEventManager:Send_ServerToPlayer(player, "on_hero_count_in_chessboard_update", 
		{heroCountInChessboard = player.heroCountInChessboard, maxHeroCountInChessboard = GetMaxHeroCountInChessboard(player:GetAssignedHero())})

    SendUpdateStatusMessage(player)
end

function SellChess( keys )
	local playerId = keys.target:GetPlayerID()
	local player = PlayerResource:GetPlayer(playerId)
	local courier = player:GetAssignedHero()

	local index = FindIndexPickedChess(player, keys.target)
	if index == nil then
		Msg("Error, can't find chess\n");
		return
	end

	if GameRules:GetGameModeEntity().stage == STAGE_BATTLE_STAGE and index.y ~= 1 then
		SendErrorMessageToPlayer(player, "CANNOT_SELL_THIS_CHESS_IN_CURRENT_STAGE")
		return
	end

	if keys.target.canSell == false then
		SendErrorMessageToPlayer(player, "THIS_CHESS_CANNOT_BE_SELL")
		return
	end

	if keys.target.temp == nil then --can not sell chess which is own by team
		courier:SetMana(courier:GetMana() + math.floor(CountHeroGold(keys.target) / 2))
	end

	EmitSoundForSell(player)

	SendAdventureJournals(player, "ADVENTURE_JOURNALS_SELL_HERO",
		{
			name = {type = "value", value = PlayerResource:GetPlayerName(playerId)},
			heroName = {type = "name", value = GetUnitName(keys.target)}
		})

	if keys.target.canBuy ~= nil and keys.target.canBuy == false then
		DestroyChess(player, keys.target, false)
	else
		DestroyChess(player, keys.target, true)
	end
end

function RegenerateShopItems( keys )
	local courier = keys.caster
	local playerId = courier:GetPlayerID()
	local player = PlayerResource:GetPlayer(playerId)
	RandomGenerateShopItems(player)
	SendShopUpdateMessage(player)
	EmitSoundOnClient("Quickbuy.Available", player)
	SendUpdateStatusMessage(player)

	SendAdventureJournals(player, "ADVENTURE_JOURNALS_REGENERATE_SHOP_ITEMS",
		{
			name = {type = "value", value = PlayerResource:GetPlayerName(playerId)}
		})
end

function GainExpForCourier( keys )
	--Msg("GainExpForCourier" .. TableToStr(keys) .. "\n")
	local courier = keys.caster
	local playerId = courier:GetPlayerID()
	local player = PlayerResource:GetPlayer(playerId)
	if courier:GetLevel() == 10 then
		SendErrorMessageToPlayer(player, "REACH_MAX_LEVEL")
		courier:SetMana(courier:GetMana() + keys.ability:GetManaCost(courier:GetLevel()))
		return
	end

	SendAdventureJournals(player, "ADVENTURE_JOURNALS_GAIN_EXP_FOR_COURIER",
		{
			name = {type = "value", value = PlayerResource:GetPlayerName(playerId)}
		})

	courier:AddExperience(4, DOTA_ModifyGold_AbilityCost, false, false)
	EmitSoundOnClient("ui.treasure_01", player)
	SendUpdateStatusMessage(player)
end

function GainExpForUnit( keys )
	--Msg("GainExpForCourier" .. TableToStr(keys) .. "\n")
	local playerId = keys.target:GetPlayerID()
	local player = PlayerResource:GetPlayer(playerId)
	local courier = player:GetAssignedHero()
	if keys.target:GetLevel() == 10 then
		SendErrorMessageToPlayer(player, "REACH_MAX_LEVEL")
		courier:SetMana(courier:GetMana() + keys.ability:GetManaCost(courier:GetLevel()))
		return
	end

	if keys.target == courier then
		SendErrorMessageToPlayer(player, "CANNOT_APPLY_TO_COURIER")
		courier:SetMana(courier:GetMana() + keys.ability:GetManaCost(courier:GetLevel()))
		return
	end

	SendAdventureJournals(player, "ADVENTURE_JOURNALS_GAIN_EXP_FOR_UNIT",
		{
			heroName = {type = "name", value = GetUnitName(keys.target)}
		})

	--keys.target:AddExperience(4, DOTA_ModifyGold_AbilityCost, false, false)
	keys.target:AddExperience(
		GameRules:GetGameModeEntity().gainExpForUnit[keys.target:GetLevel()], 
		DOTA_ModifyGold_AbilityCost, false, false)
	SendUpdateStatusMessage(player)
end

function ContainsQuality( shopItemQuliaty, quality)
	if shopItemQuliaty == QUALITY_1 then
		return true
	elseif shopItemQuliaty == QUALITY_2 then
		if quality == QUALITY_1 then
			return false
		else
			return true
		end
	elseif shopItemQuliaty == QUALITY_3 then
		if quality == QUALITY_1 or quality == QUALITY_2 then
			return false
		else
			return true
		end
	elseif shopItemQuliaty == QUALITY_4 then
		if quality == QUALITY_4 or quality == QUALITY_5 then
			return true
		else
			return false
		end
	elseif shopItemQuliaty == QUALITY_5 then
		if quality == QUALITY_5 then
			return true
		else
			return false
		end
	else
		return false
	end

end

function GetShopItemsLeftCountForQuality(shopItems, quality )
	local count = 0
	for i = 1, #shopItems do
		local shopItem = shopItems[i]
		if GetShopItemMaxCount(shopItem) - shopItem.existCount > 0 then
			if shopItem.type == TYPE_HERO then
				if ContainsQuality(shopItem.quality, quality) then
					count = count + (GetShopItemMaxCount(shopItem) - shopItem.existCount) --to add hero probability
				end
			else 
				if shopItem.quality == quality then
					count = count + (GetShopItemMaxCount(shopItem) - shopItem.existCount)
				end
			end
		end
	end

	return count
end

function GetShopItemsLeftCountTable(shopItems)
	local leftCount = {}
	leftCount[QUALITY_1] = GetShopItemsLeftCountForQuality(shopItems, QUALITY_1)
	leftCount[QUALITY_2] = GetShopItemsLeftCountForQuality(shopItems, QUALITY_2)
	leftCount[QUALITY_3] = GetShopItemsLeftCountForQuality(shopItems, QUALITY_3)
	leftCount[QUALITY_4] = GetShopItemsLeftCountForQuality(shopItems, QUALITY_4)
	leftCount[QUALITY_5] = GetShopItemsLeftCountForQuality(shopItems, QUALITY_5)
	return leftCount
end

function GetShopItem( shopItems, index, quality )

	local count = 0
	for i = 1, #shopItems do
		local shopItem = shopItems[i]
		if GetShopItemMaxCount(shopItem) - shopItem.existCount > 0 then
			if shopItem.type == TYPE_HERO then
				if ContainsQuality(shopItem.quality, quality) then
					count = count + (GetShopItemMaxCount(shopItem) - shopItem.existCount) -- must be same with GetShopItemsLeftCountForQuality
					if index <= count then
						return shopItems[i]
					end
				end
			else
				if shopItem.quality == quality then
					count = count + (GetShopItemMaxCount(shopItem) - shopItem.existCount)
					if index <= count then
						return shopItems[i]
					end
				end
			end
		end
	end

	return shopItems[1]
end

function GetShopItemsCountForQuality(shopItems, quality )
	local count = 0
	for i = 1, #shopItems do
		local shopItem = shopItems[i]
		count = count + GetShopItemMaxCount(shopItem)
	end

	return count
end

function GetShopItemsCountTable(shopItems)
	local leftCount = {}
	leftCount[QUALITY_1] = GetShopItemsCountForQuality(shopItems, QUALITY_1)
	leftCount[QUALITY_2] = GetShopItemsCountForQuality(shopItems, QUALITY_2)
	leftCount[QUALITY_3] = GetShopItemsCountForQuality(shopItems, QUALITY_3)
	leftCount[QUALITY_4] = GetShopItemsCountForQuality(shopItems, QUALITY_4)
	leftCount[QUALITY_5] = GetShopItemsCountForQuality(shopItems, QUALITY_5)
	return leftCount
end

function GetShopItemForEnemy( shopItems, index, quality )

	local count = 0
	for i = 1, #shopItems do
		local shopItem = shopItems[i]
		count = count + GetShopItemMaxCount(shopItem)
		if index <= count then
			return shopItems[i]
		end
	end

	return shopItems[1]
end

function ContainAbility( abilities, abilityName)
	for i = 1, #abilities do
		if abilities[i].abilityName == abilityName then
			return true
		end
	end
	return false
end

function RandomSelectAbility(courier, abilities)
	local shopItems = GameRules:GetGameModeEntity().shopItems
	local tmpShopItems = {}
	local tmpShopItemCount = 0
	for i = 1, #shopItems do
		if (shopItems[i].type == TYPE_ABILITY) 
			and (ContainAbility(abilities, shopItems[i].abilityName) == false)
			and IsSecondaryProfession(shopItems[i].abilityName) == false then
			-- Msg("abilityName=" .. shopItems[i].abilityName .. "\n")
			tmpShopItems[tmpShopItemCount + 1] = shopItems[i]
			tmpShopItemCount = tmpShopItemCount + 1
		end
	end
	shopItems = tmpShopItems
	local level = courier:GetLevel()
	local probability = GameRules:GetGameModeEntity().probability[level]

	local probablitiesRange = 
	{
		consumable = probability[QUALITY_1], 
		common = (probability[QUALITY_1] + probability[QUALITY_2]),
		rare = (probability[QUALITY_1] + probability[QUALITY_2] + probability[QUALITY_3]),
		epic = (probability[QUALITY_1] + probability[QUALITY_2] + probability[QUALITY_3] + probability[QUALITY_4]),
		artifact = (probability[QUALITY_1] + probability[QUALITY_2] + probability[QUALITY_3] + probability[QUALITY_4] + probability[QUALITY_5]),
	}
	for i = 1, 5 do
		probablitiesRange[i] = probability[QUALITY_1]
	end

	local leftCountTable = GetShopItemsLeftCountTable(shopItems)
	local random = RandomInt(1, 100)
	-- Msg("Random random=" .. random .. "\n")
	local quality
	if random > probablitiesRange[QUALITY_4] then
		quality = QUALITY_5
	elseif random > probablitiesRange[QUALITY_3] then
		quality = QUALITY_4
	elseif random > probablitiesRange[QUALITY_2] then
		quality = QUALITY_3
	elseif random > probablitiesRange[QUALITY_1] then
		quality = QUALITY_2
	else
		quality = QUALITY_1
	end


	if leftCountTable[quality] > 0 then
		random = RandomInt(1, leftCountTable[quality])
		local shopItem = GetShopItem(shopItems, random, quality)
		-- Msg("Random quality=" .. quality .. ", random=" .. random .. ", ability=" .. shopItem.abilityName .. "\n")
		return shopItem
	else
		-- 所有的都抽完了
		return nil
	end
end

function FindAbilityShopItem( shopItems, abilityName )
	for i = 1, #shopItems do
		if shopItems[i].abilityName == abilityName then
			return shopItems[i]
		end
	end
	return nil
end

function FindHeroShopItemsWithMaxCountLimit( )
	local shopItems = GameRules:GetGameModeEntity().shopItems
	local heroShopItems = {}
	for i = 1, #shopItems do
		if shopItems[i].type == TYPE_HERO and GetShopItemMaxCount(shopItems[i]) - shopItems[i].existCount > 0 then
			table.insert(heroShopItems, shopItems[i])
		end
	end
	return heroShopItems
end

function FindShopItem( shopItems, name )
	for i = 1, #shopItems do
		if shopItems[i].name == name then
			return shopItems[i]
		end
	end
	return nil
end

function GetAbilityCost( abilityName, level)
	local abilityShopItem = FindAbilityShopItem(GameRules:GetGameModeEntity().shopItems, abilityName)
	local qualityLevel = GetQualityLevel(abilityShopItem.quality)
	local cost = abilityShopItem.cost + (level - qualityLevel) * abilityShopItem.cost
	return cost
end

function QualityToNumber( quality )
	local level = 0
	if quality == QUALITY_1 then
		level = 1
	elseif quality == QUALITY_2 then
		level = 2
	elseif quality == QUALITY_3 then
		level = 3
	elseif quality == QUALITY_4 then
		level = 4
	elseif quality == QUALITY_5 then
		level = 5
	end

	return level
end

function NumberToQuality( number )
	local quality = QUALITY_1
	if number == 1 then
		quality = QUALITY_1
	elseif number == 2 then
		quality = QUALITY_2
	elseif number == 3 then
		quality = QUALITY_3
	elseif number == 4 then
		quality = QUALITY_4
	elseif number == 5 then
		quality = QUALITY_5
	end

	return quality
end

function CreateHeroShopItem(courier, shopItem, shopItems, quality)
	local level
	shopItems = GameRules:GetGameModeEntity().shopItems
	local heroShopItem = {name = shopItem.name, heroName = shopItem.heroName, type=TYPE_HERO, quality=quality, shopItem = shopItem}
	-- Msg("heroShopItem=" .. TableToStr(heroShopItem) .. "\n")
	local heroConfig = GameRules:GetGameModeEntity().heros[shopItem.heroName]
	-- Msg("heroConfig=" .. TableToStr(heroConfig) .. "\n")
	local index = RandomInt(1, #heroConfig.base)
	-- Msg("index=" .. index .. "\n")
	local baseInfo = heroConfig.base[index]
	heroShopItem.base = baseInfo
	local abilities = {}
	local abilitiesCount = 0

	level = QualityToNumber(quality)

	heroShopItem.cost = level

	local heroMaxLevel = math.floor(courier:GetLevel() / 2) 
	if heroMaxLevel <=0 then
		heroMaxLevel = 1
	end
	heroShopItem.level = RandomInt(1, heroMaxLevel)

	heroShopItem.cost = heroShopItem.cost + heroShopItem.level

	for i = 1, #heroConfig.abilities do
		abilitiesCount = abilitiesCount + 1
		local abilityShopItem = FindAbilityShopItem(shopItems, heroConfig.abilities[i])
		abilities[abilitiesCount] = {abilityName = heroConfig.abilities[i], level = RandomInt(GetQualityLevel(abilityShopItem.quality), math.max(GetQualityLevel(abilityShopItem.quality), math.max(heroMaxLevel, level)))}
		heroShopItem.cost = heroShopItem.cost + GetAbilityCost(abilities[abilitiesCount].abilityName, abilities[abilitiesCount].level)
		ReferenceAbility(abilities[abilitiesCount].abilityName, abilities[abilitiesCount].level)
	end

	local addAbilityCount = level
	if addAbilityCount > 3 then
		-- enter complete profession mode
		local abilitiesConfigForFinalProfession = nil
		local attackType = ATTACK_TYPE_MELEE
		local randomForAttackType = RandomInt(1, 100)
		if baseInfo.attackType == ATTACK_TYPE_MELEE then
			if randomForAttackType <= 20 then
				attackType = ATTACK_TYPE_RANGED
			else
				attackType = ATTACK_TYPE_MELEE
			end
		else
			if randomForAttackType <= 20 then
				attackType = ATTACK_TYPE_MELEE
			else
				attackType = ATTACK_TYPE_RANGED
			end
		end

		if attackType == ATTACK_TYPE_MELEE then
			abilitiesConfigForFinalProfession = GetFinalProfessionAbilitiesForMeleeWithShopItemConfig()
		else
			abilitiesConfigForFinalProfession = GetFinalProfessionAbilitiesForRangedWithShopItemConfig()
		end

		if #abilitiesConfigForFinalProfession > 0 then
			local random = RandomInt(1, #abilitiesConfigForFinalProfession)
			local finalProfessionAbility = abilitiesConfigForFinalProfession[random]
			-- add all basic profession ability
			local needsAbilities = GetAllAbilitiesForNeeds(finalProfessionAbility.name)
			for i = 1, #needsAbilities do
				local abilityShopItem = FindAbilityShopItem(shopItems, needsAbilities[i])
				abilitiesCount = abilitiesCount + 1
				abilities[abilitiesCount] = {
					abilityName = needsAbilities[i], 
					level = RandomInt(GetQualityLevel(abilityShopItem.quality), math.max(GetQualityLevel(abilityShopItem.quality), math.max(heroMaxLevel, level)))
				}
				--Msg("abilityName=" .. needsAbilities[i] .. ", quality=" .. abilityShopItem.quality .. ", qualityLevel=" .. GetQualityLevel(abilityShopItem.quality) .. ", heroLevel=" .. math.max(heroMaxLevel, level) .. "\n")
				heroShopItem.cost = heroShopItem.cost + GetAbilityCost(abilities[abilitiesCount].abilityName, abilities[abilitiesCount].level)
				ReferenceAbility(abilities[abilitiesCount].abilityName, abilities[abilitiesCount].level)
			end

			-- add all profession ability
			addAbilityCount = addAbilityCount - #needsAbilities
			local abilitiesForProfessions = GetAllAbilitiesForProfessionsWithShopItemConfig(needsAbilities)
			--[[for i = 1, #abilitiesForProfessions do
				Msg("selected abilitiesForProfessions=" .. abilitiesForProfessions[i] .. "\n")
			end]]--
			local selectedAbilitiesCount = 0
			if attackType ~= baseInfo.attackType then
				selectedAbilitiesCount = math.min(addAbilityCount, math.floor(#abilitiesForProfessions / 2))
			else
				selectedAbilitiesCount = math.min(addAbilityCount, #abilitiesForProfessions)
			end
			 
			addAbilityCount = addAbilityCount - selectedAbilitiesCount
			for i = 1, selectedAbilitiesCount do
				local random = RandomInt(1, #abilitiesForProfessions)
				local abilityShopItem = FindAbilityShopItem(shopItems, abilitiesForProfessions[random])
				abilitiesCount = abilitiesCount + 1
				abilities[abilitiesCount] = {
					abilityName = abilitiesForProfessions[random], 
					level = RandomInt(GetQualityLevel(abilityShopItem.quality), math.max(GetQualityLevel(abilityShopItem.quality), math.max(heroMaxLevel, level)))
				}

				heroShopItem.cost = heroShopItem.cost + GetAbilityCost(abilities[abilitiesCount].abilityName, abilities[abilitiesCount].level)
				ReferenceAbility(abilities[abilitiesCount].abilityName, abilities[abilitiesCount].level)

				--Msg("selected=" .. i .. ", name=" .. abilitiesForProfessions[random] ..  "\n")
				table.remove(abilitiesForProfessions, random)
			end

			--Msg("addAbilityCount=" .. addAbilityCount .. ", #heroConfig.abilities=" .. #heroConfig.abilities .. ", #needsAbilities=" .. #needsAbilities
			--	.. ", selectedAbilitiesCount=" .. selectedAbilitiesCount .. "\n")
		end
	end


	for i = 1, addAbilityCount do
		local abilityShopItem = RandomSelectAbility(courier, abilities)
		if abilityShopItem ~= nil then
			abilitiesCount = abilitiesCount + 1
			abilities[abilitiesCount] = {abilityName = abilityShopItem.abilityName, level = RandomInt(GetQualityLevel(abilityShopItem.quality), math.max(GetQualityLevel(abilityShopItem.quality), math.max(heroMaxLevel, level)))}
			heroShopItem.cost = heroShopItem.cost + GetAbilityCost(abilities[abilitiesCount].abilityName, abilities[abilitiesCount].level)
			ReferenceAbility(abilities[abilitiesCount].abilityName, abilities[abilitiesCount].level)
		end
	end

	heroShopItem.abilities = abilities
	heroShopItem.abilitiesCount = abilitiesCount
	return heroShopItem
end

function ReleaseAbility( abilityName, level)
	local shopItems = GameRules:GetGameModeEntity().shopItems
	abilityShopItem =  FindAbilityShopItem(shopItems, abilityName)
	local qualityLevel = GetQualityLevel(abilityShopItem.quality)
	level = level - qualityLevel
	if level < 0 then
		level = 0
	end
	abilityShopItem.existCount = abilityShopItem.existCount - 1 - level
end

function ReferenceAbility( abilityName, level )
	local shopItems = GameRules:GetGameModeEntity().shopItems
	abilityShopItem =  FindAbilityShopItem(shopItems, abilityName)
	local qualityLevel = GetQualityLevel(abilityShopItem.quality)
	level = level - qualityLevel
	if level < 0 then
		level = 0
	end
	abilityShopItem.existCount = abilityShopItem.existCount + 1 + level
end

function ReleaseHeroShopItem( heroShopItem )
	heroShopItem.shopItem.existCount = heroShopItem.shopItem.existCount - 1
	for i = 1, #heroShopItem.abilities do
		ReleaseAbility(heroShopItem.abilities[i].abilityName, heroShopItem.abilities[i].level)
	end
end

function ReleaseItems( hero )
	for i = 0, 8 do
		local item = hero:GetItemInSlot(i)
		if item ~= nil then
			local shopItem = item.shopItem
			shopItem.existCount = shopItem.existCount - 1
		end
	end
end

function ReleaseHero( hero )
	Msg("hero.shopItem=" .. TableToStr(hero.shopItem) .. "\n")
	hero.shopItem.shopItem.existCount = hero.shopItem.shopItem.existCount - 1
	for i = 1, #hero.abilities do
		local abilityName = hero.abilities[i]
		--Msg("abilityName=" .. abilityName .. "\n")
		local ability = hero:FindAbilityByName(abilityName)
		ReleaseAbility(abilityName, ability:GetLevel())
	end

	ReleaseItems(hero)
end

function ReleaseItem( shopItem )
	shopItem.existCount = shopItem.existCount - 1
end

function ReleaseShopItemsForPlayer( player )
	if player.shopItems ~= nil then
		for i = 1, player.shopItemCount do
			local shopItem = player.shopItems[i]
			if shopItem ~= nil and shopItem.isBought == false then
				--回收
				-- Msg("release shopItem, shopItem=" .. TableToStr(shopItem) .. "\n")
				if shopItem.shopItem.type == TYPE_HERO then
					ReleaseHeroShopItem(shopItem.shopItem)
				elseif shopItem.shopItem.type == TYPE_ABILITY then
					ReleaseAbility(shopItem.shopItem.abilityName, GetQualityLevel(shopItem.shopItem.quality))
				else
					ReleaseItem(shopItem.shopItem)
				end
			end
		end
	end

	player.shopItems = {}
end

function FilterShopItemsByShopConfigs( shopItems, player )
	local finalShopItems = {}
	local abilities = GameRules:GetGameModeEntity().abilities
	local professionConfigs = player.shopConfigs.professions
	local ignoreProfessions = {}
	local keepProfessions = {}
	for name, value in pairs(professionConfigs) do
		if value.enable == false then
			table.insert(ignoreProfessions, name)
		else
			table.insert(keepProfessions, name)
		end
	end

	for i = 1, #keepProfessions do
		local needAbilities = GetAllAbilitiesForNeeds(keepProfessions[i])
		for j = 1, #needAbilities do
			if IsIn(keepProfessions, needAbilities[j]) == false then
				table.insert(keepProfessions, needAbilities[j])
			end
		end
	end
	
	local heros = GetAllHeroChessWhichIsNotTemp(player)
	--Msg("\nherosCount=" .. (#heros) .. "\n")
	local ignoreHero = false
	if #heros >= GetMaxHeroCountInChessboard(player:GetAssignedHero()) then 
		ignoreHero = true
	end

	if IsSoloMode() then
		ignoreHero = true
	end

	for i = 1, #shopItems do
		local shopItem = shopItems[i]
		if shopItem.type == TYPE_ABILITY then
			local abilityConfig = abilities[shopItem.abilityName]
			if abilityConfig.order ~= nil then
				if IsIn(keepProfessions, abilityConfig.name) then
					table.insert(finalShopItems, shopItem)
				end
			elseif abilityConfig.needs ~= nil and abilityConfig.type ~= SecondaryProfession then
				local has = false
				for i = 1, #abilityConfig.needs do
					if IsIn(keepProfessions, abilityConfig.needs[i]) then
						has = true
					end
				end
				if has then
					table.insert(finalShopItems, shopItem)
				end
			else
				table.insert(finalShopItems, shopItem)
			end
		elseif shopItem.type == TYPE_HERO then
			--Msg("ignoreHero=" .. BoolToString(ignoreHero) .. "\n")
			if ignoreHero == false then
				table.insert(finalShopItems, shopItem)
			end
		else
			table.insert(finalShopItems, shopItem)
		end
	end

	--[[for i = 1, #keepProfessions do
		Msg("keepProfessions=" .. keepProfessions[i] .. "\n")
	end
	for i = 1, #finalShopItems do
		Msg("finalShopItems.name=" .. finalShopItems[i].name .. "\n")
	end]]--
	return finalShopItems
end

function RandomGenerateShopItems( player )
	local shopItems = GameRules:GetGameModeEntity().shopItems
	shopItems = FilterShopItemsByShopConfigs(shopItems, player)
	--[[for i = 1, #shopItems do
		Msg("name=" .. shopItems[i].name .. ", count=" .. GetShopItemMaxCount(shopItems[i]) .. "\n")
	end]]--

	local courier = player:GetAssignedHero()
	local level = courier:GetLevel()
	local probability = GameRules:GetGameModeEntity().probability[level]

	ReleaseShopItemsForPlayer(player)

	Msg("After relase, shopItems=" .. TableToStr(shopItems) .. "\n")
	player.shopItemCount = 0

	local probablitiesRange = 
	{
		consumable = probability[QUALITY_1], 
		common = (probability[QUALITY_1] + probability[QUALITY_2]),
		rare = (probability[QUALITY_1] + probability[QUALITY_2] + probability[QUALITY_3]),
		epic = (probability[QUALITY_1] + probability[QUALITY_2] + probability[QUALITY_3] + probability[QUALITY_4]),
		artifact = (probability[QUALITY_1] + probability[QUALITY_2] + probability[QUALITY_3] + probability[QUALITY_4] + probability[QUALITY_5]),
	}
	for i = 1, 5 do
		probablitiesRange[i] = probability[QUALITY_1]
	end

	for i = 1, 6 do
		local leftCountTable = GetShopItemsLeftCountTable(shopItems)
		local random = RandomInt(1, 100)
		-- Msg("Random i=" .. i .. ", random=" .. random .. "\n")
		local quality
		if random > probablitiesRange[QUALITY_4] then
			quality = QUALITY_5
		elseif random > probablitiesRange[QUALITY_3] then
			quality = QUALITY_4
		elseif random > probablitiesRange[QUALITY_2] then
			quality = QUALITY_3
		elseif random > probablitiesRange[QUALITY_1] then
			quality = QUALITY_2
		else
			quality = QUALITY_1
		end


		if leftCountTable[quality] > 0 then
			random = RandomInt(1, leftCountTable[quality])
			-- Msg("Random quality=" .. quality .. ", random=" .. random .. "\n")
			local shopItem = GetShopItem(shopItems, random, quality)
			shopItem.existCount = shopItem.existCount + 1

			if shopItem.type == TYPE_HERO then
				local shopItem = CreateHeroShopItem(courier, shopItem, shopItems, quality)
				player.shopItemCount = player.shopItemCount + 1
				player.shopItems[player.shopItemCount] = {shopItem = shopItem, isBought=false}
			else
				player.shopItemCount = player.shopItemCount + 1
				player.shopItems[player.shopItemCount] = {shopItem = shopItem, isBought=false}
				if shopItem.type == TYPE_ABILITY then
					local canUseDirectly = RandomInt(1, 10)
					--Msg("name=" .. shopItem.name .. ", canUseDirectly=" .. canUseDirectly .. "\n")
					if canUseDirectly <= 1 then
						player.shopItems[player.shopItemCount].canUseDirectly = true
					else
						player.shopItems[player.shopItemCount].canUseDirectly = false
					end

					--Msg("shopItem = " .. TableToStr(shopItem) .. "\n")
					--不需要的直接不显示可依赖
					local abilitiesConfig = GameRules:GetGameModeEntity().abilities
					local abilityConfig = abilitiesConfig[shopItem.abilityName]
					if abilityConfig.needs == nil or #abilityConfig.needs == 0 then
						player.shopItems[player.shopItemCount].canUseDirectly = false
					end

					if abilityConfig.type == SecondaryProfession then
						player.shopItems[player.shopItemCount].canUseDirectly = false
					end
					--Msg("canUseDirectly=" .. BoolToString(player.shopItems[player.shopItemCount].canUseDirectly) .. "\n")
				end
			end
			
		else
			-- 所有的都抽完了
			local shopItem = shopItems[1]
			shopItem.existCount = shopItem.existCount + 1
			player.shopItemCount = player.shopItemCount + 1
			player.shopItems[player.shopItemCount] = {shopItem = shopItem, isBought=false}
		end
	end
end

function GenerateShopItems( )
	local players = GetAllAlivePlayers()
	for i = 1, #players do
		RandomGenerateShopItems(players[i])
		-- Msg("Current shopItems=" .. TableToStr(players[i].shopItems) .. "\n")
		SendShopUpdateMessage(players[i])
	end
end

QUEST_COLLECT_ITEMS = "QUEST_COLLECT_ITEMS"
QUEST_EXCHANGE_ITEMS = "QUEST_EXCHANGE_ITEMS"
QUEST_DEFEAT_BY_ADD = "QUEST_DEFEAT_BY_ADD"
QUEST_DEFEAT_BY_ADD_AND_LIMIT_STATUS = "QUEST_DEFEAT_BY_ADD_AND_LIMIT_STATUS"
QUEST_WINNING_STREAK = "QUEST_WINNING_STREAK"
QUEST_ABILITY_REACH_LEVEL = "QUEST_ABILITY_REACH_LEVEL"
QUEST_HERO_REACH_LEVEL = "QUEST_HERO_REACH_LEVEL"

REWARD_GOLD = "REWARD_GOLD"
REWARD_EQUIPMENT = "REWARD_EQUIPMENT"
REWARD_ABILITY = "REWARD_ABILITY"
REWARD_CONSUMABLE = "REWARD_CONSUMABLE"
REWARD_ABILITY_LEVEL_UP = "REWARD_ABILITY_LEVEL_UP"
REWARD_HERO_LEVEL_UP = "REWARD_HERO_LEVEL_UP"
REWARD_ETERNAL_CONTRACT = "REWARD_ETERNAL_CONTRACT"

function GetByProbabilityFromTable( table )
	local totalProbability = 0
	for i = 1, #table do
		totalProbability = totalProbability + table[i].probability
	end

	local random = RandomInt(1, totalProbability)
	local currentProbability = 0
	for i = 1, #table do
		currentProbability = currentProbability + table[i].probability
		if random <= currentProbability then
			return table[i]
		end
	end

	return table[#table]
end

function GetShopItemsForQuests( qualities, types, filters )
	filters = filters or {}
	local shopItems = GameRules:GetGameModeEntity().shopItems
	local tmpShopItems = {}
	for i = 1, #shopItems do
		if (IsIn(types, shopItems[i].type)) 
			and IsIn(qualities, shopItems[i].quality) 
			and GetShopItemMaxCount(shopItems[i]) - shopItems[i].existCount > 0
			and IsIn(filters, shopItems[i].name) == false
			then
			table.insert(tmpShopItems, shopItems[i])
		end
	end

	return tmpShopItems
end

function GetShopItemsForSecondaryProfession( names )
	local shopItems = GameRules:GetGameModeEntity().shopItems
	local tmpShopItems = {}
	for i = 1, #shopItems do
		if (IsIn(names, shopItems[i].name))
			and GetShopItemMaxCount(shopItems[i]) - shopItems[i].existCount > 0
			then
			table.insert(tmpShopItems, shopItems[i])
		end
	end

	return tmpShopItems
end

function InitQuestsConfigs(  )
	local itemRewords = {
		{type = REWARD_GOLD, probability = 40},
		{type = REWARD_EQUIPMENT, probability = 40},
		{type = REWARD_ABILITY, probability = 40},
		{type = REWARD_CONSUMABLE, probability = 20},
	}
	local allRewords = {
		{type = REWARD_GOLD, probability = 20},
		{type = REWARD_EQUIPMENT, probability = 20},
		{type = REWARD_ABILITY, probability = 20},
		{type = REWARD_CONSUMABLE, probability = 20},
		{type = REWARD_ABILITY_LEVEL_UP, probability = 20},
		{type = REWARD_HERO_LEVEL_UP, probability = 20},
		{type = REWARD_ETERNAL_CONTRACT, probability = 20},
	}
	--[[local quests = {
		{type = QUEST_COLLECT_ITEMS, probability = 20},
		{type = QUEST_EXCHANGE_ITEMS, probability = 20},
		{type = QUEST_DEFEAT_BY_ADD, probability = 20},
		{type = QUEST_WINNING_STREAK, probability = 20},
		{type = QUEST_ABILITY_REACH_LEVEL, probability = 20},
		{type = QUEST_HERO_REACH_LEVEL, probability = 20},
	}]]--
	local quests = {
		{
			type = QUEST_COLLECT_ITEMS,
			rewords = itemRewords,
			prepareExecute = function(quest, player)
				local context = quest.context
				local sex = MAN
				if RandomInt(1, 100) > 50 then
					sex = WOMAN
				end

				context.namePublishedQuest = {
					type = "name",
					value = {
						type = "random",
						sex = sex,
						name = GenerateName(sex)
					}
				}

				local shopItems = GetShopItemsForQuests({QUALITY_1, QUALITY_2}, {TYPE_ABILITY, TYPE_CONSUMABLE}, {})
				shopItems = FilterShopItemsByShopConfigs(shopItems, player)
				local shopItem
				if #shopItems > 0 then
					shopItem = shopItems[RandomInt(1, #shopItems)]
				else
					shopItem = GameRules:GetGameModeEntity().shopItems[1]
				end
				quest.needShopItem = shopItem
				--context.count = math.min(RandomInt(2, 3), GetShopItemMaxCount(shopItem) - shopItem.existCount)
				--if context.count <= 0 then
				--	context.count = 1
				--end
				context.count = RandomInt(1, 2)
				context.count = {
					type = "value",
					value = context.count
				}
				context.itemNameNeed = {
					type = "text",
					value = "DOTA_Tooltip_ability_" .. shopItem.name
				}
				context.stage = {
					type = "value",
					value = GameRules:GetGameModeEntity().currentStage + 10
				}
				quest.cost = context.count.value * GetShopItemCost(shopItem.quality)
				quest.filterItems = {shopItem.name}
			end,
			canComplete = function ( quest, player )
				local context = quest.context
				local courier = player:GetAssignedHero()
				local item = GetItemInSlot(courier, quest.needShopItem.name)
				if item ~= nil and item:GetCurrentCharges() >= context.count.value then
					return true
				else
					return false
				end
			end,
			onComplete = function ( quest, player )
				local context = quest.context
				local courier = player:GetAssignedHero()
				local item = GetItemInSlot(courier, quest.needShopItem.name)
				DecreaseItems(item, context.count.value)
				item.shopItem.existCount = item.shopItem.existCount - context.count.value
			end
		},
		{
			type = QUEST_EXCHANGE_ITEMS,
			rewords = itemRewords,
			precheck = function ( player )
				if  GameRules:GetGameModeEntity().currentStage >= 20 then
					return true
				else
					return false
				end
			end,
			prepareExecute = function(quest, player)
				local context = quest.context
				local sex = MAN
				if RandomInt(1, 100) > 50 then
					sex = WOMAN
				end

				context.namePublishedQuest = {
					type = "name",
					value = {
						type = "random",
						sex = sex,
						name = GenerateName(sex)
					}
				}

				local shopItems = GetShopItemsForQuests({QUALITY_4}, 
					{TYPE_ABILITY, TYPE_CONSUMABLE, TYPE_EQUIPMENT}, {})
				shopItems = FilterShopItemsByShopConfigs(shopItems, player)
				local shopItem
				if #shopItems > 0 then
					shopItem = shopItems[RandomInt(1, #shopItems)]
				else
					shopItem = GameRules:GetGameModeEntity().shopItems[1]
				end
				quest.needShopItem = shopItem
				context.count = {
					type = "value",
					value = 1
				}
				context.itemNameNeed = {
					type = "text",
					value = "DOTA_Tooltip_ability_" .. shopItem.name
				}
				context.stage = {
					type = "value",
					value = GameRules:GetGameModeEntity().currentStage + 10
				}
				quest.cost = context.count.value * GetShopItemCost(shopItem.quality)

				local random = RandomInt(1, 100)
				if random <= 40 then
					quest.reword = {type = REWARD_ABILITY, probability = 40}
				elseif random <= 70 then
					quest.reword = {type = REWARD_EQUIPMENT, probability = 40}
				else 
					quest.reword = {type = REWARD_CONSUMABLE, probability = 40}
				end
				--if shopItem.type == TYPE_ABILITY then
				--	quest.reword = {type = REWARD_ABILITY, probability = 40}
				--elseif shopItem.type == TYPE_EQUIPMENT then
				--	quest.reword = {type = REWARD_EQUIPMENT, probability = 40}
				--else 
				--	quest.reword = {type = REWARD_CONSUMABLE, probability = 40}
				--end
				quest.reword.hide = true
				quest.filterItems = {shopItem.name}
			end,
			canComplete = function ( quest, player )
				local context = quest.context
				local courier = player:GetAssignedHero()
				local item = GetItemInSlot(courier, quest.needShopItem.name)
				if item ~= nil then
					return true
				else
					return false
				end
			end,
			onComplete = function ( quest, player )
				local context = quest.context
				local courier = player:GetAssignedHero()
				local item = GetItemInSlot(courier, quest.needShopItem.name)
				DecreaseItems(item, context.count.value)
				item.shopItem.existCount = item.shopItem.existCount - context.count.value
			end
		},
		{
			type = QUEST_DEFEAT_BY_ADD,
			rewords = allRewords,
			prepareExecute = function(quest, player)
				local context = quest.context
				context.stage = {
					type = "value",
					value = GameRules:GetGameModeEntity().currentStage + RandomInt(4, 9)
				}
				local random = RandomInt(1, 100)
				if random > 50 or context.stage.value <= 30 then
					quest.type = "HERO"
					context.namePublishedQuest = {
						type = "text",
						value = "hero"
					}
				else
					local unitModelInfos = {
						{name = "npc_dota_warlock_golem5", attackType = ATTACK_TYPE_MELEE},
						{name = "npc_dota_neutral_black_dragon", attackType = ATTACK_TYPE_RANGED},
						{name = "npc_dota_roshan", attackType = ATTACK_TYPE_MELEE},
					}
					quest.type = "UNIT"
					if context.stage.value <= 30 then
						quest.modelInfo = unitModelInfos[1]
					elseif context.stage.value <= 40 then
						quest.modelInfo = unitModelInfos[2]
					else
						quest.modelInfo = unitModelInfos[3]
					end
					context.namePublishedQuest = {
						type = "text",
						value = quest.modelInfo.name
					}
				end

				context.count = {
					type = "value",
					value = RandomInt(1, 3)
				}
				if context.count.value == 3 and context.stage.value <= 20 then
					context.count.value = 2
				end 
			end,
			onAccept = function ( quest, player )
				local context = quest.context
				local enmeyConfigs = GetStageEnemyConfigs(context.stage.value)
				local heroAbilityConfigs = {
					{quality = QUALITY_2, level = 3},
					{quality = QUALITY_3, level = 4},
					{quality = QUALITY_3, level = 6},
					{quality = QUALITY_4, level = 6},
					{quality = QUALITY_4, level = 8},
					{quality = QUALITY_5, level = 8},
				}
				local unitAbilityConfigs = {
					{attackDamageMin=10, attackDamageMax=12},
					{attackDamageMin=30, attackDamageMax=50},
					{attackDamageMin=50, attackDamageMax=100},
					{attackDamageMin=80, attackDamageMax=120},
					{attackDamageMin=100, attackDamageMax=150},
					{attackDamageMin=120, attackDamageMax=150},
				}
				--Msg("oldenmeyConfigs=" .. TableToStr(enmeyConfigs) .. "\n")
				
				for i = 1, context.count.value do
					if quest.type == "HERO" then
						local attackType = RandomInt(1, 2)
						if attackType == 1 then
							attackType = ATTACK_TYPE_MELEE
						else
							attackType = ATTACK_TYPE_RANGED
						end
						local heroAbilityConfig = heroAbilityConfigs[math.floor((context.stage.value - 1)/ 10) + 1]
						local heroConfig = {type = TYPE_HERO, quality = heroAbilityConfig.quality, level = heroAbilityConfig.level, attackDamageMin=1, attackDamageMax=5, drop = {types={TYPE_ABILITY, TYPE_CONSUMABLE, TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 1}}
						heroConfig.attackType = attackType
						heroConfig.count = 1
						table.insert(enmeyConfigs, heroConfig)

					else
						local unitAbilityConfig = unitAbilityConfigs[math.floor((context.stage.value - 1)/ 10) + 1]
						local unitConfig = {attackType=ATTACK_TYPE_RANGED, health=2000, attackDamageMin=unitAbilityConfig.attackDamageMin, attackDamageMax=unitAbilityConfig.attackDamageMax, drop = {types={TYPE_ABILITY, TYPE_CONSUMABLE, TYPE_EQUIPMENT}, quality = QUALITY_5, probability = 1}, abilities = {{name = "warlock_golem_flaming_fists"}}, randomAbilities = {minCount = 1, maxCount = 3, abilities = GameRules:GetGameModeEntity().unitAbilities}}
						unitConfig.name = quest.modelInfo.name
						unitConfig.attackType = quest.modelInfo.attackType
						unitConfig.count = 1
						table.insert(enmeyConfigs, unitConfig)
					end
				end
				--Msg("enmeyConfigs=" .. TableToStr(enmeyConfigs) .. "\n")
			end,
			onBattleOver = function ( quest, player, stage, status )
				local context = quest.context
				if context.stage.value == stage then
					if quest.status == "Accepted" then
						if status ~= "win" then
							quest.status = "Failed"
							if quest.rewordConfig.onCancel ~= nil then
								quest.rewordConfig.onCancel(quest)
							end
							SendAdventureJournals(player, "ADVENTURE_JOURNALS_QUEST_FAILED",
							{
								name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
								questName = {type = "text", value = quest.quest.type .. "_TITLE"}
							})
							quest.endFlag = true
							SendAdventureJournalsForAdventureJournals(player, adventureJournals)
						else
							local id = nil
							for i = 1, #player.quests do
								if player.quests[i] == quest then
									id = i
								end
							end
							OnHandleQuests({}, {id=id, PlayerID = player:GetPlayerID()})
						end
						SendQuestsUpdateMessage(player)
					else
						quest.status = "Timeout"
						SendAdventureJournals(player, "ADVENTURE_JOURNALS_QUEST_TIMEOUT",
							{
								name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
								questName = {type = "text", value = quest.quest.type .. "_TITLE"}
						})
						SendQuestsUpdateMessage(player)
					end
				end
			end,
			canComplete = function ( quest, player )
				local context = quest.context
				local courier = player:GetAssignedHero()
				if GameRules:GetGameModeEntity().currentStage > context.stage.value then
					if quest.status ~= "Failed" then
						return true
					end
				end

				return false
			end,
			onComplete = function ( quest, player )
			end
		},
		{
			type = QUEST_WINNING_STREAK,
			rewords = allRewords,
			prepareExecute = function(quest, player)
				local context = quest.context
				local sex = MAN
				if RandomInt(1, 100) > 50 then
					sex = WOMAN
				end

				context.namePublishedQuest = {
					type = "name",
					value = {
						type = "random",
						sex = sex,
						name = GenerateName(sex)
					}
				}

				context.stage = {
					type = "value",
					value = GameRules:GetGameModeEntity().currentStage + 10
				}

				local courier = player:GetAssignedHero()
				context.winningStreak = {
					type = "value",
					value = courier.winningStreak + RandomInt(3, 8)
				} 
			end,
			canComplete = function ( quest, player )
				local context = quest.context
				local courier = player:GetAssignedHero()
				if courier.winningStreak >= context.winningStreak.value then
					return true
				else
					return false
				end
			end,
			onComplete = function ( quest, player )
			end
		},
		{
			type = QUEST_ABILITY_REACH_LEVEL,
			rewords = allRewords,
			precheck = function ( player )
				local heros = GetAllHeroChessWhichIsNotTemp(player)
				if #heros == 0 then
					return false
				end
				local canUpgradeAbilites = GetCanUpgradeAbilities(heros)
				if #canUpgradeAbilites == 0 then
					return false
				end
				return true
			end,
			prepareExecute = function(quest, player)
				local context = quest.context
				local sex = MAN
				if RandomInt(1, 100) > 50 then
					sex = WOMAN
				end

				context.namePublishedQuest = {
					type = "name",
					value = {
						type = "random",
						sex = sex,
						name = GenerateName(sex)
					}
				}

				context.stage = {
					type = "value",
					value = GameRules:GetGameModeEntity().currentStage + 10
				}

				local heros = GetAllHeroChessWhichIsNotTemp(player)
				local canUpgradeAbilites = GetCanUpgradeAbilities(heros)
				local random = RandomInt(1, #canUpgradeAbilites)
				local ability = canUpgradeAbilites[random].ability
				quest.ability = ability
				context.abilityNameNeedUpgrade = {
					type = "text",
					value = "DOTA_Tooltip_ability_" .. ability:GetName()
				}
				local level = math.min(5, RandomInt(ability:GetLevel() + 1, ability:GetLevel() + 2))
				context.level = {
					type = "value",
					value = level
				}

				local hero = canUpgradeAbilites[random].hero
				context.heroName = {
					type = "name", 
					value = GetUnitName(hero)
				}
			end,
			canComplete = function ( quest, player )
				local context = quest.context
				if quest.ability:IsNull() then
					return false
				end

				if quest.ability:GetLevel() >= context.level.value then
					return true
				else
					return false
				end
			end,
			onComplete = function ( quest, player )
			end
		},
		{
			type = QUEST_HERO_REACH_LEVEL,
			rewords = allRewords,
			precheck = function ( player )
				local heros = GetAllHeroChessWhichIsNotTemp(player)
				if #heros == 0 then
					return false
				end
				for i = 1, #heros do
					if heros[i]:GetLevel() < 10 then
						return true
					end
				end
				return false
			end,
			prepareExecute = function(quest, player)
				local context = quest.context
				local sex = MAN
				if RandomInt(1, 100) > 50 then
					sex = WOMAN
				end

				context.namePublishedQuest = {
					type = "name",
					value = {
						type = "random",
						sex = sex,
						name = GenerateName(sex)
					}
				}

				context.stage = {
					type = "value",
					value = GameRules:GetGameModeEntity().currentStage + 10
				}

				local heros = GetAllHeroChessWhichIsNotTemp(player)
				local herosCanUpgrade = {}
				for i = 1, #heros do
					if heros[i]:GetLevel() < 10 then
						table.insert(herosCanUpgrade, heros[i])
					end
				end

				local random = RandomInt(1, #herosCanUpgrade)
				local hero = herosCanUpgrade[random]
				local level = math.min(10, RandomInt(hero:GetLevel() + 1, hero:GetLevel() + 3))
				context.level = {
					type = "value",
					value = level
				}

				context.heroName = {
					type = "name", 
					value = GetUnitName(hero)
				}
				quest.hero = hero
			end,
			canComplete = function ( quest, player )
				local context = quest.context
				if quest.hero:IsNull() then
					return false
				end

				if quest.hero:GetLevel() >= context.level.value then
					return true
				else
					return false
				end
			end,
			onComplete = function ( quest, player )
			end
		},
	}

	GameRules:GetGameModeEntity().quests = quests

	local rewordConfigs = {
		REWARD_GOLD = {
			type = REWARD_GOLD,
			prepareExecute = function( quest )
				local context = quest.context
				local cost = 5
				if quest.cost ~= nil then
					cost = quest.cost + 5
				end

				context.gold = {
					type = "value",
					value = cost
				}
			end,
			onComplete = function ( quest, player )
				local context = quest.context
				local courier = player:GetAssignedHero()
				courier:SetMana(courier:GetMana() + context.gold.value)
				EmitSoundForSell(player)
				SendUpdateStatusMessage(player)
			end
		},
		REWARD_EQUIPMENT = {
			type = REWARD_EQUIPMENT,
			prepareExecute = function( quest )
				local context = quest.context
				local shopItems = GetShopItemsForQuests({QUALITY_5}, 
					{TYPE_EQUIPMENT}, quest.filterItems)
				local shopItem
				if #shopItems > 0 then
					shopItem = shopItems[RandomInt(1, #shopItems)]
				else
					shopItem = GameRules:GetGameModeEntity().shopItems[1]
				end
				quest.shopItem = shopItem
				shopItem.existCount = shopItem.existCount + 1
				context.itemName = {
					type = "text",
					value = "DOTA_Tooltip_ability_" .. shopItem.name
				}
			end,
			onComplete = function ( quest, player )
				local context = quest.context
				local courier = player:GetAssignedHero()
				CreateShopItem(player, quest.shopItem, player:GetAssignedHero():GetAbsOrigin(), true)
			end,
			onCancel = function ( quest )
				local shopItem = quest.shopItem
				shopItem.existCount = shopItem.existCount - 1
			end
		},
		REWARD_ABILITY = {
			type = REWARD_ABILITY,
			prepareExecute = function( quest )
				local context = quest.context
				local shopItems = GetShopItemsForQuests({QUALITY_4, QUALITY_5}, 
					{TYPE_ABILITY}, quest.filterItems)
				local shopItem
				if #shopItems > 0 then
					shopItem = shopItems[RandomInt(1, #shopItems)]
				else
					shopItem = GameRules:GetGameModeEntity().shopItems[1]
				end
				quest.shopItem = shopItem
				shopItem.existCount = shopItem.existCount + 1
				context.itemName = {
					type = "text",
					value = "DOTA_Tooltip_ability_" .. shopItem.name
				}
			end,
			onComplete = function ( quest, player )
				local context = quest.context
				local courier = player:GetAssignedHero()
				CreateShopItem(player, quest.shopItem, player:GetAssignedHero():GetAbsOrigin(), true)
			end,
			onCancel = function ( quest )
				local shopItem = quest.shopItem
				shopItem.existCount = shopItem.existCount - 1
			end
		},
		REWARD_CONSUMABLE = {
			type = REWARD_CONSUMABLE,
			prepareExecute = function( quest )
				local context = quest.context
				local shopItems = GetShopItemsForQuests({QUALITY_4, QUALITY_5}, 
					{TYPE_CONSUMABLE}, quest.filterItems)
				local shopItem
				if #shopItems > 0 then
					shopItem = shopItems[RandomInt(1, #shopItems)]
				else
					shopItem = GameRules:GetGameModeEntity().shopItems[1]
				end
				quest.shopItem = shopItem
				shopItem.existCount = shopItem.existCount + 1
				context.itemName = {
					type = "text",
					value = "DOTA_Tooltip_ability_" .. shopItem.name
				}
			end,
			onComplete = function ( quest, player )
				local context = quest.context
				local courier = player:GetAssignedHero()
				CreateShopItem(player, quest.shopItem, player:GetAssignedHero():GetAbsOrigin(), true)
			end,
			onCancel = function ( quest )
				local shopItem = quest.shopItem
				shopItem.existCount = shopItem.existCount - 1
			end
		},
		REWARD_ABILITY_LEVEL_UP = {
			type = REWARD_ABILITY_LEVEL_UP,
			hide = true,
			prepareExecute = function( quest )
			end,
			onComplete = function ( quest, player )
				local context = quest.context
				local courier = player:GetAssignedHero()
				local heros = GetAllHeroChessInChessBoardWhichIsNotTemp(player)
				local success = false
				if #heros > 0 then
					local canUpgradeAbilites = GetCanUpgradeAbilities(heros)
					if #canUpgradeAbilites > 0 then
						local random = RandomInt(1, #canUpgradeAbilites)
						local ability = canUpgradeAbilites[random].ability
						ability:UpgradeAbility(false)
						EmitSoundForAbilityUpgrade(player)
						SendHintMessageToPlayer(player, 
							"UPGRADE_ABILITY", 
							{
								heroName = {type = "name", value = GetUnitName(canUpgradeAbilites[random].hero)}, 
								abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()}
							},
							false
						)
						quest.context.name = {type = "name", value = GetUnitName(canUpgradeAbilites[random].hero)}
						quest.context.abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()}
						success = true
					end
				end
				
				if success == false then
					Msg("When there is no ability can level up\n")
					GameRules:GetGameModeEntity().rewordConfigs.REWARD_GOLD.prepareExecute(quest)
					GameRules:GetGameModeEntity().rewordConfigs.REWARD_GOLD.onComplete(quest, player)
				end
			end,
			onCancel = function ( quest )
			end
		},
		REWARD_HERO_LEVEL_UP = {
			type = REWARD_HERO_LEVEL_UP,
			hide = true,
			prepareExecute = function( quest )
			end,
			onComplete = function ( quest, player )
				local context = quest.context
				local courier = player:GetAssignedHero()
				local heros = GetAllHeroChessInChessBoardWhichIsNotTemp(player)
				local herosCanUpgrade = {}
				local success = false
				if #heros > 0 then
					for i = 1, #heros do
						if heros[i].temp == nil and heros[i]:GetLevel() < 10 then
							table.insert(herosCanUpgrade, heros[i])
						end
					end
					if #herosCanUpgrade > 0 then
						local random = RandomInt(1, #herosCanUpgrade)
						local hero = herosCanUpgrade[random]
						hero:AddExperience(
							GameRules:GetGameModeEntity().gainExpForUnit[hero:GetLevel()], 
							DOTA_ModifyGold_AbilityCost, false, false)
						SendHintMessageToPlayer(player, 
							"UPGRADE_HERO", 
							{
								name = {type = "name", value = GetUnitName(hero)}
							},
							false
						)
						success = true 
						quest.context.name = {type = "name", value = GetUnitName(hero)}
					end
				end
				
				
				if success == false then
					GameRules:GetGameModeEntity().rewordConfigs.REWARD_GOLD.prepareExecute(quest)
					GameRules:GetGameModeEntity().rewordConfigs.REWARD_GOLD.onComplete(quest, player)
				end
			end,
			onCancel = function ( quest )
			end
		},
		REWARD_ETERNAL_CONTRACT = {
			type = REWARD_ETERNAL_CONTRACT,
			prepareExecute = function( quest )
			end,
			onComplete = function ( quest, player )
				local context = quest.context
				local courier = player:GetAssignedHero()
				local shopItem = FindShopItem(GameRules:GetGameModeEntity().shopItems, "item_eternal_contract")
				CreateShopItem(player, shopItem, player:GetAssignedHero():GetAbsOrigin(), true)
			end,
			onCancel = function ( quest )
			end
		},
	}
	GameRules:GetGameModeEntity().rewordConfigs = rewordConfigs
end

function RandomGenerateQuests( player )
	local questsCanPublish = {}
	for i = 1, #GameRules:GetGameModeEntity().quests do
		local quest = GameRules:GetGameModeEntity().quests[i]
		if quest.precheck == nil or quest.precheck(player) then
			table.insert(questsCanPublish, quest)
			if quest.type == QUEST_DEFEAT_BY_ADD then
				table.insert(questsCanPublish, quest)
				table.insert(questsCanPublish, quest)
			end
		end
	end	

	local questsOfPlayer = {}
	for i = 1, 3 do
		local quest = {}
		quest.quest = questsCanPublish[RandomInt(1, #questsCanPublish)]
		--quest.quest = questsCanPublish[7]
		quest.context = {}
		quest.filterItems = {}
		--Msg("quest=" .. TableToStr(quest) .. "\n")
		quest.quest.prepareExecute(quest, player)
		if quest.reword == nil then
			quest.reword = GetByProbabilityFromTable(quest.quest.rewords)
		end
		
		local rewordConfig = GameRules:GetGameModeEntity().rewordConfigs[quest.reword.type]
		--local rewordConfig = GameRules:GetGameModeEntity().rewordConfigs[REWARD_EQUIPMENT]
		quest.rewordConfig = rewordConfig
		if rewordConfig.hide == nil then
			quest.hide = false
		else
			quest.hide = rewordConfig.hide
		end

		if quest.reword.hide then
			quest.hide = true
		end
		
		quest.rewordConfig.prepareExecute(quest)
		quest.status = "CanAccept"
		table.insert(questsOfPlayer, quest)
	end
	player.quests = questsOfPlayer
end

function ClearQuests( player )
	local quests = player.quests
	if quests == nil then
		return
	end
	for i = 1, #quests do
		local quest = quests[i]
		if quest.endFlag ~= true then
			if quest.rewordConfig.onCancel ~= nil then
				quest.rewordConfig.onCancel(quest)
			end
			SendAdventureJournals(player, "ADVENTURE_JOURNALS_QUEST_TIMEOUT",
				{
					name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
					questName = {type = "text", value = quest.quest.type .. "_TITLE"}
			})
		end
	end
end

function GenerateQuests( )
	local players = GetAllAlivePlayers()
	--Msg("GenerateQuests\n")
	for i = 1, #players do
		ClearQuests(players[i])
		RandomGenerateQuests(players[i])
		SendQuestsUpdateMessage(players[i])
	end
end

function RandomEncounterAdventureForPlayer( player )
	local allRewords = {
		{type = REWARD_GOLD, probability = 20},
		{type = REWARD_EQUIPMENT, probability = 20},
		{type = REWARD_ABILITY, probability = 20},
		{type = REWARD_CONSUMABLE, probability = 20},
		{type = REWARD_ETERNAL_CONTRACT, probability = 20},
	}
	local reword = GetByProbabilityFromTable(allRewords)
	local rewordConfig = GameRules:GetGameModeEntity().rewordConfigs[reword.type]
	local quest = {}
	quest.context = {}
	quest.filterItems = {}
	rewordConfig.prepareExecute(quest)
	rewordConfig.onComplete(quest, player)

	local adventrueCount = 6
	local random = RandomInt(1, adventrueCount)

	SendHintMessageToPlayer(player, 
		"ADVENTURE_HINT_" .. reword.type, 
		quest.context,
		true,
		"tutorial_fanfare"
	)

	local adventureJournals = BuildAdventureJournals("ADVENTURE_JOURNALS_ADVENTURE_" .. random,
		{
			name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())}
	})
	adventureJournals[1].newLine = false

	local adventureJournal = BuildAdventureJournal(reword.type, nil)
	adventureJournal.tokens = quest.context
	adventureJournal.newLine = false
	table.insert(adventureJournals, adventureJournal)

	SendAdventureJournalsForAdventureJournals(player, adventureJournals)
end

function EncounterAdventure( )
	local players =  GetAllAlivePlayers()
	for i = 1, #players do
		local player = players[i]
		local random = RandomInt(1, 100)
		if random <= 5 then
			RandomEncounterAdventureForPlayer(player)
		end
	end
end

function CreateShopItemForHero(player, hero, shopItem, position, canUseDirectly )
	local newItem = CreateItem( shopItem.name, hero, player:GetAssignedHero())
	newItem.shopItem = shopItem

	if shopItem.type == TYPE_ABILITY then
		--不需要的直接不显示可依赖
		local abilitiesConfig = GameRules:GetGameModeEntity().abilities
		local abilityConfig = abilitiesConfig[shopItem.abilityName]
		if abilityConfig.needs == nil or #abilityConfig.needs == 0 then
			canUseDirectly = false
		end

		if abilityConfig.type == SecondaryProfession then
			canUseDirectly = false
		end

		if canUseDirectly ~= nil then
			newItem.canUseDirectly = canUseDirectly
		else
			local random = RandomInt(1, 10)
			if random <= 3 then
				newItem.canUseDirectly = true
			else
				newItem.canUseDirectly = false
			end
		end
	else
		canUseDirectly = false
	end

	--Msg("Number=" .. courier:GetNumItemsInInventory() .. "\n")
	if hero:GetNumItemsInInventory() < 9 then
		hero:AddItem(newItem)
	else
		CreateItemOnPositionForLaunch(position, newItem)
	end
end

--canUseDirectly only for ability
function CreateShopItem( player, shopItem, position, canUseDirectly)
	CreateShopItemForHero(player, player:GetAssignedHero(), shopItem, position, canUseDirectly)
end

testCount = 0
function InitUnit( unit, summon )
	summon = summon or false
	unit:SetDeathXP(0)
	unit:SetMaximumGoldBounty(0)
	unit:SetMinimumGoldBounty(0)
	unit:SetHullRadius(24)
	AddAbility(unit, "naga_siren_mirror_image", 5, false) --没有主动技能不攻击，2020.5.26后的bug
	if summon == false and unit:IsHero() and unit.canSell ~= nil then
		--RemoveAbilityAndModifier(unit, "race_human")
		
		--AddAbility(unit, "race_human", 5, true)
		--AddAbility(unit, "race_angel", 5, true)
		--AddAbility(unit, "race_demon", 5, true)
		--AddAbility(unit, "race_dragon", 3, true)

		--AddAbility(unit,"legend_profession", 5, true)
		--AddAbility(unit, "legend_race", 5, false)
		--AddAbility(unit, "profession_warrior", 5, true)
		--AddAbility(unit, "profession_berserker", 5, true)
		--AddAbility(unit, "legend_profession_berserker", 5, true)
		--AddAbility(unit, "profession_shield_warrior", 5, true)
		--AddAbility(unit, "legend_profession_shield_warrior", 5, true)
		--AddAbility(unit, "profession_assassin", 5, true)
		--AddAbility(unit, "profession_phantom_assassin", 5, true)
		--AddAbility(unit, "legend_profession_phantom_assassin", 5, true)
		--AddAbility(unit, "profession_templar_assassin", 5, true)
		--AddAbility(unit, "legend_profession_templar_assassin", 5, true)
		--AddAbility(unit, "profession_priest", 5, true)
		--AddAbility(unit, "profession_holy_light_priest", 5, true)
		--AddAbility(unit, "legend_profession_holy_light_priest", 5, true)
		--AddAbility(unit, "well_of_wizard_aura", 5, true)
		--AddAbility(unit, "profession_shadow_priest", 5, true)
		--AddAbility(unit, "legend_profession_shadow_priest", 5, true)
		--AddAbility(unit, "profession_archer", 5, true)
		--AddAbility(unit, "profession_phantom_archer", 5, true)
		--AddAbility(unit, "legend_profession_phantom_archer", 5, true)
		--AddAbility(unit, "profession_animal_trainer", 5, true)
		--AddAbility(unit, "profession_beastmaster", 5, true)
		--AddAbility(unit, "legend_profession_beastmaster", 5, true)
		--AddAbility(unit, "profession_druid", 5, true)
		--AddAbility(unit, "profession_beast_druid", 5, true)
		--AddAbility(unit, "legend_profession_beast_druid", 5, true)
		--AddAbility(unit, "profession_knight", 5, true)
		--AddAbility(unit, "profession_paladin", 5, true)
		--AddAbility(unit, "legend_profession_paladin", 5, true)
		--AddAbility(unit, "profession_mage", 3, true)
		--AddAbility(unit, "profession_fire_mage", 5, true)
		--AddAbility(unit, "legend_profession_fire_mage", 5, true)
		--AddAbility(unit, "profession_earth_mage", 5, true)
		--AddAbility(unit, "legend_profession_earth_mage", 5, true)

		--AddAbility(unit, "axe_berserkers_call", 5, true)
		--AddAbility(unit, "axe_culling_blade", 5, true)
		--AddAbility(unit, "life_stealer_rage", 5, true)
		--AddAbility(unit, "huskar_berserkers_blood", 5, true)
		--AddAbility(unit, "mars_gods_rebuke", 3, true)
		--AddAbility(unit, "mars_bulwark", 3, true)
		if testCount % 2 == 0 then
			--AddAbility(unit, "bounty_hunter_wind_walk", 3, true)
		end
		--AddAbility(unit, "nyx_assassin_vendetta", 5, true)
		--AddAbility(unit, "bounty_hunter_shuriken_toss", 5, true)
		--AddAbility(unit, "phantom_assassin_phantom_strike", 5, true)
		--AddAbility(unit, "phantom_assassin_coup_de_grace", 5, true)
		--AddAbility(unit, "templar_assassin_refraction", 5, true)
		--AddAbility(unit, "templar_assassin_psi_blades", 5, true)
		--AddAbility(unit, "enchantress_natures_attendants", 5, true)
		--AddAbility(unit, "keeper_of_the_light_chakra_magic", 5, true)
		--AddAbility(unit, "chen_divine_favor", 5, true)
		--AddAbility(unit, "chen_hand_of_god", 5, true)
		--AddAbility(unit, "dazzle_bad_juju", 5, true)
		--AddAbility(unit, "dazzle_shallow_grave", 5, true)

		--AddAbility(unit, "dazzle_shadow_wave", 5, true)
		--AddAbility(unit, "drow_ranger_marksmanship", 5, true)
		--AddAbility(unit, "drow_ranger_wave_of_silence", 5, true)
		--AddAbility(unit, "medusa_split_shot", 5, true)
		--AddAbility(unit, "phantom", 5, true)
		--AddAbility(unit, "windrunner_powershot", 5, true)
		--AddAbility(unit, "call_of_the_wild_hawk", 5, true)
		--AddAbility(unit, "beastmaster_wild_axes", 5, true)
		--AddAbility(unit, "beastmaster_inner_beast", 5, true)
		--AddAbility(unit, "call_of_the_wild_boar", 5, true)
		--AddAbility(unit, "beastmaster_primal_roar", 5, true)
		--AddAbility(unit, "treant_living_armor", 5, true)
		--AddAbility(unit, "tree_call", 5, true)
		--AddAbility(unit, "call_of_spirit_bear", 5, true)
		--AddAbility(unit, "lone_druid_true_form", 5, true)
		--AddAbility(unit, "abaddon_aphotic_shield", 5, true)
		--AddAbility(unit, "omniknight_repel", 5, true)
		--AddAbility(unit, "omniknight_purification", 5, true)
		--AddAbility(unit, "omniknight_guardian_angel", 5, true)
		--AddAbility(unit, "axe_counter_helix", 4, true)

		--AddAbility(unit, "sven_great_cleave", 5, true)

		--AddAbility(unit, "blade_dance", 4, true)
		--AddAbility(unit, "meditate", 5, true)
		--AddAbility(unit, "spell_amplify", 5, true)
		--AddAbility(unit, "skywrath_mage_arcane_bolt", 5, true)
		--AddAbility(unit, "fire_golem_summon", 3, true)
		--AddAbility(unit, "lina_dragon_slave", 5, true)
		--AddAbility(unit, "lina_light_strike_array", 5, true)
		--AddAbility(unit, "abyssal_underlord_firestorm", 5, true)
		--AddAbility(unit, "ember_spirit_flame_guard", 5, true)
		--AddAbility(unit, "guardian_of_earth", 5, true)
		--AddAbility(unit, "absolute_defense", 5, true)
		--AddAbility(unit, "earthshaker_aftershock", 5, true)
		--AddAbility(unit, "earthshaker_echo_slam", 5, true)
		--AddAbility(unit, "tidehunter_ravage", 5, true)
		--AddAbility(unit, "necronomicon_warrior_summon", 5, true)
		--AddAbility(unit, "vampiric_aura", 5, true)
		--AddAbility(unit, "necrolyte_heartstopper_aura", 3, true)
		--AddAbility(unit, "faceless_void_time_lock", 5, true)
		--AddAbility(unit, "faceless_void_chronosphere", 5, true)
		--AddAbility(unit, "senility", 5, true)
		--AddAbility(unit, "skeleton_king_reincarnation", 3, false)

		--AddAbility(unit, "zuus_thundergods_wrath", 5, false)

		--AddAbility(unit, "phantom_lancer_spirit_lance", 3, false)
		--AddAbility(unit, "phantom_lancer_juxtapose", 3, false)
		--AddAbility(unit, "chaos_knight_phantasm", 3, false)

		--AddAbility(unit, "zuus_thundergods_wrath", 5, true)
		--AddAbility(unit, "slardar_bash", 5, true)

		--AddAbility(unit, "gift_agility", 5, true)
		--AddAbility(unit, "gift_intellect", 5, true)
		--AddAbility(unit, "gift_strength", 5, true)
		--AddAbility(unit, "angelic_alliance_aura", 1, false)
		--AddAbility(unit, "armor_of_the_damned_aura", 1, false)
		--AddAbility(unit, "assault_cuirass_aura", 1, false)
		--AddAbility(unit, "cloak_of_the_undead_king_aura", 1, false)
		--AddAbility(unit, "ring_of_the_magi_aura", 1, false)
		--AddAbility(unit, "bow_of_the_sharpshooter_aura", 1, false)
		--AddAbility(unit, "elixir_of_life_aura", 1, false)
		--AddAbility(unit, "power_of_the_dragon_father_aura", 1, false)
		--AddAbility(unit, "well_of_wizard_aura", 1, false)
		--AddAbility(unit, "well_of_wizard_aura", 1, false)
		--AddAbility(unit, "eternal_contract")

		--AddAbility(unit, "profession_businessman", 5, true)
		--AddAbility(unit, "invest", 5, true)
		--AddAbility(unit, "business", 5, true)

		--AddAbility(unit, "profession_lord", 5, true)
		--AddAbility(unit, "offense", 5, true)
		--AddAbility(unit, "armorer", 5, true)
		--AddAbility(unit, "resistance", 5, true)
		--AddAbility(unit, "profession_pharmaceutist", 5, true)
		--AddAbility(unit, "health_potion", 5, true)
		--AddAbility(unit, "mana_potion", 5, true)
		--AddAbility(unit, "profession_blacksmith", 5, true)
		--AddAbility(unit, "forge", 5, true)
		--AddAbility(unit, "blacksmith_resistance", 5, true)
		--AddAbility(unit, "profession_navigator", 5, true)
		--AddAbility(unit, "explore", 5, true)
		--AddAbility(unit, "storm_sailing", 5, true)
		--AddAbility(unit, "profession_prophet", 5, true)
		--AddAbility(unit, "prophecy", 5, true)
		--AddAbility(unit, "big_prophecy", 5, true)
		--AddAbility(unit, "profession_diviner", 5, true)
		--AddAbility(unit, "luck", 5, true)
		--AddAbility(unit, "unluck", 5, true)
		--AddAbility(unit, "self_luck", 5, true)
		--AddAbility(unit, "self_unluck", 5, true)

		testCount = testCount + 1
	else
		--AddAbility(unit, "tidehunter_ravage", 5, false)
	end

	

end

function CreateHero(player, shopItem)
	local team1_grid_marker = FindBaseGridMarker(player:GetPlayerID())
	GameRules:GetGameModeEntity().createUnit = true
	local hero = CreateHeroForPlayer(shopItem.base.name, player)
	GameRules:GetGameModeEntity().createUnit = false
	InitName(player, false, hero, shopItem.base)
	hero:SetAbsOrigin(team1_grid_marker:GetOrigin())
	hero:SetControllableByPlayer(player:GetPlayerID(), false)
	hero:SetMoveCapability(DOTA_UNIT_CAP_MOVE_NONE)
	hero.attackType = hero:GetAttackCapability()
	hero:SetAttackCapability(DOTA_UNIT_CAP_NO_ATTACK)
	hero.abilities = {}
	hero.abilitiesCount = 0
	hero.shopItem = shopItem
	hero.canSell = true

	InitUnit(hero)

	-- init hero
	for i = 1, #shopItem.abilities do
		local ability = shopItem.abilities[i]
		AddAbility(hero, ability.abilityName, ability.level, true)
	end

	for i = 2, shopItem.level do
		GameRules:GetGameModeEntity().ignoreGainedLevel = true
		hero:HeroLevelUp(false)
		GameRules:GetGameModeEntity().ignoreGainedLevel = false
	end

	AddAbility(hero, "invulnerable")
	AddAbility(hero, "silenced")
	return hero
end

function GetFinalProfessionAbilitiesByFilter( filters )
	local finalProfessionAbilities = GetFinalProfessionAbilities()
	local result = {}
	--Msg("filter=" .. TableToStr(filters) .. "\n")
	for i = 1, #finalProfessionAbilities do
		local finalProfessionAbility = finalProfessionAbilities[i]
		if IsIn(filters, finalProfessionAbility.professionType) then
			table.insert(result, finalProfessionAbility)
			--Msg("result=" .. TableToStr(finalProfessionAbility) .. "\n")
		end
	end
	return result
end

function RandomSelectFinalProfession( initialProfessions, filters, minCount, maxCount )
	local tmpProfessionAbilities = GetFinalProfessionAbilitiesByFilter(filters)
	local count = RandomInt(minCount, maxCount)
	count = math.min(count, #tmpProfessionAbilities)
	for i = 1, count do
		local random = RandomInt(1, #tmpProfessionAbilities)
		table.insert(initialProfessions, tmpProfessionAbilities[random])
		table.remove(tmpProfessionAbilities, random)
	end
end

function InitShopConfigs(player)
	local finalProfessionAbilities = GetFinalProfessionAbilities()
	player.shopConfigs = {}
	player.shopConfigs.professions = {}
	for i = 1, #finalProfessionAbilities do
		player.shopConfigs.professions[finalProfessionAbilities[i].name] = {}
		player.shopConfigs.professions[finalProfessionAbilities[i].name].index = finalProfessionAbilities[i].index
		player.shopConfigs.professions[finalProfessionAbilities[i].name].enable = false
	end

	initialProfessions = {}

	if IsCorpsMode() then
		RandomSelectFinalProfession(initialProfessions, {ProfessionTank}, 2, 2)
		RandomSelectFinalProfession(initialProfessions, {ProfessionPriest}, 2, 2)
		RandomSelectFinalProfession(initialProfessions, {ProfessionPhysicalDamage, ProfessionMagicalDamage}, 4, 8)
		RandomSelectFinalProfession(initialProfessions, {ProfessionAssist, ProfessionControl}, 2, 4)
	else
		RandomSelectFinalProfession(initialProfessions, {ProfessionTank}, 1, 1)
		RandomSelectFinalProfession(initialProfessions, {ProfessionPriest}, 1, 1)
		RandomSelectFinalProfession(initialProfessions, {ProfessionPhysicalDamage, ProfessionMagicalDamage}, 2, 4)
		RandomSelectFinalProfession(initialProfessions, {ProfessionAssist, ProfessionControl}, 1, 2)
	end
	
	for i = 1, #initialProfessions do
		player.shopConfigs.professions[initialProfessions[i].name].enable = true
	end
	ShowShopConfigsPanel(player)
end

function ShowShopConfigsPanel( player )
	player.shopConfigs.isShow = true
	CustomUI:DynamicHud_Create(player:GetPlayerID(), "ShopConfigsPanel", "file://{resources}/layout/custom_game/shop_configs.xml", nil)
end

function DestoryShopConfigsPanel( player )
	CustomUI:DynamicHud_Destroy(player:GetPlayerID(), "ShopConfigsPanel")
	player.shopConfigs.isShow = false
end

function OnGetShopConfigs( index, keys )
	local itemIndex = keys.itemIndex
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	CustomGameEventManager:Send_ServerToPlayer(
		player,
		"on_receive_shop_configs", 
		player.shopConfigs)
end

function OnSwitchShopConfigs( index, keys )
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	if player.shopConfigs == nil then
		return
	end
	if player.shopConfigs.isShow then
		DestoryShopConfigsPanel(player)
	else
		ShowShopConfigsPanel(player)
	end
end

function BoolToString( value )
	return value and "true" or "false"
end

function IntegerToBool( value )
	if value == 1 then
		return true
	else
		return false
	end
end

function GetModifiedProfessionConfigCount(player, professionConfigs )
	--Msg("professionConfigs=" .. TableToStr(professionConfigs) .. "\n")
	local modifiedCount = 0
	local leftCount = 0
	for name, value in pairs(player.shopConfigs.professions) do
		if value.enable ~= professionConfigs[name].enable then
			--Msg("modified=" .. name .. ", old=" ..  BoolToString(value.enable) 
			--	.. ", new=" .. BoolToString(professionConfigs[name]) .. "\n")
			modifiedCount = modifiedCount + 1
		end
		if professionConfigs[name].enable == true then
			leftCount = leftCount + 1
		end
	end
	return {modifiedCount = modifiedCount, leftCount = leftCount}
end

function OnChangeShopConfigs( index, keys )
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	local professionConfigs = keys.professionConfigs
	for name, value in pairs(professionConfigs) do
		professionConfigs[name].enable = IntegerToBool(value.enable)
	end
	local count = GetModifiedProfessionConfigCount(player, professionConfigs)
	if count.modifiedCount <= 0 then
		DestoryShopConfigsPanel(player)
		return
	end
	--Msg("count=" .. TableToStr(count) .. "\n")

	if IsCorpsMode() then
		if count.leftCount < 6 then
			SendErrorMessageToPlayer(player, "SHOP_PROFESSION_CANNOT_LESS_THAN_VALUE")
			return
		end
	end

	local gold = count.modifiedCount * 5
	local courier = player:GetAssignedHero()
	if courier:GetMana() < gold then
		SendErrorMessageToPlayer(player, "NOT_ENOUGH_MONEY_TO_SET_SHOP_CONFIGS")
		return
	end
	courier:SetMana(courier:GetMana() - gold)
	player.shopConfigs.professions = keys.professionConfigs
	EmitSoundForBuy(player)
	SendUpdateStatusMessage(player)
	DestoryShopConfigsPanel(player)
end

function OnCancelShopConfigs( index, keys )
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	DestoryShopConfigsPanel(player)
end

function OnGetProfessionRoute( index, keys )
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	CustomGameEventManager:Send_ServerToPlayer(
		player,
		"on_receive_profession_route", 
		GameRules:GetGameModeEntity().abilities)
end

function OnSelectGameMode( index, keys )
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	GameRules:GetGameModeEntity().gameMode = keys.gameMode
	CustomGameEventManager:Send_ServerToAllClients(
		"on_receive_game_mode", 
		{gameMode = GameRules:GetGameModeEntity().gameMode})
	EmitGlobalSound("General.SelectUnit")
end

function OnBuyShopItem(index, keys)
	local itemIndex = keys.itemIndex
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	local courier = player:GetAssignedHero()
	local shopItem = player.shopItems[itemIndex].shopItem;

	if player.shopItems[itemIndex].isBought then
		return
	end

	if player.picked then
		SendErrorMessageToPlayer(player, "CANNOT_BUY_WHEN_PICKED")
		return
	end

	if courier:GetMana() - shopItem.cost < 0 then
		SendErrorMessageToPlayer(player, "NOT_ENOUGH_MONEY")
		return
	end

	if shopItem.type == TYPE_HERO then
		local emptyChessIndex = FindSpareIndexForWaitArea(player)
		if emptyChessIndex == nil then
			SendErrorMessageToPlayer(player, "NO_SPARE_PLACE")
			return
		end
		local hero = CreateHero(player, shopItem)
		PutChess(player, emptyChessIndex, hero, nil)
		courier:SetMana(courier:GetMana() - shopItem.cost)
		SendAdventureJournalsForHeroInfo(player, "ADVENTURE_JOURNALS_BUY_HERO",
			{
				name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
				heroName = {type = "name", value = GetUnitName(hero)}
			}, hero)
	else
		CreateShopItem(player, shopItem, player:GetAssignedHero():GetAbsOrigin(), player.shopItems[itemIndex].canUseDirectly)
		courier:SetMana(courier:GetMana() - shopItem.cost)
		SendAdventureJournals(player, "ADVENTURE_JOURNALS_BUY_SHOP_ITEM",
			{
				name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
				itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. shopItem.name}
			})
	end

	player.shopItems[itemIndex].isBought = true;
	EmitSoundForBuy(player)
	SendShopUpdateMessage(player)
	SendUpdateStatusMessage(player)
end

function OnShareItem( index, keys )
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	if player.shareItemPanel == true then
		CustomUI:DynamicHud_Destroy(keys.PlayerID, "ShareItemPanel")
		player.shareItemPanel = false
	else
		CustomUI:DynamicHud_Create(keys.PlayerID, "ShareItemPanel", "file://{resources}/layout/custom_game/share.xml", nil)
		player.shareItemPanel = true
	end
end

function OnCancelShareItem(index, keys)
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	CustomUI:DynamicHud_Destroy(keys.PlayerID, "ShareItemPanel")
	player.shareItemPanel = false
end

function OnConfirmShareItem(index, keys)
	--Msg("keys=" .. TableToStr(keys) .. "\n")
	local itemIndex = keys.item
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	local courier = player:GetAssignedHero()
	local item = courier:GetItemInSlot(itemIndex - 1)
	if item == nil then
		SendErrorMessageToPlayer(player, "NO_ITEM_IN_SLOT")
		return
	end

	local teamId = keys.team + DOTA_TEAM_CUSTOM_1 - 1
	local shareToPlayer = GetPlayerByTeam(teamId)
	if shareToPlayer == nil then
		SendErrorMessageToPlayer(player, "NO_THAT_PLAYER")
		return
	end

	courier:DropItemAtPositionImmediate(item, shareToPlayer:GetAssignedHero():GetAbsOrigin())
	SendAdventureJournals(player, "ADVENTURE_JOURNALS_GIVE_ITEM",
		{
			fromName = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
			toName  = {type = "value", value = PlayerResource:GetPlayerName(shareToPlayer:GetPlayerID())},
			itemName = {type = "text", value =  "DOTA_Tooltip_ability_" .. item:GetName()}
		})
	SendAdventureJournals(shareToPlayer, "ADVENTURE_JOURNALS_RECEIVE_ITEM",
		{
			fromName = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
			toName  = {type = "value", value = PlayerResource:GetPlayerName(shareToPlayer:GetPlayerID())},
			itemName = {type = "text", value =  "DOTA_Tooltip_ability_" .. item:GetName()}
		})
end

function PlayParticle(particleName, position, unit, deadTime)
	local particle = ParticleManager:CreateParticle(particleName, position, unit)
	Timers:CreateTimer(deadTime, function()
		if particle ~= nil then
			ParticleManager:DestroyParticle(particle,true)
		end
	end)
end

function GetAllUnitsInChessBoard( player )
	local heros = GetAllHeroChessInChessBoard(player)
	local units = heros
	if player.otherUnits ~= nil then
		for i = 1, #player.otherUnits do
			table.insert(units, player.otherUnits[i])
		end
	end

	for i = 1, player.enemiesCount do
		if player.enemies[i]:IsNull() == false then
			table.insert(units, player.enemies[i])
		end
	end

	return units
end

function FindNearestPosition( sparePositions, position )
	local distance = 9999
	local selectedPosition = nil
	local j = 1
	for i = 1, #sparePositions do
		local sparePosition = sparePositions[i]
		if (Vector(sparePosition.x, sparePosition.y) - Vector(position.x, position.y)):Length2D() < distance then
			distance =  (Vector(sparePosition.x, sparePosition.y) - Vector(position.x, position.y)):Length2D()
			selectedPosition = {sparePosition = sparePosition, index = i}
		end
	end
	return selectedPosition
end

function FindEmptyIndexes( player, position, indexCount )
	local units = GetAllUnitsInChessBoard(player)
	local indexStatus = {}
	for i = 1, 8 do
		indexStatus[i] = {}
		for j = 3, 10 do
			indexStatus[i][j] = false
		end
	end

	for i = 1, #units do
		local unit = units[i]
		if unit:IsNull() == false then
			local index = WorldPosToIndex(player:GetPlayerID(), unit:GetAbsOrigin())
			if index.x >=1 and index.x <= 8 and index.y >=2 and index.y <= 10 then
				indexStatus[index.x][index.y] = true
			end
		end
	end

	local spareIndexes = {}
	for i = 1, 8 do
		for j = 2, 10 do
			if indexStatus[i][j] == false then
				table.insert(spareIndexes, {x = i, y = j})
			end
		end
	end

	local index = WorldPosToIndex(player:GetPlayerID(), position)
	local selectedIndexes = {}
	if #spareIndexes < indexCount then
		return selectedIndexes
	else
		for i = 1, indexCount do
			local selectedIndex = FindNearestPosition(spareIndexes, index)
			table.remove(spareIndexes, selectedIndex.index)
			table.insert(selectedIndexes, selectedIndex.sparePosition)
		end
	end

	return selectedIndexes
end

function FindEmptyIndexesForCombinedBattle( position, indexCount )
	local players = GetAllAlivePlayers()
	local units = {}
	for i = 1, #players do
		local player = players[i]
		local unitsOfPlayer = GetAllUnitsInChessBoard(player)
		for j = 1, #unitsOfPlayer do
			local unitOfPlayer = unitsOfPlayer[j]
			table.insert(units, unitOfPlayer)
		end
	end

	local indexStatus = {}
	for i = 1, 8 do
		indexStatus[i] = {}
		for j = 3, 10 do
			indexStatus[i][j] = false
		end
	end

	for i = 1, #units do
		local unit = units[i]
		if unit:IsNull() == false then
			local index = WorldPosToIndex(-1, unit:GetAbsOrigin())
			if index.x >=1 and index.x <= 8 and index.y >=2 and index.y <= 10 then
				indexStatus[index.x][index.y] = true
			end
		end
	end

	local spareIndexes = {}
	for i = 1, 8 do
		for j = 2, 10 do
			if indexStatus[i][j] == false then
				table.insert(spareIndexes, {x = i, y = j})
			end
		end
	end

	local index = WorldPosToIndex(-1, position)
	local selectedIndexes = {}
	if #spareIndexes < indexCount then
		return selectedIndexes
	else
		for i = 1, indexCount do
			local selectedIndex = FindNearestPosition(spareIndexes, index)
			table.remove(spareIndexes, selectedIndex.index)
			table.insert(selectedIndexes, selectedIndex.sparePosition)
		end
	end

	return selectedIndexes
end

function FindEmptyPositionsForNormalBattle( player, position, positionCount )
	local indexes = FindEmptyIndexes(player, position, positionCount)
	local team1_grid_marker = FindBaseGridMarker(player:GetPlayerID())
	local positions = {}
	for i = 1, #indexes do
		local worldPosition = IndexToWorldPos(player:GetPlayerID(), indexes[i])
		local location = team1_grid_marker:GetOrigin()
		location.x = worldPosition.x
		location.y = worldPosition.y
		table.insert(positions, location)
	end

	return positions
end

function FindEmptyPositionsForCombinedBattle(  position, positionCount )
	local indexes = FindEmptyIndexesForCombinedBattle(position, positionCount)
	local team1_grid_marker = FindBaseGridMarker(-1)
	local positions = {}
	for i = 1, #indexes do
		local worldPosition = IndexToWorldPos(-1, indexes[i])
		local location = team1_grid_marker:GetOrigin()
		location.x = worldPosition.x
		location.y = worldPosition.y
		table.insert(positions, location)
	end

	return positions
end

function FindEmptyPositions( player, position, positionCount )
	local isCombinedBattle = IsCombinedBattle()
	--Msg("FindEmptyPositions=" .. (isCombinedBattle and "true" or "false") .. ", GameRules:GetGameModeEntity().currentStage=" .. GameRules:GetGameModeEntity().currentStage .. "\n")
	if isCombinedBattle then
		return FindEmptyPositionsForCombinedBattle(position, positionCount)
	else
		return FindEmptyPositionsForNormalBattle(player, position, positionCount)
	end
end

function GetLegendProfessionBeastmasterParamters( caster, currentCount, currentAttack, currentMaxHealth)
	local ability = caster:FindAbilityByName("legend_profession_beastmaster")
	if ability ~= nil then
		local count = ability:GetSpecialValueFor("call_count")
		local attack = ability:GetSpecialValueFor("call_attack")
		local maxHealth = ability:GetSpecialValueFor("call_max_health")
		return {
			count = currentCount + count,
			attack = currentAttack * attack,
			maxHealth = currentMaxHealth * maxHealth
		}
	else
		return {
			count = currentCount,
			attack = currentAttack,
			maxHealth = currentMaxHealth
		}
	end
end

HPBonusMultipleForCallUnit = 5

function CallOfTheWildHawk( keys )
	--Msg("CallOfTheWildHawk, keys=" .. TableToStr(keys) .. "\n")
	local ability = keys.ability
	local caster = keys.caster
	local level = ability:GetLevel() or 1
	local p = keys.target_points[1]

	local count = ability:GetSpecialValueFor("hawk_count")
	local attack = ability:GetSpecialValueFor("hawk_damage_tooltip")
	local maxHealth = ability:GetSpecialValueFor("hawk_hp_tooltip")

	maxHealth = maxHealth + caster:GetStrength() * HPBonusMultipleForCallUnit / count

	local beastmasterParamters = GetLegendProfessionBeastmasterParamters(caster, count, attack, maxHealth)

	for i = 1, beastmasterParamters.count do
		local unit = CustomCreateUnitByName("npc_dota_beastmaster_hawk" .. level, p, true, caster, caster, caster:GetTeam(), caster.player)
		PlayParticle("particles/units/heroes/hero_beastmaster/beastmaster_call_bird.vpcf", PATTACH_OVERHEAD_FOLLOW, unit,3)
		AddAbility(unit, "tower_true_sight", 1)

		unit:SetBaseDamageMin(beastmasterParamters.attack)
		unit:SetBaseDamageMax(beastmasterParamters.attack)
		unit:SetBaseMaxHealth(beastmasterParamters.maxHealth)
		GameRules:GetGameModeEntity().createUnit = true
		unit:RespawnUnit()
		GameRules:GetGameModeEntity().createUnit = false

		InitUnit(unit, true)
	end

end

function GetPlayerOwner(hero)
	if hero.player ~= nil then
		return hero.player
	else
		local team = hero:GetTeam()
		if team == DOTA_TEAM_BADGUYS then
			local players = GetAllPlayers()
			for i = 1, #players do
				if players[i].enemyPlayer == hero:GetPlayerOwner() then
					return players[i]
				else
					return hero:GetPlayerOwner()
				end
			end
			
		else
			return hero:GetPlayerOwner()
		end

		
	end
end

function CallOfTheWildBoar( keys )
	--Msg("CallOfTheWildBoar, keys=" .. TableToStr(keys) .. "\n")
	local ability = keys.ability
	local caster = keys.caster
	local level = ability:GetLevel() or 1

	local count = ability:GetSpecialValueFor("boar_count")
	local attack = ability:GetSpecialValueFor("boar_damage_tooltip")
	local maxHealth = ability:GetSpecialValueFor("boar_hp_tooltip")

	maxHealth = maxHealth + caster:GetStrength() * HPBonusMultipleForCallUnit / count

	local beastmasterParamters = GetLegendProfessionBeastmasterParamters(caster, count, attack, maxHealth)

	local player = GetPlayerOwner(caster)

	local selectedPositions = FindEmptyPositions( player, caster:GetAbsOrigin(), beastmasterParamters.count)
	if #selectedPositions == 0 then
		SendErrorMessageToPlayer(PlayerResource:GetPlayer(player:GetPlayerID()), "MAGIC_FAILED_FOR_CALL_OF_THE_WILD_BOAR")
		return
	end

	for i = 1, beastmasterParamters.count do
		local unit = CustomCreateUnitByName("npc_dota_beastmaster_boar"  .. level, selectedPositions[i], true, caster, caster, caster:GetTeam(), caster.player)
		PlayParticle("particles/units/heroes/hero_beastmaster/beastmaster_call_boar.vpcf", PATTACH_OVERHEAD_FOLLOW, unit, 3)
		AddAbility(unit, "beastmaster_boar_poison", level)

		unit:SetBaseDamageMin(beastmasterParamters.attack)
		unit:SetBaseDamageMax(beastmasterParamters.attack)
		unit:SetBaseMaxHealth(beastmasterParamters.maxHealth)
		GameRules:GetGameModeEntity().createUnit = true
		unit:RespawnUnit()
		GameRules:GetGameModeEntity().createUnit = false

		InitUnit(unit, true)
	end
end

function GetLegendProfessionBeastDruidParamters( caster, currentAttack, currentMaxHealth)
	local ability = caster:FindAbilityByName("legend_profession_beast_druid")
	if ability ~= nil then
		local attack = ability:GetSpecialValueFor("call_attack")
		local maxHealth = ability:GetSpecialValueFor("call_max_health")
		return {
			attack = currentAttack * attack,
			maxHealth = currentMaxHealth * maxHealth
		}
	else
		return {
			attack = currentAttack,
			maxHealth = currentMaxHealth
		}
	end
end

function TreeCall( keys )
	--Msg("TreeCall, keys=" .. TableToStr(keys) .. "\n")
	local ability = keys.ability
	local caster = keys.caster
	local level = ability:GetLevel() or 1

	local count = ability:GetSpecialValueFor("tree_count")
	local attack = ability:GetSpecialValueFor("tree_damage_tooltip")
	local maxHealth = ability:GetSpecialValueFor("tree_hp_tooltip")

	maxHealth = maxHealth + caster:GetStrength() * HPBonusMultipleForCallUnit / count

	local parameters = GetLegendProfessionBeastDruidParamters(caster, attack, maxHealth)

	local player = GetPlayerOwner(caster)
	local selectedPositions = FindEmptyPositions( player, caster:GetAbsOrigin(), count)
	if #selectedPositions == 0 then
		SendErrorMessageToPlayer(PlayerResource:GetPlayer(player:GetPlayerID()), "MAGIC_FAILED_FOR_TREE_CALL")
		return
	end

	for i = 1, count do
		local unit = CustomCreateUnitByName("npc_dota_tree_man"  .. level, selectedPositions[i], true, caster, caster, caster:GetTeam(), caster.player)
		PlayParticle("particles/units/heroes/hero_furion/furion_force_of_nature_cast.vpcf", PATTACH_OVERHEAD_FOLLOW, unit, 3)

		unit:SetBaseDamageMin(parameters.attack)
		unit:SetBaseDamageMax(parameters.attack)
		unit:SetBaseMaxHealth(parameters.maxHealth)
		GameRules:GetGameModeEntity().createUnit = true
		unit:RespawnUnit()
		GameRules:GetGameModeEntity().createUnit = false

		InitUnit(unit, true)
	end
end

function CallOfSpiritBear( keys )
	--Msg("CallOfSpiritBear, keys=" .. TableToStr(keys) .. "\n")
	local ability = keys.ability
	local caster = keys.caster
	local level = ability:GetLevel() or 1

	local count = 1
	local attack = ability:GetSpecialValueFor("bear_damage_tooltip")
	local maxHealth = ability:GetSpecialValueFor("bear_hp")

	maxHealth = maxHealth + caster:GetStrength() * HPBonusMultipleForCallUnit / count

	local parameters = GetLegendProfessionBeastDruidParamters(caster, attack, maxHealth)

	local player = GetPlayerOwner(caster)
	local selectedPositions = FindEmptyPositions( player, caster:GetAbsOrigin(), count)
	if #selectedPositions == 0 then
		SendErrorMessageToPlayer(PlayerResource:GetPlayer(player:GetPlayerID()), "MAGIC_FAILED_FOR_CALL_OF_SPIRIT_BEAR")
		return
	end

	for i = 1, count do
		local unit = CustomCreateUnitByName("npc_dota_lone_druid_bear"  .. level, selectedPositions[i], true, caster, caster, caster:GetTeam(), caster.player)
		PlayParticle("particles/units/heroes/hero_lone_druid/lone_druid_bear_spawn.vpcf", PATTACH_OVERHEAD_FOLLOW, unit, 3)

		AddAbility(unit, "lone_druid_spirit_bear_demolish", level)
		AddAbility(unit, "lone_druid_spirit_bear_entangle", level)

		unit:SetBaseDamageMin(parameters.attack)
		unit:SetBaseDamageMax(parameters.attack)
		unit:SetBaseMaxHealth(parameters.maxHealth)
		GameRules:GetGameModeEntity().createUnit = true
		unit:RespawnUnit()
		GameRules:GetGameModeEntity().createUnit = false

		InitUnit(unit, true)
	end
end

function SummonFireGolem( keys )
	--Msg("SummonFireGolem, keys=" .. TableToStr(keys) .. "\n")
	local ability = keys.ability
	local caster = keys.caster
	local level = ability:GetLevel() or 1

	local player = GetPlayerOwner(caster)
	local selectedPositions = FindEmptyPositions( player, caster:GetAbsOrigin(), 1)
	if #selectedPositions == 0 then
		SendErrorMessageToPlayer(PlayerResource:GetPlayer(player:GetPlayerID()), "MAGIC_FAILED_FOR_SUMMON_FIRE_GOLEM")
		return
	end

	local unit = CustomCreateUnitByName("npc_dota_warlock_golem"  .. level, selectedPositions[1], true, caster, caster, caster:GetTeam(), caster.player)
	PlayParticle("particles/units/heroes/hero_warlock/warlock_rain_of_chaos_start_bubble_evil.vpcf", PATTACH_OVERHEAD_FOLLOW, unit, 3)

	AddAbility(unit, "warlock_golem_flaming_fists", level)

	local maxHealth = ability:GetSpecialValueFor("golem_hp_tooltip")

	local count = 1
	maxHealth = maxHealth + caster:GetIntellect() * HPBonusMultipleForCallUnit / count

	unit:SetBaseDamageMin(ability:GetSpecialValueFor( "golem_dmg_tooltip" ))
	unit:SetBaseDamageMax(ability:GetSpecialValueFor( "golem_dmg_tooltip" ))
	unit:SetBaseMaxHealth(maxHealth)

	GameRules:GetGameModeEntity().createUnit = true
	unit:RespawnUnit()
	GameRules:GetGameModeEntity().createUnit = false

	InitUnit(unit, true)
end

function GetLegendProfessionNecromancerParamters( caster, currentAttack, currentMaxHealth, currentCount)
	local ability = caster:FindAbilityByName("legend_profession_necromancer")
	if ability ~= nil then
		local attack = ability:GetSpecialValueFor("attack") or 0
		local maxHealth = ability:GetSpecialValueFor("max_health") or 0
		local count = ability:GetSpecialValueFor("count") or 0
		--Msg("attack=" .. attack .. ", maxHealth=" .. maxHealth .. ", count=" .. count .. "\n")
		return {
			attack = math.floor(currentAttack * attack / 100),
			maxHealth = math.floor(currentMaxHealth * maxHealth / 100),
			count = math.floor(currentCount * count / 100)
		}
	else
		return {
			attack = 0,
			maxHealth = 0,
			count = 0
		}
	end
end

function GetCloakOfTheUndeadKingParamters( caster, currentAttack, currentMaxHealth, currentCount)
	local ability = caster:FindAbilityByName("cloak_of_the_undead_king_aura")
	if ability ~= nil then
		local attack = ability:GetSpecialValueFor("attack") or 0
		local maxHealth = ability:GetSpecialValueFor("max_health") or 0
		local count = ability:GetSpecialValueFor("count") or 0
		return {
			attack = math.floor(currentAttack * attack / 100),
			maxHealth = math.floor(currentMaxHealth * maxHealth / 100),
			count = math.floor(currentCount * count / 100)
		}
	else
		return {
			attack = 0,
			maxHealth = 0,
			count = 0
		}
	end
end

function SummonForNecromancer( keys, unitName, particleName )
	--Msg("SummonFireGolem, keys=" .. TableToStr(keys) .. "\n")
	local ability = keys.ability
	local caster = keys.caster
	local level = ability:GetLevel() or 1

	local count = ability:GetSpecialValueFor("count")
	local attack = ability:GetSpecialValueFor("damage_tooltip")
	local maxHealth = ability:GetSpecialValueFor("hp_tooltip")

	maxHealth = maxHealth + caster:GetIntellect() * HPBonusMultipleForCallUnit / count

	local professionParameters = GetLegendProfessionNecromancerParamters(caster, attack, maxHealth, count)
	local cloakParameters = GetCloakOfTheUndeadKingParamters(caster, attack, maxHealth, count)
	count = count + professionParameters.count + cloakParameters.count
	attack = attack + professionParameters.attack + cloakParameters.attack
	maxHealth = maxHealth + professionParameters.maxHealth + cloakParameters.maxHealth

	local player = GetPlayerOwner(caster)
	local selectedPositions = FindEmptyPositions( player, caster:GetAbsOrigin(), count)
	if #selectedPositions == 0 then
		SendErrorMessageToPlayer(PlayerResource:GetPlayer(player:GetPlayerID()), "MAGIC_FAILED_FOR_SUMMON")
		return
	end

	for i = 1, count do
		local unit = CustomCreateUnitByName(unitName .. level, selectedPositions[i], true, caster, caster, caster:GetTeam(), caster.player)
		PlayParticle(particleName, PATTACH_OVERHEAD_FOLLOW, unit, 3)

		unit:SetBaseDamageMin(attack)
		unit:SetBaseDamageMax(attack)
		unit:SetBaseMaxHealth(maxHealth)
		GameRules:GetGameModeEntity().createUnit = true
		unit:RespawnUnit()
		GameRules:GetGameModeEntity().createUnit = false

		InitUnit(unit, true)
	end
end

function InitNecromancer( caster )
	local ability = caster:FindAbilityByName("cloak_of_the_undead_king_aura")
	if ability == nil then
		AddAbility(caster, "cloak_of_the_undead_king_aura", 5, false)
	end
	ability = caster:FindAbilityByName("legend_profession_necromancer")
	if ability == nil then
		AddAbility(caster, "legend_profession_necromancer", 5, false)
	end
end

function SummonNecronomiconWarrior( keys )
	--InitNecromancer(keys.caster)
	SummonForNecromancer(keys, "npc_dota_necronomicon_warrior", "particles/items_fx/necronomicon_spawn_warrior.vpcf")
end

function SummonNecronomiconArcher( keys )
	--InitNecromancer(keys.caster)
	SummonForNecromancer(keys, "npc_dota_necronomicon_archer", "particles/items_fx/necronomicon_spawn.vpcf")
end

function SummonboneDragon( keys )
	--InitNecromancer(keys.caster)
	SummonForNecromancer(keys, "npc_bone_dragon", "particles/units/heroes/hero_visage/visage_summon_familiars_grnd_energy.vpcf")
end

function Metamorphosis(keys)
	local ability = keys.ability
	local caster = keys.caster
	local level = ability:GetLevel() or 1

	EmitSoundOn('Hero_Terrorblade.Sunder.Cast',caster)

	PlayParticle("particles/units/heroes/hero_terrorblade/terrorblade_metamorphosis_transform.vpcf", PATTACH_ABSORIGIN_FOLLOW, caster, 3)

	local metamorphosisModel = "models/heroes/terrorblade/demon.vmdl"
	caster.modelName = caster:GetModelName()
	caster:SetOriginalModel(metamorphosisModel)
	caster:SetModel(metamorphosisModel)
	caster.rangedProjectileName = caster:GetRangedProjectileName()
	caster:SetRangedProjectileName("particles/units/heroes/hero_terrorblade/terrorblade_metamorphosis_base_attack.vpcf")
	if caster.attackType == nil then
		caster.attackType = caster:GetAttackCapability()
	end
	caster:SetAttackCapability(DOTA_UNIT_CAP_RANGED_ATTACK)
end

function MetamorphosisEnd( keys )
	local caster = keys.caster
	if caster ~= nil then
		caster:SetOriginalModel(caster.modelName)
		caster:SetModel(caster.modelName)
		caster:SetRangedProjectileName(caster.rangedProjectileName)
		caster:SetAttackCapability(caster.attackType)
	end
end

function CostItem( item )
	if item:GetCurrentCharges() == 1 then
		item:Destroy()
	else
		item:SetCurrentCharges(item:GetCurrentCharges() - 1)
	end
end

function DecreaseItems( item, count )
	for i = 1, count do
		if item.shopItem.type == TYPE_EQUIPMENT then
			item:Destroy()
			return
		else
			CostItem(item)
		end
	end
end

function SetPrimaryAttribute( keys )
	-- Msg("SetPrimaryAttribute" .. TableToStr(keys) .. "\n")
	local unit = keys.target
	local item = keys.ability
	local primaryAttribute = item.shopItem.primaryAttribute
	local player = PlayerResource:GetPlayer(keys.target:GetPlayerID())
	local courier = player:GetAssignedHero()

	if unit:IsHero() == false then
		SendErrorMessageToPlayer(player, "ONLY_HERO_CAN_USE_THIS_ITEM")
		return
	end

	if keys.target == courier then
		SendErrorMessageToPlayer(player, "COURIER_CANNOT_USE_THIS_ITEM")
		return
	end

	if unit:GetPrimaryAttribute() == primaryAttribute then
		SendErrorMessageToPlayer(player, "THIS_HERO_ALREADY_SET_THIS_PRIMARY_ATTRIBUTE")
		return
	end

	unit:SetPrimaryAttribute(primaryAttribute)
	CostItem(item)
	EmitSoundForDrink(player)
end

function GetGold( keys )
	-- Msg("SetPrimaryAttribute" .. TableToStr(keys) .. "\n")
	local unit = keys.caster
	local item = keys.ability
	local gold = item.shopItem.value
	local player = PlayerResource:GetPlayer(keys.caster:GetPlayerID())
	local courier = player:GetAssignedHero()

	courier:SetMana(courier:GetMana() + gold)

	SendAdventureJournals(player, "ADVENTURE_JOURNALS_GET_GOLD",
		{
			name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
			itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. item:GetName()}
		})

	CostItem(item)
	EmitSoundForSell(player)
	SendUpdateStatusMessage(player)
end

function GetHealth( keys )
	-- Msg("SetPrimaryAttribute" .. TableToStr(keys) .. "\n")
	local unit = keys.caster
	local item = keys.ability
	local health = item.shopItem.value
	local player = PlayerResource:GetPlayer(keys.caster:GetPlayerID())
	local courier = player:GetAssignedHero()

	courier:SetHealth(courier:GetHealth() + health)

	SendAdventureJournals(player, "ADVENTURE_JOURNALS_GET_HEALTH",
		{
			name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
			itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. item:GetName()}
		})

	CostItem(item)
	EmitSoundForDrink(player)
	SendUpdateStatusMessage(player)
end

function UpgradeAbility( keys )
	local unit = keys.target
	local item = keys.ability
	local player = PlayerResource:GetPlayer(keys.target:GetPlayerID())

	if unit:IsHero() == false then
		SendErrorMessageToPlayer(player, "ONLY_HERO_CAN_USE_THIS_ITEM")
		return
	end

	local courier = player:GetAssignedHero()

	if keys.target == courier then
		SendErrorMessageToPlayer(player, "COURIER_CANNOT_USE_THIS_ITEM")
		return
	end

	if unit.temp == true then
		SendErrorMessageToPlayer(player, "THIS_UNIT_CANNOT_USE_THIS_ITEM")
		return
	end

	local hero = unit
	local canUpgradeAbilites = {}
	if hero.abilitiesCount ~= nil then
		for j = 1, hero.abilitiesCount do
			local abilityName = hero.abilities[j]
			local ability = hero:FindAbilityByName(abilityName)
			if ability ~= nil and ability:GetLevel() < 5 then -- to avoid 真熊形态出bug
				table.insert(canUpgradeAbilites, {ability = ability})
			end
		end
	end

	if #canUpgradeAbilites == 0 then
		SendErrorMessageToPlayer(player, "NO_ABILITY_CAN_UPGRADE")
		return
	end
	local random = RandomInt(1, #canUpgradeAbilites)
	local ability = canUpgradeAbilites[random].ability
	ability:UpgradeAbility(false)

	SendAdventureJournals(player, "ADVENTURE_JOURNALS_LEARN_SKILL_TO_UPGRADE",
			{
				name = {type = "name", value = GetUnitName(unit)},
				itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. item:GetName()},
				abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()},
				level = {type = "value", value = ability:GetLevel()}
			})

	EmitSoundForAbilityUpgrade(player)
	SendHintMessageToPlayer(player, 
		"UPGRADE_ABILITY", 
		{
			heroName = {type = "name", value = GetUnitName(hero)}, 
			abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. ability:GetName()}
		},
		false
	)
	CostItem(item)
end

function UseEternalContract( keys )
	local unit = keys.target
	local item = keys.ability
	local player = PlayerResource:GetPlayer(keys.target:GetPlayerID())

	if unit:IsHero() == false then
		SendErrorMessageToPlayer(player, "ONLY_HERO_CAN_USE_THIS_ITEM")
		return
	end

	local courier = player:GetAssignedHero()

	if keys.target == courier then
		SendErrorMessageToPlayer(player, "COURIER_CANNOT_USE_THIS_ITEM")
		return
	end

	if unit.temp == true then
		SendErrorMessageToPlayer(player, "THIS_UNIT_CANNOT_USE_THIS_ITEM")
		return
	end

	if unit.canSell == false then
		SendErrorMessageToPlayer(player, "THIS_UNIT_CANNOT_USE_THIS_ITEM")
		return
	end

	unit.canSell = false
	AddAbility(unit, "eternal_contract")
	SendAdventureJournals(player, "ADVENTURE_JOURNALS_USE_ETERNAL_CONTRACT",
		{
			name = {type = "value", value =  PlayerResource:GetPlayerName(player:GetPlayerID())},
			itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. item:GetName()},
			heroName = {type = "name", value = GetUnitName(unit)}
		})
	CostItem(item)
	SendHintMessageToPlayer(player, 
		"USE_ETERNAL_CONTRACT_SUCCESS", 
		nil,
		true,
		"tutorial_fanfare"
	)
	SendUpdateStatusMessage(player)
end

function UseAttributeItem( keys )
	local unit = keys.target
	local item = keys.ability
	local player = PlayerResource:GetPlayer(keys.target:GetPlayerID())

	if unit:IsHero() == false then
		SendErrorMessageToPlayer(player, "ONLY_HERO_CAN_USE_THIS_ITEM")
		return
	end

	local courier = player:GetAssignedHero()

	if keys.target == courier then
		SendErrorMessageToPlayer(player, "COURIER_CANNOT_USE_THIS_ITEM")
		return
	end

	if unit.temp == true then
		SendErrorMessageToPlayer(player, "THIS_UNIT_CANNOT_USE_THIS_ITEM")
		return
	end

	if item.shopItem.attribute == "agility" then
		unit:SetBaseAgility(unit:GetBaseAgility() + item.shopItem.value)
	elseif item.shopItem.attribute == "intellect" then
		unit:SetBaseIntellect(unit:GetBaseIntellect() + item.shopItem.value)
	elseif item.shopItem.attribute == "attributeAll" then
		unit:SetBaseAgility(unit:GetBaseAgility() + item.shopItem.value)
		unit:SetBaseIntellect(unit:GetBaseIntellect() + item.shopItem.value)
		unit:SetBaseStrength(unit:GetBaseStrength() + item.shopItem.value)
	elseif item.shopItem.attribute == "health" then
		unit:SetMaxHealth(unit:GetMaxHealth() + item.shopItem.value)
	else
		unit:SetBaseStrength(unit:GetBaseStrength() + item.shopItem.value)
	end
	
	local position = unit:GetAbsOrigin()
	GameRules:GetGameModeEntity().createUnit = true
	unit:RespawnHero(false, false) --敏捷增加有bug，需要respawn hero才能增加
	GameRules:GetGameModeEntity().createUnit = false
	unit:SetAbsOrigin(position)
	SendAdventureJournals(player, "ADVENTURE_JOURNALS_USE_ATTRIBUTE_ITEM",
		{
			itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. item:GetName()},
			heroName = {type = "name", value = GetUnitName(unit)}
		})
	CostItem(item)
	SendHintMessageToPlayer(player, 
		"USE_ATTRIBUTE_ITEM_SUCCESS", 
		nil,
		true,
		"General.LevelUp"
	)
end

function UseForgetAbilityItem( keys )
	local unit = keys.target
	local item = keys.ability
	local player = PlayerResource:GetPlayer(keys.target:GetPlayerID())

	if unit:IsHero() == false then
		SendErrorMessageToPlayer(player, "ONLY_HERO_CAN_USE_THIS_ITEM")
		return
	end

	local courier = player:GetAssignedHero()

	if keys.target == courier then
		SendErrorMessageToPlayer(player, "COURIER_CANNOT_USE_THIS_ITEM")
		return
	end

	if unit.temp == true then
		SendErrorMessageToPlayer(player, "THIS_UNIT_CANNOT_USE_THIS_ITEM")
		return
	end

	OnCancelSingleSelectDialogPanel(nil, {PlayerID = player:GetPlayerID()})

	CustomUI:DynamicHud_Create(player:GetPlayerID(), "SingleSelectDialogPanel", "file://{resources}/layout/custom_game/single_select_dialog.xml", nil)
	CustomGameEventManager:Send_ServerToPlayer(player, "create_single_select_dialog_panel", 
		{
			selections = unit.abilities, 
			confirmEventName = "on_forget_ability",
			cancelEventName = "on_cancel_single_select_dialog_panel",
			prefix = "DOTA_Tooltip_ability_",
			profix = "",
			title = "FORGET_ABILITY_TITLE",
			hint = "FORGET_ABILITY_HINT",
		})
	player.showSingleSelectDialog = true
	
	--CostItem(item)
	player.item = item
	player.hero = unit
	EmitSoundForDrink(player)
end

function OnCancelSingleSelectDialogPanel( index, keys )
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	if player.showSingleSelectDialog == true then
		CustomUI:DynamicHud_Destroy(keys.PlayerID, "SingleSelectDialogPanel")
		player.showSingleSelectDialog = false
	end
	
end

function IsRaceAbilityName(abilityName)
	if string.sub(abilityName, 1, string.len("race_")) == "race_" then
		return true
	else
		return false
	end
end

function GetRaceAbilityCount( abilityNames )
	local count = 0
	for i = 1, #abilityNames do
		if IsRaceAbilityName(abilityNames[i]) then
			count = count + 1
		end
	end

	return count
end

function HasAbilityNeedThisAbilityName( hero, abilityName )
	local abilitiesConfig = GameRules:GetGameModeEntity().abilities
	for i = 1, hero.abilitiesCount do
		local otherAbilityName = hero.abilities[i]
		local abilityConfig = abilitiesConfig[otherAbilityName]
		if otherAbilityName ~= abilityName and abilityConfig.needs ~= nil and IsIn(abilityConfig.needs, abilityName) then
			return true
		end
	end
	return false
end

function OnForgetAbility(index, keys)
	OnCancelSingleSelectDialogPanel(index, keys)

	local player = PlayerResource:GetPlayer(keys.PlayerID)
	local courier = player:GetAssignedHero()
	local item = player.item
	if item == null or item:IsNull() then
		SendErrorMessageToPlayer(player, "NO_ITEM")
		return
	end
	local hero = player.hero
	player.item = nil
	player.hero = nil
	local abilityName = keys.id
	local ability = hero:FindAbilityByName(abilityName)
	if ability == nil then
		SendErrorMessageToPlayer(player, "NO_ABILITY")
		return
	end
	
	if IsRaceAbilityName(abilityName) and GetRaceAbilityCount(hero.abilities) <= 1 then
		SendErrorMessageToPlayer(player, "CANNOT_FORGET_RACE_ABILITY")
		return
	end

	if IsSecondaryProfession(abilityName) and HasAbilityNeedThisAbilityName(hero, abilityName) then
		SendErrorMessageToPlayer(player, "CANNOT_FORGET_SECONDARY_PROFESSION_ABILITY_WHEN_THERE_IS_ABILITY_NEED_IT")
		return
	end
	
	ReleaseAbility(abilityName, ability:GetLevel())
	RemoveAbility(hero, abilityName)
	SendAdventureJournals(player, "ADVENTURE_JOURNALS_FORGET_ABILITY",
		{
			itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. item:GetName()},
			heroName = {type = "name", value = GetUnitName(hero)},
			abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. abilityName}
		})
	CostItem(item)
	SendHintMessageToPlayer(player, 
		"FORGET_ABILITY_SUCCESS", 
		nil,
		true,
		"General.FemaleLevelUp"
	)

end

function IsProfessionAbilityName(abilityName)
	if string.sub(abilityName, 1, string.len("profession_")) == "profession_" then
		return true
	else
		return false
	end
end

function GetAllProfessionNames()
	local abilitiesConfig = GameRules:GetGameModeEntity().abilities
	local professionNames = {}
	local professionConfigs = {}
	for k, abilityConfig in pairs(abilitiesConfig) do
		if IsProfessionAbilityName(k) then
			table.insert(professionConfigs, abilityConfig)
		end
	end

	table.sort(professionConfigs, function(a, b)
    	return a.index < b.index
	end)

	for i = 1, #professionConfigs do
		table.insert(professionNames, professionConfigs[i].name)
	end

	return professionNames
end

function UseRandomProfessionAbilityItem( keys )
	local item = keys.ability
	local player = PlayerResource:GetPlayer(keys.caster:GetPlayerID())

	OnCancelSingleSelectDialogPanel(nil, {PlayerID = player:GetPlayerID()})

	local professionNames = GetAllProfessionNames()
	CustomUI:DynamicHud_Create(player:GetPlayerID(), "SingleSelectDialogPanel", "file://{resources}/layout/custom_game/single_select_dialog.xml", nil)
	CustomGameEventManager:Send_ServerToPlayer(player, "create_single_select_dialog_panel", 
		{
			selections = professionNames, 
			confirmEventName = "on_get_random_profession_ability_item",
			cancelEventName = "on_cancel_single_select_dialog_panel",
			prefix = "DOTA_Tooltip_ability_",
			profix = "",
			title = "GET_PROFESSION_ABILITY_TITLE",
			hint = "GET_PROFESSION_ABILITY_HINT",
		})
	player.showSingleSelectDialog = true
	
	player.item = item
	player.hero = unit
	EmitSoundForDrink(player)
end

function OnGetRandomProfessionAbilityItem(index, keys)
	OnCancelSingleSelectDialogPanel(index, keys)

	local player = PlayerResource:GetPlayer(keys.PlayerID)
	local courier = player:GetAssignedHero()
	local item = player.item
	if item == null or item:IsNull() then
		SendErrorMessageToPlayer(player, "NO_ITEM")
		return
	end
	local hero = player.hero
	player.item = nil
	player.hero = nil
	local professionName = keys.id
	local abilityNames = GetAllAbilitiesForProfessions({professionName})
	if #abilityNames == 0 then
		SendErrorMessageToPlayer(player, "NO_ABILITIES_FOR_PROFESSION")
		return
	end

	local random = RandomInt(1, #abilityNames)
	local abilityName = abilityNames[random]
	local shopItem = FindAbilityShopItem(GameRules:GetGameModeEntity().shopItems, abilityName)
	if shopItem == nil then
		SendErrorMessageToPlayer(player, "NO_ABILITIES_FOR_PROFESSION")
		return
	end
	CreateShopItem(player, shopItem, courier:GetAbsOrigin(), true)
	SendAdventureJournals(player, "ADVENTURE_JOURNALS_GET_RANDOM_PROFESSION_ABILITY",
		{
			name = {type = "value", value = PlayerResource:GetPlayerName(keys.PlayerID)},
			itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. item:GetName()},
			abilityName = {type = "text", value = "DOTA_Tooltip_ability_" .. abilityName}
		})
	CostItem(item)
	SendHintMessageToPlayer(player, 
		"GET_PROFESSION_ABILITY_SUCCESS", 
		nil,
		true,
		"General.FemaleLevelUp"
	)
end

function GetAllShopItemNames( qualities, types, filters )
	local shopItems = GameRules:GetGameModeEntity().shopItems
	local shopItemNames = {}
		
	for i = 1, #shopItems do
		if IsIn(types, shopItems[i].type)
			and IsIn(qualities, shopItems[i].quality) 
			and GetShopItemMaxCount(shopItems[i]) - shopItems[i].existCount > 0
			and IsIn(filters, shopItems[i].name) == false
			then
			table.insert(shopItemNames, shopItems[i].name)
		end
	end
	return shopItemNames
end

function UseWishingWaterItem( keys )
	local item = keys.ability
	local player = PlayerResource:GetPlayer(keys.caster:GetPlayerID())

	OnCancelSingleSelectDialogPanel(nil, {PlayerID = player:GetPlayerID()})

	local shopItemNames = GetAllShopItemNames(
		{QUALITY_1, QUALITY_2, QUALITY_3, QUALITY_4, QUALITY_5},
		{TYPE_ABILITY, TYPE_CONSUMABLE, TYPE_EQUIPMENT}, {})
	CustomUI:DynamicHud_Create(player:GetPlayerID(), "SingleSelectDialogPanel", "file://{resources}/layout/custom_game/single_select_dialog.xml", nil)
	CustomGameEventManager:Send_ServerToPlayer(player, "create_single_select_dialog_panel", 
		{
			selections = shopItemNames, 
			confirmEventName = "on_get_wishing_item",
			cancelEventName = "on_cancel_single_select_dialog_panel",
			prefix = "DOTA_Tooltip_ability_",
			profix = "",
			title = "WISHING_ITEM_TITLE",
			hint = "WISHING_ITEM_HINT",
		})
	player.showSingleSelectDialog = true
	
	player.item = item
	player.hero = unit
	EmitSoundForDrink(player)
end

function UseWishingWaterItemForAbility( keys )
	local item = keys.ability
	local player = PlayerResource:GetPlayer(keys.caster:GetPlayerID())

	OnCancelSingleSelectDialogPanel(nil, {PlayerID = player:GetPlayerID()})

	local shopItemNames = GetAllShopItemNames(
		item.shopItem.qualities,
		{TYPE_ABILITY}, {})
	CustomUI:DynamicHud_Create(player:GetPlayerID(), "SingleSelectDialogPanel", "file://{resources}/layout/custom_game/single_select_dialog.xml", nil)
	CustomGameEventManager:Send_ServerToPlayer(player, "create_single_select_dialog_panel", 
		{
			selections = shopItemNames, 
			confirmEventName = "on_get_wishing_item",
			cancelEventName = "on_cancel_single_select_dialog_panel",
			prefix = "DOTA_Tooltip_ability_",
			profix = "",
			title = "WISHING_ITEM_TITLE",
			hint = "WISHING_ITEM_HINT",
		})
	player.showSingleSelectDialog = true
	
	player.item = item
	player.hero = unit
	EmitSoundForDrink(player)
end

function OnGetWishingItem(index, keys)
	OnCancelSingleSelectDialogPanel(index, keys)

	local player = PlayerResource:GetPlayer(keys.PlayerID)
	local courier = player:GetAssignedHero()
	local item = player.item

	if item == null or item:IsNull() then
		SendErrorMessageToPlayer(player, "NO_ITEM")
		return
	end

	local hero = player.hero
	player.item = nil
	player.hero = nil
	local itemName = keys.id
	local shopItem = FindShopItem(GameRules:GetGameModeEntity().shopItems, itemName)

	if shopItem == nil then
		SendErrorMessageToPlayer(player, "INVALID_SHOPITEM_NAME")
		return
	end

	if GetShopItemMaxCount(shopItem) - shopItem.existCount <= 0 then
		SendErrorMessageToPlayer(player, "NO_MORE_THAT_SHOPITEM")
		return
	end

	CreateShopItem(player, shopItem, courier:GetAbsOrigin(), item.shopItem.canUseDirectly)
	shopItem.existCount = shopItem.existCount + 1
	SendAdventureJournals(player, "ADVENTURE_JOURNALS_GET_WISHING_ITEM",
		{
			name = {type = "value", value = PlayerResource:GetPlayerName(keys.PlayerID)},
			itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. item:GetName()},
			wishingItem = {type = "text", value = "DOTA_Tooltip_ability_" .. shopItem.name}
		})
	CostItem(item)
	SendHintMessageToPlayer(player, 
		"WISHING_ITEM_SUCCESS", 
		nil,
		true,
		"General.FemaleLevelUp"
	)
end

function OnGetGameEndInfo( index, keys )
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	local courier = player:GetAssignedHero()
	local gameEndInfo = {}
	gameEndInfo.gameMode = GameRules:GetGameModeEntity().gameMode
	if courier:GetHealth() > 0 then
		gameEndInfo.result = "GAME_CLEAR"
		SendAdventureJournals(player, "ADVENTURE_JOURNALS_GAME_CLEAR",
		{
			playerName = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
			result = {type = "text", value = "ADVENTURE_JOURNALS_GAME_CLEAR_ENDING"}
		})
	else
		gameEndInfo.result = "GAME_OVER"
	end
	for key,value in pairs(GetPlayerLogs(player)) do
		gameEndInfo[key] = value
	end
	gameEndInfo.costTime = math.floor(GameRules:GetGameTime())

	local heros =  GetAllHeroChessWhichIsNotTemp(player)
	--Msg("herosCount=" .. #heros .. "\n")
	gameEndInfo.heros = {}
	for i = 1, #heros do
		local hero = heros[i]
		gameEndInfo.heros[i] = {}
		gameEndInfo.heros[i].name = GetUnitName(hero)
		gameEndInfo.heros[i].level = hero:GetLevel()
		gameEndInfo.heros[i].sex = hero.nameInfo.sex
		hero.shopItem = hero.shopItem or {}
		hero.shopItem.quality = QUALITY_1
		gameEndInfo.heros[i].cost = CountHeroGold(hero)

		local races = {}
		local professions = {}
		for j = 1, hero.abilitiesCount do
			local abilityName = hero.abilities[j]
			if IsRaceAbilityName(abilityName) then
				table.insert(races, abilityName)
			end

			if IsProfessionAbilityName(abilityName) then
				table.insert(professions, abilityName)
			end
		end

		if hero:HasAbility("eternal_contract") then
			gameEndInfo.heros[i].eternalContract = true
		end

		gameEndInfo.heros[i].races = races
		gameEndInfo.heros[i].professions = professions
	end

	table.insert(heros, courier)
	local equipmentAbilities = GameRules:GetGameModeEntity().equipmentAbilities
	table.insert(equipmentAbilities, 
		{name = "cornucopia", needs={"item_cornucopia_1", "item_cornucopia_2", "item_cornucopia_3", "item_cornucopia_4"}})
	gameEndInfo.combinedArtifacts = {}
	for i = 1, #equipmentAbilities do
		local equipmentAbility = equipmentAbilities[i]
		for j = 1, #heros do
			local hero = heros[j]
			if HasCombinedArtifact(hero, equipmentAbility) then
				table.insert(gameEndInfo.combinedArtifacts, equipmentAbility.name)
			end
		end
	end

	CustomGameEventManager:Send_ServerToPlayer(player, "on_receive_game_end_info", gameEndInfo)
	CustomGameEventManager:Send_ServerToPlayer(player, "on_game_over", {})
	--Msg("OnGetGameEndInfo\n")
end

function OnGetName( index, keys )
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	local courier = player:GetAssignedHero()
	local unit = EntIndexToHScript(keys.nEntityIndex)

	if IsUnitAlive(unit) and GetUnitName(unit) ~= nil then
		CustomGameEventManager:Send_ServerToPlayer(player, "on_receive_name", 
			BuildAdventureJournal("ADVENTURE_JOURNALS_BRIEF_NAME_INFO",
					{name = {type = "name", value = GetUnitName(unit)}}
				)
			)
		return
	end

	if unit == courier then
		CustomGameEventManager:Send_ServerToPlayer(player, "on_receive_name", 
			BuildAdventureJournal("ADVENTURE_JOURNALS_BRIEF_NAME_INFO",
					{name = {type = "text", value = "COURIER"}}
				)
			)
		return
	end
	--Msg("OnGetGameEndInfo\n")
end

function OnEndBattle( index, keys )
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	player.endBattle = true
	local players = GetAllAlivePlayers()
	for i = 1, #players do
		SendHintMessageToPlayer(players[i], 
			"BATTLE_OVER", 
			{
				name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())}
			},
			true,
			"badge_levelup"
		)
	end
end

function OnPrepareOver( index, keys )
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	player.prepareOver = true

	local players = GetAllAlivePlayers()
	for i = 1, #players do
		SendHintMessageToPlayer(players[i], 
			"PREPARE_OVER", 
			{
				name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())}
			},
			true,
			"badge_levelup"
		)
	end
end

function OnHandleQuests( index, keys )
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	local quest = player.quests[keys.id]
	local status = quest.status
	if status == "CanAccept" then
		quest.status = "Accepted"
		EmitSoundOnClient("quest_accepted", player)

		local adventureJournals = BuildAdventureJournals("ADVENTURE_JOURNALS_QUEST_ACCEPTED",
			{
				name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
				questName = {type = "text", value = quest.quest.type .. "_TITLE"}
			})
		adventureJournals[1].newLine = false

		local adventureJournal = BuildAdventureJournal(quest.quest.type, nil)
		adventureJournal.tokens = quest.context
		adventureJournal.newLine = false
		table.insert(adventureJournals, adventureJournal)

		if quest.hide ~= true then
			adventureJournal = BuildAdventureJournal(quest.reword.type, nil)
			adventureJournal.tokens = quest.context
			adventureJournal.newLine = false
			table.insert(adventureJournals, adventureJournal)
		end

		SendAdventureJournalsForAdventureJournals(player, adventureJournals)

		if quest.quest.onAccept ~= nil then
			quest.quest.onAccept(quest, player)
		end
	elseif status == "Accepted" then
		--Msg("Accepted")
		if quest.quest.canComplete(quest, player) then
			quest.status = "Completed"
			SendHintMessageToPlayer(player, 
				"COMPLETE_QUEST", 
				{
					quest = {type="text", value = quest.quest.type .. "_TITLE"}
				},
				true,
				"quest_complete"
			)
			quest.quest.onComplete(quest, player)
			quest.rewordConfig.onComplete(quest, player)
			quest.endFlag = true

			local adventureJournals = BuildAdventureJournals("ADVENTURE_JOURNALS_QUEST_COMPLETED",
			{
				name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
				questName = {type = "text", value = quest.quest.type .. "_TITLE"}
			})
			adventureJournals[1].newLine = false

			local adventureJournal = BuildAdventureJournal(quest.reword.type, nil)
			adventureJournal.tokens = quest.context
			adventureJournal.newLine = false
			table.insert(adventureJournals, adventureJournal)

			SendAdventureJournalsForAdventureJournals(player, adventureJournals)
		else
			SendErrorMessageToPlayer(player, "CANNOT_COMPLETE_QUEST")
			return
		end
	else
		return
	end

	SendQuestsUpdateMessage(player)
end

function SwitchRandomTeam( keys )
	local ability = keys.ability
	--Msg("SwitchRandomTeam=" .. TableToStr(keys) .. "\n")
	local player = PlayerResource:GetPlayer(keys.caster:GetPlayerID())
	player.randomTeam = not ability:GetToggleState()
	--Msg("randomTeam=" .. (player.randomTeam and "true" or "false") .. "\n")
	EmitSoundOnClient("ui.click_forward", player)
end

function RecruitTeammate( keys )
	local player = PlayerResource:GetPlayer(keys.target:GetPlayerID())
	local courier = player:GetAssignedHero()
	local hero = keys.target
	if hero.canBuy == nil or hero.canBuy == false then
		SendErrorMessageToPlayer(player, "CANNOT_RECUIT_THIS_HERO")
		return
	end

	local shopItem = hero.shopItem
	if courier:GetMana() - shopItem.cost < 0 then
		SendErrorMessageToPlayer(player, "NOT_ENOUGH_MONEY")
		return
	end

	courier:SetMana(courier:GetMana() - shopItem.cost)
	hero.temp = nil
	hero.lifeTime = nil
	hero.canBuy = nil
	RemoveAbilityAndModifier(hero, "is_random_teammate")
	EmitSoundForBuy(player)
	SendUpdateStatusMessage(player)

	SendAdventureJournals(player, "ADVENTURE_JOURNALS_BUY_HERO",
		{
			name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
			heroName = {type = "name", value = GetUnitName(hero)}
		})
end

function HireHero( keys )
	local player = PlayerResource:GetPlayer(keys.caster:GetPlayerID())
	local courier = player:GetAssignedHero()
	local attackType = RandomInt(1, 2)
	if attackType == 1 then
		attackType = ATTACK_TYPE_MELEE
	else
		attackType = ATTACK_TYPE_RANGED
	end

	local quality = math.floor(courier:GetLevel() / 2)
	if quality > 1 then
		quality = RandomInt(quality - 1, quality)
	end
	quality = NumberToQuality(quality)
	local level = courier:GetLevel()
	if level > 8 then
		level = 8
	end
	local config = {enemyConfig = {type = TYPE_HERO, attackType=attackType, quality = quality, level = level}}
	--local config = {enemyConfig = {type = TYPE_HERO, attackType=ATTACK_TYPE_MELEE, quality = QUALITY_5, level = 7, profession="profession_berserker"}}

	local emptyChessIndex = FindSpareIndexForWaitArea(player)
	if emptyChessIndex == nil then
		courier:SetMana(courier:GetMana() + 5)
		SendUpdateStatusMessage(player)
		SendErrorMessageToPlayer(player, "NO_SPARE_PLACE")
		return
	end
	local position = courier:GetAbsOrigin()
	local indexPos =  IndexToWorldPos(player:GetPlayerID(), emptyChessIndex)
	position = Vector(indexPos.x, indexPos.y, position.z)
	local hero = RandomCreateHeroForEnemy(player, config, position, false)
	hero:SetTeam(courier:GetTeam())
	hero:SetControllableByPlayer(keys.caster:GetPlayerID(), true)
	hero.canBuy = false
	hero.temp = true
	hero.lifeTime = 3
	InitUnit(hero)
	hero:SetMoveCapability(DOTA_UNIT_CAP_MOVE_NONE)
	hero.attackType = hero:GetAttackCapability()
	hero:SetAttackCapability(DOTA_UNIT_CAP_NO_ATTACK)
	AddAbility(hero, "invulnerable")
	AddAbility(hero, "silenced")
	PutChess(player, emptyChessIndex, hero, nil)
	AddAbility(hero, "is_hire_hero", 1, false)
	EmitSoundForBuy(player)
	SendUpdateStatusMessage(player)

	SendAdventureJournalsForHeroInfo(player, "ADVENTURE_JOURNALS_HIRE_HERO",
		{
			name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
			heroName = {type = "name", value = GetUnitName(hero)}
		}, hero)
end

function Identify( keys )
	local player = PlayerResource:GetPlayer(keys.caster:GetPlayerID())
	local courier = player:GetAssignedHero()
	local item = courier:GetItemInSlot(0)
	if item ~= nil and item.shopItem.type == TYPE_ABILITY then
		if item.canUseDirectly == true and item:GetCurrentCharges() == 1 then
			SendHintMessageToPlayer(player, 
				"IDENTIFY_ABILITY_ITEM_RESULT_CAN_USE_DIRECTLY", 
				{
					itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. item.shopItem.abilityName},
				},
				true,
				"ui.treasure_01"
			)
			SendAdventureJournals(player, "ADVENTURE_JOURNALS_IDENTIFY_ABILITY_ITEM_RESULT_CAN_USE_DIRECTLY",
			{
				name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
				itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. item.shopItem.abilityName},
			})
		else
			SendHintMessageToPlayer(player, 
				"IDENTIFY_ABILITY_ITEM_RESULT_CANNOT_USE_DIRECTLY", 
				{
					itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. item.shopItem.abilityName},
				},
				true,
				"ui.treasure_01"
			)
			SendAdventureJournals(player, "ADVENTURE_JOURNALS_IDENTIFY_ABILITY_ITEM_RESULT_CANNOT_USE_DIRECTLY",
			{
				name = {type = "value", value = PlayerResource:GetPlayerName(player:GetPlayerID())},
				itemName = {type = "text", value = "DOTA_Tooltip_ability_" .. item.shopItem.abilityName},
			})
		end
	else
		courier:SetMana(courier:GetMana() + 1)
		SendErrorMessageToPlayer(player, "DO_NOT_NEED_TO_IDENTIFY")
	end
end

function ApplyEquipmentModifier( keys )
	--Msg("ApplyEquipmentModifier, keys=" .. TableToStr(keys) .. "\n")
	local ability = keys.ability
	if keys.caster.GetPlayerID == nil then
		ability:ApplyDataDrivenModifier(keys.caster, keys.caster, keys.ModifierName, {})
		return
	end
	local playerId = keys.caster:GetPlayerID()
	local player = PlayerResource:GetPlayer(playerId)
	local courier = player:GetAssignedHero()
	if keys.caster ~= courier then
		ability:ApplyDataDrivenModifier(keys.caster, keys.caster, keys.ModifierName, {})
	end
end

function RemoveEquipmentModifier( keys )
	--Msg("RemoveEquipmentModifier, keys=" .. TableToStr(keys) .. "\n")
	local item = keys.ability
	if item.shopItem ~= nil then
		keys.caster:RemoveModifierByName("modifier_" .. item.shopItem.name)
	end
end

function ApplyRangedUnitModifier( keys )
	--Msg("ApplyRangedUnitModifier, keys=" .. TableToStr(keys) .. "\n")
	--Msg("ApplyRangedUnitModifier for unit=" .. keys.caster:GetName() .. "\n")
	local ability = keys.ability
	--Msg("attackType=" .. keys.target:GetAttackCapability() .. "\n")
	if (keys.target:GetAttackCapability() == DOTA_UNIT_CAP_RANGED_ATTACK) or (keys.target.attackType ~= nil and keys.target.attackType == DOTA_UNIT_CAP_RANGED_ATTACK) then
		--Msg("ApplyRangedUnitModifier for unit=" .. keys.target:GetName() .. "\n")
		ability:ApplyDataDrivenModifier(keys.caster, keys.target, keys.ModifierName, {})
	end
end

function ApplyUnitModifierExceptCourier( keys )
	--Msg("ApplyUnitModifierExceptCourier, keys=" .. TableToStr(keys) .. "\n")
	--Msg("ApplyUnitModifierExceptCourier for unit=" .. keys.caster:GetName() .. ", target=" .. keys.target:GetName() .. "\n")
	local ability = keys.ability
	
	if keys.caster.GetPlayerID == nil then
		ability:ApplyDataDrivenModifier(keys.caster, keys.target, keys.ModifierName, {})
		return
	end
	local playerId = keys.caster:GetPlayerID()
	local player = PlayerResource:GetPlayer(playerId)
	if IsIn(GetAllAliveCouriers(), keys.target) == false then
		--Msg("ApplyUnitModifierExceptCourier for unit=" .. keys.target:GetName() .. "\n")
		ability:ApplyDataDrivenModifier(keys.caster, keys.target, keys.ModifierName, {})
	end

	if keys.caster:HasModifier(keys.ModifierName) == false then
		ability:ApplyDataDrivenModifier(keys.caster, keys.caster, keys.ModifierName, {})
	end
end